﻿
using StatBuilder.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace StatBuilder
{
    public partial class MainForm : Form
    {
        #region Moves

        #region properties

        List<BindingSource> MoveBindingSources = new List<BindingSource>();
        public static BindingList<Move> MoveData { get; set; } = new BindingList<Move>();
        public static BindingSource MoveBinding;
        #endregion

        #region Bindings
        private void BindMoves()
        {
            MovesInitLists();

            //item bindings
            MoveBinding = new BindingSource(MoveData, null);

            listBoxMoves.DataSource = MoveBinding;

            UpdateMoveListDatasource();

            textBoxMoveName.DataBindings.Add(
            new Binding("Text", MoveBinding,
                "name", false, DataSourceUpdateMode.OnPropertyChanged));

            textBoxMoveDesc.DataBindings.Add(
            new Binding("Text", MoveBinding,
                "desc", false, DataSourceUpdateMode.OnPropertyChanged));

            textBoxMoveScript.DataBindings.Add(
            new Binding("Text", MoveBinding,
                "script_battle", false, DataSourceUpdateMode.OnPropertyChanged));

            textBoxMoveCharacter.DataBindings.Add(
            new Binding("Text", MoveBinding,
                "character", false, DataSourceUpdateMode.OnPropertyChanged));

            comboBoxMoveType.DataBindings.Add(
            new Binding("SelectedItem", MoveBinding,
                "element", false, DataSourceUpdateMode.OnPropertyChanged));
            comboBoxMovePhysMag.DataBindings.Add(
            new Binding("SelectedItem", MoveBinding,
                "move_type", false, DataSourceUpdateMode.OnPropertyChanged));
            
            comboBoxMoveBattlerVisibility.DataBindings.Add(
            new Binding("SelectedItem", MoveBinding,
                "show_battler", false, DataSourceUpdateMode.OnPropertyChanged));
            
            textBoxMoveDamage.DataBindings.Add(
            new Binding("Text", MoveBinding,
                "damage", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxMoveMPCost.DataBindings.Add(
            new Binding("Text", MoveBinding,
                "mp_cost", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxMoveTargets.DataBindings.Add(
            new Binding("Text", MoveBinding,
                "target_number", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxMovePriority.DataBindings.Add(
            new Binding("Text", MoveBinding,
                "speed_priority", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxMoveSpeedMultiplier.DataBindings.Add(
            new Binding("Text", MoveBinding,
                "speed_modifier", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxMoveStatusPercent.DataBindings.Add(
            new Binding("Text", MoveBinding,
                "filter_percent", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxMoveEffectPercent.DataBindings.Add(
            new Binding("Text", MoveBinding,
                "effect_percent", false, DataSourceUpdateMode.OnPropertyChanged));

            textBoxMoveForegroundSprite.DataBindings.Add(
            new Binding("Text", MoveBinding,
                "fg_animation", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxMoveBackgroundSprite.DataBindings.Add(
            new Binding("Text", MoveBinding,
                "bg_animation", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxMoveTargetSprite.DataBindings.Add(
            new Binding("Text", MoveBinding,
                "sprite_animation", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxMoveUserSprite.DataBindings.Add(
            new Binding("Text", MoveBinding,
                "self_animation", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxMoveDuration.DataBindings.Add(
            new Binding("Text", MoveBinding,
                "animation_length", false, DataSourceUpdateMode.OnPropertyChanged));
            
            checkBoxMoveTargetSkip.DataBindings.Add(
            new Binding("Checked", MoveBinding,
                "target_skip", false, DataSourceUpdateMode.OnPropertyChanged));
            checkBoxMoveTargetAlly.DataBindings.Add(
            new Binding("Checked", MoveBinding,
                "target_ally", false, DataSourceUpdateMode.OnPropertyChanged));
            checkBoxMoveTargetSelf.DataBindings.Add(
            new Binding("Checked", MoveBinding,
                "target_self", false, DataSourceUpdateMode.OnPropertyChanged));
            checkBoxMoveRotateCamera.DataBindings.Add(
            new Binding("Checked", MoveBinding,
                "camera_rotate", false, DataSourceUpdateMode.OnPropertyChanged));
            
            checkBoxMoveKnockback.DataBindings.Add(
            new Binding("Checked", MoveBinding,
                "animation_knockback", false, DataSourceUpdateMode.OnPropertyChanged));

            UpdateMoveBinding();

            // item databinding
            BindingSource tmp = new BindingSource(ItemData, null);
            comboBoxMoveItem.DataSource = tmp;
            MoveBindingSources.Add(tmp);

            tmp = new BindingSource(StatusData, null);
            comboBoxMoveStatus.DataSource = tmp;
            MoveBindingSources.Add(tmp);

            comboBoxMoveItem.DataBindings.Add(
                new Binding("SelectedItem", MoveBinding,
                    "Item", false, DataSourceUpdateMode.OnPropertyChanged));
        }

        private void MoveResetBindings()
        {
            for (int i = 0; i < MoveBindingSources.Count; i++)
            {
                MoveBindingSources[i].ResetBindings(false);
            }
        }

        private void UpdateMoveBinding()
        {
            Move current = (Move)listBoxMoves.SelectedItem;
            if (current != null)
            {
                listBoxMoveAnimation.DataSource = current.AnimationSource;

                RedoBinding(current.AnimationSource, textBoxMoveAnimationFrame, "Text", "frame");
                RedoBinding(current.AnimationSource, textBoxMoveAnimationJump, "Text", "jump");
                RedoBinding(current.AnimationSource, textBoxMoveAnimationLunge, "Text", "lunge");
                RedoBinding(current.AnimationSource, textBoxMoveAnimationLungePos, "Text", "lunge_pos");
                RedoBinding(current.AnimationSource, textBoxMoveAnimationSprite, "Text", "sprite");
                RedoBinding(current.AnimationSource, textBoxMoveAnimationSound, "Text", "sound_fx");
                RedoBinding(current.AnimationSource, textBoxMoveAnimationJumpHeight, "Text", "jump_height");
                RedoBinding(current.AnimationSource, comboBoxMoveAnimationTarget, "SelectedItem", "target");

            }
        }

        private void UpdateMoveListDatasource()
        {
            Move current = (Move)listBoxMoves.SelectedItem;
            if (current != null) {
                listBoxMoveStatusList.DataSource = current.FilterSource;
                listBoxMoveTextList.DataSource = current.TextSource;
                listBoxMoveAnimation.DataSource = current.AnimationSource;
            }
        }

        private void MovesInitLists()
        {
            comboBoxMoveType.Items.Clear();
            comboBoxMoveType.Items.AddRange(Types);
            comboBoxMoveType.SelectedIndex = 0;

            comboBoxMovePhysMag.Items.Clear();
            comboBoxMovePhysMag.Items.AddRange(model.Move.MovePhyMagTypes);
            comboBoxMovePhysMag.SelectedIndex = 0;

            comboBoxMoveBattlerVisibility.Items.Clear();
            comboBoxMoveBattlerVisibility.Items.AddRange(model.Move.ShowBattlerTypes);
            comboBoxMoveBattlerVisibility.SelectedIndex = 0;
        }
        #endregion

        #region Buttons
        private void buttonMoveTextAdd_Click(object sender, EventArgs e)
        {
            Move current = (Move)listBoxMoves.SelectedItem;
            if (current != null)
            {
                current.battle_text.Add(textBoxMoveTextLine.Text);
                textBoxMoveTextLine.Text = "";
                if (listBoxMoveTextList.SelectedIndex != listBoxMoveTextList.Items.Count - 1)
                {
                    listBoxMoveTextList.SelectedIndex++;
                }
            }
        }

        private void buttonMoveTextRemove_Click(object sender, EventArgs e)
        {
            if (listBoxMoveTextList.SelectedIndex != -1 &&
                listBoxMoveTextList.Items.Count > 0)
            {
                Move current = (Move)listBoxMoves.SelectedItem;
                if (current != null)
                {
                    current.battle_text.RemoveAt(listBoxMoveTextList.SelectedIndex);
                }
            }
        }

        private void buttonMoveStatusAdd_Click(object sender, EventArgs e)
        {
            Move current = (Move)listBoxMoves.SelectedItem;
            if (current != null)
            {
                Status selected = (Status)comboBoxMoveStatus.SelectedItem;


                if (!current.filter.Contains(selected.name))
                {
                    current.filter.Add(selected.name);
                    if (listBoxMoveStatusList.SelectedIndex != listBoxMoveStatusList.Items.Count - 1)
                    {
                        listBoxMoveStatusList.SelectedIndex++;
                    }
                }
            }
        }
        private void listBoxMoveTextList_SelectedIndexChanged(object sender, EventArgs e)
        {
            textBoxMoveTextLine.Text = (string)listBoxMoveTextList.SelectedItem;
        }

        private void buttonMoveTextUpdate_Click(object sender, EventArgs e)
        {
            Move current = (Move)listBoxMoves.SelectedItem;
            if (current != null)
            {
                if (listBoxMoveTextList.SelectedIndex > -1)
                {
                    current.battle_text[listBoxMoveTextList.SelectedIndex] = textBoxMoveTextLine.Text;
                }
            }
        }

        private void buttonMoveStatusRemove_Click(object sender, EventArgs e)
        {
            if (listBoxMoveStatusList.SelectedIndex != -1 &&
                listBoxMoveStatusList.Items.Count > 0)
            {
                Move current = (Move)listBoxMoves.SelectedItem;
                if (current != null)
                {
                    current.filter.RemoveAt(listBoxMoveStatusList.SelectedIndex);
                }
            }
        }

        private void buttonMovesAdd_Click(object sender, EventArgs e)
        {
            listboxAdd(listBoxMoves, MoveData, new Move());
            UpdateMoveListDatasource();
            TabletResetBindings();
        }

        private void buttonMovesRemove_Click(object sender, EventArgs e)
        {
            listboxRemove(listBoxMoves, MoveData);
            UpdateMoveListDatasource();
            TabletResetBindings();
        }

        private void buttonMovesMoveUp_Click(object sender, EventArgs e)
        {
            listboxMoveUp(listBoxMoves, MoveData);
            TabletResetBindings();
        }

        private void buttonMovesMoveDown_Click(object sender, EventArgs e)
        {
            listboxMoveDown(listBoxMoves, MoveData);
            TabletResetBindings();
        }

        private void buttonMoveAnimationAdd_Click(object sender, EventArgs e)
        {
            if (listBoxMoves.SelectedIndex != -1 &&
                listBoxMoves.Items.Count > 0)
            {
                Move current = (Move)listBoxMoves.SelectedItem;
                if (current != null)
                {
                    listboxAdd(listBoxMoveAnimation, current.Animations, new Move.MoveAnimation());
                    UpdateMoveListDatasource();
                    TabletResetBindings();
                }
            }
        }

        private void buttonMoveAnimationRemove_Click(object sender, EventArgs e)
        {
            if (listBoxMoves.SelectedIndex != -1 &&
                listBoxMoves.Items.Count > 0)
            {
                Move current = (Move)listBoxMoves.SelectedItem;
                if (current != null)
                {
                    listboxRemove(listBoxMoveAnimation, current.Animations);
                    UpdateMoveListDatasource();
                    TabletResetBindings();
                }
            }
        }

        private void buttonMoveAnimationMoveUp_Click(object sender, EventArgs e)
        {
            if (listBoxMoves.SelectedIndex != -1 &&
                listBoxMoves.Items.Count > 0)
            {
                Move current = (Move)listBoxMoves.SelectedItem;
                if (current != null)
                {
                    listboxMoveUp(listBoxMoveAnimation, current.Animations);
                    UpdateMoveListDatasource();
                    TabletResetBindings();
                }
            }
        }

        private void buttonMoveAnimationMoveDown_Click(object sender, EventArgs e)
        {
            if (listBoxMoves.SelectedIndex != -1 &&
                listBoxMoves.Items.Count > 0)
            {
                Move current = (Move)listBoxMoves.SelectedItem;
                if (current != null)
                {
                    listboxMoveDown(listBoxMoveAnimation, current.Animations);
                    UpdateMoveListDatasource();
                    TabletResetBindings();
                }
            }
        }


        #endregion

        #region Elements
        private void listBoxMoves_SelectedIndexChanged(object sender, EventArgs e)
        {
            textBoxMoveTextLine.Text = "";
            UpdateMoveListDatasource();
            UpdateMoveBinding();
        }

        private void listBoxMoveAnimation_Leave(object sender, EventArgs e)
        {
            Move current = (Move)listBoxMoves.SelectedItem;
            if (current != null)
            {
                current.AnimationSource.ResetBindings(false);
            }
        }

        private void listBoxMoveAnimation_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateMoveListDatasource();
        }

        private void textBoxMoveName_Leave(object sender, EventArgs e)
        {
            MoveBinding.ResetBindings(false);
        }
        #endregion

        #endregion
    }
}
