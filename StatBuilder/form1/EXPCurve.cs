﻿
using StatBuilder.model;
using StatBuilder.model.store;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace StatBuilder
{
    public partial class MainForm : Form
    {
        #region EXP growth

        #region Draw
        Pen EXPPen = new Pen(Color.White, 3);
        Brush EXPBrush = new SolidBrush(Color.DarkGreen);

        private void panelGrowthDisplay_Paint(object sender, PaintEventArgs e)
        {
            DrawGrowthPoints(sender as Panel, e.Graphics, GrowthDisplayPoints);
        }

        private void DrawGrowthPoints(Panel p, Graphics g, GrowthPoints growthPoints)
        {
            g.FillRectangle(new SolidBrush(Color.FromArgb(0, Color.Black)), p.DisplayRectangle);

            g.FillPolygon(EXPBrush, growthPoints.Points);
            //g.DrawPolygon(EXPPen, growthPoints.Points);
        }

        #endregion

        #region Properties
        GrowthPoints GrowthDisplayPoints = new GrowthPoints();

        ExpGrowth GrowthSelected = null;
        MouseButtons GrowthLastMouseState = MouseButtons.None;
        Point GrowthDrawPoint = new Point(-255, -255);
        const int GROWTH_MOUSE_X_OFFSET = -22;
        const int GROWTH_MOUSE_Y_OFFSET = -65;
        int GrowthRadius = 50;

        public static BindingList<ExpGrowth> GrowthData { get; set; } = new BindingList<ExpGrowth>();
        public static BindingSource GrowthBinding;

        private void UpdateGrowthPoints(Panel panel, ExpGrowth item, GrowthPoints GrowthPoints)
        {
            if (item != null)
            {
                GrowthPoints.Points = new Point[10];

                double[] multipliers = item.growth;
                int xcoord, ycoord;

                GrowthPoints.Points[0] = new Point(0, panel.Height);

                for (int i = 1; i < 9; i++)
                {
                    xcoord = Convert.ToInt32(((double)ExpGrowth.Levels[i - 1] / 100) * panel.Width);
                    ycoord = Convert.ToInt32(panel.Height * (1 - multipliers[i - 1]));
                    GrowthPoints.Points[i] = new Point(xcoord, ycoord);
                }

                GrowthPoints.Points[GrowthPoints.Points.Length - 1] = new Point(panel.Width, panel.Height);
            }
            else
            {
                GrowthPoints.Points = new Point[10];
                for (int i = 0; i < 10; i++)
                {
                    GrowthPoints.Points[i] = new Point(0, 0);
                }
            }
        }

        private double[] GrowthPointToDoubleArray()
        {
            double[] returner = new double[8];
            for (int i = 0; i < 8; i++)
            {
                returner[i] = 1 - ((double)GrowthDisplayPoints.Points[i + 1].Y / (double)panelGrowthDisplay.Height);
            }

            return returner;
        }
        #endregion

        #region Buttons

        private void buttonGrowthAdd_Click(object sender, EventArgs e)
        {
            listboxAdd(listBoxGrowth, GrowthData, new ExpGrowth());
            GrowthSelected = (ExpGrowth)listBoxGrowth.SelectedItem;
            UpdateGrowthPoints(panelGrowthDisplay,GrowthSelected, GrowthDisplayPoints);
            panelGrowthDisplay.Refresh();
            CharacterGrowthResetBindings();
        }

        private void buttonGrowthRemove_Click(object sender, EventArgs e)
        {
            if (GrowthData.Count > 1)
            {
                listboxRemove(listBoxGrowth, GrowthData);
                GrowthSelected = (ExpGrowth)listBoxGrowth.SelectedItem;
                UpdateGrowthPoints(panelGrowthDisplay, GrowthSelected, GrowthDisplayPoints);
                panelGrowthDisplay.Refresh();
                CharacterGrowthResetBindings();
            }
        }

        private void buttonGrowthMoveUp_Click(object sender, EventArgs e)
        {
            listboxMoveUp(listBoxGrowth, GrowthData);
            GrowthSelected = (ExpGrowth)listBoxGrowth.SelectedItem;
            UpdateGrowthPoints(panelGrowthDisplay, GrowthSelected, GrowthDisplayPoints);
            panelGrowthDisplay.Refresh();
            CharacterGrowthResetBindings();
        }

        private void buttonGrowthMoveDown_Click(object sender, EventArgs e)
        {
            listboxMoveDown(listBoxGrowth, GrowthData);
            GrowthSelected = (ExpGrowth)listBoxGrowth.SelectedItem;
            UpdateGrowthPoints(panelGrowthDisplay, GrowthSelected, GrowthDisplayPoints);
            panelGrowthDisplay.Refresh();
            CharacterGrowthResetBindings();
        }
        private void listBoxGrowth_SelectedIndexChanged(object sender, EventArgs e)
        {
            GrowthSelected = (ExpGrowth)listBoxGrowth.SelectedItem;
            UpdateGrowthPoints(panelGrowthDisplay, GrowthSelected, GrowthDisplayPoints);
            panelGrowthDisplay.Refresh();
        }
        private void textBoxGrowthName_Leave(object sender, EventArgs e)
        {
            GrowthBinding.ResetBindings(false);
            CharacterGrowthResetBindings();
        }

        #endregion

        #region Bindings
        private void BindGrowth()
        {
            //growth bindings
            GrowthBinding = new BindingSource(GrowthData, null);

            listBoxGrowth.DataSource = GrowthBinding;


            textBoxGrowthName.DataBindings.Add(
                    new Binding("Text", GrowthBinding,
                        "name", false, DataSourceUpdateMode.OnPropertyChanged));

        }
        #endregion

        #region Mouse

        private void panelGrowthDisplay_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                GrowthDrawPoint = new Point(
                                MousePosition.X - (Location.X + panelGrowthDisplay.Location.X) + GROWTH_MOUSE_X_OFFSET,
                                MousePosition.Y - (Location.Y + panelGrowthDisplay.Location.Y) + GROWTH_MOUSE_Y_OFFSET);
                for (int i = 1; i < 9; i++)
                {
                    if (GetDistance(GrowthDrawPoint, GrowthDisplayPoints.Points[i]) < GrowthRadius)
                    {
                        GrowthDisplayPoints.Points[i] = new Point(GrowthDisplayPoints.Points[i].X, GrowthDrawPoint.Y);
                        if (GrowthSelected != null)
                        {
                            GrowthSelected.growth = GrowthPointToDoubleArray();
                        }
                        panelGrowthDisplay.Refresh();
                        break;
                    }
                }
            }
            else
            {
                GrowthDrawPoint = new Point(-255, 255);
                if (GrowthLastMouseState == MouseButtons.Left)
                {
                    panelGrowthDisplay.Refresh();
                }
            }

            GrowthLastMouseState = e.Button;
        }
        #endregion

        #endregion
    }
}
