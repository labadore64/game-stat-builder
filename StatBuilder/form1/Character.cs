﻿
using StatBuilder.formDialog;
using StatBuilder.model;
using StatBuilder.model.store;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using static StatBuilder.model.Dialog;

namespace StatBuilder
{
    public partial class MainForm : Form
    {
        #region Character Menu

        #region properties
        public static BindingList<Character> CharacterData { get; set; } = new BindingList<Character>();
        public static BindingList<Dialog> CurrentDialogs { get; set; }
        public static BindingSource CharacterBinding;


        GrowthPoints[] CharacterGrowthPoints = new GrowthPoints[]
        {
            new GrowthPoints(),
            new GrowthPoints(),
            new GrowthPoints(),
            new GrowthPoints(),
            new GrowthPoints(),
            new GrowthPoints(),
            new GrowthPoints(),
            new GrowthPoints(),
            new GrowthPoints()
        };

        List<BindingSource> CharacterGrowthBindingSources = new List<BindingSource>();

        private void CharacterUpdateGrowthPoints()
        {
            Character tmp = (Character)listBoxCharacters.SelectedItem;

            if (tmp != null)
            {
                UpdateGrowthPoints(panelCharacterEXPGrowth, tmp.growth_exp_grow, CharacterGrowthPoints[0]);
                UpdateGrowthPoints(panelCharacterHPGrowth, tmp.growth_hp_grow, CharacterGrowthPoints[1]);
                UpdateGrowthPoints(panelCharacterMPGrowth, tmp.growth_mp_grow, CharacterGrowthPoints[2]);
                UpdateGrowthPoints(panelCharacterPhyPowerGrowth, tmp.growth_phy_power_grow, CharacterGrowthPoints[3]);
                UpdateGrowthPoints(panelCharacterPhyGuardGrowth, tmp.growth_phy_guard_grow, CharacterGrowthPoints[4]);
                UpdateGrowthPoints(panelCharacterMagPowerGrowth, tmp.growth_mag_power_grow, CharacterGrowthPoints[5]);
                UpdateGrowthPoints(panelCharacterMagGuardGrowth, tmp.growth_mag_guard_grow, CharacterGrowthPoints[6]);
                UpdateGrowthPoints(panelCharacterSpePowerGrowth, tmp.growth_spe_power_grow, CharacterGrowthPoints[7]);
                UpdateGrowthPoints(panelCharacterSpeGuardGrowth, tmp.growth_spe_guard_grow, CharacterGrowthPoints[8]);
            } else
            {
                UpdateGrowthPoints(panelCharacterEXPGrowth, null, CharacterGrowthPoints[0]);
                UpdateGrowthPoints(panelCharacterHPGrowth, null, CharacterGrowthPoints[1]);
                UpdateGrowthPoints(panelCharacterMPGrowth, null, CharacterGrowthPoints[2]);
                UpdateGrowthPoints(panelCharacterPhyPowerGrowth, null, CharacterGrowthPoints[3]);
                UpdateGrowthPoints(panelCharacterPhyGuardGrowth, null, CharacterGrowthPoints[4]);
                UpdateGrowthPoints(panelCharacterMagPowerGrowth, null, CharacterGrowthPoints[5]);
                UpdateGrowthPoints(panelCharacterMagGuardGrowth, null, CharacterGrowthPoints[6]);
                UpdateGrowthPoints(panelCharacterSpePowerGrowth, null, CharacterGrowthPoints[7]);
                UpdateGrowthPoints(panelCharacterSpeGuardGrowth, null, CharacterGrowthPoints[8]);
            }
        }
        #endregion

        #region Bindings
        private void BindCharacter()
        {
            // set type
            comboBoxCharacterType.Items.AddRange(Types);

            //character bindings
            CharacterBinding = new BindingSource(CharacterData, null);

            listBoxCharacters.DataSource = CharacterBinding;

            textBoxCharacterName.DataBindings.Add(
                    new Binding("Text", CharacterBinding,
                        "name", false, DataSourceUpdateMode.OnPropertyChanged));


            textBoxCharacterAbility.DataBindings.Add(
                    new Binding("Text", CharacterBinding,
                        "ability", false, DataSourceUpdateMode.OnPropertyChanged));

            textBoxCharacterBattleDesc.DataBindings.Add(
                    new Binding("Text", CharacterBinding,
                        "battle_description", false, DataSourceUpdateMode.OnPropertyChanged));

            textBoxCharacterVisualDesc.DataBindings.Add(
                new Binding("Text", CharacterBinding,
                    "ax_description", false, DataSourceUpdateMode.OnPropertyChanged));

            textBoxCharacterPhone.DataBindings.Add(
                new Binding("Text", CharacterBinding,
                    "PhoneNumber", false, DataSourceUpdateMode.OnPropertyChanged));

            // stat binding

            trackBarCharacterHP.DataBindings.Add(
                new Binding("Value", CharacterBinding,
                    "hp", false, DataSourceUpdateMode.OnPropertyChanged));

            trackBarCharacterMP.DataBindings.Add(
                new Binding("Value", CharacterBinding,
                    "mp", false, DataSourceUpdateMode.OnPropertyChanged));

            trackBarCharacterPhyPower.DataBindings.Add(
                new Binding("Value", CharacterBinding,
                    "phy_power", false, DataSourceUpdateMode.OnPropertyChanged));

            trackBarCharacterPhyGuard.DataBindings.Add(
                new Binding("Value", CharacterBinding,
                    "phy_guard", false, DataSourceUpdateMode.OnPropertyChanged));

            trackBarCharacterMagPower.DataBindings.Add(
                new Binding("Value", CharacterBinding,
                    "mag_power", false, DataSourceUpdateMode.OnPropertyChanged));

            trackBarCharacterMagGuard.DataBindings.Add(
                new Binding("Value", CharacterBinding,
                    "mag_guard", false, DataSourceUpdateMode.OnPropertyChanged));

            trackBarCharacterSpePower.DataBindings.Add(
                new Binding("Value", CharacterBinding,
                    "spe_power", false, DataSourceUpdateMode.OnPropertyChanged));

            trackBarCharacterSpeGuard.DataBindings.Add(
                new Binding("Value", CharacterBinding,
                    "spe_guard", false, DataSourceUpdateMode.OnPropertyChanged));

            // color binding
            labelCharacterRed.DataBindings.Add(
                new Binding("Text", CharacterBinding,
                    "red", false, DataSourceUpdateMode.OnPropertyChanged));

            labelCharacterGreen.DataBindings.Add(
                new Binding("Text", CharacterBinding,
                    "green", false, DataSourceUpdateMode.OnPropertyChanged));

            labelCharacterBlue.DataBindings.Add(
                new Binding("Text", CharacterBinding,
                    "blue", false, DataSourceUpdateMode.OnPropertyChanged));

            // sprite binding
            textBoxCharacterSpriteIdleFront.DataBindings.Add(
                new Binding("Text", CharacterBinding,
                    "BattleSprite.sprite_idle_front", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxCharacterSpriteIdleBack.DataBindings.Add(
                new Binding("Text", CharacterBinding,
                    "BattleSprite.sprite_idle_back", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxCharacterSpriteHitFront.DataBindings.Add(
                new Binding("Text", CharacterBinding,
                    "BattleSprite.sprite_hit_front", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxCharacterSpriteHitBack.DataBindings.Add(
                new Binding("Text", CharacterBinding,
                    "BattleSprite.sprite_hit_back", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxCharacterSpriteJump0Front.DataBindings.Add(
                new Binding("Text", CharacterBinding,
                    "BattleSprite.sprite_jump0_front", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxCharacterSpriteJump0Back.DataBindings.Add(
                new Binding("Text", CharacterBinding,
                    "BattleSprite.sprite_jump0_back", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxCharacterSpriteJump1Front.DataBindings.Add(
                new Binding("Text", CharacterBinding,
                    "BattleSprite.sprite_jump1_front", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxCharacterSpriteJump1Back.DataBindings.Add(
                new Binding("Text", CharacterBinding,
                    "BattleSprite.sprite_jump1_back", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxCharacterSpriteCast0Front.DataBindings.Add(
                new Binding("Text", CharacterBinding,
                    "BattleSprite.sprite_cast0_front", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxCharacterSpriteCast0Back.DataBindings.Add(
                new Binding("Text", CharacterBinding,
                    "BattleSprite.sprite_cast0_back", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxCharacterSpriteCast1Front.DataBindings.Add(
                new Binding("Text", CharacterBinding,
                    "BattleSprite.sprite_cast1_front", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxCharacterSpriteCast1Back.DataBindings.Add(
                new Binding("Text", CharacterBinding,
                    "BattleSprite.sprite_cast1_back", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxCharacterSpriteSpeed.DataBindings.Add(
                new Binding("Text", CharacterBinding,
                    "BattleSprite.sprite_default_speed", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxCharacterSpriteFace.DataBindings.Add(
                new Binding("Text", CharacterBinding,
                    "face_sprite", false, DataSourceUpdateMode.OnPropertyChanged));

            // type
            comboBoxCharacterType.DataBindings.Add(
                new Binding("SelectedItem", CharacterBinding,
                    "type", false, DataSourceUpdateMode.OnPropertyChanged));

            // growth
            textBoxCharacterEXPTotal.DataBindings.Add(
                new Binding("Text", CharacterBinding,
                    "total_exp", false, DataSourceUpdateMode.OnPropertyChanged));

            // growth binds
            BindingSource tmp = new BindingSource(GrowthData, null);
            comboBoxCharacterEXPGrowth.DataSource = tmp;
            CharacterGrowthBindingSources.Add(tmp);

            tmp = new BindingSource(GrowthData, null);
            comboBoxCharacterHPGrowth.DataSource = tmp;
            CharacterGrowthBindingSources.Add(tmp);

            tmp = new BindingSource(GrowthData, null);
            comboBoxCharacterMPGrowth.DataSource = tmp;
            CharacterGrowthBindingSources.Add(tmp);

            tmp = new BindingSource(GrowthData, null);
            comboBoxCharacterPhyPowerGrowth.DataSource = tmp;
            CharacterGrowthBindingSources.Add(tmp);

            tmp = new BindingSource(GrowthData, null);
            comboBoxCharacterPhyGuardGrowth.DataSource = tmp;
            CharacterGrowthBindingSources.Add(tmp);

            tmp = new BindingSource(GrowthData, null);
            comboBoxCharacterMagPowerGrowth.DataSource = tmp;
            CharacterGrowthBindingSources.Add(tmp);

            tmp = new BindingSource(GrowthData, null);
            comboBoxCharacterMagGuardGrowth.DataSource = tmp;
            CharacterGrowthBindingSources.Add(tmp);

            tmp = new BindingSource(GrowthData, null);
            comboBoxCharacterSpePowerGrowth.DataSource = tmp;
            CharacterGrowthBindingSources.Add(tmp);

            tmp = new BindingSource(GrowthData, null);
            comboBoxCharacterSpeGuardGrowth.DataSource = tmp;
            CharacterGrowthBindingSources.Add(tmp);

            comboBoxCharacterEXPGrowth.DataBindings.Add(
                new Binding("SelectedItem", CharacterBinding,
                    "growth_exp_grow", false, DataSourceUpdateMode.OnPropertyChanged));

            comboBoxCharacterHPGrowth.DataBindings.Add(
            new Binding("SelectedItem", CharacterBinding,
                "growth_hp_grow", false, DataSourceUpdateMode.OnPropertyChanged));

            comboBoxCharacterMPGrowth.DataBindings.Add(
            new Binding("SelectedItem", CharacterBinding,
                "growth_mp_grow", false, DataSourceUpdateMode.OnPropertyChanged));

            comboBoxCharacterPhyPowerGrowth.DataBindings.Add(
            new Binding("SelectedItem", CharacterBinding,
                "growth_phy_power_grow", false, DataSourceUpdateMode.OnPropertyChanged));

            comboBoxCharacterPhyGuardGrowth.DataBindings.Add(
            new Binding("SelectedItem", CharacterBinding,
                "growth_phy_guard_grow", false, DataSourceUpdateMode.OnPropertyChanged));

            comboBoxCharacterMagPowerGrowth.DataBindings.Add(
            new Binding("SelectedItem", CharacterBinding,
                "growth_mag_power_grow", false, DataSourceUpdateMode.OnPropertyChanged));

            comboBoxCharacterMagGuardGrowth.DataBindings.Add(
            new Binding("SelectedItem", CharacterBinding,
                "growth_mag_guard_grow", false, DataSourceUpdateMode.OnPropertyChanged));

            comboBoxCharacterSpePowerGrowth.DataBindings.Add(
            new Binding("SelectedItem", CharacterBinding,
                "growth_spe_power_grow", false, DataSourceUpdateMode.OnPropertyChanged));

            comboBoxCharacterSpeGuardGrowth.DataBindings.Add(
            new Binding("SelectedItem", CharacterBinding,
                "growth_spe_guard_grow", false, DataSourceUpdateMode.OnPropertyChanged));

            RefreshAll();
        
        }

        private void CharacterGrowthResetBindings()
        {
            for (int i = 0; i < CharacterGrowthBindingSources.Count; i++)
            {
                CharacterGrowthBindingSources[i].ResetBindings(false);
            }
        }
        #endregion

        #region Buttons
        private void buttonCharacterAdd_Click(object sender, EventArgs e)
        {
            listboxAdd(listBoxCharacters, CharacterData, new Character());
            panelCharacterStat.Refresh();
            RefreshAll();
        }

        private void buttonCharacterRemove_Click(object sender, EventArgs e)
        {
            listboxRemove(listBoxCharacters, CharacterData);
            panelCharacterStat.Refresh();
            RefreshAll();
        }

        private void buttonCharacterMoveUp_Click(object sender, EventArgs e)
        {
            listboxMoveUp(listBoxCharacters, CharacterData);
        }
        private void buttonCharacterMoveDown_Click(object sender, EventArgs e)
        {
            listboxMoveDown(listBoxCharacters, CharacterData);
        }
        #endregion

        #region EXP Comboboxes
        private void RefreshAll()
        {
            panelCharacterEXPGrowth.Refresh();
            panelCharacterHPGrowth.Refresh();
            panelCharacterMPGrowth.Refresh();

            panelCharacterPhyPowerGrowth.Refresh();
            panelCharacterPhyGuardGrowth.Refresh();
            panelCharacterMagPowerGrowth.Refresh();
            panelCharacterMagGuardGrowth.Refresh();
            panelCharacterSpePowerGrowth.Refresh();
            panelCharacterSpeGuardGrowth.Refresh();

            CharacterUpdateStatLabels();
        }

        private void comboBoxCharacterHPGrowth_SelectedIndexChanged(object sender, EventArgs e)
        {
            Character chara = (Character)listBoxCharacters.SelectedItem;

            if (chara != null)
            {
                ExpGrowth grow = (ExpGrowth)comboBoxCharacterHPGrowth.SelectedItem;
                if (grow == null)
                {
                    if (GrowthData.Count > 0)
                    {
                        grow = GrowthData[0];
                    }
                }

                chara.growth_hp_grow = grow;

                panelCharacterHPGrowth.Refresh();
            }
        }

        private void comboBoxCharacterEXPGrowth_SelectedIndexChanged(object sender, EventArgs e)
        {
            Character chara = (Character)listBoxCharacters.SelectedItem;

            if (chara != null)
            {
                ExpGrowth grow = (ExpGrowth)comboBoxCharacterEXPGrowth.SelectedItem;
                if (grow == null)
                {
                    if (GrowthData.Count > 0)
                    {
                        grow = GrowthData[0];
                    }
                }

                chara.growth_exp_grow = grow;

                panelCharacterEXPGrowth.Refresh();
            }
        }

        private void comboBoxCharacterMPGrowth_SelectedIndexChanged(object sender, EventArgs e)
        {
            Character chara = (Character)listBoxCharacters.SelectedItem;

            if (chara != null)
            {
                ExpGrowth grow = (ExpGrowth)comboBoxCharacterMPGrowth.SelectedItem;
                if (grow == null)
                {
                    if (GrowthData.Count > 0)
                    {
                        grow = GrowthData[0];
                    }
                }

                chara.growth_mp_grow = grow;

                panelCharacterMPGrowth.Refresh();
            }
        }

        private void comboBoxCharacterPhyPowerGrowth_SelectedIndexChanged(object sender, EventArgs e)
        {
            Character chara = (Character)listBoxCharacters.SelectedItem;

            if (chara != null)
            {
                ExpGrowth grow = (ExpGrowth)comboBoxCharacterPhyPowerGrowth.SelectedItem;
                if (grow == null)
                {
                    if (GrowthData.Count > 0)
                    {
                        grow = GrowthData[0];
                    }
                }

                chara.growth_phy_power_grow = grow;

                panelCharacterPhyPowerGrowth.Refresh();
            }
        }

        private void comboBoxCharacterPhyGuardGrowth_SelectedIndexChanged(object sender, EventArgs e)
        {
            Character chara = (Character)listBoxCharacters.SelectedItem;

            if (chara != null)
            {
                ExpGrowth grow = (ExpGrowth)comboBoxCharacterPhyGuardGrowth.SelectedItem;
                if (grow == null)
                {
                    if (GrowthData.Count > 0)
                    {
                        grow = GrowthData[0];
                    }
                }

                chara.growth_phy_guard_grow = grow;

                panelCharacterPhyGuardGrowth.Refresh();
            }
        }

        private void comboBoxCharacterMagPowerGrowth_SelectedIndexChanged(object sender, EventArgs e)
        {
            Character chara = (Character)listBoxCharacters.SelectedItem;

            if (chara != null)
            {
                ExpGrowth grow = (ExpGrowth)comboBoxCharacterMagPowerGrowth.SelectedItem;
                if (grow == null)
                {
                    if (GrowthData.Count > 0)
                    {
                        grow = GrowthData[0];
                    }
                }

                chara.growth_mag_power_grow = grow;

                panelCharacterMagPowerGrowth.Refresh();
            }
        }

        private void comboBoxCharacterMagGuardGrowth_SelectedIndexChanged(object sender, EventArgs e)
        {
            Character chara = (Character)listBoxCharacters.SelectedItem;

            if (chara != null)
            {
                ExpGrowth grow = (ExpGrowth)comboBoxCharacterMagGuardGrowth.SelectedItem;
                if (grow == null)
                {
                    if (GrowthData.Count > 0)
                    {
                        grow = GrowthData[0];
                    }
                }

                chara.growth_mag_guard_grow = grow;

                panelCharacterMagGuardGrowth.Refresh();
            }
        }

        private void comboBoxCharacterSpePowerGrowth_SelectedIndexChanged(object sender, EventArgs e)
        {
            Character chara = (Character)listBoxCharacters.SelectedItem;

            if (chara != null)
            {
                ExpGrowth grow = (ExpGrowth)comboBoxCharacterSpePowerGrowth.SelectedItem;
                if (grow == null)
                {
                    if (GrowthData.Count > 0)
                    {
                        grow = GrowthData[0];
                    }
                }

                chara.growth_spe_power_grow = grow;

                panelCharacterSpePowerGrowth.Refresh();
            }
        }

        private void comboBoxCharacterSpeGuardGrowth_SelectedIndexChanged(object sender, EventArgs e)
        {
            Character chara = (Character)listBoxCharacters.SelectedItem;

            if (chara != null)
            {
                ExpGrowth grow = (ExpGrowth)comboBoxCharacterSpeGuardGrowth.SelectedItem;
                if (grow == null)
                {
                    if (GrowthData.Count > 0)
                    {
                        grow = GrowthData[0];
                    }
                }

                chara.growth_spe_guard_grow = grow;

                panelCharacterSpeGuardGrowth.Refresh();
            }
        }


        #endregion

        #region Elements
        private void characterListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            panelCharacterStat.Refresh();
            BindDialog();
        }
        private void trackBarCharacterLevel_Scroll(object sender, EventArgs e)
        {
            if (listBoxCharacters.SelectedItem != null)
            {
                Character selected = (Character)listBoxCharacters.SelectedItem;
                selected.level = trackBarCharacterLevel.Value;
                panelCharacterStat.Refresh();
            }
            CharacterUpdateStatLabels();
        }

        private void textBoxCharacterName_Leave(object sender, EventArgs e)
        {
            CharacterBinding.ResetBindings(false);
        }

        private void textBox1_Click(object sender, EventArgs e)
        {
            colorDialog1.ShowDialog();

            textBoxCharacterColor.BackColor = colorDialog1.Color;
            labelCharacterRed.Text = colorDialog1.Color.R.ToString();
            labelCharacterGreen.Text = colorDialog1.Color.G.ToString();
            labelCharacterBlue.Text = colorDialog1.Color.B.ToString();
        }

        private void characterUpdateColor()
        {
            UpdateColor(
                textBoxCharacterColor,
                CharacterData,
                new Label[]{
                    labelCharacterRed,
                    labelCharacterGreen,
                    labelCharacterBlue
                    });
        }

        private void labelCharacterRed_TextChanged(object sender, EventArgs e)
        {
            characterUpdateColor();
        }

        private void labelCharacterGreen_TextChanged(object sender, EventArgs e)
        {
            characterUpdateColor();
        }

        private void labelCharacterBlue_TextChanged(object sender, EventArgs e)
        {
            characterUpdateColor();
        }

        #endregion

        #region Stats

        private void CharacterUpdateStatLabels()
        {
            Character chara = (Character)listBoxCharacters.SelectedItem;

            if (chara != null)
            {
                labelCharacterHP.Text = chara.display_hp.ToString();
                labelCharacterMP.Text = chara.display_mp.ToString();
                labelCharacterPhyPower.Text = chara.display_phy_power.ToString();
                labelCharacterPhyGuard.Text = chara.display_phy_guard.ToString();
                labelCharacterMagPower.Text = chara.display_mag_power.ToString();
                labelCharacterMagGuard.Text = chara.display_mag_guard.ToString();
                labelCharacterSpePower.Text = chara.display_spe_power.ToString();
                labelCharacterSpeGuard.Text = chara.display_spe_guard.ToString();
            }
        }

        private void trackBarCharacterHP_Scroll(object sender, EventArgs e)
        {
            Character chara = (Character)listBoxCharacters.SelectedItem;

            if (chara != null)
            {
                chara.hp = trackBarCharacterHP.Value;
                panelCharacterStat.Refresh();
            }

            CharacterUpdateStatLabels();
        }

        private void trackBarCharacterMP_Scroll(object sender, EventArgs e)
        {
            Character chara = (Character)listBoxCharacters.SelectedItem;

            if (chara != null)
            {
                chara.mp = trackBarCharacterMP.Value;

                panelCharacterStat.Refresh();
            }
            CharacterUpdateStatLabels();
        }

        private void trackBarCharacterPhyPower_Scroll(object sender, EventArgs e)
        {
            Character chara = (Character)listBoxCharacters.SelectedItem;

            if (chara != null)
            {
                chara.phy_power = trackBarCharacterPhyPower.Value;

                panelCharacterStat.Refresh();
            }
            CharacterUpdateStatLabels();
        }

        private void trackBarCharacterPhyGuard_Scroll(object sender, EventArgs e)
        {
            Character chara = (Character)listBoxCharacters.SelectedItem;

            if (chara != null)
            {
                chara.phy_guard = trackBarCharacterPhyGuard.Value;

                panelCharacterStat.Refresh();
            }
            CharacterUpdateStatLabels();
        }

        private void trackBarCharacterMagPower_Scroll(object sender, EventArgs e)
        {
            Character chara = (Character)listBoxCharacters.SelectedItem;

            if (chara != null)
            {
                chara.mag_power = trackBarCharacterMagPower.Value;

                panelCharacterStat.Refresh();
            }
            CharacterUpdateStatLabels();
        }

        private void trackBarCharacterMagGuard_Scroll(object sender, EventArgs e)
        {
            Character chara = (Character)listBoxCharacters.SelectedItem;

            if (chara != null)
            {
                chara.mag_guard = trackBarCharacterMagGuard.Value;

                panelCharacterStat.Refresh();
            }
            CharacterUpdateStatLabels();
        }

        private void trackBarCharacterSpePower_Scroll(object sender, EventArgs e)
        {
            Character chara = (Character)listBoxCharacters.SelectedItem;

            if (chara != null)
            {
                chara.spe_power = trackBarCharacterSpePower.Value;

                panelCharacterStat.Refresh();
            }
            CharacterUpdateStatLabels();
        }

        private void trackBarCharacterSpeGuard_Scroll(object sender, EventArgs e)
        {
            Character chara = (Character)listBoxCharacters.SelectedItem;

            if (chara != null)
            {
                chara.spe_guard = trackBarCharacterSpeGuard.Value;

                panelCharacterStat.Refresh();
            }
            CharacterUpdateStatLabels();
        }

        private void trackBarCharacterPhyPower_Scroll_1(object sender, EventArgs e)
        {
            panelCharacterStat.Refresh();
        }

        private void trackBarCharacterPhyGuard_Scroll_1(object sender, EventArgs e)
        {
            panelCharacterStat.Refresh();
        }

        private void trackBarCharacterMagPower_Scroll_1(object sender, EventArgs e)
        {
            panelCharacterStat.Refresh();
        }

        private void trackBarCharacterMagGuard_Scroll_1(object sender, EventArgs e)
        {
            panelCharacterStat.Refresh();
        }

        private void trackBarCharacterSpePower_Scroll_1(object sender, EventArgs e)
        {
            panelCharacterStat.Refresh();
        }

        private void trackBarCharacterSpeGuard_Scroll_1(object sender, EventArgs e)
        {
            panelCharacterStat.Refresh();
        }
        #endregion

        #region Draw
        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            if (listBoxCharacters.SelectedItem != null)
            {
                Character chara = (Character)listBoxCharacters.SelectedItem;
                double[] stats = new double[] {
                        (double)chara.display_phy_power/(double)trackBarCharacterPhyPower.Maximum,
                        (double)chara.display_phy_guard/(double)trackBarCharacterPhyGuard.Maximum,
                        (double)chara.display_mag_guard/(double)trackBarCharacterMagGuard.Maximum,
                        (double)chara.display_mag_power/(double)trackBarCharacterMagPower.Maximum,
                        (double)chara.display_spe_guard/(double)trackBarCharacterSpeGuard.Maximum,
                        (double)chara.display_spe_power/(double)trackBarCharacterSpePower.Maximum
                    };

                PanelDrawStat(sender, e, stats);
                ;
            }
        }

        private void panelCharacterEXPGrowth_Paint(object sender, PaintEventArgs e)
        {
            CharacterUpdateGrowthPoints();
            DrawGrowthPoints(sender as Panel, e.Graphics, CharacterGrowthPoints[0]);
        }

        private void panelCharacterHPGrowth_Paint(object sender, PaintEventArgs e)
        {
            CharacterUpdateGrowthPoints();
            DrawGrowthPoints(sender as Panel, e.Graphics, CharacterGrowthPoints[1]);
        }

        private void panelCharacterMPGrowth_Paint(object sender, PaintEventArgs e)
        {
            CharacterUpdateGrowthPoints();
            DrawGrowthPoints(sender as Panel, e.Graphics, CharacterGrowthPoints[2]);
        }

        private void panelCharacterPhyPowerGrowth_Paint(object sender, PaintEventArgs e)
        {
            CharacterUpdateGrowthPoints();
            DrawGrowthPoints(sender as Panel, e.Graphics, CharacterGrowthPoints[3]);
        }

        private void panelCharacterPhyGuardGrowth_Paint(object sender, PaintEventArgs e)
        {
            CharacterUpdateGrowthPoints();
            DrawGrowthPoints(sender as Panel, e.Graphics, CharacterGrowthPoints[4]);
        }

        private void panelCharacterMagPowerGrowth_Paint(object sender, PaintEventArgs e)
        {
            CharacterUpdateGrowthPoints();
            DrawGrowthPoints(sender as Panel, e.Graphics, CharacterGrowthPoints[5]);
        }

        private void panelCharacterMagGuardGrowth_Paint(object sender, PaintEventArgs e)
        {
            CharacterUpdateGrowthPoints();
            DrawGrowthPoints(sender as Panel, e.Graphics, CharacterGrowthPoints[6]);
        }

        private void panelCharacterSpePowerGrowth_Paint(object sender, PaintEventArgs e)
        {
            CharacterUpdateGrowthPoints();
            DrawGrowthPoints(sender as Panel, e.Graphics, CharacterGrowthPoints[7]);
        }

        private void panelCharacterSpeGuardGrowth_Paint(object sender, PaintEventArgs e)
        {
            CharacterUpdateGrowthPoints();
            DrawGrowthPoints(sender as Panel, e.Graphics, CharacterGrowthPoints[8]);
        }
        #endregion

        #endregion

        #region Dialog

        #region Bindings
        private void BindDialog()
        {
            if (listBoxCharacters.SelectedIndex > -1)
            {
                Character chara = CharacterData[listBoxCharacters.SelectedIndex];

                if (chara != null)
                {
                    listBoxCharacterDialog.DataSource = chara.DialogBinding;

                    if (chara.DialogBinding != null)
                    {
                        RedoBinding(chara.DialogBinding, textBoxCharacterDialogName, "Text", "name");
                        RedoBinding(chara.DialogBinding, checkBoxDialogEndNode, "Checked", "EndNode");
                        if (listBoxCharacterDialog.SelectedIndex > -1)
                        {
                            Dialog dia = chara.Dialogs[listBoxCharacterDialog.SelectedIndex];

                            if (dia != null)
                            {
                                listBoxCharacterDialogText.DataSource = dia.TextSource;
                                listBoxCharacterDialogOptions.DataSource = dia.OptionSource;
                            }
                        }
                    }

                    CurrentDialogs = chara.Dialogs;
                }
            }
        }
        #endregion

        #region Elements
        private void textBoxCharacterDialogName_Leave(object sender, EventArgs e)
        {
            if (listBoxCharacters.SelectedIndex > -1)
            {
                Character chara = CharacterData[listBoxCharacters.SelectedIndex];

                if (chara != null)
                {
                    chara.DialogBinding.ResetBindings(false);
                }
            }
        }

        private void listBoxCharacterDialog_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBoxCharacters.SelectedIndex > -1)
            {
                Character chara = CharacterData[listBoxCharacters.SelectedIndex];

                if (chara != null)
                {
                    listBoxCharacterDialog.DataSource = chara.DialogBinding;

                    if (chara.DialogBinding != null)
                    {
                        RedoBinding(chara.DialogBinding, textBoxCharacterDialogName, "Text", "name");

                        if (listBoxCharacterDialog.SelectedIndex > -1)
                        {
                            Dialog dia = chara.Dialogs[listBoxCharacterDialog.SelectedIndex];

                            if (dia != null)
                            {
                                listBoxCharacterDialogText.DataSource = dia.TextSource;
                                listBoxCharacterDialogOptions.DataSource = dia.OptionSource;
                            }
                        }
                    }

                    CurrentDialogs = chara.Dialogs;
                }
            }
        }

        public void CharacterUpdateDialog()
        {
            if (listBoxCharacters.SelectedIndex > -1)
            {
                Character chara = CharacterData[listBoxCharacters.SelectedIndex];

                if (chara != null)
                {
                    CurrentDialogs = chara.Dialogs;
                }
            }
        }
        #endregion

        #region Buttons
        private void buttonCharacterDialogAdd_Click(object sender, EventArgs e)
        {
            if (listBoxCharacters.SelectedIndex > -1)
            {
                Character chara = CharacterData[listBoxCharacters.SelectedIndex];

                if (chara != null)
                {

                    listboxAdd(listBoxCharacterDialog, chara.Dialogs, new Dialog());
                    RefreshAll();
                }
            }
        }

        private void buttonCharacterDialogRemove_Click(object sender, EventArgs e)
        {
            if (listBoxCharacters.SelectedIndex > -1)
            {
                Character chara = CharacterData[listBoxCharacters.SelectedIndex];

                if (chara != null)
                {

                    listboxRemove(listBoxCharacterDialog, chara.Dialogs);
                    RefreshAll();
                }
            }
        }

        private void buttonCharacterDialogMoveUp_Click(object sender, EventArgs e)
        {
            if (listBoxCharacters.SelectedIndex > -1)
            {
                Character chara = CharacterData[listBoxCharacters.SelectedIndex];

                if (chara != null)
                {

                    listboxMoveUp(listBoxCharacterDialog, chara.Dialogs);
                }
            }
        }

        private void buttonCharacterDialogMoveDown_Click(object sender, EventArgs e)
        {
            if (listBoxCharacters.SelectedIndex > -1)
            {
                Character chara = CharacterData[listBoxCharacters.SelectedIndex];

                if (chara != null)
                {

                    listboxMoveUp(listBoxCharacterDialog, chara.Dialogs);
                }
            }
        }
        #endregion

        #region DialogElements

        #region Option Buttons

        private void buttonCharacterDialogOptionAdd_Click(object sender, EventArgs e)
        {
            if (listBoxCharacters.SelectedIndex > -1)
            {
                Character chara = CharacterData[listBoxCharacters.SelectedIndex];

                if (chara != null)
                {
                    if (listBoxCharacterDialog.SelectedIndex > -1)
                    {
                        Dialog dia = chara.Dialogs[listBoxCharacterDialog.SelectedIndex];

                        if (dia != null)
                        {
                            DialogOption op = ButtonCharacterDialogOptionOpenMenu(dia,true);
                            if (op != null)
                            {
                                listboxAdd(listBoxCharacterDialogOptions, dia.Options, op);
                            }
                        }
                    }
                }
            }
        }

        private DialogText ButtonCharacterDialogTextOpenMenu(Dialog dialog, bool makeNew)
        {

            DialogText selected;

            FormDialogText form = new FormDialogText();
            form.AddMode = true;
            form.DialogList = dialog.Text;
            Tabs.Enabled = false;

            if (makeNew)
            {
                selected = new DialogText();
            } else 
            {
                selected = (DialogText)listBoxCharacterDialogText.Items[listBoxCharacterDialogText.SelectedIndex];
                form.DialogText = selected.text;
                form.BGSprite = selected.bg_sprite;
                form.FGSprite = selected.fg_sprite;
                form.Emotion = selected.emotion;
            }

            form.Obj = selected;
            if(form.ShowDialog() == DialogResult.Cancel)
            {
                Tabs.Enabled = true;
                EventBinding.ResetBindings(false);
                return null;
            }

            selected.text = form.DialogText;
            selected.bg_sprite = form.BGSprite;
            selected.fg_sprite = form.FGSprite;
            selected.emotion = form.Emotion;

            Tabs.Enabled = true;
            EventBinding.ResetBindings(false);


            return selected;
        }

        private DialogOption ButtonCharacterDialogOptionOpenMenu(Dialog dialog, bool makeNew)
        {
            dialog.CorrectNullValues();
            DialogOption selected;

            FormDialogOption form = new FormDialogOption();
            form.AddMode = true;
            form.DialogList = dialog.Text;
            Tabs.Enabled = false;

            // create data sources
            form.EventList = new BindingSource(EventData, null);
            form.EventDisabledList = new BindingSource(EventData, null);
            form.EventNeedList = new BindingSource(EventData, null);
            form.DialogSource = new BindingSource(CurrentDialogs, null);
            form.Bind();

            if (makeNew)
            {
                selected = new DialogOption();
            }
            else
            {
                selected = (DialogOption)listBoxCharacterDialogOptions.Items[listBoxCharacterDialogOptions.SelectedIndex];
                form.OptionText = selected.text;
                form.eventSet = selected.EventSet;
                form.eventDisabled = selected.EventInvisible;
                form.eventEnabled = selected.EventVisible;
                form.gotoNext = selected.gotoDialog;
            }

            form.Obj = selected;
            if (form.ShowDialog() == DialogResult.Cancel)
            {
                Tabs.Enabled = true;
                EventBinding.ResetBindings(false);
                return null;
            }

            selected.EventSet = form.eventSet;
            selected.EventVisible = form.eventEnabled;
            selected.EventInvisible = form.eventDisabled;
            selected.gotoDialog = form.gotoNext;
            
            selected.text = form.OptionText;

            Tabs.Enabled = true;
            EventBinding.ResetBindings(false);


            return selected;
        }

        private void buttonCharacterDialogOptionRemove_Click(object sender, EventArgs e)
        {
            if (listBoxCharacters.SelectedIndex > -1)
            {
                Character chara = CharacterData[listBoxCharacters.SelectedIndex];

                if (chara != null)
                {
                    if (listBoxCharacterDialog.SelectedIndex > -1)
                    {
                        Dialog dia = chara.Dialogs[listBoxCharacterDialog.SelectedIndex];

                        if (dia != null)
                        {
                            listboxRemove(listBoxCharacterDialogOptions, dia.Options);
                        }
                    }
                }
            }
        }

        private void buttonCharacterDialogOptionMoveUp_Click(object sender, EventArgs e)
        {
            if (listBoxCharacters.SelectedIndex > -1)
            {
                Character chara = CharacterData[listBoxCharacters.SelectedIndex];

                if (chara != null)
                {
                    if (listBoxCharacterDialog.SelectedIndex > -1)
                    {
                        Dialog dia = chara.Dialogs[listBoxCharacterDialog.SelectedIndex];

                        if (dia != null)
                        {
                            listboxMoveUp(listBoxCharacterDialogOptions, dia.Options);
                        }
                    }
                }
            }
        }

        private void buttonCharacterDialogOptionMoveDown_Click(object sender, EventArgs e)
        {
            if (listBoxCharacters.SelectedIndex > -1)
            {
                Character chara = CharacterData[listBoxCharacters.SelectedIndex];

                if (chara != null)
                {

                    if (listBoxCharacterDialog.SelectedIndex > -1)
                    {
                        Dialog dia = chara.Dialogs[listBoxCharacterDialog.SelectedIndex];

                        if (dia != null)
                        {
                            listboxMoveDown(listBoxCharacterDialogOptions, dia.Options);
                        }
                    }
                }
            }
        }

        private void buttonCharacterDialogTextAdd_Click(object sender, EventArgs e)
        {
            if (listBoxCharacters.SelectedIndex > -1)
            {
                Character chara = CharacterData[listBoxCharacters.SelectedIndex];

                if (chara != null)
                {
                    if (listBoxCharacterDialog.SelectedIndex > -1)
                    {
                        Dialog dia = chara.Dialogs[listBoxCharacterDialog.SelectedIndex];

                        if (dia != null)
                        {
                            DialogText op = ButtonCharacterDialogTextOpenMenu(dia, true);
                            if (op != null)
                            {
                                listboxAdd(listBoxCharacterDialogText, dia.Text, op);
                            }
                        }
                    }
                }
            }
        }

        private void buttonCharacterDialogTextRemove_Click(object sender, EventArgs e)
        {
            if (listBoxCharacters.SelectedIndex > -1)
            {
                Character chara = CharacterData[listBoxCharacters.SelectedIndex];

                if (chara != null)
                {
                    if (listBoxCharacterDialog.SelectedIndex > -1)
                    {
                        Dialog dia = chara.Dialogs[listBoxCharacterDialog.SelectedIndex];

                        if (dia != null)
                        {
                            listboxRemove(listBoxCharacterDialogText, dia.Text);
                        }
                    }
                }
            }
        }

        private void buttonCharacterDialogTextMoveUp_Click(object sender, EventArgs e)
        {
            if (listBoxCharacters.SelectedIndex > -1)
            {
                Character chara = CharacterData[listBoxCharacters.SelectedIndex];

                if (chara != null)
                {
                    if (listBoxCharacterDialog.SelectedIndex > -1)
                    {
                        Dialog dia = chara.Dialogs[listBoxCharacterDialog.SelectedIndex];

                        if (dia != null)
                        {
                            listboxMoveUp(listBoxCharacterDialogText, dia.Text);
                        }
                    }
                }
            }
        }

        private void buttonCharacterDialogTextMoveDown_Click(object sender, EventArgs e)
        {
            if (listBoxCharacters.SelectedIndex > -1)
            {
                Character chara = CharacterData[listBoxCharacters.SelectedIndex];

                if (chara != null)
                {
                    if (listBoxCharacterDialog.SelectedIndex > -1)
                    {
                        Dialog dia = chara.Dialogs[listBoxCharacterDialog.SelectedIndex];

                        if (dia != null)
                        {
                            listboxMoveDown(listBoxCharacterDialogText, dia.Text);
                        }
                    }
                }
            }
        }
        #endregion

        #endregion

        #endregion
    }
}
