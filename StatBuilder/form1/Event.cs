﻿
using StatBuilder.formEvent;
using StatBuilder.model;
using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace StatBuilder
{
    public partial class MainForm : Form
    {
        #region Events

        #region properties
        public static BindingList<Event> EventData { get; set; } = new BindingList<Event>();
        public static BindingSource EventBinding;
        #endregion

        #region Bindings
        private void BindEvent()
        {
            EventBinding = new BindingSource(EventData, null);

            listBoxEvent.DataSource = EventBinding;
        }
        #endregion

        #region Buttons
        private void buttonEventAdd_Click(object sender, EventArgs e)
        {
            if (listBoxEvent.SelectedIndex > -1) {
                Event selected = (Event)listBoxEvent.Items[listBoxEvent.SelectedIndex];

                FormEvent form = new FormEvent();
                Tabs.Enabled = false;
                form.eventObj = selected;
                form.AddMode = true;
                form.EventList = EventData;
                form.ShowDialog();
                Tabs.Enabled = true;
                EventBinding.ResetBindings(false);
            }
        }

        private void buttonEventRemove_Click(object sender, EventArgs e)
        {
            listboxRemove(listBoxEvent, EventData);
        }

        private void buttonEventMoveUp_Click(object sender, EventArgs e)
        {
            listboxMoveUp(listBoxEvent, EventData);
        }

        private void buttonEventMoveDown_Click(object sender, EventArgs e)
        {
            listboxMoveDown(listBoxEvent, EventData);
        }

        #endregion

        #region Element
        private void textBoxEventName_Leave(object sender, EventArgs e)
        {
            EventBinding.ResetBindings(false);
        }
        #endregion

        #endregion
    }
}
