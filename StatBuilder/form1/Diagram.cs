﻿
using StatBuilder.model;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace StatBuilder
{
    public partial class MainForm : Form
    {
        #region Diagrams

        #region Properties
        public static BindingList<Diagram> DiagramData { get; set; } = new BindingList<Diagram>();
        public static BindingSource DiagramBinding;
        #endregion

        #region Bindings
        private void BindDiagram()
        {
            DiagramBinding = new BindingSource(DiagramData, null);

            listBoxDiagram.DataSource = DiagramBinding;

            textBoxDiagramName.DataBindings.Add(
            new Binding("Text", DiagramBinding,
                "name", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxDiagramAltText.DataBindings.Add(
            new Binding("Text", DiagramBinding,
                "alt_text", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxDiagramSprite.DataBindings.Add(
            new Binding("Text", DiagramBinding,
                "sprite", false, DataSourceUpdateMode.OnPropertyChanged));

            textBoxDiagramXScale.DataBindings.Add(
            new Binding("Text", DiagramBinding,
                "x_scale", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxDiagramYScale.DataBindings.Add(
            new Binding("Text", DiagramBinding,
                "y_scale", false, DataSourceUpdateMode.OnPropertyChanged));

            textBoxDiagramXOffset.DataBindings.Add(
            new Binding("Text", DiagramBinding,
                "x_offset", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxDiagramYOffset.DataBindings.Add(
            new Binding("Text", DiagramBinding,
                "y_offset", false, DataSourceUpdateMode.OnPropertyChanged));
        }
        #endregion

        #region Element
        private void textBoxDiagramName_Leave(object sender, EventArgs e)
        {
            DiagramBinding.ResetBindings(false);
            updateDiagramColorBox();
        }

        private void listBoxDiagram_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateDiagramColorBox();
        }
        #endregion

        #region Buttons

        private void buttonDiagramAdd_Click(object sender, EventArgs e)
        {
            listboxAdd(listBoxDiagram, DiagramData, new Diagram());
            updateDiagramColorBox();
        }

        private void buttonDiagramRemove_Click(object sender, EventArgs e)
        {
            listboxRemove(listBoxDiagram, DiagramData);
            updateDiagramColorBox();
        }

        private void buttonDiagramMoveUp_Click(object sender, EventArgs e)
        {
            listboxMoveUp(listBoxDiagram, DiagramData);
            updateDiagramColorBox();
        }

        private void buttonDiagramMoveDown_Click(object sender, EventArgs e)
        {
            listboxMoveDown(listBoxDiagram, DiagramData);
            updateDiagramColorBox();
        }

        private void textBoxDiagramBlend_Click(object sender, EventArgs e)
        {
            colorDialog1.ShowDialog();

            textBoxDiagramBlend.BackColor = colorDialog1.Color;
            updateDiagramColor(textBoxDiagramBlend.BackColor.R,
                                textBoxDiagramBlend.BackColor.G,
                                textBoxDiagramBlend.BackColor.B);

        }

        private void updateDiagramColor(int r, int g, int b)
        {
            if(listBoxDiagram.SelectedIndex > -1)
            {
                Diagram dia = DiagramData[listBoxDiagram.SelectedIndex];
                dia.r = r;
                dia.g = g;
                dia.b = b;
            }
        }

        private void updateDiagramColorBox()
        {
            if (listBoxDiagram.SelectedIndex > -1)
            {
                Diagram dia = DiagramData[listBoxDiagram.SelectedIndex];

                textBoxDiagramBlend.BackColor = Color.FromArgb(
                        dia.r,
                        dia.g,
                        dia.b);
            }
        }

        #endregion

        #endregion
    }
}
