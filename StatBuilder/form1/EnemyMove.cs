﻿
using StatBuilder.model;
using System;
using System.Windows.Forms;

namespace StatBuilder
{
    public partial class MainForm : Form
    {

        #region properties
        BindingSource MonsterMovesBinding;
        BindingSource MonsterMovesSourceBinding;
        public static BindingSource MonsterMovesListBinding;
        #endregion

        #region Bindings
        private void BindMonsterMoves()
        {
            MonsterMovesBinding = new BindingSource(EnemyData, null);

            listBoxMonsterMoves.DataSource = MonsterMovesBinding;

            MonsterMovesSourceBinding = new BindingSource(MoveData, null);

            comboBoxMonsterMovesMove.DataSource = MonsterMovesSourceBinding;
        }

        #endregion

        #region elements

        private void listBoxMonsterMoves_SelectedIndexChanged(object sender, EventArgs e)
        {
            Enemy chara = (Enemy)listBoxMonsterMoves.SelectedItem;

            if (chara != null)
            {
                MonsterMovesListBinding = new BindingSource(chara.movesLearn.moves, null);

                textBoxMonsterMovesLevel.DataBindings.Clear();
                textBoxMonsterMovesLevel.DataBindings.Add(
                new Binding("Text", MonsterMovesListBinding,
                    "level", false, DataSourceUpdateMode.OnPropertyChanged));

                comboBoxMonsterMovesMove.DataBindings.Clear();
                comboBoxMonsterMovesMove.DataBindings.Add(
                new Binding("SelectedItem", MonsterMovesListBinding,
                    "Move", false, DataSourceUpdateMode.OnPropertyChanged));

            }
            else
            {
                MonsterMovesListBinding = null;
            }

            listBoxMonsterMovesList.DataSource = MonsterMovesListBinding;
        }
        #endregion

        #region buttons

        private void buttonMonsterMovesAdd_Click(object sender, EventArgs e)
        {
            Enemy chara = (Enemy)listBoxMonsterMoves.SelectedItem;

            if (chara != null)
            {
                chara.movesLearn.moves.Add(new MoveLearn.MoveLearnPiece(
                        null,
                        1
                    ));

                listBoxMonsterMovesList.SelectedIndex++;
            }
        }

        private void buttonMonsterMovesRemove_Click(object sender, EventArgs e)
        {
            Enemy chara = (Enemy)listBoxMonsterMoves.SelectedItem;

            if (chara != null)
            {
                if (listBoxMonsterMovesList.SelectedIndex > -1 &&
                    listBoxMonsterMovesList.Items.Count > 1)
                {
                    chara.movesLearn.moves.RemoveAt(listBoxMonsterMovesList.SelectedIndex);
                }
            }
        }

        #endregion
    }
}
