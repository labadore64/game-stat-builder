﻿
using StatBuilder.formItemEvent;
using StatBuilder.model;
using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace StatBuilder
{
    public partial class MainForm : Form
    {
        #region ItemEvents

        #region properties
        public static BindingList<ItemEvent> ItemEventData { get; set; } = new BindingList<ItemEvent>();
        public static BindingSource ItemEventBinding;
        #endregion

        #region Bindings
        private void BindItemEvent()
        {
            ItemEventBinding = new BindingSource(ItemEventData, null);

            listBoxItemEvent.DataSource = ItemEventBinding;
        }
        #endregion

        #region Buttons
        private void buttonItemEventAdd_Click(object sender, EventArgs e)
        {
            
            if (listBoxEvent.SelectedIndex > -1)
            {

                FormItemEvent form = new FormItemEvent();
                Tabs.Enabled = false;
                form.eventObj = new ItemEvent();
                form.AddMode = true;
                form.Quantity = 1;
                form.ItemBinding = new BindingSource(ItemData, null);
                form.ItemEventBinding = new BindingSource(form.eventObj, null);
                form.SetBinding();
                form.BindItem();
                form.EventList = ItemEventData;
                form.ShowDialog();
                Tabs.Enabled = true;
                EventBinding.ResetBindings(false);
            }
        }

        private void buttonItemEventRemove_Click(object sender, EventArgs e)
        {
            listboxRemove(listBoxItemEvent, ItemEventData);
        }

        private void buttonItemEventMoveUp_Click(object sender, EventArgs e)
        {
            listboxMoveUp(listBoxItemEvent, ItemEventData);
        }

        private void buttonItemEventMoveDown_Click(object sender, EventArgs e)
        {
            listboxMoveDown(listBoxItemEvent, ItemEventData);
        }

        #endregion

        #region Element
        private void textBoxItemEventName_Leave(object sender, EventArgs e)
        {
            ItemEventBinding.ResetBindings(false);
        }
        #endregion

        #endregion

    }
}
