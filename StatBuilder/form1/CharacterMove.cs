﻿
using StatBuilder.model;
using System;
using System.Windows.Forms;

namespace StatBuilder
{
    public partial class MainForm : Form
    {
        #region Character Moves

        #region properties
        BindingSource CharacterMovesListBinding;
        BindingSource CharacterMovesSourceBinding;
        public static BindingSource CharacterMovesBinding;
        #endregion

        #region Bindings
        private void BindCharacterMoves()
        {
            CharacterMovesBinding = new BindingSource(CharacterData, null);

            listBoxCharacterMoves.DataSource = CharacterMovesBinding;

            CharacterMovesSourceBinding = new BindingSource(MoveData, null);

            comboBoxCharacterMovesMove.DataSource = CharacterMovesSourceBinding;

        }
        #endregion

        #endregion

        #region element
        private void listBoxCharacterMoves_SelectedIndexChanged(object sender, EventArgs e)
        {

            Character chara = (Character)listBoxCharacterMoves.SelectedItem;

            if (chara != null)
            {
                CharacterMovesListBinding = new BindingSource(chara.movesLearn.moves, null);

                textBoxCharacterMovesLevel.DataBindings.Clear();
                textBoxCharacterMovesLevel.DataBindings.Add(
                new Binding("Text", CharacterMovesListBinding,
                    "level", false, DataSourceUpdateMode.OnPropertyChanged));

                comboBoxCharacterMovesMove.DataBindings.Clear();
                comboBoxCharacterMovesMove.DataBindings.Add(
                new Binding("SelectedItem", CharacterMovesListBinding,
                    "Move", false, DataSourceUpdateMode.OnPropertyChanged));

            }
            else
            {
                CharacterMovesListBinding = null;
            }

            listBoxCharacterMovesList.DataSource = CharacterMovesListBinding;

        }
        #endregion

        #region Buttons
        private void buttonCharacterMovesAdd_Click(object sender, EventArgs e)
        {
            Character chara = (Character)listBoxCharacterMoves.SelectedItem;

            if (chara != null)
            {
                chara.movesLearn.moves.Add(new MoveLearn.MoveLearnPiece(
                        null,
                        1
                    ));

                listBoxCharacterMovesList.SelectedIndex++;
            }

        }

        private void buttonCharacterMovesRemove_Click(object sender, EventArgs e)
        {
            Character chara = (Character)listBoxCharacterMoves.SelectedItem;

            if (chara != null)
            {
                if (listBoxCharacterMovesList.SelectedIndex > -1 &&
                    listBoxCharacterMovesList.Items.Count > 1)
                {
                    chara.movesLearn.moves.RemoveAt(listBoxCharacterMovesList.SelectedIndex);
                }
            }
        }
        #endregion
    }
}
