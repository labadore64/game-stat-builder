﻿namespace StatBuilder
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.Tabs = new System.Windows.Forms.TabControl();
            this.tabCharacters = new System.Windows.Forms.TabPage();
            this.groupBox36 = new System.Windows.Forms.GroupBox();
            this.groupBox38 = new System.Windows.Forms.GroupBox();
            this.buttonCharacterOptionDown = new System.Windows.Forms.Button();
            this.listBoxCharacterDialogOptions = new System.Windows.Forms.ListBox();
            this.buttonCharacterOptionUp = new System.Windows.Forms.Button();
            this.buttonCharacterOptionAdd = new System.Windows.Forms.Button();
            this.buttonCharacterOptionRemove = new System.Windows.Forms.Button();
            this.groupBox37 = new System.Windows.Forms.GroupBox();
            this.buttonCharacterTextDown = new System.Windows.Forms.Button();
            this.listBoxCharacterDialogText = new System.Windows.Forms.ListBox();
            this.buttonCharacterTextUp = new System.Windows.Forms.Button();
            this.buttonCharacterTextAdd = new System.Windows.Forms.Button();
            this.buttonCharacterTextRemove = new System.Windows.Forms.Button();
            this.textBoxCharacterDialogName = new System.Windows.Forms.TextBox();
            this.buttonCharacterDialogDown = new System.Windows.Forms.Button();
            this.label37 = new System.Windows.Forms.Label();
            this.listBoxCharacterDialog = new System.Windows.Forms.ListBox();
            this.buttonCharacterDialogUp = new System.Windows.Forms.Button();
            this.buttonCharacterDialogAdd = new System.Windows.Forms.Button();
            this.buttonCharacterDialogRemove = new System.Windows.Forms.Button();
            this.groupBox35 = new System.Windows.Forms.GroupBox();
            this.panelCharacterStat = new System.Windows.Forms.Panel();
            this.label42 = new System.Windows.Forms.Label();
            this.trackBarCharacterLevel = new System.Windows.Forms.TrackBar();
            this.groupBox34 = new System.Windows.Forms.GroupBox();
            this.textBoxCharacterColor = new System.Windows.Forms.TextBox();
            this.textBoxCharacterName = new System.Windows.Forms.TextBox();
            this.label228 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxCharacterPhone = new System.Windows.Forms.TextBox();
            this.textBoxCharacterAbility = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.labelCharacterRed = new System.Windows.Forms.Label();
            this.labelCharacterGreen = new System.Windows.Forms.Label();
            this.labelCharacterBlue = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.comboBoxCharacterType = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxCharacterBattleDesc = new System.Windows.Forms.TextBox();
            this.textBoxCharacterVisualDesc = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox33 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.trackBarCharacterHP = new System.Windows.Forms.TrackBar();
            this.labelCharacterHP = new System.Windows.Forms.Label();
            this.trackBarCharacterMP = new System.Windows.Forms.TrackBar();
            this.label14 = new System.Windows.Forms.Label();
            this.labelCharacterMP = new System.Windows.Forms.Label();
            this.trackBarCharacterPhyPower = new System.Windows.Forms.TrackBar();
            this.label16 = new System.Windows.Forms.Label();
            this.labelCharacterPhyPower = new System.Windows.Forms.Label();
            this.trackBarCharacterPhyGuard = new System.Windows.Forms.TrackBar();
            this.label18 = new System.Windows.Forms.Label();
            this.labelCharacterPhyGuard = new System.Windows.Forms.Label();
            this.trackBarCharacterMagPower = new System.Windows.Forms.TrackBar();
            this.labelCharacterSpeGuard = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.labelCharacterMagPower = new System.Windows.Forms.Label();
            this.trackBarCharacterSpeGuard = new System.Windows.Forms.TrackBar();
            this.trackBarCharacterMagGuard = new System.Windows.Forms.TrackBar();
            this.labelCharacterSpePower = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.labelCharacterMagGuard = new System.Windows.Forms.Label();
            this.trackBarCharacterSpePower = new System.Windows.Forms.TrackBar();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label73 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.panelCharacterEXPGrowth = new System.Windows.Forms.Panel();
            this.panelCharacterSpeGuardGrowth = new System.Windows.Forms.Panel();
            this.textBoxCharacterEXPTotal = new System.Windows.Forms.TextBox();
            this.panelCharacterPhyGuardGrowth = new System.Windows.Forms.Panel();
            this.label41 = new System.Windows.Forms.Label();
            this.panelCharacterMagGuardGrowth = new System.Windows.Forms.Panel();
            this.label63 = new System.Windows.Forms.Label();
            this.comboBoxCharacterEXPGrowth = new System.Windows.Forms.ComboBox();
            this.panelCharacterMPGrowth = new System.Windows.Forms.Panel();
            this.label77 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.comboBoxCharacterSpeGuardGrowth = new System.Windows.Forms.ComboBox();
            this.comboBoxCharacterPhyGuardGrowth = new System.Windows.Forms.ComboBox();
            this.comboBoxCharacterMagGuardGrowth = new System.Windows.Forms.ComboBox();
            this.comboBoxCharacterMPGrowth = new System.Windows.Forms.ComboBox();
            this.panelCharacterSpePowerGrowth = new System.Windows.Forms.Panel();
            this.panelCharacterPhyPowerGrowth = new System.Windows.Forms.Panel();
            this.comboBoxCharacterSpePowerGrowth = new System.Windows.Forms.ComboBox();
            this.comboBoxCharacterPhyPowerGrowth = new System.Windows.Forms.ComboBox();
            this.panelCharacterMagPowerGrowth = new System.Windows.Forms.Panel();
            this.comboBoxCharacterMagPowerGrowth = new System.Windows.Forms.ComboBox();
            this.panelCharacterHPGrowth = new System.Windows.Forms.Panel();
            this.comboBoxCharacterHPGrowth = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.textBoxCharacterSpriteFace = new System.Windows.Forms.TextBox();
            this.textBoxCharacterSpriteIdleFront = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.textBoxCharacterSpriteIdleBack = new System.Windows.Forms.TextBox();
            this.textBoxCharacterSpriteSpeed = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.textBoxCharacterSpriteHitFront = new System.Windows.Forms.TextBox();
            this.textBoxCharacterSpriteCast1Back = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.textBoxCharacterSpriteHitBack = new System.Windows.Forms.TextBox();
            this.textBoxCharacterSpriteCast1Front = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.textBoxCharacterSpriteJump0Front = new System.Windows.Forms.TextBox();
            this.textBoxCharacterSpriteCast0Back = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.textBoxCharacterSpriteJump0Back = new System.Windows.Forms.TextBox();
            this.textBoxCharacterSpriteCast0Front = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.textBoxCharacterSpriteJump1Front = new System.Windows.Forms.TextBox();
            this.textBoxCharacterSpriteJump1Back = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.buttonCharacterMoveDown = new System.Windows.Forms.Button();
            this.buttonCharacterMoveUp = new System.Windows.Forms.Button();
            this.buttonCharacterRemove = new System.Windows.Forms.Button();
            this.buttonCharacterAdd = new System.Windows.Forms.Button();
            this.listBoxCharacters = new System.Windows.Forms.ListBox();
            this.tabMonsters = new System.Windows.Forms.TabPage();
            this.groupBox32 = new System.Windows.Forms.GroupBox();
            this.checkBoxMonsterIgnoreCry = new System.Windows.Forms.CheckBox();
            this.checkBoxMonsterIgnoreDex = new System.Windows.Forms.CheckBox();
            this.checkBoxMonsterFlee = new System.Windows.Forms.CheckBox();
            this.checkBoxMonsterMove = new System.Windows.Forms.CheckBox();
            this.checkBoxMonsterJump = new System.Windows.Forms.CheckBox();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this.listBoxEncounters = new System.Windows.Forms.ListBox();
            this.checkBoxEncounterRandom = new System.Windows.Forms.CheckBox();
            this.label218 = new System.Windows.Forms.Label();
            this.textBoxEncounterName = new System.Windows.Forms.TextBox();
            this.label217 = new System.Windows.Forms.Label();
            this.textBoxEncounterEventDisabled = new System.Windows.Forms.TextBox();
            this.label216 = new System.Windows.Forms.Label();
            this.textBoxEncounterEventEnabled = new System.Windows.Forms.TextBox();
            this.label215 = new System.Windows.Forms.Label();
            this.textBoxEncounterEnemy3 = new System.Windows.Forms.TextBox();
            this.label214 = new System.Windows.Forms.Label();
            this.textBoxEncounterEnemy2 = new System.Windows.Forms.TextBox();
            this.label213 = new System.Windows.Forms.Label();
            this.textBoxEncounterEnemy1 = new System.Windows.Forms.TextBox();
            this.buttonEncounterMoveDown = new System.Windows.Forms.Button();
            this.buttonEncounterAdd = new System.Windows.Forms.Button();
            this.buttonEncounterMoveUp = new System.Windows.Forms.Button();
            this.buttonEncounterRemove = new System.Windows.Forms.Button();
            this.label91 = new System.Windows.Forms.Label();
            this.textBoxEnemyExp = new System.Windows.Forms.TextBox();
            this.label90 = new System.Windows.Forms.Label();
            this.textBoxEnemyMaxMoney = new System.Windows.Forms.TextBox();
            this.label89 = new System.Windows.Forms.Label();
            this.textBoxEnemyMinMoney = new System.Windows.Forms.TextBox();
            this.label88 = new System.Windows.Forms.Label();
            this.textBoxEnemyBribe = new System.Windows.Forms.TextBox();
            this.label87 = new System.Windows.Forms.Label();
            this.textBoxEnemyDesc = new System.Windows.Forms.TextBox();
            this.label86 = new System.Windows.Forms.Label();
            this.textBoxEnemyFlavor2 = new System.Windows.Forms.TextBox();
            this.label85 = new System.Windows.Forms.Label();
            this.textBoxEnemyFlavor1 = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.panelEnemyStat = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label83 = new System.Windows.Forms.Label();
            this.textBoxEnemySpriteShadowY = new System.Windows.Forms.TextBox();
            this.label81 = new System.Windows.Forms.Label();
            this.textBoxEnemySpriteZoomY = new System.Windows.Forms.TextBox();
            this.label82 = new System.Windows.Forms.Label();
            this.textBoxEnemySpriteZoomX = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.textBoxEnemySpriteScale = new System.Windows.Forms.TextBox();
            this.textBoxEnemySpriteIdleFront = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.textBoxEnemySpriteIdleBack = new System.Windows.Forms.TextBox();
            this.textBoxEnemySpriteSpeed = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.textBoxEnemySpriteHitFront = new System.Windows.Forms.TextBox();
            this.textBoxEnemySpriteCast1Back = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.textBoxEnemySpriteHitBack = new System.Windows.Forms.TextBox();
            this.textBoxEnemySpriteCast1Front = new System.Windows.Forms.TextBox();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.textBoxEnemySpriteJump0Front = new System.Windows.Forms.TextBox();
            this.textBoxEnemySpriteCast0Back = new System.Windows.Forms.TextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.textBoxEnemySpriteJump0Back = new System.Windows.Forms.TextBox();
            this.textBoxEnemySpriteCast0Front = new System.Windows.Forms.TextBox();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.textBoxEnemySpriteJump1Front = new System.Windows.Forms.TextBox();
            this.textBoxEnemySpriteJump1Back = new System.Windows.Forms.TextBox();
            this.label62 = new System.Windows.Forms.Label();
            this.labelEnemySpeGuard = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.trackBarEnemySpeGuard = new System.Windows.Forms.TrackBar();
            this.labelEnemySpePower = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.trackBarEnemySpePower = new System.Windows.Forms.TrackBar();
            this.labelEnemyMagGuard = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.trackBarEnemyMagGuard = new System.Windows.Forms.TrackBar();
            this.labelEnemyMagPower = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.trackBarEnemyMagPower = new System.Windows.Forms.TrackBar();
            this.labelEnemyPhyGuard = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.trackBarEnemyPhyGuard = new System.Windows.Forms.TrackBar();
            this.labelEnemyPhyPower = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.trackBarEnemyPhyPower = new System.Windows.Forms.TrackBar();
            this.labelEnemyMP = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.trackBarEnemyMP = new System.Windows.Forms.TrackBar();
            this.labelEnemyHP = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.trackBarEnemyHP = new System.Windows.Forms.TrackBar();
            this.label79 = new System.Windows.Forms.Label();
            this.textBoxEnemyVisualDesc = new System.Windows.Forms.TextBox();
            this.label80 = new System.Windows.Forms.Label();
            this.textBoxEnemyFlavor0 = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.comboBoxEnemyType = new System.Windows.Forms.ComboBox();
            this.labelEnemyBlue = new System.Windows.Forms.Label();
            this.labelEnemyGreen = new System.Windows.Forms.Label();
            this.labelEnemyRed = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.textBoxEnemyColor = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.textBoxEnemyAbility = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxEnemyName = new System.Windows.Forms.TextBox();
            this.buttonMonsterMoveDown = new System.Windows.Forms.Button();
            this.buttonMonsterMoveUp = new System.Windows.Forms.Button();
            this.buttonMonsterRemove = new System.Windows.Forms.Button();
            this.buttonMonsterAdd = new System.Windows.Forms.Button();
            this.listBoxMonsters = new System.Windows.Forms.ListBox();
            this.tabItems = new System.Windows.Forms.TabPage();
            this.label193 = new System.Windows.Forms.Label();
            this.textBoxItemHelp = new System.Windows.Forms.TextBox();
            this.label167 = new System.Windows.Forms.Label();
            this.label166 = new System.Windows.Forms.Label();
            this.label145 = new System.Windows.Forms.Label();
            this.comboBoxTabletMove = new System.Windows.Forms.ComboBox();
            this.labelTabletSpeGuard = new System.Windows.Forms.Label();
            this.label134 = new System.Windows.Forms.Label();
            this.trackBarTabletSpeGuard = new System.Windows.Forms.TrackBar();
            this.labelTabletSpePower = new System.Windows.Forms.Label();
            this.label136 = new System.Windows.Forms.Label();
            this.trackBarTabletSpePower = new System.Windows.Forms.TrackBar();
            this.labelTabletMagGuard = new System.Windows.Forms.Label();
            this.label138 = new System.Windows.Forms.Label();
            this.trackBarTabletMagGuard = new System.Windows.Forms.TrackBar();
            this.labelTabletMagPower = new System.Windows.Forms.Label();
            this.label140 = new System.Windows.Forms.Label();
            this.trackBarTabletMagPower = new System.Windows.Forms.TrackBar();
            this.labelTabletPhyGuard = new System.Windows.Forms.Label();
            this.label142 = new System.Windows.Forms.Label();
            this.trackBarTabletPhyGuard = new System.Windows.Forms.TrackBar();
            this.labelTabletPhyPower = new System.Windows.Forms.Label();
            this.label144 = new System.Windows.Forms.Label();
            this.trackBarTabletPhyPower = new System.Windows.Forms.TrackBar();
            this.label130 = new System.Windows.Forms.Label();
            this.textBoxTabletChar = new System.Windows.Forms.TextBox();
            this.label131 = new System.Windows.Forms.Label();
            this.textBoxTabletDesc = new System.Windows.Forms.TextBox();
            this.label132 = new System.Windows.Forms.Label();
            this.textBoxTabletName = new System.Windows.Forms.TextBox();
            this.buttonTabletMoveDown = new System.Windows.Forms.Button();
            this.buttonTabletMoveUp = new System.Windows.Forms.Button();
            this.buttonTabletRemove = new System.Windows.Forms.Button();
            this.buttonTabletAdd = new System.Windows.Forms.Button();
            this.listBoxTablet = new System.Windows.Forms.ListBox();
            this.labelItemSpeGuard = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.trackBarItemSpeGuard = new System.Windows.Forms.TrackBar();
            this.labelItemSpePower = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.trackBarItemSpePower = new System.Windows.Forms.TrackBar();
            this.labelItemMagGuard = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.trackBarItemMagGuard = new System.Windows.Forms.TrackBar();
            this.labelItemMagPower = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.trackBarItemMagPower = new System.Windows.Forms.TrackBar();
            this.labelItemPhyGuard = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.trackBarItemPhyGuard = new System.Windows.Forms.TrackBar();
            this.labelItemPhyPower = new System.Windows.Forms.Label();
            this.label110 = new System.Windows.Forms.Label();
            this.trackBarItemPhyPower = new System.Windows.Forms.TrackBar();
            this.label98 = new System.Windows.Forms.Label();
            this.textBoxItemPrice = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.checkBoxItemSingleUse = new System.Windows.Forms.CheckBox();
            this.checkBoxItemCanSell = new System.Windows.Forms.CheckBox();
            this.checkBoxItemCanGive = new System.Windows.Forms.CheckBox();
            this.checkBoxItemCanToss = new System.Windows.Forms.CheckBox();
            this.label97 = new System.Windows.Forms.Label();
            this.textBoxItemSprite = new System.Windows.Forms.TextBox();
            this.label96 = new System.Windows.Forms.Label();
            this.textBoxItemBookName = new System.Windows.Forms.TextBox();
            this.label94 = new System.Windows.Forms.Label();
            this.comboBoxBattleMove = new System.Windows.Forms.ComboBox();
            this.label95 = new System.Windows.Forms.Label();
            this.textBoxItemOverworldScript = new System.Windows.Forms.TextBox();
            this.label93 = new System.Windows.Forms.Label();
            this.comboBoxItemType = new System.Windows.Forms.ComboBox();
            this.label92 = new System.Windows.Forms.Label();
            this.textBoxItemDesc = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxItemName = new System.Windows.Forms.TextBox();
            this.buttonItemMoveDown = new System.Windows.Forms.Button();
            this.buttonItemMoveUp = new System.Windows.Forms.Button();
            this.buttonItemRemove = new System.Windows.Forms.Button();
            this.buttonItemAdd = new System.Windows.Forms.Button();
            this.listBoxItems = new System.Windows.Forms.ListBox();
            this.tabMove = new System.Windows.Forms.TabPage();
            this.groupBox28 = new System.Windows.Forms.GroupBox();
            this.groupBox29 = new System.Windows.Forms.GroupBox();
            this.label99 = new System.Windows.Forms.Label();
            this.comboBoxMoveType = new System.Windows.Forms.ComboBox();
            this.comboBoxMovePhysMag = new System.Windows.Forms.ComboBox();
            this.label103 = new System.Windows.Forms.Label();
            this.textBoxMoveDamage = new System.Windows.Forms.TextBox();
            this.textBoxMoveMPCost = new System.Windows.Forms.TextBox();
            this.label153 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.textBoxMoveCharacter = new System.Windows.Forms.TextBox();
            this.label107 = new System.Windows.Forms.Label();
            this.textBoxMoveTargets = new System.Windows.Forms.TextBox();
            this.label123 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.textBoxMoveStatusPercent = new System.Windows.Forms.TextBox();
            this.textBoxMovePriority = new System.Windows.Forms.TextBox();
            this.label113 = new System.Windows.Forms.Label();
            this.textBoxMoveSpeedMultiplier = new System.Windows.Forms.TextBox();
            this.label112 = new System.Windows.Forms.Label();
            this.label115 = new System.Windows.Forms.Label();
            this.comboBoxMoveItem = new System.Windows.Forms.ComboBox();
            this.textBoxMoveEffectPercent = new System.Windows.Forms.TextBox();
            this.label114 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxMoveName = new System.Windows.Forms.TextBox();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.label227 = new System.Windows.Forms.Label();
            this.textBoxMoveAnimationLungePos = new System.Windows.Forms.TextBox();
            this.label199 = new System.Windows.Forms.Label();
            this.textBoxMoveAnimationJumpHeight = new System.Windows.Forms.TextBox();
            this.comboBoxMoveAnimationTarget = new System.Windows.Forms.ComboBox();
            this.label198 = new System.Windows.Forms.Label();
            this.label197 = new System.Windows.Forms.Label();
            this.label196 = new System.Windows.Forms.Label();
            this.label195 = new System.Windows.Forms.Label();
            this.label194 = new System.Windows.Forms.Label();
            this.label120 = new System.Windows.Forms.Label();
            this.textBoxMoveAnimationFrame = new System.Windows.Forms.TextBox();
            this.textBoxMoveAnimationSound = new System.Windows.Forms.TextBox();
            this.textBoxMoveAnimationSprite = new System.Windows.Forms.TextBox();
            this.textBoxMoveAnimationLunge = new System.Windows.Forms.TextBox();
            this.textBoxMoveAnimationJump = new System.Windows.Forms.TextBox();
            this.buttonMoveAnimationAdd = new System.Windows.Forms.Button();
            this.buttonMoveAnimationRemove = new System.Windows.Forms.Button();
            this.listBoxMoveAnimation = new System.Windows.Forms.ListBox();
            this.buttonMoveAnimationUp = new System.Windows.Forms.Button();
            this.buttonMoveAnimationDown = new System.Windows.Forms.Button();
            this.listBoxMoves = new System.Windows.Forms.ListBox();
            this.buttonMovesMoveDown = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label122 = new System.Windows.Forms.Label();
            this.comboBoxMoveBattlerVisibility = new System.Windows.Forms.ComboBox();
            this.label121 = new System.Windows.Forms.Label();
            this.textBoxMoveDuration = new System.Windows.Forms.TextBox();
            this.label118 = new System.Windows.Forms.Label();
            this.textBoxMoveUserSprite = new System.Windows.Forms.TextBox();
            this.label119 = new System.Windows.Forms.Label();
            this.textBoxMoveTargetSprite = new System.Windows.Forms.TextBox();
            this.label117 = new System.Windows.Forms.Label();
            this.textBoxMoveBackgroundSprite = new System.Windows.Forms.TextBox();
            this.label116 = new System.Windows.Forms.Label();
            this.textBoxMoveForegroundSprite = new System.Windows.Forms.TextBox();
            this.buttonMovesMoveUp = new System.Windows.Forms.Button();
            this.textBoxMoveDesc = new System.Windows.Forms.TextBox();
            this.buttonMovesRemove = new System.Windows.Forms.Button();
            this.label101 = new System.Windows.Forms.Label();
            this.buttonMovesAdd = new System.Windows.Forms.Button();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.buttonMoveTextUpdate = new System.Windows.Forms.Button();
            this.buttonMoveTextRemove = new System.Windows.Forms.Button();
            this.buttonMoveTextAdd = new System.Windows.Forms.Button();
            this.listBoxMoveTextList = new System.Windows.Forms.ListBox();
            this.textBoxMoveTextLine = new System.Windows.Forms.TextBox();
            this.textBoxMoveScript = new System.Windows.Forms.TextBox();
            this.label105 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.comboBoxMoveStatus = new System.Windows.Forms.ComboBox();
            this.buttonMoveStatusRemove = new System.Windows.Forms.Button();
            this.buttonMoveStatusAdd = new System.Windows.Forms.Button();
            this.listBoxMoveStatusList = new System.Windows.Forms.ListBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.checkBoxMoveKnockback = new System.Windows.Forms.CheckBox();
            this.checkBoxMoveRotateCamera = new System.Windows.Forms.CheckBox();
            this.checkBoxMoveTargetSelf = new System.Windows.Forms.CheckBox();
            this.checkBoxMoveTargetAlly = new System.Windows.Forms.CheckBox();
            this.checkBoxMoveTargetSkip = new System.Windows.Forms.CheckBox();
            this.groupBox27 = new System.Windows.Forms.GroupBox();
            this.listBoxMonsterMoves = new System.Windows.Forms.ListBox();
            this.listBoxMonsterMovesList = new System.Windows.Forms.ListBox();
            this.buttonMonsterMovesAdd = new System.Windows.Forms.Button();
            this.buttonMonsterMovesRemove = new System.Windows.Forms.Button();
            this.label156 = new System.Windows.Forms.Label();
            this.comboBoxMonsterMovesMove = new System.Windows.Forms.ComboBox();
            this.label157 = new System.Windows.Forms.Label();
            this.textBoxMonsterMovesLevel = new System.Windows.Forms.TextBox();
            this.groupBox26 = new System.Windows.Forms.GroupBox();
            this.listBoxCharacterMoves = new System.Windows.Forms.ListBox();
            this.listBoxCharacterMovesList = new System.Windows.Forms.ListBox();
            this.buttonCharacterMovesAdd = new System.Windows.Forms.Button();
            this.buttonCharacterMovesRemove = new System.Windows.Forms.Button();
            this.comboBoxCharacterMovesMove = new System.Windows.Forms.ComboBox();
            this.textBoxCharacterMovesLevel = new System.Windows.Forms.TextBox();
            this.label154 = new System.Windows.Forms.Label();
            this.label155 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.label170 = new System.Windows.Forms.Label();
            this.textBoxWordDesc = new System.Windows.Forms.TextBox();
            this.label169 = new System.Windows.Forms.Label();
            this.textBoxWordName = new System.Windows.Forms.TextBox();
            this.comboBoxWordMove = new System.Windows.Forms.ComboBox();
            this.listBoxWords = new System.Windows.Forms.ListBox();
            this.buttonWordAdd = new System.Windows.Forms.Button();
            this.buttonWordRemove = new System.Windows.Forms.Button();
            this.buttonWordUp = new System.Windows.Forms.Button();
            this.buttonWordDown = new System.Windows.Forms.Button();
            this.label172 = new System.Windows.Forms.Label();
            this.tabBook = new System.Windows.Forms.TabPage();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.textBoxDiagramBlend = new System.Windows.Forms.TextBox();
            this.label192 = new System.Windows.Forms.Label();
            this.label190 = new System.Windows.Forms.Label();
            this.textBoxDiagramYOffset = new System.Windows.Forms.TextBox();
            this.label191 = new System.Windows.Forms.Label();
            this.textBoxDiagramXOffset = new System.Windows.Forms.TextBox();
            this.label189 = new System.Windows.Forms.Label();
            this.textBoxDiagramName = new System.Windows.Forms.TextBox();
            this.label188 = new System.Windows.Forms.Label();
            this.textBoxDiagramSprite = new System.Windows.Forms.TextBox();
            this.label185 = new System.Windows.Forms.Label();
            this.textBoxDiagramAltText = new System.Windows.Forms.TextBox();
            this.label186 = new System.Windows.Forms.Label();
            this.textBoxDiagramYScale = new System.Windows.Forms.TextBox();
            this.label187 = new System.Windows.Forms.Label();
            this.buttonDiagramMoveDown = new System.Windows.Forms.Button();
            this.buttonDiagramMoveUp = new System.Windows.Forms.Button();
            this.textBoxDiagramXScale = new System.Windows.Forms.TextBox();
            this.listBoxDiagram = new System.Windows.Forms.ListBox();
            this.buttonDiagramRemove = new System.Windows.Forms.Button();
            this.buttonDiagramAdd = new System.Windows.Forms.Button();
            this.buttonBookSave = new System.Windows.Forms.Button();
            this.buttonBookLoad = new System.Windows.Forms.Button();
            this.richTextBoxBook = new System.Windows.Forms.RichTextBox();
            this.label152 = new System.Windows.Forms.Label();
            this.textBoxBookFilename = new System.Windows.Forms.TextBox();
            this.label151 = new System.Windows.Forms.Label();
            this.textBoxBookAuthor = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxBookName = new System.Windows.Forms.TextBox();
            this.buttonBookMoveDown = new System.Windows.Forms.Button();
            this.buttonBookMoveUp = new System.Windows.Forms.Button();
            this.buttonBookRemove = new System.Windows.Forms.Button();
            this.buttonBookAdd = new System.Windows.Forms.Button();
            this.listBoxBooks = new System.Windows.Forms.ListBox();
            this.tabBird = new System.Windows.Forms.TabPage();
            this.groupBox30 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.listBoxBird = new System.Windows.Forms.ListBox();
            this.buttonBirdMoveDown = new System.Windows.Forms.Button();
            this.label146 = new System.Windows.Forms.Label();
            this.buttonBirdMoveUp = new System.Windows.Forms.Button();
            this.label150 = new System.Windows.Forms.Label();
            this.buttonBirdRemove = new System.Windows.Forms.Button();
            this.textBoxBirdFemaleSprite = new System.Windows.Forms.TextBox();
            this.buttonBirdAdd = new System.Windows.Forms.Button();
            this.textBoxBirdName = new System.Windows.Forms.TextBox();
            this.label147 = new System.Windows.Forms.Label();
            this.textBoxBirdSpawnRatio = new System.Windows.Forms.TextBox();
            this.textBoxBirdMaleSprite = new System.Windows.Forms.TextBox();
            this.textBoxBirdDesc = new System.Windows.Forms.TextBox();
            this.label141 = new System.Windows.Forms.Label();
            this.label149 = new System.Windows.Forms.Label();
            this.textBoxBirdMetricWeight = new System.Windows.Forms.TextBox();
            this.label133 = new System.Windows.Forms.Label();
            this.label143 = new System.Windows.Forms.Label();
            this.textBoxBirdOffset = new System.Windows.Forms.TextBox();
            this.textBoxBirdMetricHeight = new System.Windows.Forms.TextBox();
            this.textBoxBirdLongName = new System.Windows.Forms.TextBox();
            this.label139 = new System.Windows.Forms.Label();
            this.label148 = new System.Windows.Forms.Label();
            this.textBoxBirdWeight = new System.Windows.Forms.TextBox();
            this.textBoxBirdScale = new System.Windows.Forms.TextBox();
            this.label137 = new System.Windows.Forms.Label();
            this.textBoxBirdHeight = new System.Windows.Forms.TextBox();
            this.label135 = new System.Windows.Forms.Label();
            this.tabExp = new System.Windows.Forms.TabPage();
            this.panelGrowthDisplay = new System.Windows.Forms.Panel();
            this.label40 = new System.Windows.Forms.Label();
            this.textBoxGrowthName = new System.Windows.Forms.TextBox();
            this.buttonGrowthMoveDown = new System.Windows.Forms.Button();
            this.buttonGrowthMoveUp = new System.Windows.Forms.Button();
            this.buttonGrowthRemove = new System.Windows.Forms.Button();
            this.buttonGrowthAdd = new System.Windows.Forms.Button();
            this.listBoxGrowth = new System.Windows.Forms.ListBox();
            this.tabBattleScripts = new System.Windows.Forms.TabPage();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.listBoxStatus = new System.Windows.Forms.ListBox();
            this.label129 = new System.Windows.Forms.Label();
            this.buttonStatusAdd = new System.Windows.Forms.Button();
            this.textBoxStatusShader = new System.Windows.Forms.TextBox();
            this.buttonStatusRemove = new System.Windows.Forms.Button();
            this.label128 = new System.Windows.Forms.Label();
            this.buttonStatusMoveUp = new System.Windows.Forms.Button();
            this.textBoxStatusFinalText = new System.Windows.Forms.TextBox();
            this.buttonStatusMoveDown = new System.Windows.Forms.Button();
            this.label127 = new System.Windows.Forms.Label();
            this.textBoxStatusName = new System.Windows.Forms.TextBox();
            this.textBoxStatusSprite = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.checkBoxStatusHarmful = new System.Windows.Forms.CheckBox();
            this.textBoxStatusDesc = new System.Windows.Forms.TextBox();
            this.label125 = new System.Windows.Forms.Label();
            this.label124 = new System.Windows.Forms.Label();
            this.textBoxStatusMaxTurns = new System.Windows.Forms.TextBox();
            this.textBoxStatusMinTurns = new System.Windows.Forms.TextBox();
            this.label126 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.checkBoxEndPhasePause = new System.Windows.Forms.CheckBox();
            this.label168 = new System.Windows.Forms.Label();
            this.textBoxEndPhaseConditionScript = new System.Windows.Forms.TextBox();
            this.checkBoxEndPhaseTarget = new System.Windows.Forms.CheckBox();
            this.buttonEndPhaseMoveDown = new System.Windows.Forms.Button();
            this.label165 = new System.Windows.Forms.Label();
            this.buttonEndPhaseMoveUp = new System.Windows.Forms.Button();
            this.textBoxEndPhaseLength = new System.Windows.Forms.TextBox();
            this.buttonEndPhaseRemove = new System.Windows.Forms.Button();
            this.label164 = new System.Windows.Forms.Label();
            this.buttonEndPhaseAdd = new System.Windows.Forms.Button();
            this.textBoxEndPhaseName = new System.Windows.Forms.TextBox();
            this.listBoxEndPhase = new System.Windows.Forms.ListBox();
            this.label160 = new System.Windows.Forms.Label();
            this.label163 = new System.Windows.Forms.Label();
            this.textBoxEndPhaseScript = new System.Windows.Forms.TextBox();
            this.textBoxEndPhaseFgSprite = new System.Windows.Forms.TextBox();
            this.label161 = new System.Windows.Forms.Label();
            this.textBoxEndPhaseBgSprite = new System.Windows.Forms.TextBox();
            this.textBoxEndPhaseTargetSprite = new System.Windows.Forms.TextBox();
            this.label162 = new System.Windows.Forms.Label();
            this.tabAXHud = new System.Windows.Forms.TabPage();
            this.groupBox31 = new System.Windows.Forms.GroupBox();
            this.buttonItemEventDown = new System.Windows.Forms.Button();
            this.listBoxItemEvent = new System.Windows.Forms.ListBox();
            this.buttonItemEventUp = new System.Windows.Forms.Button();
            this.buttonItemEventAdd = new System.Windows.Forms.Button();
            this.buttonItemEventRemove = new System.Windows.Forms.Button();
            this.groupBox25 = new System.Windows.Forms.GroupBox();
            this.buttonEventDown = new System.Windows.Forms.Button();
            this.listBoxEvent = new System.Windows.Forms.ListBox();
            this.buttonEventUp = new System.Windows.Forms.Button();
            this.buttonEventAdd = new System.Windows.Forms.Button();
            this.buttonEventRemove = new System.Windows.Forms.Button();
            this.groupBox24 = new System.Windows.Forms.GroupBox();
            this.label231 = new System.Windows.Forms.Label();
            this.textBoxVoicemailDialogID = new System.Windows.Forms.TextBox();
            this.label232 = new System.Windows.Forms.Label();
            this.textBoxVoicemailDesc = new System.Windows.Forms.TextBox();
            this.label233 = new System.Windows.Forms.Label();
            this.textBoxVoicemailSender = new System.Windows.Forms.TextBox();
            this.label234 = new System.Windows.Forms.Label();
            this.buttonVoicemailMoveDown = new System.Windows.Forms.Button();
            this.textBoxVoicemailName = new System.Windows.Forms.TextBox();
            this.listBoxVoicemail = new System.Windows.Forms.ListBox();
            this.buttonVoicemailMoveUp = new System.Windows.Forms.Button();
            this.buttonVoicemailAdd = new System.Windows.Forms.Button();
            this.buttonVoicemailRemove = new System.Windows.Forms.Button();
            this.groupBox23 = new System.Windows.Forms.GroupBox();
            this.label226 = new System.Windows.Forms.Label();
            this.textBoxSMSTitle = new System.Windows.Forms.TextBox();
            this.label225 = new System.Windows.Forms.Label();
            this.textBoxSMSText = new System.Windows.Forms.TextBox();
            this.label229 = new System.Windows.Forms.Label();
            this.textBoxSMSSender = new System.Windows.Forms.TextBox();
            this.label230 = new System.Windows.Forms.Label();
            this.buttonSMSMoveDown = new System.Windows.Forms.Button();
            this.textBoxSMSName = new System.Windows.Forms.TextBox();
            this.listBoxSMS = new System.Windows.Forms.ListBox();
            this.buttonSMSMoveUp = new System.Windows.Forms.Button();
            this.buttonSMSAdd = new System.Windows.Forms.Button();
            this.buttonSMSRemove = new System.Windows.Forms.Button();
            this.groupBox22 = new System.Windows.Forms.GroupBox();
            this.comboBoxTaskCompleteID = new System.Windows.Forms.ComboBox();
            this.comboBoxTaskEventID = new System.Windows.Forms.ComboBox();
            this.label224 = new System.Windows.Forms.Label();
            this.label223 = new System.Windows.Forms.Label();
            this.textBoxTaskDesc = new System.Windows.Forms.TextBox();
            this.textBoxTaskSprite = new System.Windows.Forms.TextBox();
            this.label222 = new System.Windows.Forms.Label();
            this.label221 = new System.Windows.Forms.Label();
            this.label220 = new System.Windows.Forms.Label();
            this.textBoxTaskName = new System.Windows.Forms.TextBox();
            this.label219 = new System.Windows.Forms.Label();
            this.buttonTasksMoveDown = new System.Windows.Forms.Button();
            this.textBoxTaskID = new System.Windows.Forms.TextBox();
            this.listBoxTasks = new System.Windows.Forms.ListBox();
            this.buttonTasksMoveUp = new System.Windows.Forms.Button();
            this.buttonTasksAdd = new System.Windows.Forms.Button();
            this.buttonTasksRemove = new System.Windows.Forms.Button();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this.buttonTextEntryRemove = new System.Windows.Forms.Button();
            this.label203 = new System.Windows.Forms.Label();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.label202 = new System.Windows.Forms.Label();
            this.textBoxTextGraphicsAngle = new System.Windows.Forms.TextBox();
            this.label212 = new System.Windows.Forms.Label();
            this.textBoxTextGraphicsB = new System.Windows.Forms.TextBox();
            this.label210 = new System.Windows.Forms.Label();
            this.textBoxTextGraphicsG = new System.Windows.Forms.TextBox();
            this.label211 = new System.Windows.Forms.Label();
            this.textBoxTextGraphicsR = new System.Windows.Forms.TextBox();
            this.label209 = new System.Windows.Forms.Label();
            this.textBoxTextGraphicsAlpha = new System.Windows.Forms.TextBox();
            this.label208 = new System.Windows.Forms.Label();
            this.textBoxTextGraphicsSprite = new System.Windows.Forms.TextBox();
            this.label206 = new System.Windows.Forms.Label();
            this.textBoxTextGraphicsYScale = new System.Windows.Forms.TextBox();
            this.label207 = new System.Windows.Forms.Label();
            this.textBoxTextGraphicsXScale = new System.Windows.Forms.TextBox();
            this.label205 = new System.Windows.Forms.Label();
            this.textBoxTextGraphicsY = new System.Windows.Forms.TextBox();
            this.label204 = new System.Windows.Forms.Label();
            this.textBoxTextGraphicsX = new System.Windows.Forms.TextBox();
            this.buttonTextGraphicsRemove = new System.Windows.Forms.Button();
            this.listBoxTextGraphics = new System.Windows.Forms.ListBox();
            this.buttonTextGraphicsAdd = new System.Windows.Forms.Button();
            this.listBoxTextEntry = new System.Windows.Forms.ListBox();
            this.buttonTextEntryAdd = new System.Windows.Forms.Button();
            this.textBoxTextEntryText = new System.Windows.Forms.TextBox();
            this.label200 = new System.Windows.Forms.Label();
            this.textBoxTextboxEventID = new System.Windows.Forms.TextBox();
            this.label201 = new System.Windows.Forms.Label();
            this.buttonTextboxMoveDown = new System.Windows.Forms.Button();
            this.buttonTextboxMoveUp = new System.Windows.Forms.Button();
            this.textBoxTextboxName = new System.Windows.Forms.TextBox();
            this.listBoxTextbox = new System.Windows.Forms.ListBox();
            this.buttonTextboxRemove = new System.Windows.Forms.Button();
            this.buttonTextboxAdd = new System.Windows.Forms.Button();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.label180 = new System.Windows.Forms.Label();
            this.textBoxHUDChildCustomHUDText = new System.Windows.Forms.TextBox();
            this.label179 = new System.Windows.Forms.Label();
            this.textBoxHUDChildVar = new System.Windows.Forms.TextBox();
            this.label182 = new System.Windows.Forms.Label();
            this.buttonHUDChildMoveDown = new System.Windows.Forms.Button();
            this.buttonHUDChildMoveUp = new System.Windows.Forms.Button();
            this.textBoxHUDChildName = new System.Windows.Forms.TextBox();
            this.listBoxHUDChild = new System.Windows.Forms.ListBox();
            this.buttonHUDChildRemove = new System.Windows.Forms.Button();
            this.label181 = new System.Windows.Forms.Label();
            this.comboBoxHUDChildTemplate = new System.Windows.Forms.ComboBox();
            this.buttonHUDChildAdd = new System.Windows.Forms.Button();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.label184 = new System.Windows.Forms.Label();
            this.textBoxHUDDefReq = new System.Windows.Forms.TextBox();
            this.textBoxHUDDefArg = new System.Windows.Forms.TextBox();
            this.label178 = new System.Windows.Forms.Label();
            this.label177 = new System.Windows.Forms.Label();
            this.comboBoxHUDDefFunc = new System.Windows.Forms.ComboBox();
            this.label176 = new System.Windows.Forms.Label();
            this.comboBoxHUDDefInput = new System.Windows.Forms.ComboBox();
            this.buttonHUDDefMoveDown = new System.Windows.Forms.Button();
            this.buttonHUDDefMoveUp = new System.Windows.Forms.Button();
            this.listBoxHUDDef = new System.Windows.Forms.ListBox();
            this.buttonHUDDefRemove = new System.Windows.Forms.Button();
            this.buttonHUDDefAdd = new System.Windows.Forms.Button();
            this.textBoxHUDText = new System.Windows.Forms.TextBox();
            this.label175 = new System.Windows.Forms.Label();
            this.label174 = new System.Windows.Forms.Label();
            this.textBoxHUDDefaultName = new System.Windows.Forms.TextBox();
            this.buttonHUDMoveDown = new System.Windows.Forms.Button();
            this.label173 = new System.Windows.Forms.Label();
            this.buttonHUDMoveUp = new System.Windows.Forms.Button();
            this.textBoxHUDID = new System.Windows.Forms.TextBox();
            this.buttonHUDRemove = new System.Windows.Forms.Button();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.label183 = new System.Windows.Forms.Label();
            this.textBoxHUDInfokeysReq = new System.Windows.Forms.TextBox();
            this.buttonHUDInfokeyMoveDown = new System.Windows.Forms.Button();
            this.textBoxHUDInfokeysText = new System.Windows.Forms.TextBox();
            this.buttonHUDInfokeyMoveUp = new System.Windows.Forms.Button();
            this.listBoxHUDInfokeys = new System.Windows.Forms.ListBox();
            this.buttonHUDInfokeyRemove = new System.Windows.Forms.Button();
            this.label171 = new System.Windows.Forms.Label();
            this.buttonHUDInfokeyAdd = new System.Windows.Forms.Button();
            this.buttonHUDAdd = new System.Windows.Forms.Button();
            this.listBoxHUD = new System.Windows.Forms.ListBox();
            this.buttonExportAll = new System.Windows.Forms.Button();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.saveFileExportThis = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialogImport = new System.Windows.Forms.OpenFileDialog();
            this.openFileDialogBook = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialogBook = new System.Windows.Forms.SaveFileDialog();
            this.buttonImportAll = new System.Windows.Forms.Button();
            this.folderSelector = new System.Windows.Forms.FolderBrowserDialog();
            this.checkBoxDialogEndNode = new System.Windows.Forms.CheckBox();
            this.Tabs.SuspendLayout();
            this.tabCharacters.SuspendLayout();
            this.groupBox36.SuspendLayout();
            this.groupBox38.SuspendLayout();
            this.groupBox37.SuspendLayout();
            this.groupBox35.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarCharacterLevel)).BeginInit();
            this.groupBox34.SuspendLayout();
            this.groupBox33.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarCharacterHP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarCharacterMP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarCharacterPhyPower)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarCharacterPhyGuard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarCharacterMagPower)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarCharacterSpeGuard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarCharacterMagGuard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarCharacterSpePower)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabMonsters.SuspendLayout();
            this.groupBox32.SuspendLayout();
            this.groupBox21.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarEnemySpeGuard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarEnemySpePower)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarEnemyMagGuard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarEnemyMagPower)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarEnemyPhyGuard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarEnemyPhyPower)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarEnemyMP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarEnemyHP)).BeginInit();
            this.tabItems.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarTabletSpeGuard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarTabletSpePower)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarTabletMagGuard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarTabletMagPower)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarTabletPhyGuard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarTabletPhyPower)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarItemSpeGuard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarItemSpePower)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarItemMagGuard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarItemMagPower)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarItemPhyGuard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarItemPhyPower)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.tabMove.SuspendLayout();
            this.groupBox28.SuspendLayout();
            this.groupBox29.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox27.SuspendLayout();
            this.groupBox26.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.tabBook.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.tabBird.SuspendLayout();
            this.groupBox30.SuspendLayout();
            this.tabExp.SuspendLayout();
            this.tabBattleScripts.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.tabAXHud.SuspendLayout();
            this.groupBox31.SuspendLayout();
            this.groupBox25.SuspendLayout();
            this.groupBox24.SuspendLayout();
            this.groupBox23.SuspendLayout();
            this.groupBox22.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.groupBox20.SuspendLayout();
            this.groupBox19.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.SuspendLayout();
            // 
            // Tabs
            // 
            this.Tabs.Controls.Add(this.tabCharacters);
            this.Tabs.Controls.Add(this.tabMonsters);
            this.Tabs.Controls.Add(this.tabItems);
            this.Tabs.Controls.Add(this.tabMove);
            this.Tabs.Controls.Add(this.tabBook);
            this.Tabs.Controls.Add(this.tabBird);
            this.Tabs.Controls.Add(this.tabExp);
            this.Tabs.Controls.Add(this.tabBattleScripts);
            this.Tabs.Controls.Add(this.tabAXHud);
            this.Tabs.Location = new System.Drawing.Point(12, 12);
            this.Tabs.Name = "Tabs";
            this.Tabs.SelectedIndex = 0;
            this.Tabs.Size = new System.Drawing.Size(1325, 650);
            this.Tabs.TabIndex = 0;
            this.Tabs.SelectedIndexChanged += new System.EventHandler(this.Tabs_SelectedIndexChanged);
            // 
            // tabCharacters
            // 
            this.tabCharacters.BackColor = System.Drawing.Color.Silver;
            this.tabCharacters.Controls.Add(this.groupBox36);
            this.tabCharacters.Controls.Add(this.groupBox35);
            this.tabCharacters.Controls.Add(this.groupBox34);
            this.tabCharacters.Controls.Add(this.groupBox33);
            this.tabCharacters.Controls.Add(this.groupBox3);
            this.tabCharacters.Controls.Add(this.groupBox1);
            this.tabCharacters.Controls.Add(this.buttonCharacterMoveDown);
            this.tabCharacters.Controls.Add(this.buttonCharacterMoveUp);
            this.tabCharacters.Controls.Add(this.buttonCharacterRemove);
            this.tabCharacters.Controls.Add(this.buttonCharacterAdd);
            this.tabCharacters.Controls.Add(this.listBoxCharacters);
            this.tabCharacters.Location = new System.Drawing.Point(4, 22);
            this.tabCharacters.Name = "tabCharacters";
            this.tabCharacters.Padding = new System.Windows.Forms.Padding(3);
            this.tabCharacters.Size = new System.Drawing.Size(1317, 624);
            this.tabCharacters.TabIndex = 0;
            this.tabCharacters.Text = "Characters";
            // 
            // groupBox36
            // 
            this.groupBox36.BackColor = System.Drawing.Color.Gainsboro;
            this.groupBox36.Controls.Add(this.checkBoxDialogEndNode);
            this.groupBox36.Controls.Add(this.groupBox38);
            this.groupBox36.Controls.Add(this.groupBox37);
            this.groupBox36.Controls.Add(this.textBoxCharacterDialogName);
            this.groupBox36.Controls.Add(this.buttonCharacterDialogDown);
            this.groupBox36.Controls.Add(this.label37);
            this.groupBox36.Controls.Add(this.listBoxCharacterDialog);
            this.groupBox36.Controls.Add(this.buttonCharacterDialogUp);
            this.groupBox36.Controls.Add(this.buttonCharacterDialogAdd);
            this.groupBox36.Controls.Add(this.buttonCharacterDialogRemove);
            this.groupBox36.Location = new System.Drawing.Point(132, 273);
            this.groupBox36.Name = "groupBox36";
            this.groupBox36.Size = new System.Drawing.Size(303, 335);
            this.groupBox36.TabIndex = 92;
            this.groupBox36.TabStop = false;
            this.groupBox36.Text = "Dialog";
            // 
            // groupBox38
            // 
            this.groupBox38.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox38.Controls.Add(this.buttonCharacterOptionDown);
            this.groupBox38.Controls.Add(this.listBoxCharacterDialogOptions);
            this.groupBox38.Controls.Add(this.buttonCharacterOptionUp);
            this.groupBox38.Controls.Add(this.buttonCharacterOptionAdd);
            this.groupBox38.Controls.Add(this.buttonCharacterOptionRemove);
            this.groupBox38.Location = new System.Drawing.Point(126, 187);
            this.groupBox38.Name = "groupBox38";
            this.groupBox38.Size = new System.Drawing.Size(147, 134);
            this.groupBox38.TabIndex = 177;
            this.groupBox38.TabStop = false;
            this.groupBox38.Text = "Options";
            // 
            // buttonCharacterOptionDown
            // 
            this.buttonCharacterOptionDown.Location = new System.Drawing.Point(68, 99);
            this.buttonCharacterOptionDown.Name = "buttonCharacterOptionDown";
            this.buttonCharacterOptionDown.Size = new System.Drawing.Size(58, 24);
            this.buttonCharacterOptionDown.TabIndex = 164;
            this.buttonCharacterOptionDown.Text = "Down";
            this.buttonCharacterOptionDown.UseVisualStyleBackColor = true;
            this.buttonCharacterOptionDown.Click += new System.EventHandler(this.buttonCharacterDialogOptionMoveDown_Click);
            // 
            // listBoxCharacterDialogOptions
            // 
            this.listBoxCharacterDialogOptions.AccessibleRole = System.Windows.Forms.AccessibleRole.List;
            this.listBoxCharacterDialogOptions.FormattingEnabled = true;
            this.listBoxCharacterDialogOptions.Location = new System.Drawing.Point(9, 19);
            this.listBoxCharacterDialogOptions.Name = "listBoxCharacterDialogOptions";
            this.listBoxCharacterDialogOptions.Size = new System.Drawing.Size(120, 43);
            this.listBoxCharacterDialogOptions.TabIndex = 160;
            this.listBoxCharacterDialogOptions.DoubleClick += new System.EventHandler(this.listBoxCharacterDialogOptions_DoubleClick);
            // 
            // buttonCharacterOptionUp
            // 
            this.buttonCharacterOptionUp.Location = new System.Drawing.Point(68, 69);
            this.buttonCharacterOptionUp.Name = "buttonCharacterOptionUp";
            this.buttonCharacterOptionUp.Size = new System.Drawing.Size(58, 24);
            this.buttonCharacterOptionUp.TabIndex = 163;
            this.buttonCharacterOptionUp.Text = "Up";
            this.buttonCharacterOptionUp.UseVisualStyleBackColor = true;
            this.buttonCharacterOptionUp.Click += new System.EventHandler(this.buttonCharacterDialogOptionMoveUp_Click);
            // 
            // buttonCharacterOptionAdd
            // 
            this.buttonCharacterOptionAdd.Location = new System.Drawing.Point(6, 68);
            this.buttonCharacterOptionAdd.Name = "buttonCharacterOptionAdd";
            this.buttonCharacterOptionAdd.Size = new System.Drawing.Size(58, 25);
            this.buttonCharacterOptionAdd.TabIndex = 161;
            this.buttonCharacterOptionAdd.Text = "Add";
            this.buttonCharacterOptionAdd.UseVisualStyleBackColor = true;
            this.buttonCharacterOptionAdd.Click += new System.EventHandler(this.buttonCharacterDialogOptionAdd_Click);
            // 
            // buttonCharacterOptionRemove
            // 
            this.buttonCharacterOptionRemove.Location = new System.Drawing.Point(6, 99);
            this.buttonCharacterOptionRemove.Name = "buttonCharacterOptionRemove";
            this.buttonCharacterOptionRemove.Size = new System.Drawing.Size(58, 25);
            this.buttonCharacterOptionRemove.TabIndex = 162;
            this.buttonCharacterOptionRemove.Text = "Remove";
            this.buttonCharacterOptionRemove.UseVisualStyleBackColor = true;
            this.buttonCharacterOptionRemove.Click += new System.EventHandler(this.buttonCharacterDialogOptionRemove_Click);
            // 
            // groupBox37
            // 
            this.groupBox37.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox37.Controls.Add(this.buttonCharacterTextDown);
            this.groupBox37.Controls.Add(this.listBoxCharacterDialogText);
            this.groupBox37.Controls.Add(this.buttonCharacterTextUp);
            this.groupBox37.Controls.Add(this.buttonCharacterTextAdd);
            this.groupBox37.Controls.Add(this.buttonCharacterTextRemove);
            this.groupBox37.Location = new System.Drawing.Point(126, 47);
            this.groupBox37.Name = "groupBox37";
            this.groupBox37.Size = new System.Drawing.Size(147, 134);
            this.groupBox37.TabIndex = 176;
            this.groupBox37.TabStop = false;
            this.groupBox37.Text = "Text";
            // 
            // buttonCharacterTextDown
            // 
            this.buttonCharacterTextDown.Location = new System.Drawing.Point(68, 99);
            this.buttonCharacterTextDown.Name = "buttonCharacterTextDown";
            this.buttonCharacterTextDown.Size = new System.Drawing.Size(58, 24);
            this.buttonCharacterTextDown.TabIndex = 164;
            this.buttonCharacterTextDown.Text = "Down";
            this.buttonCharacterTextDown.UseVisualStyleBackColor = true;
            this.buttonCharacterTextDown.Click += new System.EventHandler(this.buttonCharacterDialogTextMoveDown_Click);
            // 
            // listBoxCharacterDialogText
            // 
            this.listBoxCharacterDialogText.AccessibleRole = System.Windows.Forms.AccessibleRole.List;
            this.listBoxCharacterDialogText.FormattingEnabled = true;
            this.listBoxCharacterDialogText.Location = new System.Drawing.Point(9, 19);
            this.listBoxCharacterDialogText.Name = "listBoxCharacterDialogText";
            this.listBoxCharacterDialogText.Size = new System.Drawing.Size(120, 43);
            this.listBoxCharacterDialogText.TabIndex = 160;
            this.listBoxCharacterDialogText.DoubleClick += new System.EventHandler(this.listBoxCharacterDialogText_DoubleClick);
            // 
            // buttonCharacterTextUp
            // 
            this.buttonCharacterTextUp.Location = new System.Drawing.Point(68, 69);
            this.buttonCharacterTextUp.Name = "buttonCharacterTextUp";
            this.buttonCharacterTextUp.Size = new System.Drawing.Size(58, 24);
            this.buttonCharacterTextUp.TabIndex = 163;
            this.buttonCharacterTextUp.Text = "Up";
            this.buttonCharacterTextUp.UseVisualStyleBackColor = true;
            this.buttonCharacterTextUp.Click += new System.EventHandler(this.buttonCharacterDialogTextMoveUp_Click);
            // 
            // buttonCharacterTextAdd
            // 
            this.buttonCharacterTextAdd.Location = new System.Drawing.Point(6, 68);
            this.buttonCharacterTextAdd.Name = "buttonCharacterTextAdd";
            this.buttonCharacterTextAdd.Size = new System.Drawing.Size(58, 25);
            this.buttonCharacterTextAdd.TabIndex = 161;
            this.buttonCharacterTextAdd.Text = "Add";
            this.buttonCharacterTextAdd.UseVisualStyleBackColor = true;
            this.buttonCharacterTextAdd.Click += new System.EventHandler(this.buttonCharacterDialogTextAdd_Click);
            // 
            // buttonCharacterTextRemove
            // 
            this.buttonCharacterTextRemove.Location = new System.Drawing.Point(6, 99);
            this.buttonCharacterTextRemove.Name = "buttonCharacterTextRemove";
            this.buttonCharacterTextRemove.Size = new System.Drawing.Size(58, 25);
            this.buttonCharacterTextRemove.TabIndex = 162;
            this.buttonCharacterTextRemove.Text = "Remove";
            this.buttonCharacterTextRemove.UseVisualStyleBackColor = true;
            this.buttonCharacterTextRemove.Click += new System.EventHandler(this.buttonCharacterDialogTextRemove_Click);
            // 
            // textBoxCharacterDialogName
            // 
            this.textBoxCharacterDialogName.AccessibleDescription = "Character name";
            this.textBoxCharacterDialogName.AccessibleName = "Name";
            this.textBoxCharacterDialogName.Location = new System.Drawing.Point(126, 21);
            this.textBoxCharacterDialogName.Name = "textBoxCharacterDialogName";
            this.textBoxCharacterDialogName.Size = new System.Drawing.Size(84, 20);
            this.textBoxCharacterDialogName.TabIndex = 47;
            this.textBoxCharacterDialogName.Leave += new System.EventHandler(this.textBoxCharacterDialogName_Leave);
            // 
            // buttonCharacterDialogDown
            // 
            this.buttonCharacterDialogDown.Location = new System.Drawing.Point(66, 308);
            this.buttonCharacterDialogDown.Name = "buttonCharacterDialogDown";
            this.buttonCharacterDialogDown.Size = new System.Drawing.Size(58, 24);
            this.buttonCharacterDialogDown.TabIndex = 169;
            this.buttonCharacterDialogDown.Text = "Down";
            this.buttonCharacterDialogDown.UseVisualStyleBackColor = true;
            this.buttonCharacterDialogDown.Click += new System.EventHandler(this.buttonCharacterDialogMoveDown_Click);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(123, 4);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(35, 13);
            this.label37.TabIndex = 46;
            this.label37.Text = "Name";
            // 
            // listBoxCharacterDialog
            // 
            this.listBoxCharacterDialog.AccessibleRole = System.Windows.Forms.AccessibleRole.List;
            this.listBoxCharacterDialog.FormattingEnabled = true;
            this.listBoxCharacterDialog.Location = new System.Drawing.Point(7, 18);
            this.listBoxCharacterDialog.Name = "listBoxCharacterDialog";
            this.listBoxCharacterDialog.Size = new System.Drawing.Size(113, 251);
            this.listBoxCharacterDialog.TabIndex = 165;
            this.listBoxCharacterDialog.SelectedIndexChanged += new System.EventHandler(this.listBoxCharacterDialog_SelectedIndexChanged);
            // 
            // buttonCharacterDialogUp
            // 
            this.buttonCharacterDialogUp.Location = new System.Drawing.Point(66, 278);
            this.buttonCharacterDialogUp.Name = "buttonCharacterDialogUp";
            this.buttonCharacterDialogUp.Size = new System.Drawing.Size(58, 24);
            this.buttonCharacterDialogUp.TabIndex = 168;
            this.buttonCharacterDialogUp.Text = "Up";
            this.buttonCharacterDialogUp.UseVisualStyleBackColor = true;
            this.buttonCharacterDialogUp.Click += new System.EventHandler(this.buttonCharacterDialogMoveUp_Click);
            // 
            // buttonCharacterDialogAdd
            // 
            this.buttonCharacterDialogAdd.Location = new System.Drawing.Point(6, 277);
            this.buttonCharacterDialogAdd.Name = "buttonCharacterDialogAdd";
            this.buttonCharacterDialogAdd.Size = new System.Drawing.Size(58, 25);
            this.buttonCharacterDialogAdd.TabIndex = 166;
            this.buttonCharacterDialogAdd.Text = "Add";
            this.buttonCharacterDialogAdd.UseVisualStyleBackColor = true;
            this.buttonCharacterDialogAdd.Click += new System.EventHandler(this.buttonCharacterDialogAdd_Click);
            // 
            // buttonCharacterDialogRemove
            // 
            this.buttonCharacterDialogRemove.Location = new System.Drawing.Point(6, 308);
            this.buttonCharacterDialogRemove.Name = "buttonCharacterDialogRemove";
            this.buttonCharacterDialogRemove.Size = new System.Drawing.Size(58, 25);
            this.buttonCharacterDialogRemove.TabIndex = 167;
            this.buttonCharacterDialogRemove.Text = "Remove";
            this.buttonCharacterDialogRemove.UseVisualStyleBackColor = true;
            this.buttonCharacterDialogRemove.Click += new System.EventHandler(this.buttonCharacterDialogRemove_Click);
            // 
            // groupBox35
            // 
            this.groupBox35.BackColor = System.Drawing.Color.Gainsboro;
            this.groupBox35.Controls.Add(this.panelCharacterStat);
            this.groupBox35.Controls.Add(this.label42);
            this.groupBox35.Controls.Add(this.trackBarCharacterLevel);
            this.groupBox35.Location = new System.Drawing.Point(657, 8);
            this.groupBox35.Name = "groupBox35";
            this.groupBox35.Size = new System.Drawing.Size(302, 271);
            this.groupBox35.TabIndex = 91;
            this.groupBox35.TabStop = false;
            this.groupBox35.Text = "Stat Preview";
            // 
            // panelCharacterStat
            // 
            this.panelCharacterStat.BackColor = System.Drawing.Color.Black;
            this.panelCharacterStat.Location = new System.Drawing.Point(6, 18);
            this.panelCharacterStat.Name = "panelCharacterStat";
            this.panelCharacterStat.Size = new System.Drawing.Size(239, 242);
            this.panelCharacterStat.TabIndex = 74;
            this.panelCharacterStat.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(251, 14);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(33, 13);
            this.label42.TabIndex = 63;
            this.label42.Text = "Level";
            // 
            // trackBarCharacterLevel
            // 
            this.trackBarCharacterLevel.LargeChange = 10;
            this.trackBarCharacterLevel.Location = new System.Drawing.Point(251, 30);
            this.trackBarCharacterLevel.Maximum = 100;
            this.trackBarCharacterLevel.Name = "trackBarCharacterLevel";
            this.trackBarCharacterLevel.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trackBarCharacterLevel.Size = new System.Drawing.Size(45, 230);
            this.trackBarCharacterLevel.TabIndex = 64;
            this.trackBarCharacterLevel.TickFrequency = 5;
            this.trackBarCharacterLevel.Value = 100;
            this.trackBarCharacterLevel.Scroll += new System.EventHandler(this.trackBarCharacterLevel_Scroll);
            // 
            // groupBox34
            // 
            this.groupBox34.BackColor = System.Drawing.Color.Gainsboro;
            this.groupBox34.Controls.Add(this.textBoxCharacterColor);
            this.groupBox34.Controls.Add(this.textBoxCharacterName);
            this.groupBox34.Controls.Add(this.label228);
            this.groupBox34.Controls.Add(this.label1);
            this.groupBox34.Controls.Add(this.textBoxCharacterPhone);
            this.groupBox34.Controls.Add(this.textBoxCharacterAbility);
            this.groupBox34.Controls.Add(this.label8);
            this.groupBox34.Controls.Add(this.label11);
            this.groupBox34.Controls.Add(this.label13);
            this.groupBox34.Controls.Add(this.label15);
            this.groupBox34.Controls.Add(this.label17);
            this.groupBox34.Controls.Add(this.labelCharacterRed);
            this.groupBox34.Controls.Add(this.labelCharacterGreen);
            this.groupBox34.Controls.Add(this.labelCharacterBlue);
            this.groupBox34.Controls.Add(this.label38);
            this.groupBox34.Controls.Add(this.comboBoxCharacterType);
            this.groupBox34.Controls.Add(this.label9);
            this.groupBox34.Controls.Add(this.textBoxCharacterBattleDesc);
            this.groupBox34.Controls.Add(this.textBoxCharacterVisualDesc);
            this.groupBox34.Controls.Add(this.label10);
            this.groupBox34.Location = new System.Drawing.Point(132, 8);
            this.groupBox34.Name = "groupBox34";
            this.groupBox34.Size = new System.Drawing.Size(303, 257);
            this.groupBox34.TabIndex = 90;
            this.groupBox34.TabStop = false;
            this.groupBox34.Text = "Main";
            // 
            // textBoxCharacterColor
            // 
            this.textBoxCharacterColor.Location = new System.Drawing.Point(96, 30);
            this.textBoxCharacterColor.Multiline = true;
            this.textBoxCharacterColor.Name = "textBoxCharacterColor";
            this.textBoxCharacterColor.ReadOnly = true;
            this.textBoxCharacterColor.Size = new System.Drawing.Size(81, 76);
            this.textBoxCharacterColor.TabIndex = 12;
            this.textBoxCharacterColor.Click += new System.EventHandler(this.textBox1_Click);
            // 
            // textBoxCharacterName
            // 
            this.textBoxCharacterName.AccessibleDescription = "Character name";
            this.textBoxCharacterName.AccessibleName = "Name";
            this.textBoxCharacterName.Location = new System.Drawing.Point(6, 30);
            this.textBoxCharacterName.Name = "textBoxCharacterName";
            this.textBoxCharacterName.Size = new System.Drawing.Size(84, 20);
            this.textBoxCharacterName.TabIndex = 6;
            this.textBoxCharacterName.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            this.textBoxCharacterName.Leave += new System.EventHandler(this.textBoxCharacterName_Leave);
            // 
            // label228
            // 
            this.label228.AutoSize = true;
            this.label228.Location = new System.Drawing.Point(183, 210);
            this.label228.Name = "label228";
            this.label228.Size = new System.Drawing.Size(38, 13);
            this.label228.TabIndex = 15;
            this.label228.Text = "Phone";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Name";
            // 
            // textBoxCharacterPhone
            // 
            this.textBoxCharacterPhone.AccessibleDescription = "Phone Number";
            this.textBoxCharacterPhone.AccessibleName = "Phone";
            this.textBoxCharacterPhone.Location = new System.Drawing.Point(186, 227);
            this.textBoxCharacterPhone.Name = "textBoxCharacterPhone";
            this.textBoxCharacterPhone.Size = new System.Drawing.Size(94, 20);
            this.textBoxCharacterPhone.TabIndex = 16;
            // 
            // textBoxCharacterAbility
            // 
            this.textBoxCharacterAbility.AccessibleDescription = "Character ability";
            this.textBoxCharacterAbility.AccessibleName = "Ability";
            this.textBoxCharacterAbility.Location = new System.Drawing.Point(6, 70);
            this.textBoxCharacterAbility.Name = "textBoxCharacterAbility";
            this.textBoxCharacterAbility.Size = new System.Drawing.Size(84, 20);
            this.textBoxCharacterAbility.TabIndex = 8;
            this.textBoxCharacterAbility.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 53);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(34, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Ability";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(93, 14);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(31, 13);
            this.label11.TabIndex = 11;
            this.label11.Text = "Color";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(106, 110);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(18, 13);
            this.label13.TabIndex = 40;
            this.label13.Text = "R:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(129, 110);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(18, 13);
            this.label15.TabIndex = 41;
            this.label15.Text = "G:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(152, 110);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(17, 13);
            this.label17.TabIndex = 42;
            this.label17.Text = "B:";
            // 
            // labelCharacterRed
            // 
            this.labelCharacterRed.AutoSize = true;
            this.labelCharacterRed.Location = new System.Drawing.Point(107, 123);
            this.labelCharacterRed.Name = "labelCharacterRed";
            this.labelCharacterRed.Size = new System.Drawing.Size(13, 13);
            this.labelCharacterRed.TabIndex = 43;
            this.labelCharacterRed.Text = "0";
            this.labelCharacterRed.TextChanged += new System.EventHandler(this.labelCharacterRed_TextChanged);
            // 
            // labelCharacterGreen
            // 
            this.labelCharacterGreen.AutoSize = true;
            this.labelCharacterGreen.Location = new System.Drawing.Point(130, 123);
            this.labelCharacterGreen.Name = "labelCharacterGreen";
            this.labelCharacterGreen.Size = new System.Drawing.Size(13, 13);
            this.labelCharacterGreen.TabIndex = 44;
            this.labelCharacterGreen.Text = "0";
            this.labelCharacterGreen.TextChanged += new System.EventHandler(this.labelCharacterGreen_TextChanged);
            // 
            // labelCharacterBlue
            // 
            this.labelCharacterBlue.AutoSize = true;
            this.labelCharacterBlue.Location = new System.Drawing.Point(153, 123);
            this.labelCharacterBlue.Name = "labelCharacterBlue";
            this.labelCharacterBlue.Size = new System.Drawing.Size(13, 13);
            this.labelCharacterBlue.TabIndex = 45;
            this.labelCharacterBlue.Text = "0";
            this.labelCharacterBlue.TextChanged += new System.EventHandler(this.labelCharacterBlue_TextChanged);
            // 
            // label38
            // 
            this.label38.AccessibleDescription = "Character element";
            this.label38.AccessibleName = "element";
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(4, 93);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(45, 13);
            this.label38.TabIndex = 9;
            this.label38.Text = "Element";
            // 
            // comboBoxCharacterType
            // 
            this.comboBoxCharacterType.FormattingEnabled = true;
            this.comboBoxCharacterType.Location = new System.Drawing.Point(6, 110);
            this.comboBoxCharacterType.Name = "comboBoxCharacterType";
            this.comboBoxCharacterType.Size = new System.Drawing.Size(84, 21);
            this.comboBoxCharacterType.TabIndex = 10;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(183, 13);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(90, 13);
            this.label9.TabIndex = 13;
            this.label9.Text = "Battle Description";
            // 
            // textBoxCharacterBattleDesc
            // 
            this.textBoxCharacterBattleDesc.AccessibleDescription = "Battle Description";
            this.textBoxCharacterBattleDesc.AccessibleName = "Description in Battle";
            this.textBoxCharacterBattleDesc.Location = new System.Drawing.Point(183, 30);
            this.textBoxCharacterBattleDesc.Multiline = true;
            this.textBoxCharacterBattleDesc.Name = "textBoxCharacterBattleDesc";
            this.textBoxCharacterBattleDesc.Size = new System.Drawing.Size(111, 178);
            this.textBoxCharacterBattleDesc.TabIndex = 14;
            this.textBoxCharacterBattleDesc.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // textBoxCharacterVisualDesc
            // 
            this.textBoxCharacterVisualDesc.AccessibleDescription = "Visual Description";
            this.textBoxCharacterVisualDesc.AccessibleName = "Visual Description";
            this.textBoxCharacterVisualDesc.Location = new System.Drawing.Point(6, 149);
            this.textBoxCharacterVisualDesc.Multiline = true;
            this.textBoxCharacterVisualDesc.Name = "textBoxCharacterVisualDesc";
            this.textBoxCharacterVisualDesc.Size = new System.Drawing.Size(171, 98);
            this.textBoxCharacterVisualDesc.TabIndex = 18;
            this.textBoxCharacterVisualDesc.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 133);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(91, 13);
            this.label10.TabIndex = 17;
            this.label10.Text = "Visual Description";
            // 
            // groupBox33
            // 
            this.groupBox33.BackColor = System.Drawing.Color.Gainsboro;
            this.groupBox33.Controls.Add(this.label12);
            this.groupBox33.Controls.Add(this.trackBarCharacterHP);
            this.groupBox33.Controls.Add(this.labelCharacterHP);
            this.groupBox33.Controls.Add(this.trackBarCharacterMP);
            this.groupBox33.Controls.Add(this.label14);
            this.groupBox33.Controls.Add(this.labelCharacterMP);
            this.groupBox33.Controls.Add(this.trackBarCharacterPhyPower);
            this.groupBox33.Controls.Add(this.label16);
            this.groupBox33.Controls.Add(this.labelCharacterPhyPower);
            this.groupBox33.Controls.Add(this.trackBarCharacterPhyGuard);
            this.groupBox33.Controls.Add(this.label18);
            this.groupBox33.Controls.Add(this.labelCharacterPhyGuard);
            this.groupBox33.Controls.Add(this.trackBarCharacterMagPower);
            this.groupBox33.Controls.Add(this.labelCharacterSpeGuard);
            this.groupBox33.Controls.Add(this.label20);
            this.groupBox33.Controls.Add(this.label26);
            this.groupBox33.Controls.Add(this.labelCharacterMagPower);
            this.groupBox33.Controls.Add(this.trackBarCharacterSpeGuard);
            this.groupBox33.Controls.Add(this.trackBarCharacterMagGuard);
            this.groupBox33.Controls.Add(this.labelCharacterSpePower);
            this.groupBox33.Controls.Add(this.label22);
            this.groupBox33.Controls.Add(this.label24);
            this.groupBox33.Controls.Add(this.labelCharacterMagGuard);
            this.groupBox33.Controls.Add(this.trackBarCharacterSpePower);
            this.groupBox33.Location = new System.Drawing.Point(441, 8);
            this.groupBox33.Name = "groupBox33";
            this.groupBox33.Size = new System.Drawing.Size(209, 598);
            this.groupBox33.TabIndex = 89;
            this.groupBox33.TabStop = false;
            this.groupBox33.Text = "Stats";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(8, 17);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(22, 13);
            this.label12.TabIndex = 19;
            this.label12.Text = "HP";
            // 
            // trackBarCharacterHP
            // 
            this.trackBarCharacterHP.AccessibleDescription = "HP Stat";
            this.trackBarCharacterHP.AccessibleName = "HP";
            this.trackBarCharacterHP.BackColor = System.Drawing.Color.WhiteSmoke;
            this.trackBarCharacterHP.LargeChange = 20;
            this.trackBarCharacterHP.Location = new System.Drawing.Point(11, 40);
            this.trackBarCharacterHP.Maximum = 500;
            this.trackBarCharacterHP.Name = "trackBarCharacterHP";
            this.trackBarCharacterHP.Size = new System.Drawing.Size(167, 45);
            this.trackBarCharacterHP.TabIndex = 20;
            this.trackBarCharacterHP.TickFrequency = 25;
            this.trackBarCharacterHP.ValueChanged += new System.EventHandler(this.trackBarCharacterHP_Scroll);
            // 
            // labelCharacterHP
            // 
            this.labelCharacterHP.AutoSize = true;
            this.labelCharacterHP.Location = new System.Drawing.Point(184, 40);
            this.labelCharacterHP.Name = "labelCharacterHP";
            this.labelCharacterHP.Size = new System.Drawing.Size(13, 13);
            this.labelCharacterHP.TabIndex = 18;
            this.labelCharacterHP.Text = "0";
            // 
            // trackBarCharacterMP
            // 
            this.trackBarCharacterMP.AccessibleDescription = "Energy Stat";
            this.trackBarCharacterMP.AccessibleName = "Energy";
            this.trackBarCharacterMP.BackColor = System.Drawing.Color.WhiteSmoke;
            this.trackBarCharacterMP.LargeChange = 20;
            this.trackBarCharacterMP.Location = new System.Drawing.Point(11, 114);
            this.trackBarCharacterMP.Maximum = 250;
            this.trackBarCharacterMP.Name = "trackBarCharacterMP";
            this.trackBarCharacterMP.Size = new System.Drawing.Size(167, 45);
            this.trackBarCharacterMP.TabIndex = 22;
            this.trackBarCharacterMP.TickFrequency = 12;
            this.trackBarCharacterMP.ValueChanged += new System.EventHandler(this.trackBarCharacterMP_Scroll);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(8, 91);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(23, 13);
            this.label14.TabIndex = 21;
            this.label14.Text = "MP";
            // 
            // labelCharacterMP
            // 
            this.labelCharacterMP.AutoSize = true;
            this.labelCharacterMP.Location = new System.Drawing.Point(184, 114);
            this.labelCharacterMP.Name = "labelCharacterMP";
            this.labelCharacterMP.Size = new System.Drawing.Size(13, 13);
            this.labelCharacterMP.TabIndex = 21;
            this.labelCharacterMP.Text = "0";
            // 
            // trackBarCharacterPhyPower
            // 
            this.trackBarCharacterPhyPower.AccessibleDescription = "Physical Power Stat";
            this.trackBarCharacterPhyPower.AccessibleName = "Physical Power";
            this.trackBarCharacterPhyPower.BackColor = System.Drawing.Color.WhiteSmoke;
            this.trackBarCharacterPhyPower.LargeChange = 20;
            this.trackBarCharacterPhyPower.Location = new System.Drawing.Point(11, 185);
            this.trackBarCharacterPhyPower.Maximum = 500;
            this.trackBarCharacterPhyPower.Name = "trackBarCharacterPhyPower";
            this.trackBarCharacterPhyPower.Size = new System.Drawing.Size(167, 45);
            this.trackBarCharacterPhyPower.TabIndex = 24;
            this.trackBarCharacterPhyPower.TickFrequency = 25;
            this.trackBarCharacterPhyPower.Scroll += new System.EventHandler(this.trackBarCharacterPhyPower_Scroll_1);
            this.trackBarCharacterPhyPower.ValueChanged += new System.EventHandler(this.trackBarCharacterPhyPower_Scroll);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(8, 162);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(63, 13);
            this.label16.TabIndex = 23;
            this.label16.Text = "Phys Power";
            // 
            // labelCharacterPhyPower
            // 
            this.labelCharacterPhyPower.AutoSize = true;
            this.labelCharacterPhyPower.Location = new System.Drawing.Point(184, 185);
            this.labelCharacterPhyPower.Name = "labelCharacterPhyPower";
            this.labelCharacterPhyPower.Size = new System.Drawing.Size(13, 13);
            this.labelCharacterPhyPower.TabIndex = 24;
            this.labelCharacterPhyPower.Text = "0";
            // 
            // trackBarCharacterPhyGuard
            // 
            this.trackBarCharacterPhyGuard.AccessibleDescription = "Physical Guard Stat";
            this.trackBarCharacterPhyGuard.AccessibleName = "Physical Guard";
            this.trackBarCharacterPhyGuard.BackColor = System.Drawing.Color.WhiteSmoke;
            this.trackBarCharacterPhyGuard.LargeChange = 20;
            this.trackBarCharacterPhyGuard.Location = new System.Drawing.Point(11, 254);
            this.trackBarCharacterPhyGuard.Maximum = 500;
            this.trackBarCharacterPhyGuard.Name = "trackBarCharacterPhyGuard";
            this.trackBarCharacterPhyGuard.Size = new System.Drawing.Size(167, 45);
            this.trackBarCharacterPhyGuard.TabIndex = 26;
            this.trackBarCharacterPhyGuard.TickFrequency = 25;
            this.trackBarCharacterPhyGuard.Scroll += new System.EventHandler(this.trackBarCharacterPhyGuard_Scroll_1);
            this.trackBarCharacterPhyGuard.ValueChanged += new System.EventHandler(this.trackBarCharacterPhyGuard_Scroll);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(8, 231);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(62, 13);
            this.label18.TabIndex = 25;
            this.label18.Text = "Phys Guard";
            // 
            // labelCharacterPhyGuard
            // 
            this.labelCharacterPhyGuard.AutoSize = true;
            this.labelCharacterPhyGuard.Location = new System.Drawing.Point(184, 254);
            this.labelCharacterPhyGuard.Name = "labelCharacterPhyGuard";
            this.labelCharacterPhyGuard.Size = new System.Drawing.Size(13, 13);
            this.labelCharacterPhyGuard.TabIndex = 27;
            this.labelCharacterPhyGuard.Text = "0";
            // 
            // trackBarCharacterMagPower
            // 
            this.trackBarCharacterMagPower.AccessibleDescription = "Magic Power Stat";
            this.trackBarCharacterMagPower.AccessibleName = "Magic Power";
            this.trackBarCharacterMagPower.BackColor = System.Drawing.Color.WhiteSmoke;
            this.trackBarCharacterMagPower.LargeChange = 20;
            this.trackBarCharacterMagPower.Location = new System.Drawing.Point(11, 328);
            this.trackBarCharacterMagPower.Maximum = 500;
            this.trackBarCharacterMagPower.Name = "trackBarCharacterMagPower";
            this.trackBarCharacterMagPower.Size = new System.Drawing.Size(167, 45);
            this.trackBarCharacterMagPower.TabIndex = 28;
            this.trackBarCharacterMagPower.TickFrequency = 25;
            this.trackBarCharacterMagPower.Scroll += new System.EventHandler(this.trackBarCharacterMagPower_Scroll_1);
            this.trackBarCharacterMagPower.ValueChanged += new System.EventHandler(this.trackBarCharacterMagPower_Scroll);
            // 
            // labelCharacterSpeGuard
            // 
            this.labelCharacterSpeGuard.AutoSize = true;
            this.labelCharacterSpeGuard.Location = new System.Drawing.Point(184, 541);
            this.labelCharacterSpeGuard.Name = "labelCharacterSpeGuard";
            this.labelCharacterSpeGuard.Size = new System.Drawing.Size(13, 13);
            this.labelCharacterSpeGuard.TabIndex = 39;
            this.labelCharacterSpeGuard.Text = "0";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(8, 305);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(69, 13);
            this.label20.TabIndex = 27;
            this.label20.Text = "Magic Power";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(8, 518);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(70, 13);
            this.label26.TabIndex = 33;
            this.label26.Text = "Speed Guard";
            // 
            // labelCharacterMagPower
            // 
            this.labelCharacterMagPower.AutoSize = true;
            this.labelCharacterMagPower.Location = new System.Drawing.Point(184, 328);
            this.labelCharacterMagPower.Name = "labelCharacterMagPower";
            this.labelCharacterMagPower.Size = new System.Drawing.Size(13, 13);
            this.labelCharacterMagPower.TabIndex = 30;
            this.labelCharacterMagPower.Text = "0";
            // 
            // trackBarCharacterSpeGuard
            // 
            this.trackBarCharacterSpeGuard.AccessibleDescription = "Speed Guard Stat";
            this.trackBarCharacterSpeGuard.AccessibleName = "Speed Guard";
            this.trackBarCharacterSpeGuard.BackColor = System.Drawing.Color.WhiteSmoke;
            this.trackBarCharacterSpeGuard.Location = new System.Drawing.Point(11, 541);
            this.trackBarCharacterSpeGuard.Maximum = 20;
            this.trackBarCharacterSpeGuard.Name = "trackBarCharacterSpeGuard";
            this.trackBarCharacterSpeGuard.Size = new System.Drawing.Size(167, 45);
            this.trackBarCharacterSpeGuard.TabIndex = 34;
            this.trackBarCharacterSpeGuard.Scroll += new System.EventHandler(this.trackBarCharacterSpeGuard_Scroll_1);
            this.trackBarCharacterSpeGuard.ValueChanged += new System.EventHandler(this.trackBarCharacterSpeGuard_Scroll);
            // 
            // trackBarCharacterMagGuard
            // 
            this.trackBarCharacterMagGuard.AccessibleDescription = "Magic Guard Stat";
            this.trackBarCharacterMagGuard.AccessibleName = "Magic Guard";
            this.trackBarCharacterMagGuard.BackColor = System.Drawing.Color.WhiteSmoke;
            this.trackBarCharacterMagGuard.LargeChange = 20;
            this.trackBarCharacterMagGuard.Location = new System.Drawing.Point(11, 396);
            this.trackBarCharacterMagGuard.Maximum = 500;
            this.trackBarCharacterMagGuard.Name = "trackBarCharacterMagGuard";
            this.trackBarCharacterMagGuard.Size = new System.Drawing.Size(167, 45);
            this.trackBarCharacterMagGuard.TabIndex = 30;
            this.trackBarCharacterMagGuard.TickFrequency = 25;
            this.trackBarCharacterMagGuard.Scroll += new System.EventHandler(this.trackBarCharacterMagGuard_Scroll_1);
            this.trackBarCharacterMagGuard.ValueChanged += new System.EventHandler(this.trackBarCharacterMagGuard_Scroll);
            // 
            // labelCharacterSpePower
            // 
            this.labelCharacterSpePower.AutoSize = true;
            this.labelCharacterSpePower.Location = new System.Drawing.Point(184, 471);
            this.labelCharacterSpePower.Name = "labelCharacterSpePower";
            this.labelCharacterSpePower.Size = new System.Drawing.Size(13, 13);
            this.labelCharacterSpePower.TabIndex = 36;
            this.labelCharacterSpePower.Text = "0";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(8, 373);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(68, 13);
            this.label22.TabIndex = 29;
            this.label22.Text = "Magic Guard";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(8, 448);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(71, 13);
            this.label24.TabIndex = 31;
            this.label24.Text = "Speed Power";
            // 
            // labelCharacterMagGuard
            // 
            this.labelCharacterMagGuard.AutoSize = true;
            this.labelCharacterMagGuard.Location = new System.Drawing.Point(184, 396);
            this.labelCharacterMagGuard.Name = "labelCharacterMagGuard";
            this.labelCharacterMagGuard.Size = new System.Drawing.Size(13, 13);
            this.labelCharacterMagGuard.TabIndex = 33;
            this.labelCharacterMagGuard.Text = "0";
            // 
            // trackBarCharacterSpePower
            // 
            this.trackBarCharacterSpePower.AccessibleDescription = "Speed Power Stat";
            this.trackBarCharacterSpePower.AccessibleName = "Speed Power";
            this.trackBarCharacterSpePower.BackColor = System.Drawing.Color.WhiteSmoke;
            this.trackBarCharacterSpePower.LargeChange = 20;
            this.trackBarCharacterSpePower.Location = new System.Drawing.Point(11, 471);
            this.trackBarCharacterSpePower.Maximum = 500;
            this.trackBarCharacterSpePower.Name = "trackBarCharacterSpePower";
            this.trackBarCharacterSpePower.Size = new System.Drawing.Size(167, 45);
            this.trackBarCharacterSpePower.TabIndex = 32;
            this.trackBarCharacterSpePower.TickFrequency = 25;
            this.trackBarCharacterSpePower.Scroll += new System.EventHandler(this.trackBarCharacterSpePower_Scroll_1);
            this.trackBarCharacterSpePower.ValueChanged += new System.EventHandler(this.trackBarCharacterSpePower_Scroll);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Gainsboro;
            this.groupBox3.Controls.Add(this.label73);
            this.groupBox3.Controls.Add(this.label69);
            this.groupBox3.Controls.Add(this.label75);
            this.groupBox3.Controls.Add(this.label67);
            this.groupBox3.Controls.Add(this.panelCharacterEXPGrowth);
            this.groupBox3.Controls.Add(this.panelCharacterSpeGuardGrowth);
            this.groupBox3.Controls.Add(this.textBoxCharacterEXPTotal);
            this.groupBox3.Controls.Add(this.panelCharacterPhyGuardGrowth);
            this.groupBox3.Controls.Add(this.label41);
            this.groupBox3.Controls.Add(this.panelCharacterMagGuardGrowth);
            this.groupBox3.Controls.Add(this.label63);
            this.groupBox3.Controls.Add(this.comboBoxCharacterEXPGrowth);
            this.groupBox3.Controls.Add(this.panelCharacterMPGrowth);
            this.groupBox3.Controls.Add(this.label77);
            this.groupBox3.Controls.Add(this.label71);
            this.groupBox3.Controls.Add(this.label84);
            this.groupBox3.Controls.Add(this.label65);
            this.groupBox3.Controls.Add(this.comboBoxCharacterSpeGuardGrowth);
            this.groupBox3.Controls.Add(this.comboBoxCharacterPhyGuardGrowth);
            this.groupBox3.Controls.Add(this.comboBoxCharacterMagGuardGrowth);
            this.groupBox3.Controls.Add(this.comboBoxCharacterMPGrowth);
            this.groupBox3.Controls.Add(this.panelCharacterSpePowerGrowth);
            this.groupBox3.Controls.Add(this.panelCharacterPhyPowerGrowth);
            this.groupBox3.Controls.Add(this.comboBoxCharacterSpePowerGrowth);
            this.groupBox3.Controls.Add(this.comboBoxCharacterPhyPowerGrowth);
            this.groupBox3.Controls.Add(this.panelCharacterMagPowerGrowth);
            this.groupBox3.Controls.Add(this.comboBoxCharacterMagPowerGrowth);
            this.groupBox3.Controls.Add(this.panelCharacterHPGrowth);
            this.groupBox3.Controls.Add(this.comboBoxCharacterHPGrowth);
            this.groupBox3.Location = new System.Drawing.Point(965, 8);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(333, 536);
            this.groupBox3.TabIndex = 88;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Stat Growth";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(19, 403);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(95, 13);
            this.label73.TabIndex = 76;
            this.label73.Text = "Spe Guard Growth";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(19, 187);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(94, 13);
            this.label69.TabIndex = 68;
            this.label69.Text = "Phy Guard Growth";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(19, 295);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(97, 13);
            this.label75.TabIndex = 72;
            this.label75.Text = "Mag Guard Growth";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(19, 79);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(60, 13);
            this.label67.TabIndex = 64;
            this.label67.Text = "MP Growth";
            // 
            // panelCharacterEXPGrowth
            // 
            this.panelCharacterEXPGrowth.BackColor = System.Drawing.Color.Black;
            this.panelCharacterEXPGrowth.Location = new System.Drawing.Point(166, 459);
            this.panelCharacterEXPGrowth.Name = "panelCharacterEXPGrowth";
            this.panelCharacterEXPGrowth.Size = new System.Drawing.Size(150, 63);
            this.panelCharacterEXPGrowth.TabIndex = 84;
            this.panelCharacterEXPGrowth.Paint += new System.Windows.Forms.PaintEventHandler(this.panelCharacterEXPGrowth_Paint);
            // 
            // panelCharacterSpeGuardGrowth
            // 
            this.panelCharacterSpeGuardGrowth.BackColor = System.Drawing.Color.Black;
            this.panelCharacterSpeGuardGrowth.Location = new System.Drawing.Point(175, 406);
            this.panelCharacterSpeGuardGrowth.Name = "panelCharacterSpeGuardGrowth";
            this.panelCharacterSpeGuardGrowth.Size = new System.Drawing.Size(140, 48);
            this.panelCharacterSpeGuardGrowth.TabIndex = 108;
            this.panelCharacterSpeGuardGrowth.Paint += new System.Windows.Forms.PaintEventHandler(this.panelCharacterSpeGuardGrowth_Paint);
            // 
            // textBoxCharacterEXPTotal
            // 
            this.textBoxCharacterEXPTotal.AccessibleDescription = "EXP Required to get to level 100";
            this.textBoxCharacterEXPTotal.AccessibleName = "EXP Total";
            this.textBoxCharacterEXPTotal.Location = new System.Drawing.Point(60, 502);
            this.textBoxCharacterEXPTotal.Name = "textBoxCharacterEXPTotal";
            this.textBoxCharacterEXPTotal.Size = new System.Drawing.Size(100, 20);
            this.textBoxCharacterEXPTotal.TabIndex = 61;
            this.textBoxCharacterEXPTotal.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // panelCharacterPhyGuardGrowth
            // 
            this.panelCharacterPhyGuardGrowth.BackColor = System.Drawing.Color.Black;
            this.panelCharacterPhyGuardGrowth.Location = new System.Drawing.Point(175, 190);
            this.panelCharacterPhyGuardGrowth.Name = "panelCharacterPhyGuardGrowth";
            this.panelCharacterPhyGuardGrowth.Size = new System.Drawing.Size(140, 48);
            this.panelCharacterPhyGuardGrowth.TabIndex = 96;
            this.panelCharacterPhyGuardGrowth.Paint += new System.Windows.Forms.PaintEventHandler(this.panelCharacterPhyGuardGrowth_Paint);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(23, 505);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(31, 13);
            this.label41.TabIndex = 60;
            this.label41.Text = "Total";
            // 
            // panelCharacterMagGuardGrowth
            // 
            this.panelCharacterMagGuardGrowth.BackColor = System.Drawing.Color.Black;
            this.panelCharacterMagGuardGrowth.Location = new System.Drawing.Point(175, 298);
            this.panelCharacterMagGuardGrowth.Name = "panelCharacterMagGuardGrowth";
            this.panelCharacterMagGuardGrowth.Size = new System.Drawing.Size(140, 48);
            this.panelCharacterMagGuardGrowth.TabIndex = 102;
            this.panelCharacterMagGuardGrowth.Paint += new System.Windows.Forms.PaintEventHandler(this.panelCharacterMagGuardGrowth_Paint);
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(19, 454);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(65, 13);
            this.label63.TabIndex = 58;
            this.label63.Text = "EXP Growth";
            // 
            // comboBoxCharacterEXPGrowth
            // 
            this.comboBoxCharacterEXPGrowth.AccessibleDescription = "Growth Pattern for EXP";
            this.comboBoxCharacterEXPGrowth.AccessibleName = "EXP Growth";
            this.comboBoxCharacterEXPGrowth.FormattingEnabled = true;
            this.comboBoxCharacterEXPGrowth.Location = new System.Drawing.Point(22, 475);
            this.comboBoxCharacterEXPGrowth.Name = "comboBoxCharacterEXPGrowth";
            this.comboBoxCharacterEXPGrowth.Size = new System.Drawing.Size(138, 21);
            this.comboBoxCharacterEXPGrowth.TabIndex = 59;
            this.comboBoxCharacterEXPGrowth.SelectedIndexChanged += new System.EventHandler(this.comboBoxCharacterEXPGrowth_SelectedIndexChanged);
            // 
            // panelCharacterMPGrowth
            // 
            this.panelCharacterMPGrowth.BackColor = System.Drawing.Color.Black;
            this.panelCharacterMPGrowth.Location = new System.Drawing.Point(175, 82);
            this.panelCharacterMPGrowth.Name = "panelCharacterMPGrowth";
            this.panelCharacterMPGrowth.Size = new System.Drawing.Size(140, 48);
            this.panelCharacterMPGrowth.TabIndex = 90;
            this.panelCharacterMPGrowth.Paint += new System.Windows.Forms.PaintEventHandler(this.panelCharacterMPGrowth_Paint);
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(19, 349);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(96, 13);
            this.label77.TabIndex = 74;
            this.label77.Text = "Spe Power Growth";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(19, 133);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(95, 13);
            this.label71.TabIndex = 66;
            this.label71.Text = "Phy Power Growth";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(19, 241);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(98, 13);
            this.label84.TabIndex = 70;
            this.label84.Text = "Mag Power Growth";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(19, 25);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(59, 13);
            this.label65.TabIndex = 62;
            this.label65.Text = "HP Growth";
            // 
            // comboBoxCharacterSpeGuardGrowth
            // 
            this.comboBoxCharacterSpeGuardGrowth.AccessibleDescription = "Speed guard over time";
            this.comboBoxCharacterSpeGuardGrowth.AccessibleName = "Speed Guard";
            this.comboBoxCharacterSpeGuardGrowth.FormattingEnabled = true;
            this.comboBoxCharacterSpeGuardGrowth.Location = new System.Drawing.Point(22, 424);
            this.comboBoxCharacterSpeGuardGrowth.Name = "comboBoxCharacterSpeGuardGrowth";
            this.comboBoxCharacterSpeGuardGrowth.Size = new System.Drawing.Size(138, 21);
            this.comboBoxCharacterSpeGuardGrowth.TabIndex = 77;
            this.comboBoxCharacterSpeGuardGrowth.SelectedIndexChanged += new System.EventHandler(this.comboBoxCharacterSpeGuardGrowth_SelectedIndexChanged);
            // 
            // comboBoxCharacterPhyGuardGrowth
            // 
            this.comboBoxCharacterPhyGuardGrowth.AccessibleDescription = "Physical Guard over Time";
            this.comboBoxCharacterPhyGuardGrowth.AccessibleName = "Physical Guard";
            this.comboBoxCharacterPhyGuardGrowth.FormattingEnabled = true;
            this.comboBoxCharacterPhyGuardGrowth.Location = new System.Drawing.Point(22, 208);
            this.comboBoxCharacterPhyGuardGrowth.Name = "comboBoxCharacterPhyGuardGrowth";
            this.comboBoxCharacterPhyGuardGrowth.Size = new System.Drawing.Size(138, 21);
            this.comboBoxCharacterPhyGuardGrowth.TabIndex = 69;
            this.comboBoxCharacterPhyGuardGrowth.SelectedIndexChanged += new System.EventHandler(this.comboBoxCharacterPhyGuardGrowth_SelectedIndexChanged);
            // 
            // comboBoxCharacterMagGuardGrowth
            // 
            this.comboBoxCharacterMagGuardGrowth.AccessibleDescription = "Magic guard over time";
            this.comboBoxCharacterMagGuardGrowth.AccessibleName = "Magic guard";
            this.comboBoxCharacterMagGuardGrowth.FormattingEnabled = true;
            this.comboBoxCharacterMagGuardGrowth.Location = new System.Drawing.Point(22, 316);
            this.comboBoxCharacterMagGuardGrowth.Name = "comboBoxCharacterMagGuardGrowth";
            this.comboBoxCharacterMagGuardGrowth.Size = new System.Drawing.Size(138, 21);
            this.comboBoxCharacterMagGuardGrowth.TabIndex = 73;
            this.comboBoxCharacterMagGuardGrowth.SelectedIndexChanged += new System.EventHandler(this.comboBoxCharacterMagGuardGrowth_SelectedIndexChanged);
            // 
            // comboBoxCharacterMPGrowth
            // 
            this.comboBoxCharacterMPGrowth.AccessibleDescription = "Energy Growth over time";
            this.comboBoxCharacterMPGrowth.AccessibleName = "Energy Growth";
            this.comboBoxCharacterMPGrowth.FormattingEnabled = true;
            this.comboBoxCharacterMPGrowth.Location = new System.Drawing.Point(22, 100);
            this.comboBoxCharacterMPGrowth.Name = "comboBoxCharacterMPGrowth";
            this.comboBoxCharacterMPGrowth.Size = new System.Drawing.Size(138, 21);
            this.comboBoxCharacterMPGrowth.TabIndex = 65;
            this.comboBoxCharacterMPGrowth.SelectedIndexChanged += new System.EventHandler(this.comboBoxCharacterMPGrowth_SelectedIndexChanged);
            // 
            // panelCharacterSpePowerGrowth
            // 
            this.panelCharacterSpePowerGrowth.BackColor = System.Drawing.Color.Black;
            this.panelCharacterSpePowerGrowth.Location = new System.Drawing.Point(175, 352);
            this.panelCharacterSpePowerGrowth.Name = "panelCharacterSpePowerGrowth";
            this.panelCharacterSpePowerGrowth.Size = new System.Drawing.Size(140, 48);
            this.panelCharacterSpePowerGrowth.TabIndex = 105;
            this.panelCharacterSpePowerGrowth.Paint += new System.Windows.Forms.PaintEventHandler(this.panelCharacterSpePowerGrowth_Paint);
            // 
            // panelCharacterPhyPowerGrowth
            // 
            this.panelCharacterPhyPowerGrowth.BackColor = System.Drawing.Color.Black;
            this.panelCharacterPhyPowerGrowth.Location = new System.Drawing.Point(175, 136);
            this.panelCharacterPhyPowerGrowth.Name = "panelCharacterPhyPowerGrowth";
            this.panelCharacterPhyPowerGrowth.Size = new System.Drawing.Size(140, 48);
            this.panelCharacterPhyPowerGrowth.TabIndex = 93;
            this.panelCharacterPhyPowerGrowth.Paint += new System.Windows.Forms.PaintEventHandler(this.panelCharacterPhyPowerGrowth_Paint);
            // 
            // comboBoxCharacterSpePowerGrowth
            // 
            this.comboBoxCharacterSpePowerGrowth.AccessibleDescription = "Speed Power over time";
            this.comboBoxCharacterSpePowerGrowth.AccessibleName = "Speed Power";
            this.comboBoxCharacterSpePowerGrowth.FormattingEnabled = true;
            this.comboBoxCharacterSpePowerGrowth.Location = new System.Drawing.Point(22, 370);
            this.comboBoxCharacterSpePowerGrowth.Name = "comboBoxCharacterSpePowerGrowth";
            this.comboBoxCharacterSpePowerGrowth.Size = new System.Drawing.Size(138, 21);
            this.comboBoxCharacterSpePowerGrowth.TabIndex = 75;
            this.comboBoxCharacterSpePowerGrowth.SelectedIndexChanged += new System.EventHandler(this.comboBoxCharacterSpePowerGrowth_SelectedIndexChanged);
            // 
            // comboBoxCharacterPhyPowerGrowth
            // 
            this.comboBoxCharacterPhyPowerGrowth.AccessibleDescription = "Physical Power Growth Over time";
            this.comboBoxCharacterPhyPowerGrowth.AccessibleName = "Physical Power Growth";
            this.comboBoxCharacterPhyPowerGrowth.FormattingEnabled = true;
            this.comboBoxCharacterPhyPowerGrowth.Location = new System.Drawing.Point(22, 154);
            this.comboBoxCharacterPhyPowerGrowth.Name = "comboBoxCharacterPhyPowerGrowth";
            this.comboBoxCharacterPhyPowerGrowth.Size = new System.Drawing.Size(138, 21);
            this.comboBoxCharacterPhyPowerGrowth.TabIndex = 67;
            this.comboBoxCharacterPhyPowerGrowth.SelectedIndexChanged += new System.EventHandler(this.comboBoxCharacterPhyPowerGrowth_SelectedIndexChanged);
            // 
            // panelCharacterMagPowerGrowth
            // 
            this.panelCharacterMagPowerGrowth.BackColor = System.Drawing.Color.Black;
            this.panelCharacterMagPowerGrowth.Location = new System.Drawing.Point(175, 244);
            this.panelCharacterMagPowerGrowth.Name = "panelCharacterMagPowerGrowth";
            this.panelCharacterMagPowerGrowth.Size = new System.Drawing.Size(140, 48);
            this.panelCharacterMagPowerGrowth.TabIndex = 99;
            this.panelCharacterMagPowerGrowth.Paint += new System.Windows.Forms.PaintEventHandler(this.panelCharacterMagPowerGrowth_Paint);
            // 
            // comboBoxCharacterMagPowerGrowth
            // 
            this.comboBoxCharacterMagPowerGrowth.AccessibleDescription = "Magic power over time";
            this.comboBoxCharacterMagPowerGrowth.AccessibleName = "Magic Power";
            this.comboBoxCharacterMagPowerGrowth.FormattingEnabled = true;
            this.comboBoxCharacterMagPowerGrowth.Location = new System.Drawing.Point(22, 262);
            this.comboBoxCharacterMagPowerGrowth.Name = "comboBoxCharacterMagPowerGrowth";
            this.comboBoxCharacterMagPowerGrowth.Size = new System.Drawing.Size(138, 21);
            this.comboBoxCharacterMagPowerGrowth.TabIndex = 71;
            this.comboBoxCharacterMagPowerGrowth.SelectedIndexChanged += new System.EventHandler(this.comboBoxCharacterMagPowerGrowth_SelectedIndexChanged);
            // 
            // panelCharacterHPGrowth
            // 
            this.panelCharacterHPGrowth.BackColor = System.Drawing.Color.Black;
            this.panelCharacterHPGrowth.Location = new System.Drawing.Point(175, 28);
            this.panelCharacterHPGrowth.Name = "panelCharacterHPGrowth";
            this.panelCharacterHPGrowth.Size = new System.Drawing.Size(140, 48);
            this.panelCharacterHPGrowth.TabIndex = 87;
            this.panelCharacterHPGrowth.Paint += new System.Windows.Forms.PaintEventHandler(this.panelCharacterHPGrowth_Paint);
            // 
            // comboBoxCharacterHPGrowth
            // 
            this.comboBoxCharacterHPGrowth.AccessibleDescription = "HP Growth over time";
            this.comboBoxCharacterHPGrowth.AccessibleName = "HP Growth";
            this.comboBoxCharacterHPGrowth.FormattingEnabled = true;
            this.comboBoxCharacterHPGrowth.Location = new System.Drawing.Point(22, 46);
            this.comboBoxCharacterHPGrowth.Name = "comboBoxCharacterHPGrowth";
            this.comboBoxCharacterHPGrowth.Size = new System.Drawing.Size(138, 21);
            this.comboBoxCharacterHPGrowth.TabIndex = 63;
            this.comboBoxCharacterHPGrowth.SelectedIndexChanged += new System.EventHandler(this.comboBoxCharacterHPGrowth_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Gainsboro;
            this.groupBox1.Controls.Add(this.label36);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.textBoxCharacterSpriteFace);
            this.groupBox1.Controls.Add(this.textBoxCharacterSpriteIdleFront);
            this.groupBox1.Controls.Add(this.label35);
            this.groupBox1.Controls.Add(this.textBoxCharacterSpriteIdleBack);
            this.groupBox1.Controls.Add(this.textBoxCharacterSpriteSpeed);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.label31);
            this.groupBox1.Controls.Add(this.textBoxCharacterSpriteHitFront);
            this.groupBox1.Controls.Add(this.textBoxCharacterSpriteCast1Back);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.label32);
            this.groupBox1.Controls.Add(this.textBoxCharacterSpriteHitBack);
            this.groupBox1.Controls.Add(this.textBoxCharacterSpriteCast1Front);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Controls.Add(this.label33);
            this.groupBox1.Controls.Add(this.textBoxCharacterSpriteJump0Front);
            this.groupBox1.Controls.Add(this.textBoxCharacterSpriteCast0Back);
            this.groupBox1.Controls.Add(this.label28);
            this.groupBox1.Controls.Add(this.label34);
            this.groupBox1.Controls.Add(this.textBoxCharacterSpriteJump0Back);
            this.groupBox1.Controls.Add(this.textBoxCharacterSpriteCast0Front);
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.label29);
            this.groupBox1.Controls.Add(this.textBoxCharacterSpriteJump1Front);
            this.groupBox1.Controls.Add(this.textBoxCharacterSpriteJump1Back);
            this.groupBox1.Controls.Add(this.label30);
            this.groupBox1.Location = new System.Drawing.Point(657, 288);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(296, 317);
            this.groupBox1.TabIndex = 49;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Sprites";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(151, 270);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(61, 13);
            this.label36.TabIndex = 61;
            this.label36.Text = "Face Sprite";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 16);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(51, 13);
            this.label19.TabIndex = 35;
            this.label19.Text = "Idle Front";
            // 
            // textBoxCharacterSpriteFace
            // 
            this.textBoxCharacterSpriteFace.AccessibleDescription = "Face Portrait Sprite";
            this.textBoxCharacterSpriteFace.AccessibleName = "Face Sprite";
            this.textBoxCharacterSpriteFace.Location = new System.Drawing.Point(154, 286);
            this.textBoxCharacterSpriteFace.Name = "textBoxCharacterSpriteFace";
            this.textBoxCharacterSpriteFace.Size = new System.Drawing.Size(133, 20);
            this.textBoxCharacterSpriteFace.TabIndex = 62;
            this.textBoxCharacterSpriteFace.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // textBoxCharacterSpriteIdleFront
            // 
            this.textBoxCharacterSpriteIdleFront.AccessibleDescription = "Idle Front Sprite";
            this.textBoxCharacterSpriteIdleFront.AccessibleName = "Idle Front Sprite";
            this.textBoxCharacterSpriteIdleFront.Location = new System.Drawing.Point(9, 33);
            this.textBoxCharacterSpriteIdleFront.Name = "textBoxCharacterSpriteIdleFront";
            this.textBoxCharacterSpriteIdleFront.Size = new System.Drawing.Size(133, 20);
            this.textBoxCharacterSpriteIdleFront.TabIndex = 36;
            this.textBoxCharacterSpriteIdleFront.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(6, 270);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(68, 13);
            this.label35.TabIndex = 59;
            this.label35.Text = "Sprite Speed";
            // 
            // textBoxCharacterSpriteIdleBack
            // 
            this.textBoxCharacterSpriteIdleBack.AccessibleDescription = "Idle Back Sprite";
            this.textBoxCharacterSpriteIdleBack.AccessibleName = "Idle Back Sprite";
            this.textBoxCharacterSpriteIdleBack.Location = new System.Drawing.Point(154, 33);
            this.textBoxCharacterSpriteIdleBack.Name = "textBoxCharacterSpriteIdleBack";
            this.textBoxCharacterSpriteIdleBack.Size = new System.Drawing.Size(133, 20);
            this.textBoxCharacterSpriteIdleBack.TabIndex = 38;
            this.textBoxCharacterSpriteIdleBack.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // textBoxCharacterSpriteSpeed
            // 
            this.textBoxCharacterSpriteSpeed.AccessibleDescription = "Sprite Animation Speed";
            this.textBoxCharacterSpriteSpeed.AccessibleName = "Sprite Speed";
            this.textBoxCharacterSpriteSpeed.Location = new System.Drawing.Point(9, 286);
            this.textBoxCharacterSpriteSpeed.Name = "textBoxCharacterSpriteSpeed";
            this.textBoxCharacterSpriteSpeed.Size = new System.Drawing.Size(133, 20);
            this.textBoxCharacterSpriteSpeed.TabIndex = 60;
            this.textBoxCharacterSpriteSpeed.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(151, 16);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(52, 13);
            this.label21.TabIndex = 37;
            this.label21.Text = "Idle Back";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(151, 227);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(62, 13);
            this.label31.TabIndex = 57;
            this.label31.Text = "Cast1 Back";
            // 
            // textBoxCharacterSpriteHitFront
            // 
            this.textBoxCharacterSpriteHitFront.AccessibleDescription = "Hit Front Sprite";
            this.textBoxCharacterSpriteHitFront.AccessibleName = "Hit Front Sprite";
            this.textBoxCharacterSpriteHitFront.Location = new System.Drawing.Point(9, 73);
            this.textBoxCharacterSpriteHitFront.Name = "textBoxCharacterSpriteHitFront";
            this.textBoxCharacterSpriteHitFront.Size = new System.Drawing.Size(133, 20);
            this.textBoxCharacterSpriteHitFront.TabIndex = 40;
            this.textBoxCharacterSpriteHitFront.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // textBoxCharacterSpriteCast1Back
            // 
            this.textBoxCharacterSpriteCast1Back.AccessibleDescription = "Cast1 Back Sprite";
            this.textBoxCharacterSpriteCast1Back.AccessibleName = "Cast1 Back Sprite";
            this.textBoxCharacterSpriteCast1Back.Location = new System.Drawing.Point(154, 244);
            this.textBoxCharacterSpriteCast1Back.Name = "textBoxCharacterSpriteCast1Back";
            this.textBoxCharacterSpriteCast1Back.Size = new System.Drawing.Size(133, 20);
            this.textBoxCharacterSpriteCast1Back.TabIndex = 58;
            this.textBoxCharacterSpriteCast1Back.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(6, 56);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(47, 13);
            this.label25.TabIndex = 39;
            this.label25.Text = "Hit Front";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(6, 227);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(61, 13);
            this.label32.TabIndex = 55;
            this.label32.Text = "Cast1 Front";
            // 
            // textBoxCharacterSpriteHitBack
            // 
            this.textBoxCharacterSpriteHitBack.AccessibleDescription = "Hit Back Sprite";
            this.textBoxCharacterSpriteHitBack.AccessibleName = "Hit Back Sprite";
            this.textBoxCharacterSpriteHitBack.Location = new System.Drawing.Point(154, 73);
            this.textBoxCharacterSpriteHitBack.Name = "textBoxCharacterSpriteHitBack";
            this.textBoxCharacterSpriteHitBack.Size = new System.Drawing.Size(133, 20);
            this.textBoxCharacterSpriteHitBack.TabIndex = 42;
            this.textBoxCharacterSpriteHitBack.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // textBoxCharacterSpriteCast1Front
            // 
            this.textBoxCharacterSpriteCast1Front.AccessibleDescription = "Cast1 Front Sprite";
            this.textBoxCharacterSpriteCast1Front.AccessibleName = "Cast1 Front Sprite";
            this.textBoxCharacterSpriteCast1Front.Location = new System.Drawing.Point(9, 244);
            this.textBoxCharacterSpriteCast1Front.Name = "textBoxCharacterSpriteCast1Front";
            this.textBoxCharacterSpriteCast1Front.Size = new System.Drawing.Size(133, 20);
            this.textBoxCharacterSpriteCast1Front.TabIndex = 56;
            this.textBoxCharacterSpriteCast1Front.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(151, 56);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(48, 13);
            this.label23.TabIndex = 41;
            this.label23.Text = "Hit Back";
            // 
            // label33
            // 
            this.label33.AccessibleDescription = "";
            this.label33.AccessibleName = "";
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(151, 186);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(62, 13);
            this.label33.TabIndex = 53;
            this.label33.Text = "Cast0 Back";
            // 
            // textBoxCharacterSpriteJump0Front
            // 
            this.textBoxCharacterSpriteJump0Front.AccessibleDescription = "Jump0 Front Sprite";
            this.textBoxCharacterSpriteJump0Front.AccessibleName = "Jump0 Front Sprite";
            this.textBoxCharacterSpriteJump0Front.Location = new System.Drawing.Point(9, 122);
            this.textBoxCharacterSpriteJump0Front.Name = "textBoxCharacterSpriteJump0Front";
            this.textBoxCharacterSpriteJump0Front.Size = new System.Drawing.Size(133, 20);
            this.textBoxCharacterSpriteJump0Front.TabIndex = 44;
            this.textBoxCharacterSpriteJump0Front.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // textBoxCharacterSpriteCast0Back
            // 
            this.textBoxCharacterSpriteCast0Back.AccessibleDescription = "Cast0 Back Sprite";
            this.textBoxCharacterSpriteCast0Back.AccessibleName = "Cast0 Back Sprite";
            this.textBoxCharacterSpriteCast0Back.Location = new System.Drawing.Point(154, 203);
            this.textBoxCharacterSpriteCast0Back.Name = "textBoxCharacterSpriteCast0Back";
            this.textBoxCharacterSpriteCast0Back.Size = new System.Drawing.Size(133, 20);
            this.textBoxCharacterSpriteCast0Back.TabIndex = 54;
            this.textBoxCharacterSpriteCast0Back.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(6, 105);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(65, 13);
            this.label28.TabIndex = 43;
            this.label28.Text = "Jump0 Front";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(6, 186);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(61, 13);
            this.label34.TabIndex = 51;
            this.label34.Text = "Cast0 Front";
            // 
            // textBoxCharacterSpriteJump0Back
            // 
            this.textBoxCharacterSpriteJump0Back.AccessibleDescription = "Jump0 Back Sprite";
            this.textBoxCharacterSpriteJump0Back.AccessibleName = "Jump0 Back Sprite";
            this.textBoxCharacterSpriteJump0Back.Location = new System.Drawing.Point(154, 122);
            this.textBoxCharacterSpriteJump0Back.Name = "textBoxCharacterSpriteJump0Back";
            this.textBoxCharacterSpriteJump0Back.Size = new System.Drawing.Size(133, 20);
            this.textBoxCharacterSpriteJump0Back.TabIndex = 46;
            this.textBoxCharacterSpriteJump0Back.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // textBoxCharacterSpriteCast0Front
            // 
            this.textBoxCharacterSpriteCast0Front.AccessibleDescription = "Cast0 Front Sprite";
            this.textBoxCharacterSpriteCast0Front.AccessibleName = "Cast0 Front Sprite";
            this.textBoxCharacterSpriteCast0Front.Location = new System.Drawing.Point(9, 203);
            this.textBoxCharacterSpriteCast0Front.Name = "textBoxCharacterSpriteCast0Front";
            this.textBoxCharacterSpriteCast0Front.Size = new System.Drawing.Size(133, 20);
            this.textBoxCharacterSpriteCast0Front.TabIndex = 52;
            this.textBoxCharacterSpriteCast0Front.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(151, 105);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(66, 13);
            this.label27.TabIndex = 45;
            this.label27.Text = "Jump0 Back";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(151, 146);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(66, 13);
            this.label29.TabIndex = 49;
            this.label29.Text = "Jump1 Back";
            // 
            // textBoxCharacterSpriteJump1Front
            // 
            this.textBoxCharacterSpriteJump1Front.AccessibleDescription = "Jump1 Front Sprite";
            this.textBoxCharacterSpriteJump1Front.AccessibleName = "Jump1 Front Sprite";
            this.textBoxCharacterSpriteJump1Front.Location = new System.Drawing.Point(9, 163);
            this.textBoxCharacterSpriteJump1Front.Name = "textBoxCharacterSpriteJump1Front";
            this.textBoxCharacterSpriteJump1Front.Size = new System.Drawing.Size(133, 20);
            this.textBoxCharacterSpriteJump1Front.TabIndex = 48;
            this.textBoxCharacterSpriteJump1Front.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // textBoxCharacterSpriteJump1Back
            // 
            this.textBoxCharacterSpriteJump1Back.AccessibleDescription = "Jump1 Back Sprite";
            this.textBoxCharacterSpriteJump1Back.AccessibleName = "Jump1 Back Sprite";
            this.textBoxCharacterSpriteJump1Back.Location = new System.Drawing.Point(154, 163);
            this.textBoxCharacterSpriteJump1Back.Name = "textBoxCharacterSpriteJump1Back";
            this.textBoxCharacterSpriteJump1Back.Size = new System.Drawing.Size(133, 20);
            this.textBoxCharacterSpriteJump1Back.TabIndex = 50;
            this.textBoxCharacterSpriteJump1Back.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(6, 146);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(65, 13);
            this.label30.TabIndex = 47;
            this.label30.Text = "Jump1 Front";
            // 
            // buttonCharacterMoveDown
            // 
            this.buttonCharacterMoveDown.AccessibleDescription = "Move a character down in the roster.";
            this.buttonCharacterMoveDown.AccessibleName = "Move down";
            this.buttonCharacterMoveDown.Location = new System.Drawing.Point(6, 595);
            this.buttonCharacterMoveDown.Name = "buttonCharacterMoveDown";
            this.buttonCharacterMoveDown.Size = new System.Drawing.Size(120, 23);
            this.buttonCharacterMoveDown.TabIndex = 4;
            this.buttonCharacterMoveDown.Text = "Move Down";
            this.buttonCharacterMoveDown.UseVisualStyleBackColor = true;
            this.buttonCharacterMoveDown.Click += new System.EventHandler(this.buttonCharacterMoveDown_Click);
            // 
            // buttonCharacterMoveUp
            // 
            this.buttonCharacterMoveUp.AccessibleDescription = "Move a character up in the roster.";
            this.buttonCharacterMoveUp.AccessibleName = "Move up";
            this.buttonCharacterMoveUp.Location = new System.Drawing.Point(6, 568);
            this.buttonCharacterMoveUp.Name = "buttonCharacterMoveUp";
            this.buttonCharacterMoveUp.Size = new System.Drawing.Size(120, 23);
            this.buttonCharacterMoveUp.TabIndex = 3;
            this.buttonCharacterMoveUp.Text = "Move Up";
            this.buttonCharacterMoveUp.UseVisualStyleBackColor = true;
            this.buttonCharacterMoveUp.Click += new System.EventHandler(this.buttonCharacterMoveUp_Click);
            // 
            // buttonCharacterRemove
            // 
            this.buttonCharacterRemove.AccessibleDescription = "Remove a character from the roster.";
            this.buttonCharacterRemove.AccessibleName = "Remove character";
            this.buttonCharacterRemove.Location = new System.Drawing.Point(6, 539);
            this.buttonCharacterRemove.Name = "buttonCharacterRemove";
            this.buttonCharacterRemove.Size = new System.Drawing.Size(120, 23);
            this.buttonCharacterRemove.TabIndex = 2;
            this.buttonCharacterRemove.Text = "Remove";
            this.buttonCharacterRemove.UseVisualStyleBackColor = true;
            this.buttonCharacterRemove.Click += new System.EventHandler(this.buttonCharacterRemove_Click);
            // 
            // buttonCharacterAdd
            // 
            this.buttonCharacterAdd.AccessibleDescription = "Add a new character to the roster.";
            this.buttonCharacterAdd.AccessibleName = "Add Character";
            this.buttonCharacterAdd.Location = new System.Drawing.Point(6, 510);
            this.buttonCharacterAdd.Name = "buttonCharacterAdd";
            this.buttonCharacterAdd.Size = new System.Drawing.Size(120, 23);
            this.buttonCharacterAdd.TabIndex = 1;
            this.buttonCharacterAdd.Text = "Add";
            this.buttonCharacterAdd.UseVisualStyleBackColor = true;
            this.buttonCharacterAdd.Click += new System.EventHandler(this.buttonCharacterAdd_Click);
            // 
            // listBoxCharacters
            // 
            this.listBoxCharacters.AccessibleDescription = "List of Characters in the game.";
            this.listBoxCharacters.AccessibleName = "Character List";
            this.listBoxCharacters.AccessibleRole = System.Windows.Forms.AccessibleRole.List;
            this.listBoxCharacters.FormattingEnabled = true;
            this.listBoxCharacters.Location = new System.Drawing.Point(6, 6);
            this.listBoxCharacters.Name = "listBoxCharacters";
            this.listBoxCharacters.Size = new System.Drawing.Size(120, 498);
            this.listBoxCharacters.TabIndex = 0;
            this.listBoxCharacters.SelectedIndexChanged += new System.EventHandler(this.characterListBox_SelectedIndexChanged);
            // 
            // tabMonsters
            // 
            this.tabMonsters.Controls.Add(this.groupBox32);
            this.tabMonsters.Controls.Add(this.groupBox21);
            this.tabMonsters.Controls.Add(this.label91);
            this.tabMonsters.Controls.Add(this.textBoxEnemyExp);
            this.tabMonsters.Controls.Add(this.label90);
            this.tabMonsters.Controls.Add(this.textBoxEnemyMaxMoney);
            this.tabMonsters.Controls.Add(this.label89);
            this.tabMonsters.Controls.Add(this.textBoxEnemyMinMoney);
            this.tabMonsters.Controls.Add(this.label88);
            this.tabMonsters.Controls.Add(this.textBoxEnemyBribe);
            this.tabMonsters.Controls.Add(this.label87);
            this.tabMonsters.Controls.Add(this.textBoxEnemyDesc);
            this.tabMonsters.Controls.Add(this.label86);
            this.tabMonsters.Controls.Add(this.textBoxEnemyFlavor2);
            this.tabMonsters.Controls.Add(this.label85);
            this.tabMonsters.Controls.Add(this.textBoxEnemyFlavor1);
            this.tabMonsters.Controls.Add(this.label50);
            this.tabMonsters.Controls.Add(this.panelEnemyStat);
            this.tabMonsters.Controls.Add(this.groupBox2);
            this.tabMonsters.Controls.Add(this.labelEnemySpeGuard);
            this.tabMonsters.Controls.Add(this.label64);
            this.tabMonsters.Controls.Add(this.trackBarEnemySpeGuard);
            this.tabMonsters.Controls.Add(this.labelEnemySpePower);
            this.tabMonsters.Controls.Add(this.label66);
            this.tabMonsters.Controls.Add(this.trackBarEnemySpePower);
            this.tabMonsters.Controls.Add(this.labelEnemyMagGuard);
            this.tabMonsters.Controls.Add(this.label68);
            this.tabMonsters.Controls.Add(this.trackBarEnemyMagGuard);
            this.tabMonsters.Controls.Add(this.labelEnemyMagPower);
            this.tabMonsters.Controls.Add(this.label70);
            this.tabMonsters.Controls.Add(this.trackBarEnemyMagPower);
            this.tabMonsters.Controls.Add(this.labelEnemyPhyGuard);
            this.tabMonsters.Controls.Add(this.label72);
            this.tabMonsters.Controls.Add(this.trackBarEnemyPhyGuard);
            this.tabMonsters.Controls.Add(this.labelEnemyPhyPower);
            this.tabMonsters.Controls.Add(this.label74);
            this.tabMonsters.Controls.Add(this.trackBarEnemyPhyPower);
            this.tabMonsters.Controls.Add(this.labelEnemyMP);
            this.tabMonsters.Controls.Add(this.label76);
            this.tabMonsters.Controls.Add(this.trackBarEnemyMP);
            this.tabMonsters.Controls.Add(this.labelEnemyHP);
            this.tabMonsters.Controls.Add(this.label78);
            this.tabMonsters.Controls.Add(this.trackBarEnemyHP);
            this.tabMonsters.Controls.Add(this.label79);
            this.tabMonsters.Controls.Add(this.textBoxEnemyVisualDesc);
            this.tabMonsters.Controls.Add(this.label80);
            this.tabMonsters.Controls.Add(this.textBoxEnemyFlavor0);
            this.tabMonsters.Controls.Add(this.label39);
            this.tabMonsters.Controls.Add(this.comboBoxEnemyType);
            this.tabMonsters.Controls.Add(this.labelEnemyBlue);
            this.tabMonsters.Controls.Add(this.labelEnemyGreen);
            this.tabMonsters.Controls.Add(this.labelEnemyRed);
            this.tabMonsters.Controls.Add(this.label43);
            this.tabMonsters.Controls.Add(this.label44);
            this.tabMonsters.Controls.Add(this.label45);
            this.tabMonsters.Controls.Add(this.textBoxEnemyColor);
            this.tabMonsters.Controls.Add(this.label46);
            this.tabMonsters.Controls.Add(this.label47);
            this.tabMonsters.Controls.Add(this.textBoxEnemyAbility);
            this.tabMonsters.Controls.Add(this.label2);
            this.tabMonsters.Controls.Add(this.textBoxEnemyName);
            this.tabMonsters.Controls.Add(this.buttonMonsterMoveDown);
            this.tabMonsters.Controls.Add(this.buttonMonsterMoveUp);
            this.tabMonsters.Controls.Add(this.buttonMonsterRemove);
            this.tabMonsters.Controls.Add(this.buttonMonsterAdd);
            this.tabMonsters.Controls.Add(this.listBoxMonsters);
            this.tabMonsters.Location = new System.Drawing.Point(4, 22);
            this.tabMonsters.Name = "tabMonsters";
            this.tabMonsters.Padding = new System.Windows.Forms.Padding(3);
            this.tabMonsters.Size = new System.Drawing.Size(1317, 624);
            this.tabMonsters.TabIndex = 1;
            this.tabMonsters.Text = "Monsters";
            this.tabMonsters.UseVisualStyleBackColor = true;
            // 
            // groupBox32
            // 
            this.groupBox32.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox32.Controls.Add(this.checkBoxMonsterIgnoreCry);
            this.groupBox32.Controls.Add(this.checkBoxMonsterIgnoreDex);
            this.groupBox32.Controls.Add(this.checkBoxMonsterFlee);
            this.groupBox32.Controls.Add(this.checkBoxMonsterMove);
            this.groupBox32.Controls.Add(this.checkBoxMonsterJump);
            this.groupBox32.Location = new System.Drawing.Point(135, 456);
            this.groupBox32.Name = "groupBox32";
            this.groupBox32.Size = new System.Drawing.Size(103, 135);
            this.groupBox32.TabIndex = 134;
            this.groupBox32.TabStop = false;
            this.groupBox32.Text = "Triggers";
            // 
            // checkBoxMonsterIgnoreCry
            // 
            this.checkBoxMonsterIgnoreCry.AutoSize = true;
            this.checkBoxMonsterIgnoreCry.Location = new System.Drawing.Point(8, 110);
            this.checkBoxMonsterIgnoreCry.Name = "checkBoxMonsterIgnoreCry";
            this.checkBoxMonsterIgnoreCry.Size = new System.Drawing.Size(74, 17);
            this.checkBoxMonsterIgnoreCry.TabIndex = 4;
            this.checkBoxMonsterIgnoreCry.Text = "Ignore Cry";
            this.checkBoxMonsterIgnoreCry.UseVisualStyleBackColor = true;
            // 
            // checkBoxMonsterIgnoreDex
            // 
            this.checkBoxMonsterIgnoreDex.AutoSize = true;
            this.checkBoxMonsterIgnoreDex.Location = new System.Drawing.Point(8, 87);
            this.checkBoxMonsterIgnoreDex.Name = "checkBoxMonsterIgnoreDex";
            this.checkBoxMonsterIgnoreDex.Size = new System.Drawing.Size(78, 17);
            this.checkBoxMonsterIgnoreDex.TabIndex = 3;
            this.checkBoxMonsterIgnoreDex.Text = "Ignore Dex";
            this.checkBoxMonsterIgnoreDex.UseVisualStyleBackColor = true;
            // 
            // checkBoxMonsterFlee
            // 
            this.checkBoxMonsterFlee.AutoSize = true;
            this.checkBoxMonsterFlee.Location = new System.Drawing.Point(8, 66);
            this.checkBoxMonsterFlee.Name = "checkBoxMonsterFlee";
            this.checkBoxMonsterFlee.Size = new System.Drawing.Size(73, 17);
            this.checkBoxMonsterFlee.TabIndex = 2;
            this.checkBoxMonsterFlee.Text = "Can\'t Flee";
            this.checkBoxMonsterFlee.UseVisualStyleBackColor = true;
            // 
            // checkBoxMonsterMove
            // 
            this.checkBoxMonsterMove.AutoSize = true;
            this.checkBoxMonsterMove.Location = new System.Drawing.Point(8, 43);
            this.checkBoxMonsterMove.Name = "checkBoxMonsterMove";
            this.checkBoxMonsterMove.Size = new System.Drawing.Size(80, 17);
            this.checkBoxMonsterMove.TabIndex = 1;
            this.checkBoxMonsterMove.Text = "Can\'t Move";
            this.checkBoxMonsterMove.UseVisualStyleBackColor = true;
            // 
            // checkBoxMonsterJump
            // 
            this.checkBoxMonsterJump.AutoSize = true;
            this.checkBoxMonsterJump.Location = new System.Drawing.Point(8, 20);
            this.checkBoxMonsterJump.Name = "checkBoxMonsterJump";
            this.checkBoxMonsterJump.Size = new System.Drawing.Size(78, 17);
            this.checkBoxMonsterJump.TabIndex = 0;
            this.checkBoxMonsterJump.Text = "Can\'t Jump";
            this.checkBoxMonsterJump.UseVisualStyleBackColor = true;
            // 
            // groupBox21
            // 
            this.groupBox21.BackColor = System.Drawing.Color.Gainsboro;
            this.groupBox21.Controls.Add(this.listBoxEncounters);
            this.groupBox21.Controls.Add(this.checkBoxEncounterRandom);
            this.groupBox21.Controls.Add(this.label218);
            this.groupBox21.Controls.Add(this.textBoxEncounterName);
            this.groupBox21.Controls.Add(this.label217);
            this.groupBox21.Controls.Add(this.textBoxEncounterEventDisabled);
            this.groupBox21.Controls.Add(this.label216);
            this.groupBox21.Controls.Add(this.textBoxEncounterEventEnabled);
            this.groupBox21.Controls.Add(this.label215);
            this.groupBox21.Controls.Add(this.textBoxEncounterEnemy3);
            this.groupBox21.Controls.Add(this.label214);
            this.groupBox21.Controls.Add(this.textBoxEncounterEnemy2);
            this.groupBox21.Controls.Add(this.label213);
            this.groupBox21.Controls.Add(this.textBoxEncounterEnemy1);
            this.groupBox21.Controls.Add(this.buttonEncounterMoveDown);
            this.groupBox21.Controls.Add(this.buttonEncounterAdd);
            this.groupBox21.Controls.Add(this.buttonEncounterMoveUp);
            this.groupBox21.Controls.Add(this.buttonEncounterRemove);
            this.groupBox21.Location = new System.Drawing.Point(939, 362);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(372, 251);
            this.groupBox21.TabIndex = 133;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "Encounters";
            // 
            // listBoxEncounters
            // 
            this.listBoxEncounters.AccessibleRole = System.Windows.Forms.AccessibleRole.List;
            this.listBoxEncounters.FormattingEnabled = true;
            this.listBoxEncounters.Location = new System.Drawing.Point(7, 20);
            this.listBoxEncounters.Name = "listBoxEncounters";
            this.listBoxEncounters.Size = new System.Drawing.Size(166, 160);
            this.listBoxEncounters.TabIndex = 134;
            // 
            // checkBoxEncounterRandom
            // 
            this.checkBoxEncounterRandom.AutoSize = true;
            this.checkBoxEncounterRandom.Location = new System.Drawing.Point(182, 216);
            this.checkBoxEncounterRandom.Name = "checkBoxEncounterRandom";
            this.checkBoxEncounterRandom.Size = new System.Drawing.Size(120, 17);
            this.checkBoxEncounterRandom.TabIndex = 149;
            this.checkBoxEncounterRandom.Text = "Randomized Order?";
            this.checkBoxEncounterRandom.UseVisualStyleBackColor = true;
            // 
            // label218
            // 
            this.label218.AutoSize = true;
            this.label218.Location = new System.Drawing.Point(179, 10);
            this.label218.Name = "label218";
            this.label218.Size = new System.Drawing.Size(35, 13);
            this.label218.TabIndex = 148;
            this.label218.Text = "Name";
            // 
            // textBoxEncounterName
            // 
            this.textBoxEncounterName.Location = new System.Drawing.Point(182, 27);
            this.textBoxEncounterName.Name = "textBoxEncounterName";
            this.textBoxEncounterName.Size = new System.Drawing.Size(133, 20);
            this.textBoxEncounterName.TabIndex = 147;
            this.textBoxEncounterName.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            this.textBoxEncounterName.Leave += new System.EventHandler(this.textBoxEncounterName_Leave);
            // 
            // label217
            // 
            this.label217.AutoSize = true;
            this.label217.Location = new System.Drawing.Point(270, 170);
            this.label217.Name = "label217";
            this.label217.Size = new System.Drawing.Size(79, 13);
            this.label217.TabIndex = 146;
            this.label217.Text = "Event Disabled";
            // 
            // textBoxEncounterEventDisabled
            // 
            this.textBoxEncounterEventDisabled.Location = new System.Drawing.Point(273, 187);
            this.textBoxEncounterEventDisabled.Name = "textBoxEncounterEventDisabled";
            this.textBoxEncounterEventDisabled.Size = new System.Drawing.Size(85, 20);
            this.textBoxEncounterEventDisabled.TabIndex = 145;
            this.textBoxEncounterEventDisabled.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label216
            // 
            this.label216.AutoSize = true;
            this.label216.Location = new System.Drawing.Point(179, 170);
            this.label216.Name = "label216";
            this.label216.Size = new System.Drawing.Size(77, 13);
            this.label216.TabIndex = 144;
            this.label216.Text = "Event Enabled";
            // 
            // textBoxEncounterEventEnabled
            // 
            this.textBoxEncounterEventEnabled.Location = new System.Drawing.Point(182, 187);
            this.textBoxEncounterEventEnabled.Name = "textBoxEncounterEventEnabled";
            this.textBoxEncounterEventEnabled.Size = new System.Drawing.Size(85, 20);
            this.textBoxEncounterEventEnabled.TabIndex = 143;
            this.textBoxEncounterEventEnabled.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label215
            // 
            this.label215.AutoSize = true;
            this.label215.Location = new System.Drawing.Point(179, 130);
            this.label215.Name = "label215";
            this.label215.Size = new System.Drawing.Size(48, 13);
            this.label215.TabIndex = 142;
            this.label215.Text = "Enemy 3";
            // 
            // textBoxEncounterEnemy3
            // 
            this.textBoxEncounterEnemy3.Location = new System.Drawing.Point(182, 147);
            this.textBoxEncounterEnemy3.Name = "textBoxEncounterEnemy3";
            this.textBoxEncounterEnemy3.Size = new System.Drawing.Size(133, 20);
            this.textBoxEncounterEnemy3.TabIndex = 141;
            this.textBoxEncounterEnemy3.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label214
            // 
            this.label214.AutoSize = true;
            this.label214.Location = new System.Drawing.Point(180, 91);
            this.label214.Name = "label214";
            this.label214.Size = new System.Drawing.Size(48, 13);
            this.label214.TabIndex = 140;
            this.label214.Text = "Enemy 2";
            // 
            // textBoxEncounterEnemy2
            // 
            this.textBoxEncounterEnemy2.Location = new System.Drawing.Point(183, 108);
            this.textBoxEncounterEnemy2.Name = "textBoxEncounterEnemy2";
            this.textBoxEncounterEnemy2.Size = new System.Drawing.Size(133, 20);
            this.textBoxEncounterEnemy2.TabIndex = 139;
            this.textBoxEncounterEnemy2.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label213
            // 
            this.label213.AutoSize = true;
            this.label213.Location = new System.Drawing.Point(179, 51);
            this.label213.Name = "label213";
            this.label213.Size = new System.Drawing.Size(48, 13);
            this.label213.TabIndex = 81;
            this.label213.Text = "Enemy 1";
            // 
            // textBoxEncounterEnemy1
            // 
            this.textBoxEncounterEnemy1.Location = new System.Drawing.Point(182, 68);
            this.textBoxEncounterEnemy1.Name = "textBoxEncounterEnemy1";
            this.textBoxEncounterEnemy1.Size = new System.Drawing.Size(133, 20);
            this.textBoxEncounterEnemy1.TabIndex = 80;
            this.textBoxEncounterEnemy1.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // buttonEncounterMoveDown
            // 
            this.buttonEncounterMoveDown.Location = new System.Drawing.Point(92, 214);
            this.buttonEncounterMoveDown.Name = "buttonEncounterMoveDown";
            this.buttonEncounterMoveDown.Size = new System.Drawing.Size(80, 23);
            this.buttonEncounterMoveDown.TabIndex = 138;
            this.buttonEncounterMoveDown.Text = "Move Down";
            this.buttonEncounterMoveDown.UseVisualStyleBackColor = true;
            this.buttonEncounterMoveDown.Click += new System.EventHandler(this.buttonEncounterMoveDown_Click);
            // 
            // buttonEncounterAdd
            // 
            this.buttonEncounterAdd.Location = new System.Drawing.Point(6, 187);
            this.buttonEncounterAdd.Name = "buttonEncounterAdd";
            this.buttonEncounterAdd.Size = new System.Drawing.Size(80, 23);
            this.buttonEncounterAdd.TabIndex = 135;
            this.buttonEncounterAdd.Text = "Add";
            this.buttonEncounterAdd.UseVisualStyleBackColor = true;
            this.buttonEncounterAdd.Click += new System.EventHandler(this.buttonEncounterAdd_Click);
            // 
            // buttonEncounterMoveUp
            // 
            this.buttonEncounterMoveUp.Location = new System.Drawing.Point(92, 187);
            this.buttonEncounterMoveUp.Name = "buttonEncounterMoveUp";
            this.buttonEncounterMoveUp.Size = new System.Drawing.Size(80, 23);
            this.buttonEncounterMoveUp.TabIndex = 137;
            this.buttonEncounterMoveUp.Text = "Move Up";
            this.buttonEncounterMoveUp.UseVisualStyleBackColor = true;
            this.buttonEncounterMoveUp.Click += new System.EventHandler(this.buttonEncounterMoveUp_Click);
            // 
            // buttonEncounterRemove
            // 
            this.buttonEncounterRemove.Location = new System.Drawing.Point(6, 216);
            this.buttonEncounterRemove.Name = "buttonEncounterRemove";
            this.buttonEncounterRemove.Size = new System.Drawing.Size(80, 23);
            this.buttonEncounterRemove.TabIndex = 136;
            this.buttonEncounterRemove.Text = "Remove";
            this.buttonEncounterRemove.UseVisualStyleBackColor = true;
            this.buttonEncounterRemove.Click += new System.EventHandler(this.buttonEncounterRemove_Click);
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Location = new System.Drawing.Point(1170, 125);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(28, 13);
            this.label91.TabIndex = 132;
            this.label91.Text = "EXP";
            // 
            // textBoxEnemyExp
            // 
            this.textBoxEnemyExp.Location = new System.Drawing.Point(1173, 142);
            this.textBoxEnemyExp.Name = "textBoxEnemyExp";
            this.textBoxEnemyExp.Size = new System.Drawing.Size(58, 20);
            this.textBoxEnemyExp.TabIndex = 131;
            this.textBoxEnemyExp.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(1169, 85);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(62, 13);
            this.label90.TabIndex = 130;
            this.label90.Text = "Max Money";
            // 
            // textBoxEnemyMaxMoney
            // 
            this.textBoxEnemyMaxMoney.Location = new System.Drawing.Point(1172, 102);
            this.textBoxEnemyMaxMoney.Name = "textBoxEnemyMaxMoney";
            this.textBoxEnemyMaxMoney.Size = new System.Drawing.Size(58, 20);
            this.textBoxEnemyMaxMoney.TabIndex = 129;
            this.textBoxEnemyMaxMoney.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(1167, 45);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(59, 13);
            this.label89.TabIndex = 128;
            this.label89.Text = "Min Money";
            // 
            // textBoxEnemyMinMoney
            // 
            this.textBoxEnemyMinMoney.Location = new System.Drawing.Point(1170, 62);
            this.textBoxEnemyMinMoney.Name = "textBoxEnemyMinMoney";
            this.textBoxEnemyMinMoney.Size = new System.Drawing.Size(58, 20);
            this.textBoxEnemyMinMoney.TabIndex = 127;
            this.textBoxEnemyMinMoney.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(1167, 6);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(70, 13);
            this.label88.TabIndex = 81;
            this.label88.Text = "Bribe Amount";
            // 
            // textBoxEnemyBribe
            // 
            this.textBoxEnemyBribe.Location = new System.Drawing.Point(1170, 23);
            this.textBoxEnemyBribe.Name = "textBoxEnemyBribe";
            this.textBoxEnemyBribe.Size = new System.Drawing.Size(58, 20);
            this.textBoxEnemyBribe.TabIndex = 80;
            this.textBoxEnemyBribe.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(134, 127);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(60, 13);
            this.label87.TabIndex = 126;
            this.label87.Text = "Description";
            // 
            // textBoxEnemyDesc
            // 
            this.textBoxEnemyDesc.Location = new System.Drawing.Point(134, 144);
            this.textBoxEnemyDesc.Multiline = true;
            this.textBoxEnemyDesc.Name = "textBoxEnemyDesc";
            this.textBoxEnemyDesc.Size = new System.Drawing.Size(227, 178);
            this.textBoxEnemyDesc.TabIndex = 125;
            this.textBoxEnemyDesc.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(939, 132);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(45, 13);
            this.label86.TabIndex = 124;
            this.label86.Text = "Flavor 1";
            // 
            // textBoxEnemyFlavor2
            // 
            this.textBoxEnemyFlavor2.Location = new System.Drawing.Point(939, 149);
            this.textBoxEnemyFlavor2.Multiline = true;
            this.textBoxEnemyFlavor2.Name = "textBoxEnemyFlavor2";
            this.textBoxEnemyFlavor2.Size = new System.Drawing.Size(222, 43);
            this.textBoxEnemyFlavor2.TabIndex = 123;
            this.textBoxEnemyFlavor2.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(939, 69);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(45, 13);
            this.label85.TabIndex = 122;
            this.label85.Text = "Flavor 1";
            // 
            // textBoxEnemyFlavor1
            // 
            this.textBoxEnemyFlavor1.Location = new System.Drawing.Point(939, 86);
            this.textBoxEnemyFlavor1.Multiline = true;
            this.textBoxEnemyFlavor1.Name = "textBoxEnemyFlavor1";
            this.textBoxEnemyFlavor1.Size = new System.Drawing.Size(222, 43);
            this.textBoxEnemyFlavor1.TabIndex = 121;
            this.textBoxEnemyFlavor1.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(674, 6);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(57, 13);
            this.label50.TabIndex = 120;
            this.label50.Text = "Stat Visual";
            // 
            // panelEnemyStat
            // 
            this.panelEnemyStat.BackColor = System.Drawing.Color.Black;
            this.panelEnemyStat.Location = new System.Drawing.Point(671, 22);
            this.panelEnemyStat.Name = "panelEnemyStat";
            this.panelEnemyStat.Size = new System.Drawing.Size(190, 190);
            this.panelEnemyStat.TabIndex = 119;
            this.panelEnemyStat.Paint += new System.Windows.Forms.PaintEventHandler(this.panelEnemyStat_Paint);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Gainsboro;
            this.groupBox2.Controls.Add(this.label83);
            this.groupBox2.Controls.Add(this.textBoxEnemySpriteShadowY);
            this.groupBox2.Controls.Add(this.label81);
            this.groupBox2.Controls.Add(this.textBoxEnemySpriteZoomY);
            this.groupBox2.Controls.Add(this.label82);
            this.groupBox2.Controls.Add(this.textBoxEnemySpriteZoomX);
            this.groupBox2.Controls.Add(this.label48);
            this.groupBox2.Controls.Add(this.label49);
            this.groupBox2.Controls.Add(this.textBoxEnemySpriteScale);
            this.groupBox2.Controls.Add(this.textBoxEnemySpriteIdleFront);
            this.groupBox2.Controls.Add(this.label51);
            this.groupBox2.Controls.Add(this.textBoxEnemySpriteIdleBack);
            this.groupBox2.Controls.Add(this.textBoxEnemySpriteSpeed);
            this.groupBox2.Controls.Add(this.label52);
            this.groupBox2.Controls.Add(this.label53);
            this.groupBox2.Controls.Add(this.textBoxEnemySpriteHitFront);
            this.groupBox2.Controls.Add(this.textBoxEnemySpriteCast1Back);
            this.groupBox2.Controls.Add(this.label54);
            this.groupBox2.Controls.Add(this.label55);
            this.groupBox2.Controls.Add(this.textBoxEnemySpriteHitBack);
            this.groupBox2.Controls.Add(this.textBoxEnemySpriteCast1Front);
            this.groupBox2.Controls.Add(this.label56);
            this.groupBox2.Controls.Add(this.label57);
            this.groupBox2.Controls.Add(this.textBoxEnemySpriteJump0Front);
            this.groupBox2.Controls.Add(this.textBoxEnemySpriteCast0Back);
            this.groupBox2.Controls.Add(this.label58);
            this.groupBox2.Controls.Add(this.label59);
            this.groupBox2.Controls.Add(this.textBoxEnemySpriteJump0Back);
            this.groupBox2.Controls.Add(this.textBoxEnemySpriteCast0Front);
            this.groupBox2.Controls.Add(this.label60);
            this.groupBox2.Controls.Add(this.label61);
            this.groupBox2.Controls.Add(this.textBoxEnemySpriteJump1Front);
            this.groupBox2.Controls.Add(this.textBoxEnemySpriteJump1Back);
            this.groupBox2.Controls.Add(this.label62);
            this.groupBox2.Location = new System.Drawing.Point(621, 220);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(298, 398);
            this.groupBox2.TabIndex = 118;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Sprites";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(6, 348);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(80, 13);
            this.label83.TabIndex = 79;
            this.label83.Text = "Shadow Height";
            // 
            // textBoxEnemySpriteShadowY
            // 
            this.textBoxEnemySpriteShadowY.Location = new System.Drawing.Point(9, 364);
            this.textBoxEnemySpriteShadowY.Name = "textBoxEnemySpriteShadowY";
            this.textBoxEnemySpriteShadowY.Size = new System.Drawing.Size(133, 20);
            this.textBoxEnemySpriteShadowY.TabIndex = 78;
            this.textBoxEnemySpriteShadowY.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(151, 309);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(44, 13);
            this.label81.TabIndex = 77;
            this.label81.Text = "Zoom Y";
            // 
            // textBoxEnemySpriteZoomY
            // 
            this.textBoxEnemySpriteZoomY.Location = new System.Drawing.Point(154, 325);
            this.textBoxEnemySpriteZoomY.Name = "textBoxEnemySpriteZoomY";
            this.textBoxEnemySpriteZoomY.Size = new System.Drawing.Size(133, 20);
            this.textBoxEnemySpriteZoomY.TabIndex = 76;
            this.textBoxEnemySpriteZoomY.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(6, 309);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(44, 13);
            this.label82.TabIndex = 75;
            this.label82.Text = "Zoom X";
            // 
            // textBoxEnemySpriteZoomX
            // 
            this.textBoxEnemySpriteZoomX.Location = new System.Drawing.Point(9, 325);
            this.textBoxEnemySpriteZoomX.Name = "textBoxEnemySpriteZoomX";
            this.textBoxEnemySpriteZoomX.Size = new System.Drawing.Size(133, 20);
            this.textBoxEnemySpriteZoomX.TabIndex = 74;
            this.textBoxEnemySpriteZoomX.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(151, 270);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(64, 13);
            this.label48.TabIndex = 73;
            this.label48.Text = "Sprite Scale";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(6, 16);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(51, 13);
            this.label49.TabIndex = 47;
            this.label49.Text = "Idle Front";
            // 
            // textBoxEnemySpriteScale
            // 
            this.textBoxEnemySpriteScale.Location = new System.Drawing.Point(154, 286);
            this.textBoxEnemySpriteScale.Name = "textBoxEnemySpriteScale";
            this.textBoxEnemySpriteScale.Size = new System.Drawing.Size(133, 20);
            this.textBoxEnemySpriteScale.TabIndex = 72;
            this.textBoxEnemySpriteScale.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // textBoxEnemySpriteIdleFront
            // 
            this.textBoxEnemySpriteIdleFront.Location = new System.Drawing.Point(9, 33);
            this.textBoxEnemySpriteIdleFront.Name = "textBoxEnemySpriteIdleFront";
            this.textBoxEnemySpriteIdleFront.Size = new System.Drawing.Size(133, 20);
            this.textBoxEnemySpriteIdleFront.TabIndex = 46;
            this.textBoxEnemySpriteIdleFront.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(6, 270);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(68, 13);
            this.label51.TabIndex = 71;
            this.label51.Text = "Sprite Speed";
            // 
            // textBoxEnemySpriteIdleBack
            // 
            this.textBoxEnemySpriteIdleBack.Location = new System.Drawing.Point(154, 33);
            this.textBoxEnemySpriteIdleBack.Name = "textBoxEnemySpriteIdleBack";
            this.textBoxEnemySpriteIdleBack.Size = new System.Drawing.Size(133, 20);
            this.textBoxEnemySpriteIdleBack.TabIndex = 48;
            this.textBoxEnemySpriteIdleBack.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // textBoxEnemySpriteSpeed
            // 
            this.textBoxEnemySpriteSpeed.Location = new System.Drawing.Point(9, 286);
            this.textBoxEnemySpriteSpeed.Name = "textBoxEnemySpriteSpeed";
            this.textBoxEnemySpriteSpeed.Size = new System.Drawing.Size(133, 20);
            this.textBoxEnemySpriteSpeed.TabIndex = 70;
            this.textBoxEnemySpriteSpeed.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(151, 16);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(52, 13);
            this.label52.TabIndex = 49;
            this.label52.Text = "Idle Back";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(151, 227);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(62, 13);
            this.label53.TabIndex = 69;
            this.label53.Text = "Cast1 Back";
            // 
            // textBoxEnemySpriteHitFront
            // 
            this.textBoxEnemySpriteHitFront.Location = new System.Drawing.Point(9, 73);
            this.textBoxEnemySpriteHitFront.Name = "textBoxEnemySpriteHitFront";
            this.textBoxEnemySpriteHitFront.Size = new System.Drawing.Size(133, 20);
            this.textBoxEnemySpriteHitFront.TabIndex = 50;
            this.textBoxEnemySpriteHitFront.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // textBoxEnemySpriteCast1Back
            // 
            this.textBoxEnemySpriteCast1Back.Location = new System.Drawing.Point(154, 244);
            this.textBoxEnemySpriteCast1Back.Name = "textBoxEnemySpriteCast1Back";
            this.textBoxEnemySpriteCast1Back.Size = new System.Drawing.Size(133, 20);
            this.textBoxEnemySpriteCast1Back.TabIndex = 68;
            this.textBoxEnemySpriteCast1Back.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(6, 56);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(47, 13);
            this.label54.TabIndex = 51;
            this.label54.Text = "Hit Front";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(6, 227);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(61, 13);
            this.label55.TabIndex = 67;
            this.label55.Text = "Cast1 Front";
            // 
            // textBoxEnemySpriteHitBack
            // 
            this.textBoxEnemySpriteHitBack.Location = new System.Drawing.Point(154, 73);
            this.textBoxEnemySpriteHitBack.Name = "textBoxEnemySpriteHitBack";
            this.textBoxEnemySpriteHitBack.Size = new System.Drawing.Size(133, 20);
            this.textBoxEnemySpriteHitBack.TabIndex = 52;
            this.textBoxEnemySpriteHitBack.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // textBoxEnemySpriteCast1Front
            // 
            this.textBoxEnemySpriteCast1Front.Location = new System.Drawing.Point(9, 244);
            this.textBoxEnemySpriteCast1Front.Name = "textBoxEnemySpriteCast1Front";
            this.textBoxEnemySpriteCast1Front.Size = new System.Drawing.Size(133, 20);
            this.textBoxEnemySpriteCast1Front.TabIndex = 66;
            this.textBoxEnemySpriteCast1Front.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(151, 56);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(48, 13);
            this.label56.TabIndex = 53;
            this.label56.Text = "Hit Back";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(151, 186);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(62, 13);
            this.label57.TabIndex = 65;
            this.label57.Text = "Cast0 Back";
            // 
            // textBoxEnemySpriteJump0Front
            // 
            this.textBoxEnemySpriteJump0Front.Location = new System.Drawing.Point(9, 122);
            this.textBoxEnemySpriteJump0Front.Name = "textBoxEnemySpriteJump0Front";
            this.textBoxEnemySpriteJump0Front.Size = new System.Drawing.Size(133, 20);
            this.textBoxEnemySpriteJump0Front.TabIndex = 54;
            this.textBoxEnemySpriteJump0Front.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // textBoxEnemySpriteCast0Back
            // 
            this.textBoxEnemySpriteCast0Back.Location = new System.Drawing.Point(154, 203);
            this.textBoxEnemySpriteCast0Back.Name = "textBoxEnemySpriteCast0Back";
            this.textBoxEnemySpriteCast0Back.Size = new System.Drawing.Size(133, 20);
            this.textBoxEnemySpriteCast0Back.TabIndex = 64;
            this.textBoxEnemySpriteCast0Back.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(6, 105);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(65, 13);
            this.label58.TabIndex = 55;
            this.label58.Text = "Jump0 Front";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(6, 186);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(61, 13);
            this.label59.TabIndex = 63;
            this.label59.Text = "Cast0 Front";
            // 
            // textBoxEnemySpriteJump0Back
            // 
            this.textBoxEnemySpriteJump0Back.Location = new System.Drawing.Point(154, 122);
            this.textBoxEnemySpriteJump0Back.Name = "textBoxEnemySpriteJump0Back";
            this.textBoxEnemySpriteJump0Back.Size = new System.Drawing.Size(133, 20);
            this.textBoxEnemySpriteJump0Back.TabIndex = 56;
            this.textBoxEnemySpriteJump0Back.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // textBoxEnemySpriteCast0Front
            // 
            this.textBoxEnemySpriteCast0Front.Location = new System.Drawing.Point(9, 203);
            this.textBoxEnemySpriteCast0Front.Name = "textBoxEnemySpriteCast0Front";
            this.textBoxEnemySpriteCast0Front.Size = new System.Drawing.Size(133, 20);
            this.textBoxEnemySpriteCast0Front.TabIndex = 62;
            this.textBoxEnemySpriteCast0Front.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(151, 105);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(66, 13);
            this.label60.TabIndex = 57;
            this.label60.Text = "Jump0 Back";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(151, 146);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(66, 13);
            this.label61.TabIndex = 61;
            this.label61.Text = "Jump1 Back";
            // 
            // textBoxEnemySpriteJump1Front
            // 
            this.textBoxEnemySpriteJump1Front.Location = new System.Drawing.Point(9, 163);
            this.textBoxEnemySpriteJump1Front.Name = "textBoxEnemySpriteJump1Front";
            this.textBoxEnemySpriteJump1Front.Size = new System.Drawing.Size(133, 20);
            this.textBoxEnemySpriteJump1Front.TabIndex = 58;
            this.textBoxEnemySpriteJump1Front.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // textBoxEnemySpriteJump1Back
            // 
            this.textBoxEnemySpriteJump1Back.Location = new System.Drawing.Point(154, 163);
            this.textBoxEnemySpriteJump1Back.Name = "textBoxEnemySpriteJump1Back";
            this.textBoxEnemySpriteJump1Back.Size = new System.Drawing.Size(133, 20);
            this.textBoxEnemySpriteJump1Back.TabIndex = 60;
            this.textBoxEnemySpriteJump1Back.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(6, 146);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(65, 13);
            this.label62.TabIndex = 59;
            this.label62.Text = "Jump1 Front";
            // 
            // labelEnemySpeGuard
            // 
            this.labelEnemySpeGuard.AutoSize = true;
            this.labelEnemySpeGuard.Location = new System.Drawing.Point(592, 530);
            this.labelEnemySpeGuard.Name = "labelEnemySpeGuard";
            this.labelEnemySpeGuard.Size = new System.Drawing.Size(13, 13);
            this.labelEnemySpeGuard.TabIndex = 117;
            this.labelEnemySpeGuard.Text = "0";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(371, 507);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(70, 13);
            this.label64.TabIndex = 116;
            this.label64.Text = "Speed Guard";
            // 
            // trackBarEnemySpeGuard
            // 
            this.trackBarEnemySpeGuard.LargeChange = 200;
            this.trackBarEnemySpeGuard.Location = new System.Drawing.Point(374, 530);
            this.trackBarEnemySpeGuard.Maximum = 20;
            this.trackBarEnemySpeGuard.Name = "trackBarEnemySpeGuard";
            this.trackBarEnemySpeGuard.Size = new System.Drawing.Size(212, 45);
            this.trackBarEnemySpeGuard.SmallChange = 100;
            this.trackBarEnemySpeGuard.TabIndex = 115;
            this.trackBarEnemySpeGuard.Scroll += new System.EventHandler(this.trackBarEnemySpeGuard_Scroll);
            // 
            // labelEnemySpePower
            // 
            this.labelEnemySpePower.AutoSize = true;
            this.labelEnemySpePower.Location = new System.Drawing.Point(592, 460);
            this.labelEnemySpePower.Name = "labelEnemySpePower";
            this.labelEnemySpePower.Size = new System.Drawing.Size(13, 13);
            this.labelEnemySpePower.TabIndex = 114;
            this.labelEnemySpePower.Text = "0";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(371, 437);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(71, 13);
            this.label66.TabIndex = 113;
            this.label66.Text = "Speed Power";
            // 
            // trackBarEnemySpePower
            // 
            this.trackBarEnemySpePower.LargeChange = 200;
            this.trackBarEnemySpePower.Location = new System.Drawing.Point(374, 460);
            this.trackBarEnemySpePower.Maximum = 500;
            this.trackBarEnemySpePower.Name = "trackBarEnemySpePower";
            this.trackBarEnemySpePower.Size = new System.Drawing.Size(212, 45);
            this.trackBarEnemySpePower.SmallChange = 100;
            this.trackBarEnemySpePower.TabIndex = 112;
            this.trackBarEnemySpePower.TickFrequency = 25;
            this.trackBarEnemySpePower.Scroll += new System.EventHandler(this.trackBarEnemySpePower_Scroll);
            // 
            // labelEnemyMagGuard
            // 
            this.labelEnemyMagGuard.AutoSize = true;
            this.labelEnemyMagGuard.Location = new System.Drawing.Point(592, 385);
            this.labelEnemyMagGuard.Name = "labelEnemyMagGuard";
            this.labelEnemyMagGuard.Size = new System.Drawing.Size(13, 13);
            this.labelEnemyMagGuard.TabIndex = 111;
            this.labelEnemyMagGuard.Text = "0";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(371, 362);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(68, 13);
            this.label68.TabIndex = 110;
            this.label68.Text = "Magic Guard";
            // 
            // trackBarEnemyMagGuard
            // 
            this.trackBarEnemyMagGuard.LargeChange = 200;
            this.trackBarEnemyMagGuard.Location = new System.Drawing.Point(374, 385);
            this.trackBarEnemyMagGuard.Maximum = 500;
            this.trackBarEnemyMagGuard.Name = "trackBarEnemyMagGuard";
            this.trackBarEnemyMagGuard.Size = new System.Drawing.Size(212, 45);
            this.trackBarEnemyMagGuard.SmallChange = 100;
            this.trackBarEnemyMagGuard.TabIndex = 109;
            this.trackBarEnemyMagGuard.TickFrequency = 25;
            this.trackBarEnemyMagGuard.Scroll += new System.EventHandler(this.trackBarEnemyMagGuard_Scroll);
            // 
            // labelEnemyMagPower
            // 
            this.labelEnemyMagPower.AutoSize = true;
            this.labelEnemyMagPower.Location = new System.Drawing.Point(592, 317);
            this.labelEnemyMagPower.Name = "labelEnemyMagPower";
            this.labelEnemyMagPower.Size = new System.Drawing.Size(13, 13);
            this.labelEnemyMagPower.TabIndex = 108;
            this.labelEnemyMagPower.Text = "0";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(371, 294);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(69, 13);
            this.label70.TabIndex = 107;
            this.label70.Text = "Magic Power";
            // 
            // trackBarEnemyMagPower
            // 
            this.trackBarEnemyMagPower.LargeChange = 200;
            this.trackBarEnemyMagPower.Location = new System.Drawing.Point(374, 317);
            this.trackBarEnemyMagPower.Maximum = 500;
            this.trackBarEnemyMagPower.Name = "trackBarEnemyMagPower";
            this.trackBarEnemyMagPower.Size = new System.Drawing.Size(212, 45);
            this.trackBarEnemyMagPower.SmallChange = 100;
            this.trackBarEnemyMagPower.TabIndex = 106;
            this.trackBarEnemyMagPower.TickFrequency = 25;
            this.trackBarEnemyMagPower.Scroll += new System.EventHandler(this.trackBarEnemyMagPower_Scroll);
            // 
            // labelEnemyPhyGuard
            // 
            this.labelEnemyPhyGuard.AutoSize = true;
            this.labelEnemyPhyGuard.Location = new System.Drawing.Point(592, 243);
            this.labelEnemyPhyGuard.Name = "labelEnemyPhyGuard";
            this.labelEnemyPhyGuard.Size = new System.Drawing.Size(13, 13);
            this.labelEnemyPhyGuard.TabIndex = 105;
            this.labelEnemyPhyGuard.Text = "0";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(371, 220);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(62, 13);
            this.label72.TabIndex = 104;
            this.label72.Text = "Phys Guard";
            // 
            // trackBarEnemyPhyGuard
            // 
            this.trackBarEnemyPhyGuard.LargeChange = 200;
            this.trackBarEnemyPhyGuard.Location = new System.Drawing.Point(374, 243);
            this.trackBarEnemyPhyGuard.Maximum = 500;
            this.trackBarEnemyPhyGuard.Name = "trackBarEnemyPhyGuard";
            this.trackBarEnemyPhyGuard.Size = new System.Drawing.Size(212, 45);
            this.trackBarEnemyPhyGuard.SmallChange = 100;
            this.trackBarEnemyPhyGuard.TabIndex = 103;
            this.trackBarEnemyPhyGuard.TickFrequency = 25;
            this.trackBarEnemyPhyGuard.Scroll += new System.EventHandler(this.trackBarEnemyPhyGuard_Scroll);
            // 
            // labelEnemyPhyPower
            // 
            this.labelEnemyPhyPower.AutoSize = true;
            this.labelEnemyPhyPower.Location = new System.Drawing.Point(592, 174);
            this.labelEnemyPhyPower.Name = "labelEnemyPhyPower";
            this.labelEnemyPhyPower.Size = new System.Drawing.Size(13, 13);
            this.labelEnemyPhyPower.TabIndex = 102;
            this.labelEnemyPhyPower.Text = "0";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(371, 151);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(63, 13);
            this.label74.TabIndex = 101;
            this.label74.Text = "Phys Power";
            // 
            // trackBarEnemyPhyPower
            // 
            this.trackBarEnemyPhyPower.LargeChange = 200;
            this.trackBarEnemyPhyPower.Location = new System.Drawing.Point(374, 174);
            this.trackBarEnemyPhyPower.Maximum = 500;
            this.trackBarEnemyPhyPower.Name = "trackBarEnemyPhyPower";
            this.trackBarEnemyPhyPower.Size = new System.Drawing.Size(212, 45);
            this.trackBarEnemyPhyPower.SmallChange = 100;
            this.trackBarEnemyPhyPower.TabIndex = 100;
            this.trackBarEnemyPhyPower.TickFrequency = 25;
            this.trackBarEnemyPhyPower.Scroll += new System.EventHandler(this.trackBarEnemyPhyPower_Scroll);
            // 
            // labelEnemyMP
            // 
            this.labelEnemyMP.AutoSize = true;
            this.labelEnemyMP.Location = new System.Drawing.Point(592, 103);
            this.labelEnemyMP.Name = "labelEnemyMP";
            this.labelEnemyMP.Size = new System.Drawing.Size(13, 13);
            this.labelEnemyMP.TabIndex = 99;
            this.labelEnemyMP.Text = "0";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(371, 80);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(23, 13);
            this.label76.TabIndex = 98;
            this.label76.Text = "MP";
            // 
            // trackBarEnemyMP
            // 
            this.trackBarEnemyMP.LargeChange = 200;
            this.trackBarEnemyMP.Location = new System.Drawing.Point(374, 103);
            this.trackBarEnemyMP.Maximum = 6000;
            this.trackBarEnemyMP.Name = "trackBarEnemyMP";
            this.trackBarEnemyMP.Size = new System.Drawing.Size(212, 45);
            this.trackBarEnemyMP.TabIndex = 97;
            this.trackBarEnemyMP.TickFrequency = 100;
            this.trackBarEnemyMP.Scroll += new System.EventHandler(this.trackBarEnemyMP_Scroll);
            // 
            // labelEnemyHP
            // 
            this.labelEnemyHP.AutoSize = true;
            this.labelEnemyHP.Location = new System.Drawing.Point(592, 29);
            this.labelEnemyHP.Name = "labelEnemyHP";
            this.labelEnemyHP.Size = new System.Drawing.Size(13, 13);
            this.labelEnemyHP.TabIndex = 96;
            this.labelEnemyHP.Text = "0";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(371, 6);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(22, 13);
            this.label78.TabIndex = 95;
            this.label78.Text = "HP";
            // 
            // trackBarEnemyHP
            // 
            this.trackBarEnemyHP.LargeChange = 200;
            this.trackBarEnemyHP.Location = new System.Drawing.Point(374, 29);
            this.trackBarEnemyHP.Maximum = 5000;
            this.trackBarEnemyHP.Name = "trackBarEnemyHP";
            this.trackBarEnemyHP.Size = new System.Drawing.Size(212, 45);
            this.trackBarEnemyHP.TabIndex = 94;
            this.trackBarEnemyHP.TickFrequency = 100;
            this.trackBarEnemyHP.Scroll += new System.EventHandler(this.trackBarEnemyHP_Scroll);
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(134, 325);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(91, 13);
            this.label79.TabIndex = 93;
            this.label79.Text = "Visual Description";
            // 
            // textBoxEnemyVisualDesc
            // 
            this.textBoxEnemyVisualDesc.Location = new System.Drawing.Point(134, 342);
            this.textBoxEnemyVisualDesc.Multiline = true;
            this.textBoxEnemyVisualDesc.Name = "textBoxEnemyVisualDesc";
            this.textBoxEnemyVisualDesc.Size = new System.Drawing.Size(227, 108);
            this.textBoxEnemyVisualDesc.TabIndex = 92;
            this.textBoxEnemyVisualDesc.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(938, 6);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(45, 13);
            this.label80.TabIndex = 91;
            this.label80.Text = "Flavor 0";
            // 
            // textBoxEnemyFlavor0
            // 
            this.textBoxEnemyFlavor0.Location = new System.Drawing.Point(938, 23);
            this.textBoxEnemyFlavor0.Multiline = true;
            this.textBoxEnemyFlavor0.Name = "textBoxEnemyFlavor0";
            this.textBoxEnemyFlavor0.Size = new System.Drawing.Size(223, 43);
            this.textBoxEnemyFlavor0.TabIndex = 90;
            this.textBoxEnemyFlavor0.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(133, 86);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(31, 13);
            this.label39.TabIndex = 89;
            this.label39.Text = "Type";
            // 
            // comboBoxEnemyType
            // 
            this.comboBoxEnemyType.FormattingEnabled = true;
            this.comboBoxEnemyType.Location = new System.Drawing.Point(135, 103);
            this.comboBoxEnemyType.Name = "comboBoxEnemyType";
            this.comboBoxEnemyType.Size = new System.Drawing.Size(100, 21);
            this.comboBoxEnemyType.TabIndex = 88;
            // 
            // labelEnemyBlue
            // 
            this.labelEnemyBlue.AutoSize = true;
            this.labelEnemyBlue.Location = new System.Drawing.Point(325, 111);
            this.labelEnemyBlue.Name = "labelEnemyBlue";
            this.labelEnemyBlue.Size = new System.Drawing.Size(13, 13);
            this.labelEnemyBlue.TabIndex = 87;
            this.labelEnemyBlue.Text = "0";
            this.labelEnemyBlue.TextChanged += new System.EventHandler(this.labelEnemyBlue_TextChanged);
            // 
            // labelEnemyGreen
            // 
            this.labelEnemyGreen.AutoSize = true;
            this.labelEnemyGreen.Location = new System.Drawing.Point(288, 111);
            this.labelEnemyGreen.Name = "labelEnemyGreen";
            this.labelEnemyGreen.Size = new System.Drawing.Size(13, 13);
            this.labelEnemyGreen.TabIndex = 86;
            this.labelEnemyGreen.Text = "0";
            this.labelEnemyGreen.TextChanged += new System.EventHandler(this.labelEnemyGreen_TextChanged);
            // 
            // labelEnemyRed
            // 
            this.labelEnemyRed.AutoSize = true;
            this.labelEnemyRed.Location = new System.Drawing.Point(253, 111);
            this.labelEnemyRed.Name = "labelEnemyRed";
            this.labelEnemyRed.Size = new System.Drawing.Size(13, 13);
            this.labelEnemyRed.TabIndex = 85;
            this.labelEnemyRed.Text = "0";
            this.labelEnemyRed.TextChanged += new System.EventHandler(this.labelEnemyRed_TextChanged);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(324, 98);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(17, 13);
            this.label43.TabIndex = 84;
            this.label43.Text = "B:";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(287, 98);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(18, 13);
            this.label44.TabIndex = 83;
            this.label44.Text = "G:";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(252, 98);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(18, 13);
            this.label45.TabIndex = 82;
            this.label45.Text = "R:";
            // 
            // textBoxEnemyColor
            // 
            this.textBoxEnemyColor.Location = new System.Drawing.Point(242, 22);
            this.textBoxEnemyColor.Multiline = true;
            this.textBoxEnemyColor.Name = "textBoxEnemyColor";
            this.textBoxEnemyColor.ReadOnly = true;
            this.textBoxEnemyColor.Size = new System.Drawing.Size(113, 71);
            this.textBoxEnemyColor.TabIndex = 81;
            this.textBoxEnemyColor.Click += new System.EventHandler(this.textBoxEnemyColor_Click);
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(239, 6);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(31, 13);
            this.label46.TabIndex = 80;
            this.label46.Text = "Color";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(132, 46);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(34, 13);
            this.label47.TabIndex = 79;
            this.label47.Text = "Ability";
            // 
            // textBoxEnemyAbility
            // 
            this.textBoxEnemyAbility.Location = new System.Drawing.Point(135, 63);
            this.textBoxEnemyAbility.Name = "textBoxEnemyAbility";
            this.textBoxEnemyAbility.Size = new System.Drawing.Size(100, 20);
            this.textBoxEnemyAbility.TabIndex = 78;
            this.textBoxEnemyAbility.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(135, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Name";
            // 
            // textBoxEnemyName
            // 
            this.textBoxEnemyName.Location = new System.Drawing.Point(135, 23);
            this.textBoxEnemyName.Name = "textBoxEnemyName";
            this.textBoxEnemyName.Size = new System.Drawing.Size(100, 20);
            this.textBoxEnemyName.TabIndex = 10;
            this.textBoxEnemyName.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            this.textBoxEnemyName.Leave += new System.EventHandler(this.textBoxEnemyName_Leave);
            // 
            // buttonMonsterMoveDown
            // 
            this.buttonMonsterMoveDown.Location = new System.Drawing.Point(6, 595);
            this.buttonMonsterMoveDown.Name = "buttonMonsterMoveDown";
            this.buttonMonsterMoveDown.Size = new System.Drawing.Size(120, 23);
            this.buttonMonsterMoveDown.TabIndex = 9;
            this.buttonMonsterMoveDown.Text = "Move Down";
            this.buttonMonsterMoveDown.UseVisualStyleBackColor = true;
            this.buttonMonsterMoveDown.Click += new System.EventHandler(this.buttonMonsterMoveDown_Click);
            // 
            // buttonMonsterMoveUp
            // 
            this.buttonMonsterMoveUp.Location = new System.Drawing.Point(6, 568);
            this.buttonMonsterMoveUp.Name = "buttonMonsterMoveUp";
            this.buttonMonsterMoveUp.Size = new System.Drawing.Size(120, 23);
            this.buttonMonsterMoveUp.TabIndex = 8;
            this.buttonMonsterMoveUp.Text = "Move Up";
            this.buttonMonsterMoveUp.UseVisualStyleBackColor = true;
            this.buttonMonsterMoveUp.Click += new System.EventHandler(this.buttonMonsterMoveUp_Click);
            // 
            // buttonMonsterRemove
            // 
            this.buttonMonsterRemove.Location = new System.Drawing.Point(6, 539);
            this.buttonMonsterRemove.Name = "buttonMonsterRemove";
            this.buttonMonsterRemove.Size = new System.Drawing.Size(120, 23);
            this.buttonMonsterRemove.TabIndex = 7;
            this.buttonMonsterRemove.Text = "Remove";
            this.buttonMonsterRemove.UseVisualStyleBackColor = true;
            this.buttonMonsterRemove.Click += new System.EventHandler(this.buttonMonsterRemove_Click);
            // 
            // buttonMonsterAdd
            // 
            this.buttonMonsterAdd.Location = new System.Drawing.Point(6, 510);
            this.buttonMonsterAdd.Name = "buttonMonsterAdd";
            this.buttonMonsterAdd.Size = new System.Drawing.Size(120, 23);
            this.buttonMonsterAdd.TabIndex = 6;
            this.buttonMonsterAdd.Text = "Add";
            this.buttonMonsterAdd.UseVisualStyleBackColor = true;
            this.buttonMonsterAdd.Click += new System.EventHandler(this.buttonMonsterAdd_Click);
            // 
            // listBoxMonsters
            // 
            this.listBoxMonsters.AccessibleRole = System.Windows.Forms.AccessibleRole.List;
            this.listBoxMonsters.FormattingEnabled = true;
            this.listBoxMonsters.Location = new System.Drawing.Point(6, 6);
            this.listBoxMonsters.Name = "listBoxMonsters";
            this.listBoxMonsters.Size = new System.Drawing.Size(120, 498);
            this.listBoxMonsters.TabIndex = 5;
            this.listBoxMonsters.SelectedIndexChanged += new System.EventHandler(this.listBoxMonsters_SelectedIndexChanged);
            // 
            // tabItems
            // 
            this.tabItems.Controls.Add(this.label193);
            this.tabItems.Controls.Add(this.textBoxItemHelp);
            this.tabItems.Controls.Add(this.label167);
            this.tabItems.Controls.Add(this.label166);
            this.tabItems.Controls.Add(this.label145);
            this.tabItems.Controls.Add(this.comboBoxTabletMove);
            this.tabItems.Controls.Add(this.labelTabletSpeGuard);
            this.tabItems.Controls.Add(this.label134);
            this.tabItems.Controls.Add(this.trackBarTabletSpeGuard);
            this.tabItems.Controls.Add(this.labelTabletSpePower);
            this.tabItems.Controls.Add(this.label136);
            this.tabItems.Controls.Add(this.trackBarTabletSpePower);
            this.tabItems.Controls.Add(this.labelTabletMagGuard);
            this.tabItems.Controls.Add(this.label138);
            this.tabItems.Controls.Add(this.trackBarTabletMagGuard);
            this.tabItems.Controls.Add(this.labelTabletMagPower);
            this.tabItems.Controls.Add(this.label140);
            this.tabItems.Controls.Add(this.trackBarTabletMagPower);
            this.tabItems.Controls.Add(this.labelTabletPhyGuard);
            this.tabItems.Controls.Add(this.label142);
            this.tabItems.Controls.Add(this.trackBarTabletPhyGuard);
            this.tabItems.Controls.Add(this.labelTabletPhyPower);
            this.tabItems.Controls.Add(this.label144);
            this.tabItems.Controls.Add(this.trackBarTabletPhyPower);
            this.tabItems.Controls.Add(this.label130);
            this.tabItems.Controls.Add(this.textBoxTabletChar);
            this.tabItems.Controls.Add(this.label131);
            this.tabItems.Controls.Add(this.textBoxTabletDesc);
            this.tabItems.Controls.Add(this.label132);
            this.tabItems.Controls.Add(this.textBoxTabletName);
            this.tabItems.Controls.Add(this.buttonTabletMoveDown);
            this.tabItems.Controls.Add(this.buttonTabletMoveUp);
            this.tabItems.Controls.Add(this.buttonTabletRemove);
            this.tabItems.Controls.Add(this.buttonTabletAdd);
            this.tabItems.Controls.Add(this.listBoxTablet);
            this.tabItems.Controls.Add(this.labelItemSpeGuard);
            this.tabItems.Controls.Add(this.label100);
            this.tabItems.Controls.Add(this.trackBarItemSpeGuard);
            this.tabItems.Controls.Add(this.labelItemSpePower);
            this.tabItems.Controls.Add(this.label102);
            this.tabItems.Controls.Add(this.trackBarItemSpePower);
            this.tabItems.Controls.Add(this.labelItemMagGuard);
            this.tabItems.Controls.Add(this.label104);
            this.tabItems.Controls.Add(this.trackBarItemMagGuard);
            this.tabItems.Controls.Add(this.labelItemMagPower);
            this.tabItems.Controls.Add(this.label106);
            this.tabItems.Controls.Add(this.trackBarItemMagPower);
            this.tabItems.Controls.Add(this.labelItemPhyGuard);
            this.tabItems.Controls.Add(this.label108);
            this.tabItems.Controls.Add(this.trackBarItemPhyGuard);
            this.tabItems.Controls.Add(this.labelItemPhyPower);
            this.tabItems.Controls.Add(this.label110);
            this.tabItems.Controls.Add(this.trackBarItemPhyPower);
            this.tabItems.Controls.Add(this.label98);
            this.tabItems.Controls.Add(this.textBoxItemPrice);
            this.tabItems.Controls.Add(this.groupBox4);
            this.tabItems.Controls.Add(this.label97);
            this.tabItems.Controls.Add(this.textBoxItemSprite);
            this.tabItems.Controls.Add(this.label96);
            this.tabItems.Controls.Add(this.textBoxItemBookName);
            this.tabItems.Controls.Add(this.label94);
            this.tabItems.Controls.Add(this.comboBoxBattleMove);
            this.tabItems.Controls.Add(this.label95);
            this.tabItems.Controls.Add(this.textBoxItemOverworldScript);
            this.tabItems.Controls.Add(this.label93);
            this.tabItems.Controls.Add(this.comboBoxItemType);
            this.tabItems.Controls.Add(this.label92);
            this.tabItems.Controls.Add(this.textBoxItemDesc);
            this.tabItems.Controls.Add(this.label3);
            this.tabItems.Controls.Add(this.textBoxItemName);
            this.tabItems.Controls.Add(this.buttonItemMoveDown);
            this.tabItems.Controls.Add(this.buttonItemMoveUp);
            this.tabItems.Controls.Add(this.buttonItemRemove);
            this.tabItems.Controls.Add(this.buttonItemAdd);
            this.tabItems.Controls.Add(this.listBoxItems);
            this.tabItems.Location = new System.Drawing.Point(4, 22);
            this.tabItems.Name = "tabItems";
            this.tabItems.Size = new System.Drawing.Size(1317, 624);
            this.tabItems.TabIndex = 2;
            this.tabItems.Text = "Items and Tablets";
            this.tabItems.UseVisualStyleBackColor = true;
            // 
            // label193
            // 
            this.label193.AutoSize = true;
            this.label193.Location = new System.Drawing.Point(129, 230);
            this.label193.Name = "label193";
            this.label193.Size = new System.Drawing.Size(48, 13);
            this.label193.TabIndex = 190;
            this.label193.Text = "Help File";
            // 
            // textBoxItemHelp
            // 
            this.textBoxItemHelp.Location = new System.Drawing.Point(129, 247);
            this.textBoxItemHelp.Name = "textBoxItemHelp";
            this.textBoxItemHelp.Size = new System.Drawing.Size(100, 20);
            this.textBoxItemHelp.TabIndex = 189;
            this.textBoxItemHelp.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label167
            // 
            this.label167.AutoSize = true;
            this.label167.Location = new System.Drawing.Point(722, 0);
            this.label167.Name = "label167";
            this.label167.Size = new System.Drawing.Size(42, 13);
            this.label167.TabIndex = 188;
            this.label167.Text = "Tablets";
            // 
            // label166
            // 
            this.label166.AutoSize = true;
            this.label166.Location = new System.Drawing.Point(3, 4);
            this.label166.Name = "label166";
            this.label166.Size = new System.Drawing.Size(32, 13);
            this.label166.TabIndex = 187;
            this.label166.Text = "Items";
            // 
            // label145
            // 
            this.label145.AutoSize = true;
            this.label145.Location = new System.Drawing.Point(862, 137);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(64, 13);
            this.label145.TabIndex = 186;
            this.label145.Text = "Battle Move";
            // 
            // comboBoxTabletMove
            // 
            this.comboBoxTabletMove.FormattingEnabled = true;
            this.comboBoxTabletMove.Location = new System.Drawing.Point(864, 154);
            this.comboBoxTabletMove.Name = "comboBoxTabletMove";
            this.comboBoxTabletMove.Size = new System.Drawing.Size(100, 21);
            this.comboBoxTabletMove.TabIndex = 185;
            // 
            // labelTabletSpeGuard
            // 
            this.labelTabletSpeGuard.AutoSize = true;
            this.labelTabletSpeGuard.Location = new System.Drawing.Point(1083, 565);
            this.labelTabletSpeGuard.Name = "labelTabletSpeGuard";
            this.labelTabletSpeGuard.Size = new System.Drawing.Size(13, 13);
            this.labelTabletSpeGuard.TabIndex = 184;
            this.labelTabletSpeGuard.Text = "0";
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.Location = new System.Drawing.Point(862, 542);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(70, 13);
            this.label134.TabIndex = 183;
            this.label134.Text = "Speed Guard";
            // 
            // trackBarTabletSpeGuard
            // 
            this.trackBarTabletSpeGuard.LargeChange = 10;
            this.trackBarTabletSpeGuard.Location = new System.Drawing.Point(865, 565);
            this.trackBarTabletSpeGuard.Maximum = 5;
            this.trackBarTabletSpeGuard.Name = "trackBarTabletSpeGuard";
            this.trackBarTabletSpeGuard.Size = new System.Drawing.Size(212, 45);
            this.trackBarTabletSpeGuard.TabIndex = 182;
            // 
            // labelTabletSpePower
            // 
            this.labelTabletSpePower.AutoSize = true;
            this.labelTabletSpePower.Location = new System.Drawing.Point(1083, 495);
            this.labelTabletSpePower.Name = "labelTabletSpePower";
            this.labelTabletSpePower.Size = new System.Drawing.Size(13, 13);
            this.labelTabletSpePower.TabIndex = 181;
            this.labelTabletSpePower.Text = "0";
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Location = new System.Drawing.Point(862, 472);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(71, 13);
            this.label136.TabIndex = 180;
            this.label136.Text = "Speed Power";
            // 
            // trackBarTabletSpePower
            // 
            this.trackBarTabletSpePower.LargeChange = 1;
            this.trackBarTabletSpePower.Location = new System.Drawing.Point(865, 495);
            this.trackBarTabletSpePower.Maximum = 20;
            this.trackBarTabletSpePower.Name = "trackBarTabletSpePower";
            this.trackBarTabletSpePower.Size = new System.Drawing.Size(212, 45);
            this.trackBarTabletSpePower.TabIndex = 179;
            this.trackBarTabletSpePower.TickFrequency = 2;
            // 
            // labelTabletMagGuard
            // 
            this.labelTabletMagGuard.AutoSize = true;
            this.labelTabletMagGuard.Location = new System.Drawing.Point(1083, 420);
            this.labelTabletMagGuard.Name = "labelTabletMagGuard";
            this.labelTabletMagGuard.Size = new System.Drawing.Size(13, 13);
            this.labelTabletMagGuard.TabIndex = 178;
            this.labelTabletMagGuard.Text = "0";
            // 
            // label138
            // 
            this.label138.AutoSize = true;
            this.label138.Location = new System.Drawing.Point(862, 397);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(68, 13);
            this.label138.TabIndex = 177;
            this.label138.Text = "Magic Guard";
            // 
            // trackBarTabletMagGuard
            // 
            this.trackBarTabletMagGuard.LargeChange = 1;
            this.trackBarTabletMagGuard.Location = new System.Drawing.Point(865, 420);
            this.trackBarTabletMagGuard.Maximum = 20;
            this.trackBarTabletMagGuard.Name = "trackBarTabletMagGuard";
            this.trackBarTabletMagGuard.Size = new System.Drawing.Size(212, 45);
            this.trackBarTabletMagGuard.TabIndex = 176;
            this.trackBarTabletMagGuard.TickFrequency = 2;
            // 
            // labelTabletMagPower
            // 
            this.labelTabletMagPower.AutoSize = true;
            this.labelTabletMagPower.Location = new System.Drawing.Point(1083, 352);
            this.labelTabletMagPower.Name = "labelTabletMagPower";
            this.labelTabletMagPower.Size = new System.Drawing.Size(13, 13);
            this.labelTabletMagPower.TabIndex = 175;
            this.labelTabletMagPower.Text = "0";
            // 
            // label140
            // 
            this.label140.AutoSize = true;
            this.label140.Location = new System.Drawing.Point(862, 329);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(69, 13);
            this.label140.TabIndex = 174;
            this.label140.Text = "Magic Power";
            // 
            // trackBarTabletMagPower
            // 
            this.trackBarTabletMagPower.LargeChange = 1;
            this.trackBarTabletMagPower.Location = new System.Drawing.Point(865, 352);
            this.trackBarTabletMagPower.Maximum = 20;
            this.trackBarTabletMagPower.Name = "trackBarTabletMagPower";
            this.trackBarTabletMagPower.Size = new System.Drawing.Size(212, 45);
            this.trackBarTabletMagPower.TabIndex = 173;
            this.trackBarTabletMagPower.TickFrequency = 2;
            // 
            // labelTabletPhyGuard
            // 
            this.labelTabletPhyGuard.AutoSize = true;
            this.labelTabletPhyGuard.Location = new System.Drawing.Point(1083, 278);
            this.labelTabletPhyGuard.Name = "labelTabletPhyGuard";
            this.labelTabletPhyGuard.Size = new System.Drawing.Size(13, 13);
            this.labelTabletPhyGuard.TabIndex = 172;
            this.labelTabletPhyGuard.Text = "0";
            // 
            // label142
            // 
            this.label142.AutoSize = true;
            this.label142.Location = new System.Drawing.Point(862, 255);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(62, 13);
            this.label142.TabIndex = 171;
            this.label142.Text = "Phys Guard";
            // 
            // trackBarTabletPhyGuard
            // 
            this.trackBarTabletPhyGuard.LargeChange = 1;
            this.trackBarTabletPhyGuard.Location = new System.Drawing.Point(865, 278);
            this.trackBarTabletPhyGuard.Maximum = 20;
            this.trackBarTabletPhyGuard.Name = "trackBarTabletPhyGuard";
            this.trackBarTabletPhyGuard.Size = new System.Drawing.Size(212, 45);
            this.trackBarTabletPhyGuard.TabIndex = 170;
            this.trackBarTabletPhyGuard.TickFrequency = 2;
            // 
            // labelTabletPhyPower
            // 
            this.labelTabletPhyPower.AutoSize = true;
            this.labelTabletPhyPower.Location = new System.Drawing.Point(1083, 209);
            this.labelTabletPhyPower.Name = "labelTabletPhyPower";
            this.labelTabletPhyPower.Size = new System.Drawing.Size(13, 13);
            this.labelTabletPhyPower.TabIndex = 169;
            this.labelTabletPhyPower.Text = "0";
            // 
            // label144
            // 
            this.label144.AutoSize = true;
            this.label144.Location = new System.Drawing.Point(862, 186);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(63, 13);
            this.label144.TabIndex = 168;
            this.label144.Text = "Phys Power";
            // 
            // trackBarTabletPhyPower
            // 
            this.trackBarTabletPhyPower.LargeChange = 1;
            this.trackBarTabletPhyPower.Location = new System.Drawing.Point(865, 209);
            this.trackBarTabletPhyPower.Maximum = 20;
            this.trackBarTabletPhyPower.Name = "trackBarTabletPhyPower";
            this.trackBarTabletPhyPower.Size = new System.Drawing.Size(212, 45);
            this.trackBarTabletPhyPower.TabIndex = 167;
            this.trackBarTabletPhyPower.TickFrequency = 2;
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Location = new System.Drawing.Point(970, 137);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(53, 13);
            this.label130.TabIndex = 166;
            this.label130.Text = "Character";
            // 
            // textBoxTabletChar
            // 
            this.textBoxTabletChar.Location = new System.Drawing.Point(973, 154);
            this.textBoxTabletChar.Name = "textBoxTabletChar";
            this.textBoxTabletChar.Size = new System.Drawing.Size(35, 20);
            this.textBoxTabletChar.TabIndex = 165;
            this.textBoxTabletChar.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Location = new System.Drawing.Point(862, 43);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(60, 13);
            this.label131.TabIndex = 164;
            this.label131.Text = "Description";
            // 
            // textBoxTabletDesc
            // 
            this.textBoxTabletDesc.Location = new System.Drawing.Point(862, 60);
            this.textBoxTabletDesc.Multiline = true;
            this.textBoxTabletDesc.Name = "textBoxTabletDesc";
            this.textBoxTabletDesc.Size = new System.Drawing.Size(196, 74);
            this.textBoxTabletDesc.TabIndex = 163;
            this.textBoxTabletDesc.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.Location = new System.Drawing.Point(862, 3);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(35, 13);
            this.label132.TabIndex = 162;
            this.label132.Text = "Name";
            // 
            // textBoxTabletName
            // 
            this.textBoxTabletName.Location = new System.Drawing.Point(862, 20);
            this.textBoxTabletName.Name = "textBoxTabletName";
            this.textBoxTabletName.Size = new System.Drawing.Size(100, 20);
            this.textBoxTabletName.TabIndex = 161;
            this.textBoxTabletName.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // buttonTabletMoveDown
            // 
            this.buttonTabletMoveDown.Location = new System.Drawing.Point(725, 482);
            this.buttonTabletMoveDown.Name = "buttonTabletMoveDown";
            this.buttonTabletMoveDown.Size = new System.Drawing.Size(120, 23);
            this.buttonTabletMoveDown.TabIndex = 160;
            this.buttonTabletMoveDown.Text = "Move Down";
            this.buttonTabletMoveDown.UseVisualStyleBackColor = true;
            // 
            // buttonTabletMoveUp
            // 
            this.buttonTabletMoveUp.Location = new System.Drawing.Point(725, 455);
            this.buttonTabletMoveUp.Name = "buttonTabletMoveUp";
            this.buttonTabletMoveUp.Size = new System.Drawing.Size(120, 23);
            this.buttonTabletMoveUp.TabIndex = 159;
            this.buttonTabletMoveUp.Text = "Move Up";
            this.buttonTabletMoveUp.UseVisualStyleBackColor = true;
            // 
            // buttonTabletRemove
            // 
            this.buttonTabletRemove.Location = new System.Drawing.Point(725, 426);
            this.buttonTabletRemove.Name = "buttonTabletRemove";
            this.buttonTabletRemove.Size = new System.Drawing.Size(120, 23);
            this.buttonTabletRemove.TabIndex = 158;
            this.buttonTabletRemove.Text = "Remove";
            this.buttonTabletRemove.UseVisualStyleBackColor = true;
            // 
            // buttonTabletAdd
            // 
            this.buttonTabletAdd.Location = new System.Drawing.Point(725, 397);
            this.buttonTabletAdd.Name = "buttonTabletAdd";
            this.buttonTabletAdd.Size = new System.Drawing.Size(120, 23);
            this.buttonTabletAdd.TabIndex = 157;
            this.buttonTabletAdd.Text = "Add";
            this.buttonTabletAdd.UseVisualStyleBackColor = true;
            // 
            // listBoxTablet
            // 
            this.listBoxTablet.AccessibleRole = System.Windows.Forms.AccessibleRole.List;
            this.listBoxTablet.FormattingEnabled = true;
            this.listBoxTablet.Location = new System.Drawing.Point(725, 16);
            this.listBoxTablet.Name = "listBoxTablet";
            this.listBoxTablet.Size = new System.Drawing.Size(120, 368);
            this.listBoxTablet.TabIndex = 156;
            // 
            // labelItemSpeGuard
            // 
            this.labelItemSpeGuard.AutoSize = true;
            this.labelItemSpeGuard.Location = new System.Drawing.Point(692, 389);
            this.labelItemSpeGuard.Name = "labelItemSpeGuard";
            this.labelItemSpeGuard.Size = new System.Drawing.Size(13, 13);
            this.labelItemSpeGuard.TabIndex = 135;
            this.labelItemSpeGuard.Text = "0";
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(471, 366);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(70, 13);
            this.label100.TabIndex = 134;
            this.label100.Text = "Speed Guard";
            // 
            // trackBarItemSpeGuard
            // 
            this.trackBarItemSpeGuard.LargeChange = 10;
            this.trackBarItemSpeGuard.Location = new System.Drawing.Point(474, 389);
            this.trackBarItemSpeGuard.Maximum = 50;
            this.trackBarItemSpeGuard.Name = "trackBarItemSpeGuard";
            this.trackBarItemSpeGuard.Size = new System.Drawing.Size(212, 45);
            this.trackBarItemSpeGuard.TabIndex = 133;
            this.trackBarItemSpeGuard.TickFrequency = 2;
            this.trackBarItemSpeGuard.Scroll += new System.EventHandler(this.trackBarItemSpeGuard_Scroll);
            // 
            // labelItemSpePower
            // 
            this.labelItemSpePower.AutoSize = true;
            this.labelItemSpePower.Location = new System.Drawing.Point(692, 319);
            this.labelItemSpePower.Name = "labelItemSpePower";
            this.labelItemSpePower.Size = new System.Drawing.Size(13, 13);
            this.labelItemSpePower.TabIndex = 132;
            this.labelItemSpePower.Text = "0";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Location = new System.Drawing.Point(471, 296);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(71, 13);
            this.label102.TabIndex = 131;
            this.label102.Text = "Speed Power";
            // 
            // trackBarItemSpePower
            // 
            this.trackBarItemSpePower.LargeChange = 10;
            this.trackBarItemSpePower.Location = new System.Drawing.Point(474, 319);
            this.trackBarItemSpePower.Maximum = 50;
            this.trackBarItemSpePower.Name = "trackBarItemSpePower";
            this.trackBarItemSpePower.Size = new System.Drawing.Size(212, 45);
            this.trackBarItemSpePower.TabIndex = 130;
            this.trackBarItemSpePower.TickFrequency = 2;
            this.trackBarItemSpePower.Scroll += new System.EventHandler(this.trackBarItemSpePower_Scroll);
            // 
            // labelItemMagGuard
            // 
            this.labelItemMagGuard.AutoSize = true;
            this.labelItemMagGuard.Location = new System.Drawing.Point(692, 244);
            this.labelItemMagGuard.Name = "labelItemMagGuard";
            this.labelItemMagGuard.Size = new System.Drawing.Size(13, 13);
            this.labelItemMagGuard.TabIndex = 129;
            this.labelItemMagGuard.Text = "0";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(471, 221);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(68, 13);
            this.label104.TabIndex = 128;
            this.label104.Text = "Magic Guard";
            // 
            // trackBarItemMagGuard
            // 
            this.trackBarItemMagGuard.LargeChange = 10;
            this.trackBarItemMagGuard.Location = new System.Drawing.Point(474, 244);
            this.trackBarItemMagGuard.Maximum = 50;
            this.trackBarItemMagGuard.Name = "trackBarItemMagGuard";
            this.trackBarItemMagGuard.Size = new System.Drawing.Size(212, 45);
            this.trackBarItemMagGuard.TabIndex = 127;
            this.trackBarItemMagGuard.TickFrequency = 2;
            this.trackBarItemMagGuard.Scroll += new System.EventHandler(this.trackBarItemMagGuard_Scroll);
            // 
            // labelItemMagPower
            // 
            this.labelItemMagPower.AutoSize = true;
            this.labelItemMagPower.Location = new System.Drawing.Point(692, 176);
            this.labelItemMagPower.Name = "labelItemMagPower";
            this.labelItemMagPower.Size = new System.Drawing.Size(13, 13);
            this.labelItemMagPower.TabIndex = 126;
            this.labelItemMagPower.Text = "0";
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(471, 153);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(69, 13);
            this.label106.TabIndex = 125;
            this.label106.Text = "Magic Power";
            // 
            // trackBarItemMagPower
            // 
            this.trackBarItemMagPower.LargeChange = 10;
            this.trackBarItemMagPower.Location = new System.Drawing.Point(474, 176);
            this.trackBarItemMagPower.Maximum = 50;
            this.trackBarItemMagPower.Name = "trackBarItemMagPower";
            this.trackBarItemMagPower.Size = new System.Drawing.Size(212, 45);
            this.trackBarItemMagPower.TabIndex = 124;
            this.trackBarItemMagPower.TickFrequency = 2;
            this.trackBarItemMagPower.Scroll += new System.EventHandler(this.trackBarItemMagPower_Scroll);
            // 
            // labelItemPhyGuard
            // 
            this.labelItemPhyGuard.AutoSize = true;
            this.labelItemPhyGuard.Location = new System.Drawing.Point(692, 102);
            this.labelItemPhyGuard.Name = "labelItemPhyGuard";
            this.labelItemPhyGuard.Size = new System.Drawing.Size(13, 13);
            this.labelItemPhyGuard.TabIndex = 123;
            this.labelItemPhyGuard.Text = "0";
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Location = new System.Drawing.Point(471, 79);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(62, 13);
            this.label108.TabIndex = 122;
            this.label108.Text = "Phys Guard";
            // 
            // trackBarItemPhyGuard
            // 
            this.trackBarItemPhyGuard.LargeChange = 10;
            this.trackBarItemPhyGuard.Location = new System.Drawing.Point(474, 102);
            this.trackBarItemPhyGuard.Maximum = 50;
            this.trackBarItemPhyGuard.Name = "trackBarItemPhyGuard";
            this.trackBarItemPhyGuard.Size = new System.Drawing.Size(212, 45);
            this.trackBarItemPhyGuard.TabIndex = 121;
            this.trackBarItemPhyGuard.TickFrequency = 2;
            this.trackBarItemPhyGuard.Scroll += new System.EventHandler(this.trackBarItemPhyGuard_Scroll);
            // 
            // labelItemPhyPower
            // 
            this.labelItemPhyPower.AutoSize = true;
            this.labelItemPhyPower.Location = new System.Drawing.Point(692, 33);
            this.labelItemPhyPower.Name = "labelItemPhyPower";
            this.labelItemPhyPower.Size = new System.Drawing.Size(13, 13);
            this.labelItemPhyPower.TabIndex = 120;
            this.labelItemPhyPower.Text = "0";
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Location = new System.Drawing.Point(471, 10);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(63, 13);
            this.label110.TabIndex = 119;
            this.label110.Text = "Phys Power";
            // 
            // trackBarItemPhyPower
            // 
            this.trackBarItemPhyPower.LargeChange = 10;
            this.trackBarItemPhyPower.Location = new System.Drawing.Point(474, 33);
            this.trackBarItemPhyPower.Maximum = 50;
            this.trackBarItemPhyPower.Name = "trackBarItemPhyPower";
            this.trackBarItemPhyPower.Size = new System.Drawing.Size(212, 45);
            this.trackBarItemPhyPower.TabIndex = 118;
            this.trackBarItemPhyPower.TickFrequency = 2;
            this.trackBarItemPhyPower.Scroll += new System.EventHandler(this.trackBarItemPhyPower_Scroll);
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(365, 150);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(31, 13);
            this.label98.TabIndex = 102;
            this.label98.Text = "Price";
            // 
            // textBoxItemPrice
            // 
            this.textBoxItemPrice.Location = new System.Drawing.Point(365, 167);
            this.textBoxItemPrice.Name = "textBoxItemPrice";
            this.textBoxItemPrice.Size = new System.Drawing.Size(57, 20);
            this.textBoxItemPrice.TabIndex = 101;
            this.textBoxItemPrice.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.checkBoxItemSingleUse);
            this.groupBox4.Controls.Add(this.checkBoxItemCanSell);
            this.groupBox4.Controls.Add(this.checkBoxItemCanGive);
            this.groupBox4.Controls.Add(this.checkBoxItemCanToss);
            this.groupBox4.Location = new System.Drawing.Point(360, 27);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(94, 120);
            this.groupBox4.TabIndex = 100;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Triggers";
            // 
            // checkBoxItemSingleUse
            // 
            this.checkBoxItemSingleUse.AutoSize = true;
            this.checkBoxItemSingleUse.Location = new System.Drawing.Point(8, 89);
            this.checkBoxItemSingleUse.Name = "checkBoxItemSingleUse";
            this.checkBoxItemSingleUse.Size = new System.Drawing.Size(77, 17);
            this.checkBoxItemSingleUse.TabIndex = 3;
            this.checkBoxItemSingleUse.Text = "Single Use";
            this.checkBoxItemSingleUse.UseVisualStyleBackColor = true;
            // 
            // checkBoxItemCanSell
            // 
            this.checkBoxItemCanSell.AutoSize = true;
            this.checkBoxItemCanSell.Location = new System.Drawing.Point(8, 66);
            this.checkBoxItemCanSell.Name = "checkBoxItemCanSell";
            this.checkBoxItemCanSell.Size = new System.Drawing.Size(65, 17);
            this.checkBoxItemCanSell.TabIndex = 2;
            this.checkBoxItemCanSell.Text = "Can Sell";
            this.checkBoxItemCanSell.UseVisualStyleBackColor = true;
            // 
            // checkBoxItemCanGive
            // 
            this.checkBoxItemCanGive.AutoSize = true;
            this.checkBoxItemCanGive.Location = new System.Drawing.Point(8, 43);
            this.checkBoxItemCanGive.Name = "checkBoxItemCanGive";
            this.checkBoxItemCanGive.Size = new System.Drawing.Size(70, 17);
            this.checkBoxItemCanGive.TabIndex = 1;
            this.checkBoxItemCanGive.Text = "Can Give";
            this.checkBoxItemCanGive.UseVisualStyleBackColor = true;
            // 
            // checkBoxItemCanToss
            // 
            this.checkBoxItemCanToss.AutoSize = true;
            this.checkBoxItemCanToss.Location = new System.Drawing.Point(8, 20);
            this.checkBoxItemCanToss.Name = "checkBoxItemCanToss";
            this.checkBoxItemCanToss.Size = new System.Drawing.Size(71, 17);
            this.checkBoxItemCanToss.TabIndex = 0;
            this.checkBoxItemCanToss.Text = "Can Toss";
            this.checkBoxItemCanToss.UseVisualStyleBackColor = true;
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(243, 191);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(34, 13);
            this.label97.TabIndex = 99;
            this.label97.Text = "Sprite";
            // 
            // textBoxItemSprite
            // 
            this.textBoxItemSprite.Location = new System.Drawing.Point(243, 208);
            this.textBoxItemSprite.Name = "textBoxItemSprite";
            this.textBoxItemSprite.Size = new System.Drawing.Size(100, 20);
            this.textBoxItemSprite.TabIndex = 98;
            this.textBoxItemSprite.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(129, 190);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(63, 13);
            this.label96.TabIndex = 97;
            this.label96.Text = "Book Name";
            // 
            // textBoxItemBookName
            // 
            this.textBoxItemBookName.Location = new System.Drawing.Point(129, 207);
            this.textBoxItemBookName.Name = "textBoxItemBookName";
            this.textBoxItemBookName.Size = new System.Drawing.Size(100, 20);
            this.textBoxItemBookName.TabIndex = 96;
            this.textBoxItemBookName.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Location = new System.Drawing.Point(242, 150);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(64, 13);
            this.label94.TabIndex = 95;
            this.label94.Text = "Battle Move";
            // 
            // comboBoxBattleMove
            // 
            this.comboBoxBattleMove.FormattingEnabled = true;
            this.comboBoxBattleMove.Location = new System.Drawing.Point(244, 167);
            this.comboBoxBattleMove.Name = "comboBoxBattleMove";
            this.comboBoxBattleMove.Size = new System.Drawing.Size(100, 21);
            this.comboBoxBattleMove.TabIndex = 94;
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(129, 150);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(85, 13);
            this.label95.TabIndex = 93;
            this.label95.Text = "Overworld Script";
            // 
            // textBoxItemOverworldScript
            // 
            this.textBoxItemOverworldScript.Location = new System.Drawing.Point(129, 167);
            this.textBoxItemOverworldScript.Name = "textBoxItemOverworldScript";
            this.textBoxItemOverworldScript.Size = new System.Drawing.Size(100, 20);
            this.textBoxItemOverworldScript.TabIndex = 92;
            this.textBoxItemOverworldScript.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(242, 16);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(31, 13);
            this.label93.TabIndex = 91;
            this.label93.Text = "Type";
            // 
            // comboBoxItemType
            // 
            this.comboBoxItemType.FormattingEnabled = true;
            this.comboBoxItemType.Location = new System.Drawing.Point(244, 33);
            this.comboBoxItemType.Name = "comboBoxItemType";
            this.comboBoxItemType.Size = new System.Drawing.Size(100, 21);
            this.comboBoxItemType.TabIndex = 90;
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Location = new System.Drawing.Point(129, 56);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(60, 13);
            this.label92.TabIndex = 18;
            this.label92.Text = "Description";
            // 
            // textBoxItemDesc
            // 
            this.textBoxItemDesc.Location = new System.Drawing.Point(129, 73);
            this.textBoxItemDesc.Multiline = true;
            this.textBoxItemDesc.Name = "textBoxItemDesc";
            this.textBoxItemDesc.Size = new System.Drawing.Size(196, 74);
            this.textBoxItemDesc.TabIndex = 17;
            this.textBoxItemDesc.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(129, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "Name";
            // 
            // textBoxItemName
            // 
            this.textBoxItemName.Location = new System.Drawing.Point(129, 33);
            this.textBoxItemName.Name = "textBoxItemName";
            this.textBoxItemName.Size = new System.Drawing.Size(100, 20);
            this.textBoxItemName.TabIndex = 15;
            this.textBoxItemName.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            this.textBoxItemName.Leave += new System.EventHandler(this.textBoxItemName_Leave);
            // 
            // buttonItemMoveDown
            // 
            this.buttonItemMoveDown.Location = new System.Drawing.Point(3, 592);
            this.buttonItemMoveDown.Name = "buttonItemMoveDown";
            this.buttonItemMoveDown.Size = new System.Drawing.Size(120, 23);
            this.buttonItemMoveDown.TabIndex = 14;
            this.buttonItemMoveDown.Text = "Move Down";
            this.buttonItemMoveDown.UseVisualStyleBackColor = true;
            this.buttonItemMoveDown.Click += new System.EventHandler(this.buttonItemMoveDown_Click);
            // 
            // buttonItemMoveUp
            // 
            this.buttonItemMoveUp.Location = new System.Drawing.Point(3, 565);
            this.buttonItemMoveUp.Name = "buttonItemMoveUp";
            this.buttonItemMoveUp.Size = new System.Drawing.Size(120, 23);
            this.buttonItemMoveUp.TabIndex = 13;
            this.buttonItemMoveUp.Text = "Move Up";
            this.buttonItemMoveUp.UseVisualStyleBackColor = true;
            this.buttonItemMoveUp.Click += new System.EventHandler(this.buttonItemMoveUp_Click);
            // 
            // buttonItemRemove
            // 
            this.buttonItemRemove.Location = new System.Drawing.Point(3, 536);
            this.buttonItemRemove.Name = "buttonItemRemove";
            this.buttonItemRemove.Size = new System.Drawing.Size(120, 23);
            this.buttonItemRemove.TabIndex = 12;
            this.buttonItemRemove.Text = "Remove";
            this.buttonItemRemove.UseVisualStyleBackColor = true;
            this.buttonItemRemove.Click += new System.EventHandler(this.buttonItemRemove_Click);
            // 
            // buttonItemAdd
            // 
            this.buttonItemAdd.Location = new System.Drawing.Point(3, 507);
            this.buttonItemAdd.Name = "buttonItemAdd";
            this.buttonItemAdd.Size = new System.Drawing.Size(120, 23);
            this.buttonItemAdd.TabIndex = 11;
            this.buttonItemAdd.Text = "Add";
            this.buttonItemAdd.UseVisualStyleBackColor = true;
            this.buttonItemAdd.Click += new System.EventHandler(this.buttonItemAdd_Click);
            // 
            // listBoxItems
            // 
            this.listBoxItems.AccessibleRole = System.Windows.Forms.AccessibleRole.List;
            this.listBoxItems.FormattingEnabled = true;
            this.listBoxItems.Location = new System.Drawing.Point(3, 20);
            this.listBoxItems.Name = "listBoxItems";
            this.listBoxItems.Size = new System.Drawing.Size(120, 472);
            this.listBoxItems.TabIndex = 10;
            this.listBoxItems.SelectedIndexChanged += new System.EventHandler(this.listBoxItems_SelectedIndexChanged);
            // 
            // tabMove
            // 
            this.tabMove.BackColor = System.Drawing.Color.Silver;
            this.tabMove.Controls.Add(this.groupBox28);
            this.tabMove.Controls.Add(this.groupBox27);
            this.tabMove.Controls.Add(this.groupBox26);
            this.tabMove.Controls.Add(this.groupBox11);
            this.tabMove.Location = new System.Drawing.Point(4, 22);
            this.tabMove.Name = "tabMove";
            this.tabMove.Size = new System.Drawing.Size(1317, 624);
            this.tabMove.TabIndex = 3;
            this.tabMove.Text = "Moves";
            // 
            // groupBox28
            // 
            this.groupBox28.BackColor = System.Drawing.Color.Gainsboro;
            this.groupBox28.Controls.Add(this.groupBox29);
            this.groupBox28.Controls.Add(this.label4);
            this.groupBox28.Controls.Add(this.textBoxMoveName);
            this.groupBox28.Controls.Add(this.groupBox17);
            this.groupBox28.Controls.Add(this.listBoxMoves);
            this.groupBox28.Controls.Add(this.buttonMovesMoveDown);
            this.groupBox28.Controls.Add(this.groupBox6);
            this.groupBox28.Controls.Add(this.buttonMovesMoveUp);
            this.groupBox28.Controls.Add(this.textBoxMoveDesc);
            this.groupBox28.Controls.Add(this.buttonMovesRemove);
            this.groupBox28.Controls.Add(this.label101);
            this.groupBox28.Controls.Add(this.buttonMovesAdd);
            this.groupBox28.Controls.Add(this.groupBox7);
            this.groupBox28.Controls.Add(this.textBoxMoveScript);
            this.groupBox28.Controls.Add(this.label105);
            this.groupBox28.Controls.Add(this.groupBox8);
            this.groupBox28.Controls.Add(this.groupBox5);
            this.groupBox28.Location = new System.Drawing.Point(6, 3);
            this.groupBox28.Name = "groupBox28";
            this.groupBox28.Size = new System.Drawing.Size(681, 547);
            this.groupBox28.TabIndex = 158;
            this.groupBox28.TabStop = false;
            this.groupBox28.Text = "Moves";
            // 
            // groupBox29
            // 
            this.groupBox29.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox29.Controls.Add(this.label99);
            this.groupBox29.Controls.Add(this.comboBoxMoveType);
            this.groupBox29.Controls.Add(this.comboBoxMovePhysMag);
            this.groupBox29.Controls.Add(this.label103);
            this.groupBox29.Controls.Add(this.textBoxMoveDamage);
            this.groupBox29.Controls.Add(this.textBoxMoveMPCost);
            this.groupBox29.Controls.Add(this.label153);
            this.groupBox29.Controls.Add(this.label109);
            this.groupBox29.Controls.Add(this.textBoxMoveCharacter);
            this.groupBox29.Controls.Add(this.label107);
            this.groupBox29.Controls.Add(this.textBoxMoveTargets);
            this.groupBox29.Controls.Add(this.label123);
            this.groupBox29.Controls.Add(this.label111);
            this.groupBox29.Controls.Add(this.textBoxMoveStatusPercent);
            this.groupBox29.Controls.Add(this.textBoxMovePriority);
            this.groupBox29.Controls.Add(this.label113);
            this.groupBox29.Controls.Add(this.textBoxMoveSpeedMultiplier);
            this.groupBox29.Controls.Add(this.label112);
            this.groupBox29.Controls.Add(this.label115);
            this.groupBox29.Controls.Add(this.comboBoxMoveItem);
            this.groupBox29.Controls.Add(this.textBoxMoveEffectPercent);
            this.groupBox29.Controls.Add(this.label114);
            this.groupBox29.Location = new System.Drawing.Point(137, 146);
            this.groupBox29.Name = "groupBox29";
            this.groupBox29.Size = new System.Drawing.Size(243, 191);
            this.groupBox29.TabIndex = 159;
            this.groupBox29.TabStop = false;
            this.groupBox29.Text = "Stats";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(6, 16);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(31, 13);
            this.label99.TabIndex = 104;
            this.label99.Text = "Type";
            // 
            // comboBoxMoveType
            // 
            this.comboBoxMoveType.FormattingEnabled = true;
            this.comboBoxMoveType.Location = new System.Drawing.Point(7, 32);
            this.comboBoxMoveType.Name = "comboBoxMoveType";
            this.comboBoxMoveType.Size = new System.Drawing.Size(81, 21);
            this.comboBoxMoveType.TabIndex = 103;
            // 
            // comboBoxMovePhysMag
            // 
            this.comboBoxMovePhysMag.FormattingEnabled = true;
            this.comboBoxMovePhysMag.Location = new System.Drawing.Point(95, 32);
            this.comboBoxMovePhysMag.Name = "comboBoxMovePhysMag";
            this.comboBoxMovePhysMag.Size = new System.Drawing.Size(75, 21);
            this.comboBoxMovePhysMag.TabIndex = 106;
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(92, 16);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(56, 13);
            this.label103.TabIndex = 107;
            this.label103.Text = "Phys/Mag";
            // 
            // textBoxMoveDamage
            // 
            this.textBoxMoveDamage.Location = new System.Drawing.Point(7, 72);
            this.textBoxMoveDamage.Name = "textBoxMoveDamage";
            this.textBoxMoveDamage.Size = new System.Drawing.Size(60, 20);
            this.textBoxMoveDamage.TabIndex = 110;
            this.textBoxMoveDamage.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // textBoxMoveMPCost
            // 
            this.textBoxMoveMPCost.Location = new System.Drawing.Point(88, 74);
            this.textBoxMoveMPCost.Name = "textBoxMoveMPCost";
            this.textBoxMoveMPCost.Size = new System.Drawing.Size(60, 20);
            this.textBoxMoveMPCost.TabIndex = 112;
            this.textBoxMoveMPCost.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label153
            // 
            this.label153.AutoSize = true;
            this.label153.Location = new System.Drawing.Point(177, 16);
            this.label153.Name = "label153";
            this.label153.Size = new System.Drawing.Size(53, 13);
            this.label153.TabIndex = 135;
            this.label153.Text = "Character";
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Location = new System.Drawing.Point(85, 57);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(47, 13);
            this.label109.TabIndex = 113;
            this.label109.Text = "MP Cost";
            // 
            // textBoxMoveCharacter
            // 
            this.textBoxMoveCharacter.Location = new System.Drawing.Point(184, 32);
            this.textBoxMoveCharacter.Name = "textBoxMoveCharacter";
            this.textBoxMoveCharacter.Size = new System.Drawing.Size(40, 20);
            this.textBoxMoveCharacter.TabIndex = 134;
            this.textBoxMoveCharacter.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Location = new System.Drawing.Point(6, 55);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(47, 13);
            this.label107.TabIndex = 111;
            this.label107.Text = "Damage";
            // 
            // textBoxMoveTargets
            // 
            this.textBoxMoveTargets.Location = new System.Drawing.Point(163, 72);
            this.textBoxMoveTargets.Name = "textBoxMoveTargets";
            this.textBoxMoveTargets.Size = new System.Drawing.Size(60, 20);
            this.textBoxMoveTargets.TabIndex = 114;
            this.textBoxMoveTargets.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Location = new System.Drawing.Point(164, 97);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(77, 13);
            this.label123.TabIndex = 133;
            this.label123.Text = "Status Percent";
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(160, 57);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(43, 13);
            this.label111.TabIndex = 115;
            this.label111.Text = "Targets";
            // 
            // textBoxMoveStatusPercent
            // 
            this.textBoxMoveStatusPercent.Location = new System.Drawing.Point(164, 113);
            this.textBoxMoveStatusPercent.Name = "textBoxMoveStatusPercent";
            this.textBoxMoveStatusPercent.Size = new System.Drawing.Size(60, 20);
            this.textBoxMoveStatusPercent.TabIndex = 132;
            this.textBoxMoveStatusPercent.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // textBoxMovePriority
            // 
            this.textBoxMovePriority.Location = new System.Drawing.Point(9, 113);
            this.textBoxMovePriority.Name = "textBoxMovePriority";
            this.textBoxMovePriority.Size = new System.Drawing.Size(60, 20);
            this.textBoxMovePriority.TabIndex = 116;
            this.textBoxMovePriority.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Location = new System.Drawing.Point(5, 96);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(38, 13);
            this.label113.TabIndex = 117;
            this.label113.Text = "Priority";
            // 
            // textBoxMoveSpeedMultiplier
            // 
            this.textBoxMoveSpeedMultiplier.Location = new System.Drawing.Point(88, 113);
            this.textBoxMoveSpeedMultiplier.Name = "textBoxMoveSpeedMultiplier";
            this.textBoxMoveSpeedMultiplier.Size = new System.Drawing.Size(60, 20);
            this.textBoxMoveSpeedMultiplier.TabIndex = 118;
            this.textBoxMoveSpeedMultiplier.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Location = new System.Drawing.Point(85, 96);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(38, 13);
            this.label112.TabIndex = 119;
            this.label112.Text = "Speed";
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Location = new System.Drawing.Point(160, 137);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(75, 13);
            this.label115.TabIndex = 123;
            this.label115.Text = "Effect Percent";
            // 
            // comboBoxMoveItem
            // 
            this.comboBoxMoveItem.FormattingEnabled = true;
            this.comboBoxMoveItem.Location = new System.Drawing.Point(8, 153);
            this.comboBoxMoveItem.Name = "comboBoxMoveItem";
            this.comboBoxMoveItem.Size = new System.Drawing.Size(113, 21);
            this.comboBoxMoveItem.TabIndex = 120;
            // 
            // textBoxMoveEffectPercent
            // 
            this.textBoxMoveEffectPercent.Location = new System.Drawing.Point(164, 154);
            this.textBoxMoveEffectPercent.Name = "textBoxMoveEffectPercent";
            this.textBoxMoveEffectPercent.Size = new System.Drawing.Size(60, 20);
            this.textBoxMoveEffectPercent.TabIndex = 122;
            this.textBoxMoveEffectPercent.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Location = new System.Drawing.Point(6, 136);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(27, 13);
            this.label114.TabIndex = 121;
            this.label114.Text = "Item";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(137, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 21;
            this.label4.Text = "Name";
            // 
            // textBoxMoveName
            // 
            this.textBoxMoveName.Location = new System.Drawing.Point(137, 27);
            this.textBoxMoveName.Name = "textBoxMoveName";
            this.textBoxMoveName.Size = new System.Drawing.Size(100, 20);
            this.textBoxMoveName.TabIndex = 20;
            this.textBoxMoveName.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            this.textBoxMoveName.Leave += new System.EventHandler(this.textBoxMoveName_Leave);
            // 
            // groupBox17
            // 
            this.groupBox17.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox17.Controls.Add(this.label227);
            this.groupBox17.Controls.Add(this.textBoxMoveAnimationLungePos);
            this.groupBox17.Controls.Add(this.label199);
            this.groupBox17.Controls.Add(this.textBoxMoveAnimationJumpHeight);
            this.groupBox17.Controls.Add(this.comboBoxMoveAnimationTarget);
            this.groupBox17.Controls.Add(this.label198);
            this.groupBox17.Controls.Add(this.label197);
            this.groupBox17.Controls.Add(this.label196);
            this.groupBox17.Controls.Add(this.label195);
            this.groupBox17.Controls.Add(this.label194);
            this.groupBox17.Controls.Add(this.label120);
            this.groupBox17.Controls.Add(this.textBoxMoveAnimationFrame);
            this.groupBox17.Controls.Add(this.textBoxMoveAnimationSound);
            this.groupBox17.Controls.Add(this.textBoxMoveAnimationSprite);
            this.groupBox17.Controls.Add(this.textBoxMoveAnimationLunge);
            this.groupBox17.Controls.Add(this.textBoxMoveAnimationJump);
            this.groupBox17.Controls.Add(this.buttonMoveAnimationAdd);
            this.groupBox17.Controls.Add(this.buttonMoveAnimationRemove);
            this.groupBox17.Controls.Add(this.listBoxMoveAnimation);
            this.groupBox17.Controls.Add(this.buttonMoveAnimationUp);
            this.groupBox17.Controls.Add(this.buttonMoveAnimationDown);
            this.groupBox17.Location = new System.Drawing.Point(384, 297);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(288, 228);
            this.groupBox17.TabIndex = 155;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "Move Animation";
            // 
            // label227
            // 
            this.label227.AutoSize = true;
            this.label227.Location = new System.Drawing.Point(205, 91);
            this.label227.Name = "label227";
            this.label227.Size = new System.Drawing.Size(77, 13);
            this.label227.TabIndex = 173;
            this.label227.Text = "Lunge Position";
            // 
            // textBoxMoveAnimationLungePos
            // 
            this.textBoxMoveAnimationLungePos.Location = new System.Drawing.Point(208, 107);
            this.textBoxMoveAnimationLungePos.Name = "textBoxMoveAnimationLungePos";
            this.textBoxMoveAnimationLungePos.Size = new System.Drawing.Size(70, 20);
            this.textBoxMoveAnimationLungePos.TabIndex = 172;
            this.textBoxMoveAnimationLungePos.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label199
            // 
            this.label199.AutoSize = true;
            this.label199.Location = new System.Drawing.Point(208, 54);
            this.label199.Name = "label199";
            this.label199.Size = new System.Drawing.Size(66, 13);
            this.label199.TabIndex = 170;
            this.label199.Text = "Jump Height";
            // 
            // textBoxMoveAnimationJumpHeight
            // 
            this.textBoxMoveAnimationJumpHeight.Location = new System.Drawing.Point(207, 68);
            this.textBoxMoveAnimationJumpHeight.Name = "textBoxMoveAnimationJumpHeight";
            this.textBoxMoveAnimationJumpHeight.Size = new System.Drawing.Size(70, 20);
            this.textBoxMoveAnimationJumpHeight.TabIndex = 171;
            this.textBoxMoveAnimationJumpHeight.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // comboBoxMoveAnimationTarget
            // 
            this.comboBoxMoveAnimationTarget.FormattingEnabled = true;
            this.comboBoxMoveAnimationTarget.Items.AddRange(new object[] {
            "none",
            "user",
            "target1",
            "target2",
            "target3"});
            this.comboBoxMoveAnimationTarget.Location = new System.Drawing.Point(208, 27);
            this.comboBoxMoveAnimationTarget.Name = "comboBoxMoveAnimationTarget";
            this.comboBoxMoveAnimationTarget.Size = new System.Drawing.Size(77, 21);
            this.comboBoxMoveAnimationTarget.TabIndex = 156;
            // 
            // label198
            // 
            this.label198.AutoSize = true;
            this.label198.Location = new System.Drawing.Point(205, 11);
            this.label198.Name = "label198";
            this.label198.Size = new System.Drawing.Size(38, 13);
            this.label198.TabIndex = 157;
            this.label198.Text = "Target";
            // 
            // label197
            // 
            this.label197.AutoSize = true;
            this.label197.Location = new System.Drawing.Point(131, 174);
            this.label197.Name = "label197";
            this.label197.Size = new System.Drawing.Size(69, 13);
            this.label197.TabIndex = 169;
            this.label197.Text = "Sound Effect";
            // 
            // label196
            // 
            this.label196.AutoSize = true;
            this.label196.Location = new System.Drawing.Point(131, 130);
            this.label196.Name = "label196";
            this.label196.Size = new System.Drawing.Size(74, 13);
            this.label196.TabIndex = 168;
            this.label196.Text = "Sprite Change";
            // 
            // label195
            // 
            this.label195.AutoSize = true;
            this.label195.Location = new System.Drawing.Point(128, 91);
            this.label195.Name = "label195";
            this.label195.Size = new System.Drawing.Size(37, 13);
            this.label195.TabIndex = 167;
            this.label195.Text = "Lunge";
            // 
            // label194
            // 
            this.label194.AutoSize = true;
            this.label194.Location = new System.Drawing.Point(132, 53);
            this.label194.Name = "label194";
            this.label194.Size = new System.Drawing.Size(32, 13);
            this.label194.TabIndex = 159;
            this.label194.Text = "Jump";
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Location = new System.Drawing.Point(128, 32);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(36, 13);
            this.label120.TabIndex = 159;
            this.label120.Text = "Frame";
            // 
            // textBoxMoveAnimationFrame
            // 
            this.textBoxMoveAnimationFrame.Location = new System.Drawing.Point(166, 28);
            this.textBoxMoveAnimationFrame.Name = "textBoxMoveAnimationFrame";
            this.textBoxMoveAnimationFrame.Size = new System.Drawing.Size(39, 20);
            this.textBoxMoveAnimationFrame.TabIndex = 166;
            this.textBoxMoveAnimationFrame.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            this.textBoxMoveAnimationFrame.Leave += new System.EventHandler(this.listBoxMoveAnimation_Leave);
            // 
            // textBoxMoveAnimationSound
            // 
            this.textBoxMoveAnimationSound.Location = new System.Drawing.Point(131, 190);
            this.textBoxMoveAnimationSound.Name = "textBoxMoveAnimationSound";
            this.textBoxMoveAnimationSound.Size = new System.Drawing.Size(70, 20);
            this.textBoxMoveAnimationSound.TabIndex = 165;
            this.textBoxMoveAnimationSound.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // textBoxMoveAnimationSprite
            // 
            this.textBoxMoveAnimationSprite.Location = new System.Drawing.Point(131, 146);
            this.textBoxMoveAnimationSprite.Name = "textBoxMoveAnimationSprite";
            this.textBoxMoveAnimationSprite.Size = new System.Drawing.Size(70, 20);
            this.textBoxMoveAnimationSprite.TabIndex = 163;
            this.textBoxMoveAnimationSprite.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // textBoxMoveAnimationLunge
            // 
            this.textBoxMoveAnimationLunge.Location = new System.Drawing.Point(131, 107);
            this.textBoxMoveAnimationLunge.Name = "textBoxMoveAnimationLunge";
            this.textBoxMoveAnimationLunge.Size = new System.Drawing.Size(70, 20);
            this.textBoxMoveAnimationLunge.TabIndex = 157;
            this.textBoxMoveAnimationLunge.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // textBoxMoveAnimationJump
            // 
            this.textBoxMoveAnimationJump.Location = new System.Drawing.Point(131, 67);
            this.textBoxMoveAnimationJump.Name = "textBoxMoveAnimationJump";
            this.textBoxMoveAnimationJump.Size = new System.Drawing.Size(70, 20);
            this.textBoxMoveAnimationJump.TabIndex = 159;
            this.textBoxMoveAnimationJump.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // buttonMoveAnimationAdd
            // 
            this.buttonMoveAnimationAdd.Location = new System.Drawing.Point(6, 160);
            this.buttonMoveAnimationAdd.Name = "buttonMoveAnimationAdd";
            this.buttonMoveAnimationAdd.Size = new System.Drawing.Size(62, 23);
            this.buttonMoveAnimationAdd.TabIndex = 159;
            this.buttonMoveAnimationAdd.Text = "Add";
            this.buttonMoveAnimationAdd.UseVisualStyleBackColor = true;
            this.buttonMoveAnimationAdd.Click += new System.EventHandler(this.buttonMoveAnimationAdd_Click);
            // 
            // buttonMoveAnimationRemove
            // 
            this.buttonMoveAnimationRemove.Location = new System.Drawing.Point(6, 189);
            this.buttonMoveAnimationRemove.Name = "buttonMoveAnimationRemove";
            this.buttonMoveAnimationRemove.Size = new System.Drawing.Size(62, 23);
            this.buttonMoveAnimationRemove.TabIndex = 160;
            this.buttonMoveAnimationRemove.Text = "Remove";
            this.buttonMoveAnimationRemove.UseVisualStyleBackColor = true;
            this.buttonMoveAnimationRemove.Click += new System.EventHandler(this.buttonMoveAnimationRemove_Click);
            // 
            // listBoxMoveAnimation
            // 
            this.listBoxMoveAnimation.AccessibleRole = System.Windows.Forms.AccessibleRole.List;
            this.listBoxMoveAnimation.FormattingEnabled = true;
            this.listBoxMoveAnimation.Location = new System.Drawing.Point(6, 23);
            this.listBoxMoveAnimation.Name = "listBoxMoveAnimation";
            this.listBoxMoveAnimation.Size = new System.Drawing.Size(120, 134);
            this.listBoxMoveAnimation.TabIndex = 159;
            this.listBoxMoveAnimation.SelectedIndexChanged += new System.EventHandler(this.listBoxMoveAnimation_SelectedIndexChanged);
            // 
            // buttonMoveAnimationUp
            // 
            this.buttonMoveAnimationUp.Location = new System.Drawing.Point(71, 160);
            this.buttonMoveAnimationUp.Name = "buttonMoveAnimationUp";
            this.buttonMoveAnimationUp.Size = new System.Drawing.Size(56, 23);
            this.buttonMoveAnimationUp.TabIndex = 161;
            this.buttonMoveAnimationUp.Text = "Up";
            this.buttonMoveAnimationUp.UseVisualStyleBackColor = true;
            this.buttonMoveAnimationUp.Click += new System.EventHandler(this.buttonMoveAnimationMoveUp_Click);
            // 
            // buttonMoveAnimationDown
            // 
            this.buttonMoveAnimationDown.Location = new System.Drawing.Point(71, 189);
            this.buttonMoveAnimationDown.Name = "buttonMoveAnimationDown";
            this.buttonMoveAnimationDown.Size = new System.Drawing.Size(56, 23);
            this.buttonMoveAnimationDown.TabIndex = 162;
            this.buttonMoveAnimationDown.Text = "Down";
            this.buttonMoveAnimationDown.UseVisualStyleBackColor = true;
            this.buttonMoveAnimationDown.Click += new System.EventHandler(this.buttonMoveAnimationMoveDown_Click);
            // 
            // listBoxMoves
            // 
            this.listBoxMoves.AccessibleRole = System.Windows.Forms.AccessibleRole.List;
            this.listBoxMoves.FormattingEnabled = true;
            this.listBoxMoves.Location = new System.Drawing.Point(6, 16);
            this.listBoxMoves.Name = "listBoxMoves";
            this.listBoxMoves.Size = new System.Drawing.Size(120, 329);
            this.listBoxMoves.TabIndex = 15;
            this.listBoxMoves.SelectedIndexChanged += new System.EventHandler(this.listBoxMoves_SelectedIndexChanged);
            // 
            // buttonMovesMoveDown
            // 
            this.buttonMovesMoveDown.Location = new System.Drawing.Point(69, 378);
            this.buttonMovesMoveDown.Name = "buttonMovesMoveDown";
            this.buttonMovesMoveDown.Size = new System.Drawing.Size(57, 23);
            this.buttonMovesMoveDown.TabIndex = 19;
            this.buttonMovesMoveDown.Text = "Down";
            this.buttonMovesMoveDown.UseVisualStyleBackColor = true;
            this.buttonMovesMoveDown.Click += new System.EventHandler(this.buttonMovesMoveDown_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox6.Controls.Add(this.label122);
            this.groupBox6.Controls.Add(this.comboBoxMoveBattlerVisibility);
            this.groupBox6.Controls.Add(this.label121);
            this.groupBox6.Controls.Add(this.textBoxMoveDuration);
            this.groupBox6.Controls.Add(this.label118);
            this.groupBox6.Controls.Add(this.textBoxMoveUserSprite);
            this.groupBox6.Controls.Add(this.label119);
            this.groupBox6.Controls.Add(this.textBoxMoveTargetSprite);
            this.groupBox6.Controls.Add(this.label117);
            this.groupBox6.Controls.Add(this.textBoxMoveBackgroundSprite);
            this.groupBox6.Controls.Add(this.label116);
            this.groupBox6.Controls.Add(this.textBoxMoveForegroundSprite);
            this.groupBox6.Location = new System.Drawing.Point(137, 343);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(241, 192);
            this.groupBox6.TabIndex = 124;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Graphics";
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Location = new System.Drawing.Point(11, 136);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(76, 13);
            this.label122.TabIndex = 126;
            this.label122.Text = "Battler Visibility";
            // 
            // comboBoxMoveBattlerVisibility
            // 
            this.comboBoxMoveBattlerVisibility.FormattingEnabled = true;
            this.comboBoxMoveBattlerVisibility.Location = new System.Drawing.Point(13, 153);
            this.comboBoxMoveBattlerVisibility.Name = "comboBoxMoveBattlerVisibility";
            this.comboBoxMoveBattlerVisibility.Size = new System.Drawing.Size(209, 21);
            this.comboBoxMoveBattlerVisibility.TabIndex = 125;
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Location = new System.Drawing.Point(9, 96);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(77, 13);
            this.label121.TabIndex = 134;
            this.label121.Text = "Move Duration";
            // 
            // textBoxMoveDuration
            // 
            this.textBoxMoveDuration.Location = new System.Drawing.Point(12, 113);
            this.textBoxMoveDuration.Name = "textBoxMoveDuration";
            this.textBoxMoveDuration.Size = new System.Drawing.Size(100, 20);
            this.textBoxMoveDuration.TabIndex = 133;
            this.textBoxMoveDuration.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Location = new System.Drawing.Point(118, 56);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(59, 13);
            this.label118.TabIndex = 132;
            this.label118.Text = "User Sprite";
            // 
            // textBoxMoveUserSprite
            // 
            this.textBoxMoveUserSprite.Location = new System.Drawing.Point(118, 73);
            this.textBoxMoveUserSprite.Name = "textBoxMoveUserSprite";
            this.textBoxMoveUserSprite.Size = new System.Drawing.Size(100, 20);
            this.textBoxMoveUserSprite.TabIndex = 131;
            this.textBoxMoveUserSprite.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Location = new System.Drawing.Point(9, 56);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(68, 13);
            this.label119.TabIndex = 130;
            this.label119.Text = "Target Sprite";
            // 
            // textBoxMoveTargetSprite
            // 
            this.textBoxMoveTargetSprite.Location = new System.Drawing.Point(12, 73);
            this.textBoxMoveTargetSprite.Name = "textBoxMoveTargetSprite";
            this.textBoxMoveTargetSprite.Size = new System.Drawing.Size(100, 20);
            this.textBoxMoveTargetSprite.TabIndex = 129;
            this.textBoxMoveTargetSprite.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Location = new System.Drawing.Point(118, 16);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(95, 13);
            this.label117.TabIndex = 128;
            this.label117.Text = "Background Sprite";
            // 
            // textBoxMoveBackgroundSprite
            // 
            this.textBoxMoveBackgroundSprite.Location = new System.Drawing.Point(118, 33);
            this.textBoxMoveBackgroundSprite.Name = "textBoxMoveBackgroundSprite";
            this.textBoxMoveBackgroundSprite.Size = new System.Drawing.Size(100, 20);
            this.textBoxMoveBackgroundSprite.TabIndex = 127;
            this.textBoxMoveBackgroundSprite.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Location = new System.Drawing.Point(9, 16);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(91, 13);
            this.label116.TabIndex = 126;
            this.label116.Text = "Foreground Sprite";
            // 
            // textBoxMoveForegroundSprite
            // 
            this.textBoxMoveForegroundSprite.Location = new System.Drawing.Point(12, 33);
            this.textBoxMoveForegroundSprite.Name = "textBoxMoveForegroundSprite";
            this.textBoxMoveForegroundSprite.Size = new System.Drawing.Size(100, 20);
            this.textBoxMoveForegroundSprite.TabIndex = 125;
            this.textBoxMoveForegroundSprite.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // buttonMovesMoveUp
            // 
            this.buttonMovesMoveUp.Location = new System.Drawing.Point(69, 352);
            this.buttonMovesMoveUp.Name = "buttonMovesMoveUp";
            this.buttonMovesMoveUp.Size = new System.Drawing.Size(57, 23);
            this.buttonMovesMoveUp.TabIndex = 18;
            this.buttonMovesMoveUp.Text = "Up";
            this.buttonMovesMoveUp.UseVisualStyleBackColor = true;
            this.buttonMovesMoveUp.Click += new System.EventHandler(this.buttonMovesMoveUp_Click);
            // 
            // textBoxMoveDesc
            // 
            this.textBoxMoveDesc.Location = new System.Drawing.Point(137, 66);
            this.textBoxMoveDesc.Multiline = true;
            this.textBoxMoveDesc.Name = "textBoxMoveDesc";
            this.textBoxMoveDesc.Size = new System.Drawing.Size(196, 74);
            this.textBoxMoveDesc.TabIndex = 101;
            this.textBoxMoveDesc.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // buttonMovesRemove
            // 
            this.buttonMovesRemove.Location = new System.Drawing.Point(6, 378);
            this.buttonMovesRemove.Name = "buttonMovesRemove";
            this.buttonMovesRemove.Size = new System.Drawing.Size(57, 23);
            this.buttonMovesRemove.TabIndex = 17;
            this.buttonMovesRemove.Text = "Remove";
            this.buttonMovesRemove.UseVisualStyleBackColor = true;
            this.buttonMovesRemove.Click += new System.EventHandler(this.buttonMovesRemove_Click);
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(137, 50);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(60, 13);
            this.label101.TabIndex = 102;
            this.label101.Text = "Description";
            // 
            // buttonMovesAdd
            // 
            this.buttonMovesAdd.Location = new System.Drawing.Point(6, 351);
            this.buttonMovesAdd.Name = "buttonMovesAdd";
            this.buttonMovesAdd.Size = new System.Drawing.Size(57, 23);
            this.buttonMovesAdd.TabIndex = 16;
            this.buttonMovesAdd.Text = "Add";
            this.buttonMovesAdd.UseVisualStyleBackColor = true;
            this.buttonMovesAdd.Click += new System.EventHandler(this.buttonMovesAdd_Click);
            // 
            // groupBox7
            // 
            this.groupBox7.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox7.Controls.Add(this.buttonMoveTextUpdate);
            this.groupBox7.Controls.Add(this.buttonMoveTextRemove);
            this.groupBox7.Controls.Add(this.buttonMoveTextAdd);
            this.groupBox7.Controls.Add(this.listBoxMoveTextList);
            this.groupBox7.Controls.Add(this.textBoxMoveTextLine);
            this.groupBox7.Location = new System.Drawing.Point(384, 155);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(288, 136);
            this.groupBox7.TabIndex = 126;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Text";
            // 
            // buttonMoveTextUpdate
            // 
            this.buttonMoveTextUpdate.Location = new System.Drawing.Point(208, 52);
            this.buttonMoveTextUpdate.Name = "buttonMoveTextUpdate";
            this.buttonMoveTextUpdate.Size = new System.Drawing.Size(75, 23);
            this.buttonMoveTextUpdate.TabIndex = 131;
            this.buttonMoveTextUpdate.Text = "Update";
            this.buttonMoveTextUpdate.UseVisualStyleBackColor = true;
            this.buttonMoveTextUpdate.Click += new System.EventHandler(this.buttonMoveTextUpdate_Click);
            // 
            // buttonMoveTextRemove
            // 
            this.buttonMoveTextRemove.Location = new System.Drawing.Point(208, 81);
            this.buttonMoveTextRemove.Name = "buttonMoveTextRemove";
            this.buttonMoveTextRemove.Size = new System.Drawing.Size(75, 23);
            this.buttonMoveTextRemove.TabIndex = 130;
            this.buttonMoveTextRemove.Text = "Remove";
            this.buttonMoveTextRemove.UseVisualStyleBackColor = true;
            this.buttonMoveTextRemove.Click += new System.EventHandler(this.buttonMoveTextRemove_Click);
            // 
            // buttonMoveTextAdd
            // 
            this.buttonMoveTextAdd.Location = new System.Drawing.Point(208, 24);
            this.buttonMoveTextAdd.Name = "buttonMoveTextAdd";
            this.buttonMoveTextAdd.Size = new System.Drawing.Size(75, 23);
            this.buttonMoveTextAdd.TabIndex = 129;
            this.buttonMoveTextAdd.Text = "Add";
            this.buttonMoveTextAdd.UseVisualStyleBackColor = true;
            this.buttonMoveTextAdd.Click += new System.EventHandler(this.buttonMoveTextAdd_Click);
            // 
            // listBoxMoveTextList
            // 
            this.listBoxMoveTextList.AccessibleRole = System.Windows.Forms.AccessibleRole.List;
            this.listBoxMoveTextList.FormattingEnabled = true;
            this.listBoxMoveTextList.Location = new System.Drawing.Point(6, 52);
            this.listBoxMoveTextList.Name = "listBoxMoveTextList";
            this.listBoxMoveTextList.Size = new System.Drawing.Size(196, 69);
            this.listBoxMoveTextList.TabIndex = 128;
            this.listBoxMoveTextList.SelectedIndexChanged += new System.EventHandler(this.listBoxMoveTextList_SelectedIndexChanged);
            // 
            // textBoxMoveTextLine
            // 
            this.textBoxMoveTextLine.Location = new System.Drawing.Point(6, 26);
            this.textBoxMoveTextLine.Name = "textBoxMoveTextLine";
            this.textBoxMoveTextLine.Size = new System.Drawing.Size(196, 20);
            this.textBoxMoveTextLine.TabIndex = 127;
            this.textBoxMoveTextLine.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // textBoxMoveScript
            // 
            this.textBoxMoveScript.Location = new System.Drawing.Point(246, 27);
            this.textBoxMoveScript.Name = "textBoxMoveScript";
            this.textBoxMoveScript.Size = new System.Drawing.Size(123, 20);
            this.textBoxMoveScript.TabIndex = 108;
            this.textBoxMoveScript.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(246, 10);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(34, 13);
            this.label105.TabIndex = 109;
            this.label105.Text = "Script";
            // 
            // groupBox8
            // 
            this.groupBox8.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox8.Controls.Add(this.comboBoxMoveStatus);
            this.groupBox8.Controls.Add(this.buttonMoveStatusRemove);
            this.groupBox8.Controls.Add(this.buttonMoveStatusAdd);
            this.groupBox8.Controls.Add(this.listBoxMoveStatusList);
            this.groupBox8.Location = new System.Drawing.Point(384, 13);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(183, 136);
            this.groupBox8.TabIndex = 131;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Status";
            // 
            // comboBoxMoveStatus
            // 
            this.comboBoxMoveStatus.FormattingEnabled = true;
            this.comboBoxMoveStatus.Location = new System.Drawing.Point(6, 25);
            this.comboBoxMoveStatus.Name = "comboBoxMoveStatus";
            this.comboBoxMoveStatus.Size = new System.Drawing.Size(99, 21);
            this.comboBoxMoveStatus.TabIndex = 132;
            // 
            // buttonMoveStatusRemove
            // 
            this.buttonMoveStatusRemove.Location = new System.Drawing.Point(108, 52);
            this.buttonMoveStatusRemove.Name = "buttonMoveStatusRemove";
            this.buttonMoveStatusRemove.Size = new System.Drawing.Size(69, 25);
            this.buttonMoveStatusRemove.TabIndex = 130;
            this.buttonMoveStatusRemove.Text = "Remove";
            this.buttonMoveStatusRemove.UseVisualStyleBackColor = true;
            this.buttonMoveStatusRemove.Click += new System.EventHandler(this.buttonMoveStatusRemove_Click);
            // 
            // buttonMoveStatusAdd
            // 
            this.buttonMoveStatusAdd.Location = new System.Drawing.Point(108, 23);
            this.buttonMoveStatusAdd.Name = "buttonMoveStatusAdd";
            this.buttonMoveStatusAdd.Size = new System.Drawing.Size(69, 25);
            this.buttonMoveStatusAdd.TabIndex = 129;
            this.buttonMoveStatusAdd.Text = "Add";
            this.buttonMoveStatusAdd.UseVisualStyleBackColor = true;
            this.buttonMoveStatusAdd.Click += new System.EventHandler(this.buttonMoveStatusAdd_Click);
            // 
            // listBoxMoveStatusList
            // 
            this.listBoxMoveStatusList.AccessibleRole = System.Windows.Forms.AccessibleRole.List;
            this.listBoxMoveStatusList.FormattingEnabled = true;
            this.listBoxMoveStatusList.Location = new System.Drawing.Point(6, 52);
            this.listBoxMoveStatusList.Name = "listBoxMoveStatusList";
            this.listBoxMoveStatusList.Size = new System.Drawing.Size(99, 69);
            this.listBoxMoveStatusList.TabIndex = 128;
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox5.Controls.Add(this.checkBoxMoveKnockback);
            this.groupBox5.Controls.Add(this.checkBoxMoveRotateCamera);
            this.groupBox5.Controls.Add(this.checkBoxMoveTargetSelf);
            this.groupBox5.Controls.Add(this.checkBoxMoveTargetAlly);
            this.groupBox5.Controls.Add(this.checkBoxMoveTargetSkip);
            this.groupBox5.Location = new System.Drawing.Point(6, 404);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(120, 136);
            this.groupBox5.TabIndex = 105;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Triggers";
            // 
            // checkBoxMoveKnockback
            // 
            this.checkBoxMoveKnockback.AutoSize = true;
            this.checkBoxMoveKnockback.Location = new System.Drawing.Point(8, 112);
            this.checkBoxMoveKnockback.Name = "checkBoxMoveKnockback";
            this.checkBoxMoveKnockback.Size = new System.Drawing.Size(81, 17);
            this.checkBoxMoveKnockback.TabIndex = 4;
            this.checkBoxMoveKnockback.Text = "Knockback";
            this.checkBoxMoveKnockback.UseVisualStyleBackColor = true;
            // 
            // checkBoxMoveRotateCamera
            // 
            this.checkBoxMoveRotateCamera.AutoSize = true;
            this.checkBoxMoveRotateCamera.Location = new System.Drawing.Point(8, 89);
            this.checkBoxMoveRotateCamera.Name = "checkBoxMoveRotateCamera";
            this.checkBoxMoveRotateCamera.Size = new System.Drawing.Size(97, 17);
            this.checkBoxMoveRotateCamera.TabIndex = 3;
            this.checkBoxMoveRotateCamera.Text = "Rotate Camera";
            this.checkBoxMoveRotateCamera.UseVisualStyleBackColor = true;
            // 
            // checkBoxMoveTargetSelf
            // 
            this.checkBoxMoveTargetSelf.AutoSize = true;
            this.checkBoxMoveTargetSelf.Location = new System.Drawing.Point(8, 66);
            this.checkBoxMoveTargetSelf.Name = "checkBoxMoveTargetSelf";
            this.checkBoxMoveTargetSelf.Size = new System.Drawing.Size(78, 17);
            this.checkBoxMoveTargetSelf.TabIndex = 2;
            this.checkBoxMoveTargetSelf.Text = "Target Self";
            this.checkBoxMoveTargetSelf.UseVisualStyleBackColor = true;
            // 
            // checkBoxMoveTargetAlly
            // 
            this.checkBoxMoveTargetAlly.AutoSize = true;
            this.checkBoxMoveTargetAlly.Location = new System.Drawing.Point(8, 43);
            this.checkBoxMoveTargetAlly.Name = "checkBoxMoveTargetAlly";
            this.checkBoxMoveTargetAlly.Size = new System.Drawing.Size(76, 17);
            this.checkBoxMoveTargetAlly.TabIndex = 1;
            this.checkBoxMoveTargetAlly.Text = "Target Ally";
            this.checkBoxMoveTargetAlly.UseVisualStyleBackColor = true;
            // 
            // checkBoxMoveTargetSkip
            // 
            this.checkBoxMoveTargetSkip.AutoSize = true;
            this.checkBoxMoveTargetSkip.Location = new System.Drawing.Point(8, 20);
            this.checkBoxMoveTargetSkip.Name = "checkBoxMoveTargetSkip";
            this.checkBoxMoveTargetSkip.Size = new System.Drawing.Size(81, 17);
            this.checkBoxMoveTargetSkip.TabIndex = 0;
            this.checkBoxMoveTargetSkip.Text = "Target Skip";
            this.checkBoxMoveTargetSkip.UseVisualStyleBackColor = true;
            // 
            // groupBox27
            // 
            this.groupBox27.BackColor = System.Drawing.Color.Gainsboro;
            this.groupBox27.Controls.Add(this.listBoxMonsterMoves);
            this.groupBox27.Controls.Add(this.listBoxMonsterMovesList);
            this.groupBox27.Controls.Add(this.buttonMonsterMovesAdd);
            this.groupBox27.Controls.Add(this.buttonMonsterMovesRemove);
            this.groupBox27.Controls.Add(this.label156);
            this.groupBox27.Controls.Add(this.comboBoxMonsterMovesMove);
            this.groupBox27.Controls.Add(this.label157);
            this.groupBox27.Controls.Add(this.textBoxMonsterMovesLevel);
            this.groupBox27.Location = new System.Drawing.Point(694, 149);
            this.groupBox27.Name = "groupBox27";
            this.groupBox27.Size = new System.Drawing.Size(395, 139);
            this.groupBox27.TabIndex = 157;
            this.groupBox27.TabStop = false;
            this.groupBox27.Text = "Enemy Moves";
            // 
            // listBoxMonsterMoves
            // 
            this.listBoxMonsterMoves.AccessibleRole = System.Windows.Forms.AccessibleRole.List;
            this.listBoxMonsterMoves.FormattingEnabled = true;
            this.listBoxMonsterMoves.Location = new System.Drawing.Point(6, 19);
            this.listBoxMonsterMoves.Name = "listBoxMonsterMoves";
            this.listBoxMonsterMoves.Size = new System.Drawing.Size(120, 108);
            this.listBoxMonsterMoves.TabIndex = 145;
            this.listBoxMonsterMoves.SelectedIndexChanged += new System.EventHandler(this.listBoxMonsterMoves_SelectedIndexChanged);
            // 
            // listBoxMonsterMovesList
            // 
            this.listBoxMonsterMovesList.AccessibleRole = System.Windows.Forms.AccessibleRole.List;
            this.listBoxMonsterMovesList.FormattingEnabled = true;
            this.listBoxMonsterMovesList.Location = new System.Drawing.Point(132, 19);
            this.listBoxMonsterMovesList.Name = "listBoxMonsterMovesList";
            this.listBoxMonsterMovesList.Size = new System.Drawing.Size(120, 108);
            this.listBoxMonsterMovesList.TabIndex = 146;
            // 
            // buttonMonsterMovesAdd
            // 
            this.buttonMonsterMovesAdd.Location = new System.Drawing.Point(258, 107);
            this.buttonMonsterMovesAdd.Name = "buttonMonsterMovesAdd";
            this.buttonMonsterMovesAdd.Size = new System.Drawing.Size(59, 23);
            this.buttonMonsterMovesAdd.TabIndex = 147;
            this.buttonMonsterMovesAdd.Text = "Add";
            this.buttonMonsterMovesAdd.UseVisualStyleBackColor = true;
            this.buttonMonsterMovesAdd.Click += new System.EventHandler(this.buttonMonsterMovesAdd_Click);
            // 
            // buttonMonsterMovesRemove
            // 
            this.buttonMonsterMovesRemove.Location = new System.Drawing.Point(323, 107);
            this.buttonMonsterMovesRemove.Name = "buttonMonsterMovesRemove";
            this.buttonMonsterMovesRemove.Size = new System.Drawing.Size(62, 23);
            this.buttonMonsterMovesRemove.TabIndex = 148;
            this.buttonMonsterMovesRemove.Text = "Remove";
            this.buttonMonsterMovesRemove.UseVisualStyleBackColor = true;
            this.buttonMonsterMovesRemove.Click += new System.EventHandler(this.buttonMonsterMovesRemove_Click);
            // 
            // label156
            // 
            this.label156.AutoSize = true;
            this.label156.Location = new System.Drawing.Point(261, 61);
            this.label156.Name = "label156";
            this.label156.Size = new System.Drawing.Size(34, 13);
            this.label156.TabIndex = 152;
            this.label156.Text = "Move";
            // 
            // comboBoxMonsterMovesMove
            // 
            this.comboBoxMonsterMovesMove.FormattingEnabled = true;
            this.comboBoxMonsterMovesMove.Location = new System.Drawing.Point(261, 80);
            this.comboBoxMonsterMovesMove.Name = "comboBoxMonsterMovesMove";
            this.comboBoxMonsterMovesMove.Size = new System.Drawing.Size(88, 21);
            this.comboBoxMonsterMovesMove.TabIndex = 149;
            // 
            // label157
            // 
            this.label157.AutoSize = true;
            this.label157.Location = new System.Drawing.Point(261, 20);
            this.label157.Name = "label157";
            this.label157.Size = new System.Drawing.Size(33, 13);
            this.label157.TabIndex = 151;
            this.label157.Text = "Level";
            // 
            // textBoxMonsterMovesLevel
            // 
            this.textBoxMonsterMovesLevel.Location = new System.Drawing.Point(261, 39);
            this.textBoxMonsterMovesLevel.Name = "textBoxMonsterMovesLevel";
            this.textBoxMonsterMovesLevel.Size = new System.Drawing.Size(88, 20);
            this.textBoxMonsterMovesLevel.TabIndex = 150;
            this.textBoxMonsterMovesLevel.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // groupBox26
            // 
            this.groupBox26.BackColor = System.Drawing.Color.Gainsboro;
            this.groupBox26.Controls.Add(this.listBoxCharacterMoves);
            this.groupBox26.Controls.Add(this.listBoxCharacterMovesList);
            this.groupBox26.Controls.Add(this.buttonCharacterMovesAdd);
            this.groupBox26.Controls.Add(this.buttonCharacterMovesRemove);
            this.groupBox26.Controls.Add(this.comboBoxCharacterMovesMove);
            this.groupBox26.Controls.Add(this.textBoxCharacterMovesLevel);
            this.groupBox26.Controls.Add(this.label154);
            this.groupBox26.Controls.Add(this.label155);
            this.groupBox26.Location = new System.Drawing.Point(694, 3);
            this.groupBox26.Name = "groupBox26";
            this.groupBox26.Size = new System.Drawing.Size(395, 140);
            this.groupBox26.TabIndex = 156;
            this.groupBox26.TabStop = false;
            this.groupBox26.Text = "Character Moves";
            // 
            // listBoxCharacterMoves
            // 
            this.listBoxCharacterMoves.AccessibleRole = System.Windows.Forms.AccessibleRole.List;
            this.listBoxCharacterMoves.FormattingEnabled = true;
            this.listBoxCharacterMoves.Location = new System.Drawing.Point(6, 19);
            this.listBoxCharacterMoves.Name = "listBoxCharacterMoves";
            this.listBoxCharacterMoves.Size = new System.Drawing.Size(120, 108);
            this.listBoxCharacterMoves.TabIndex = 136;
            this.listBoxCharacterMoves.SelectedIndexChanged += new System.EventHandler(this.listBoxCharacterMoves_SelectedIndexChanged);
            // 
            // listBoxCharacterMovesList
            // 
            this.listBoxCharacterMovesList.AccessibleRole = System.Windows.Forms.AccessibleRole.List;
            this.listBoxCharacterMovesList.FormattingEnabled = true;
            this.listBoxCharacterMovesList.Location = new System.Drawing.Point(132, 19);
            this.listBoxCharacterMovesList.Name = "listBoxCharacterMovesList";
            this.listBoxCharacterMovesList.Size = new System.Drawing.Size(120, 108);
            this.listBoxCharacterMovesList.TabIndex = 137;
            // 
            // buttonCharacterMovesAdd
            // 
            this.buttonCharacterMovesAdd.Location = new System.Drawing.Point(264, 96);
            this.buttonCharacterMovesAdd.Name = "buttonCharacterMovesAdd";
            this.buttonCharacterMovesAdd.Size = new System.Drawing.Size(53, 23);
            this.buttonCharacterMovesAdd.TabIndex = 138;
            this.buttonCharacterMovesAdd.Text = "Add";
            this.buttonCharacterMovesAdd.UseVisualStyleBackColor = true;
            this.buttonCharacterMovesAdd.Click += new System.EventHandler(this.buttonCharacterMovesAdd_Click);
            // 
            // buttonCharacterMovesRemove
            // 
            this.buttonCharacterMovesRemove.Location = new System.Drawing.Point(323, 96);
            this.buttonCharacterMovesRemove.Name = "buttonCharacterMovesRemove";
            this.buttonCharacterMovesRemove.Size = new System.Drawing.Size(62, 23);
            this.buttonCharacterMovesRemove.TabIndex = 139;
            this.buttonCharacterMovesRemove.Text = "Remove";
            this.buttonCharacterMovesRemove.UseVisualStyleBackColor = true;
            this.buttonCharacterMovesRemove.Click += new System.EventHandler(this.buttonCharacterMovesRemove_Click);
            // 
            // comboBoxCharacterMovesMove
            // 
            this.comboBoxCharacterMovesMove.FormattingEnabled = true;
            this.comboBoxCharacterMovesMove.Location = new System.Drawing.Point(264, 69);
            this.comboBoxCharacterMovesMove.Name = "comboBoxCharacterMovesMove";
            this.comboBoxCharacterMovesMove.Size = new System.Drawing.Size(88, 21);
            this.comboBoxCharacterMovesMove.TabIndex = 140;
            // 
            // textBoxCharacterMovesLevel
            // 
            this.textBoxCharacterMovesLevel.Location = new System.Drawing.Point(261, 27);
            this.textBoxCharacterMovesLevel.Name = "textBoxCharacterMovesLevel";
            this.textBoxCharacterMovesLevel.Size = new System.Drawing.Size(88, 20);
            this.textBoxCharacterMovesLevel.TabIndex = 141;
            this.textBoxCharacterMovesLevel.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label154
            // 
            this.label154.AutoSize = true;
            this.label154.Location = new System.Drawing.Point(258, 13);
            this.label154.Name = "label154";
            this.label154.Size = new System.Drawing.Size(33, 13);
            this.label154.TabIndex = 142;
            this.label154.Text = "Level";
            // 
            // label155
            // 
            this.label155.AutoSize = true;
            this.label155.Location = new System.Drawing.Point(261, 53);
            this.label155.Name = "label155";
            this.label155.Size = new System.Drawing.Size(34, 13);
            this.label155.TabIndex = 143;
            this.label155.Text = "Move";
            // 
            // groupBox11
            // 
            this.groupBox11.BackColor = System.Drawing.Color.Gainsboro;
            this.groupBox11.Controls.Add(this.label170);
            this.groupBox11.Controls.Add(this.textBoxWordDesc);
            this.groupBox11.Controls.Add(this.label169);
            this.groupBox11.Controls.Add(this.textBoxWordName);
            this.groupBox11.Controls.Add(this.comboBoxWordMove);
            this.groupBox11.Controls.Add(this.listBoxWords);
            this.groupBox11.Controls.Add(this.buttonWordAdd);
            this.groupBox11.Controls.Add(this.buttonWordRemove);
            this.groupBox11.Controls.Add(this.buttonWordUp);
            this.groupBox11.Controls.Add(this.buttonWordDown);
            this.groupBox11.Controls.Add(this.label172);
            this.groupBox11.Location = new System.Drawing.Point(693, 294);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(291, 207);
            this.groupBox11.TabIndex = 154;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Word Attacks";
            // 
            // label170
            // 
            this.label170.AutoSize = true;
            this.label170.Location = new System.Drawing.Point(6, 108);
            this.label170.Name = "label170";
            this.label170.Size = new System.Drawing.Size(60, 13);
            this.label170.TabIndex = 158;
            this.label170.Text = "Description";
            // 
            // textBoxWordDesc
            // 
            this.textBoxWordDesc.Location = new System.Drawing.Point(6, 124);
            this.textBoxWordDesc.Multiline = true;
            this.textBoxWordDesc.Name = "textBoxWordDesc";
            this.textBoxWordDesc.Size = new System.Drawing.Size(117, 69);
            this.textBoxWordDesc.TabIndex = 157;
            // 
            // label169
            // 
            this.label169.AutoSize = true;
            this.label169.Location = new System.Drawing.Point(132, 16);
            this.label169.Name = "label169";
            this.label169.Size = new System.Drawing.Size(35, 13);
            this.label169.TabIndex = 156;
            this.label169.Text = "Name";
            // 
            // textBoxWordName
            // 
            this.textBoxWordName.Location = new System.Drawing.Point(135, 33);
            this.textBoxWordName.Name = "textBoxWordName";
            this.textBoxWordName.Size = new System.Drawing.Size(78, 20);
            this.textBoxWordName.TabIndex = 155;
            this.textBoxWordName.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            this.textBoxWordName.Leave += new System.EventHandler(this.textBoxWordName_Leave);
            // 
            // comboBoxWordMove
            // 
            this.comboBoxWordMove.FormattingEnabled = true;
            this.comboBoxWordMove.Location = new System.Drawing.Point(135, 76);
            this.comboBoxWordMove.Name = "comboBoxWordMove";
            this.comboBoxWordMove.Size = new System.Drawing.Size(121, 21);
            this.comboBoxWordMove.TabIndex = 27;
            // 
            // listBoxWords
            // 
            this.listBoxWords.AccessibleRole = System.Windows.Forms.AccessibleRole.List;
            this.listBoxWords.FormattingEnabled = true;
            this.listBoxWords.Location = new System.Drawing.Point(6, 19);
            this.listBoxWords.Name = "listBoxWords";
            this.listBoxWords.Size = new System.Drawing.Size(120, 82);
            this.listBoxWords.TabIndex = 20;
            // 
            // buttonWordAdd
            // 
            this.buttonWordAdd.Location = new System.Drawing.Point(135, 124);
            this.buttonWordAdd.Name = "buttonWordAdd";
            this.buttonWordAdd.Size = new System.Drawing.Size(62, 23);
            this.buttonWordAdd.TabIndex = 21;
            this.buttonWordAdd.Text = "Add";
            this.buttonWordAdd.UseVisualStyleBackColor = true;
            this.buttonWordAdd.Click += new System.EventHandler(this.buttonWordAdd_Click);
            // 
            // buttonWordRemove
            // 
            this.buttonWordRemove.Location = new System.Drawing.Point(135, 153);
            this.buttonWordRemove.Name = "buttonWordRemove";
            this.buttonWordRemove.Size = new System.Drawing.Size(62, 23);
            this.buttonWordRemove.TabIndex = 22;
            this.buttonWordRemove.Text = "Remove";
            this.buttonWordRemove.UseVisualStyleBackColor = true;
            this.buttonWordRemove.Click += new System.EventHandler(this.buttonWordRemove_Click);
            // 
            // buttonWordUp
            // 
            this.buttonWordUp.Location = new System.Drawing.Point(200, 124);
            this.buttonWordUp.Name = "buttonWordUp";
            this.buttonWordUp.Size = new System.Drawing.Size(56, 23);
            this.buttonWordUp.TabIndex = 23;
            this.buttonWordUp.Text = "Up";
            this.buttonWordUp.UseVisualStyleBackColor = true;
            this.buttonWordUp.Click += new System.EventHandler(this.buttonWordUp_Click);
            // 
            // buttonWordDown
            // 
            this.buttonWordDown.Location = new System.Drawing.Point(200, 153);
            this.buttonWordDown.Name = "buttonWordDown";
            this.buttonWordDown.Size = new System.Drawing.Size(56, 23);
            this.buttonWordDown.TabIndex = 24;
            this.buttonWordDown.Text = "Down";
            this.buttonWordDown.UseVisualStyleBackColor = true;
            this.buttonWordDown.Click += new System.EventHandler(this.buttonWordDown_Click);
            // 
            // label172
            // 
            this.label172.AutoSize = true;
            this.label172.Location = new System.Drawing.Point(132, 60);
            this.label172.Name = "label172";
            this.label172.Size = new System.Drawing.Size(34, 13);
            this.label172.TabIndex = 26;
            this.label172.Text = "Move";
            // 
            // tabBook
            // 
            this.tabBook.BackColor = System.Drawing.Color.Silver;
            this.tabBook.Controls.Add(this.groupBox16);
            this.tabBook.Controls.Add(this.buttonBookSave);
            this.tabBook.Controls.Add(this.buttonBookLoad);
            this.tabBook.Controls.Add(this.richTextBoxBook);
            this.tabBook.Controls.Add(this.label152);
            this.tabBook.Controls.Add(this.textBoxBookFilename);
            this.tabBook.Controls.Add(this.label151);
            this.tabBook.Controls.Add(this.textBoxBookAuthor);
            this.tabBook.Controls.Add(this.label6);
            this.tabBook.Controls.Add(this.textBoxBookName);
            this.tabBook.Controls.Add(this.buttonBookMoveDown);
            this.tabBook.Controls.Add(this.buttonBookMoveUp);
            this.tabBook.Controls.Add(this.buttonBookRemove);
            this.tabBook.Controls.Add(this.buttonBookAdd);
            this.tabBook.Controls.Add(this.listBoxBooks);
            this.tabBook.Location = new System.Drawing.Point(4, 22);
            this.tabBook.Name = "tabBook";
            this.tabBook.Size = new System.Drawing.Size(1317, 624);
            this.tabBook.TabIndex = 5;
            this.tabBook.Text = "Books";
            // 
            // groupBox16
            // 
            this.groupBox16.BackColor = System.Drawing.Color.Gainsboro;
            this.groupBox16.Controls.Add(this.textBoxDiagramBlend);
            this.groupBox16.Controls.Add(this.label192);
            this.groupBox16.Controls.Add(this.label190);
            this.groupBox16.Controls.Add(this.textBoxDiagramYOffset);
            this.groupBox16.Controls.Add(this.label191);
            this.groupBox16.Controls.Add(this.textBoxDiagramXOffset);
            this.groupBox16.Controls.Add(this.label189);
            this.groupBox16.Controls.Add(this.textBoxDiagramName);
            this.groupBox16.Controls.Add(this.label188);
            this.groupBox16.Controls.Add(this.textBoxDiagramSprite);
            this.groupBox16.Controls.Add(this.label185);
            this.groupBox16.Controls.Add(this.textBoxDiagramAltText);
            this.groupBox16.Controls.Add(this.label186);
            this.groupBox16.Controls.Add(this.textBoxDiagramYScale);
            this.groupBox16.Controls.Add(this.label187);
            this.groupBox16.Controls.Add(this.buttonDiagramMoveDown);
            this.groupBox16.Controls.Add(this.buttonDiagramMoveUp);
            this.groupBox16.Controls.Add(this.textBoxDiagramXScale);
            this.groupBox16.Controls.Add(this.listBoxDiagram);
            this.groupBox16.Controls.Add(this.buttonDiagramRemove);
            this.groupBox16.Controls.Add(this.buttonDiagramAdd);
            this.groupBox16.Location = new System.Drawing.Point(129, 126);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(291, 345);
            this.groupBox16.TabIndex = 150;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Diagrams";
            // 
            // textBoxDiagramBlend
            // 
            this.textBoxDiagramBlend.Location = new System.Drawing.Point(135, 197);
            this.textBoxDiagramBlend.Multiline = true;
            this.textBoxDiagramBlend.Name = "textBoxDiagramBlend";
            this.textBoxDiagramBlend.ReadOnly = true;
            this.textBoxDiagramBlend.Size = new System.Drawing.Size(74, 39);
            this.textBoxDiagramBlend.TabIndex = 152;
            this.textBoxDiagramBlend.Click += new System.EventHandler(this.textBoxDiagramBlend_Click);
            // 
            // label192
            // 
            this.label192.AutoSize = true;
            this.label192.Location = new System.Drawing.Point(132, 181);
            this.label192.Name = "label192";
            this.label192.Size = new System.Drawing.Size(34, 13);
            this.label192.TabIndex = 151;
            this.label192.Text = "Blend";
            // 
            // label190
            // 
            this.label190.AutoSize = true;
            this.label190.Location = new System.Drawing.Point(202, 137);
            this.label190.Name = "label190";
            this.label190.Size = new System.Drawing.Size(45, 13);
            this.label190.TabIndex = 163;
            this.label190.Text = "Y Offset";
            // 
            // textBoxDiagramYOffset
            // 
            this.textBoxDiagramYOffset.Location = new System.Drawing.Point(205, 153);
            this.textBoxDiagramYOffset.Name = "textBoxDiagramYOffset";
            this.textBoxDiagramYOffset.Size = new System.Drawing.Size(59, 20);
            this.textBoxDiagramYOffset.TabIndex = 162;
            this.textBoxDiagramYOffset.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label191
            // 
            this.label191.AutoSize = true;
            this.label191.Location = new System.Drawing.Point(132, 137);
            this.label191.Name = "label191";
            this.label191.Size = new System.Drawing.Size(45, 13);
            this.label191.TabIndex = 161;
            this.label191.Text = "X Offset";
            // 
            // textBoxDiagramXOffset
            // 
            this.textBoxDiagramXOffset.Location = new System.Drawing.Point(135, 153);
            this.textBoxDiagramXOffset.Name = "textBoxDiagramXOffset";
            this.textBoxDiagramXOffset.Size = new System.Drawing.Size(59, 20);
            this.textBoxDiagramXOffset.TabIndex = 160;
            this.textBoxDiagramXOffset.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label189
            // 
            this.label189.AutoSize = true;
            this.label189.Location = new System.Drawing.Point(132, 16);
            this.label189.Name = "label189";
            this.label189.Size = new System.Drawing.Size(35, 13);
            this.label189.TabIndex = 159;
            this.label189.Text = "Name";
            // 
            // textBoxDiagramName
            // 
            this.textBoxDiagramName.Location = new System.Drawing.Point(135, 32);
            this.textBoxDiagramName.Name = "textBoxDiagramName";
            this.textBoxDiagramName.Size = new System.Drawing.Size(150, 20);
            this.textBoxDiagramName.TabIndex = 158;
            this.textBoxDiagramName.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label188
            // 
            this.label188.AutoSize = true;
            this.label188.Location = new System.Drawing.Point(132, 55);
            this.label188.Name = "label188";
            this.label188.Size = new System.Drawing.Size(34, 13);
            this.label188.TabIndex = 157;
            this.label188.Text = "Sprite";
            // 
            // textBoxDiagramSprite
            // 
            this.textBoxDiagramSprite.Location = new System.Drawing.Point(134, 71);
            this.textBoxDiagramSprite.Name = "textBoxDiagramSprite";
            this.textBoxDiagramSprite.Size = new System.Drawing.Size(150, 20);
            this.textBoxDiagramSprite.TabIndex = 156;
            this.textBoxDiagramSprite.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label185
            // 
            this.label185.AutoSize = true;
            this.label185.Location = new System.Drawing.Point(132, 239);
            this.label185.Name = "label185";
            this.label185.Size = new System.Drawing.Size(43, 13);
            this.label185.TabIndex = 155;
            this.label185.Text = "Alt Text";
            // 
            // textBoxDiagramAltText
            // 
            this.textBoxDiagramAltText.Location = new System.Drawing.Point(134, 255);
            this.textBoxDiagramAltText.Multiline = true;
            this.textBoxDiagramAltText.Name = "textBoxDiagramAltText";
            this.textBoxDiagramAltText.Size = new System.Drawing.Size(150, 80);
            this.textBoxDiagramAltText.TabIndex = 154;
            this.textBoxDiagramAltText.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label186
            // 
            this.label186.AutoSize = true;
            this.label186.Location = new System.Drawing.Point(201, 98);
            this.label186.Name = "label186";
            this.label186.Size = new System.Drawing.Size(44, 13);
            this.label186.TabIndex = 153;
            this.label186.Text = "Y Scale";
            // 
            // textBoxDiagramYScale
            // 
            this.textBoxDiagramYScale.Location = new System.Drawing.Point(204, 114);
            this.textBoxDiagramYScale.Name = "textBoxDiagramYScale";
            this.textBoxDiagramYScale.Size = new System.Drawing.Size(59, 20);
            this.textBoxDiagramYScale.TabIndex = 152;
            this.textBoxDiagramYScale.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label187
            // 
            this.label187.AutoSize = true;
            this.label187.Location = new System.Drawing.Point(131, 98);
            this.label187.Name = "label187";
            this.label187.Size = new System.Drawing.Size(44, 13);
            this.label187.TabIndex = 151;
            this.label187.Text = "X Scale";
            // 
            // buttonDiagramMoveDown
            // 
            this.buttonDiagramMoveDown.Location = new System.Drawing.Point(71, 310);
            this.buttonDiagramMoveDown.Name = "buttonDiagramMoveDown";
            this.buttonDiagramMoveDown.Size = new System.Drawing.Size(58, 25);
            this.buttonDiagramMoveDown.TabIndex = 14;
            this.buttonDiagramMoveDown.Text = "Down";
            this.buttonDiagramMoveDown.UseVisualStyleBackColor = true;
            this.buttonDiagramMoveDown.Click += new System.EventHandler(this.buttonDiagramMoveDown_Click);
            // 
            // buttonDiagramMoveUp
            // 
            this.buttonDiagramMoveUp.Location = new System.Drawing.Point(71, 279);
            this.buttonDiagramMoveUp.Name = "buttonDiagramMoveUp";
            this.buttonDiagramMoveUp.Size = new System.Drawing.Size(58, 25);
            this.buttonDiagramMoveUp.TabIndex = 13;
            this.buttonDiagramMoveUp.Text = "Up";
            this.buttonDiagramMoveUp.UseVisualStyleBackColor = true;
            this.buttonDiagramMoveUp.Click += new System.EventHandler(this.buttonDiagramMoveUp_Click);
            // 
            // textBoxDiagramXScale
            // 
            this.textBoxDiagramXScale.Location = new System.Drawing.Point(134, 114);
            this.textBoxDiagramXScale.Name = "textBoxDiagramXScale";
            this.textBoxDiagramXScale.Size = new System.Drawing.Size(59, 20);
            this.textBoxDiagramXScale.TabIndex = 150;
            this.textBoxDiagramXScale.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // listBoxDiagram
            // 
            this.listBoxDiagram.AccessibleRole = System.Windows.Forms.AccessibleRole.List;
            this.listBoxDiagram.FormattingEnabled = true;
            this.listBoxDiagram.Location = new System.Drawing.Point(6, 19);
            this.listBoxDiagram.Name = "listBoxDiagram";
            this.listBoxDiagram.Size = new System.Drawing.Size(120, 186);
            this.listBoxDiagram.TabIndex = 11;
            this.listBoxDiagram.SelectedIndexChanged += new System.EventHandler(this.listBoxDiagram_SelectedIndexChanged);
            // 
            // buttonDiagramRemove
            // 
            this.buttonDiagramRemove.Location = new System.Drawing.Point(6, 310);
            this.buttonDiagramRemove.Name = "buttonDiagramRemove";
            this.buttonDiagramRemove.Size = new System.Drawing.Size(58, 25);
            this.buttonDiagramRemove.TabIndex = 12;
            this.buttonDiagramRemove.Text = "Remove";
            this.buttonDiagramRemove.UseVisualStyleBackColor = true;
            this.buttonDiagramRemove.Click += new System.EventHandler(this.buttonDiagramRemove_Click);
            // 
            // buttonDiagramAdd
            // 
            this.buttonDiagramAdd.Location = new System.Drawing.Point(6, 279);
            this.buttonDiagramAdd.Name = "buttonDiagramAdd";
            this.buttonDiagramAdd.Size = new System.Drawing.Size(58, 25);
            this.buttonDiagramAdd.TabIndex = 11;
            this.buttonDiagramAdd.Text = "Add";
            this.buttonDiagramAdd.UseVisualStyleBackColor = true;
            this.buttonDiagramAdd.Click += new System.EventHandler(this.buttonDiagramAdd_Click);
            // 
            // buttonBookSave
            // 
            this.buttonBookSave.Location = new System.Drawing.Point(345, 56);
            this.buttonBookSave.Name = "buttonBookSave";
            this.buttonBookSave.Size = new System.Drawing.Size(75, 23);
            this.buttonBookSave.TabIndex = 38;
            this.buttonBookSave.Text = "Save";
            this.buttonBookSave.UseVisualStyleBackColor = true;
            this.buttonBookSave.Click += new System.EventHandler(this.buttonBookSave_Click);
            // 
            // buttonBookLoad
            // 
            this.buttonBookLoad.Location = new System.Drawing.Point(345, 17);
            this.buttonBookLoad.Name = "buttonBookLoad";
            this.buttonBookLoad.Size = new System.Drawing.Size(75, 23);
            this.buttonBookLoad.TabIndex = 37;
            this.buttonBookLoad.Text = "Load";
            this.buttonBookLoad.UseVisualStyleBackColor = true;
            this.buttonBookLoad.Click += new System.EventHandler(this.buttonBookLoad_Click);
            // 
            // richTextBoxBook
            // 
            this.richTextBoxBook.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBoxBook.Location = new System.Drawing.Point(426, 16);
            this.richTextBoxBook.Name = "richTextBoxBook";
            this.richTextBoxBook.Size = new System.Drawing.Size(880, 595);
            this.richTextBoxBook.TabIndex = 36;
            this.richTextBoxBook.Text = "";
            // 
            // label152
            // 
            this.label152.AutoSize = true;
            this.label152.Location = new System.Drawing.Point(126, 43);
            this.label152.Name = "label152";
            this.label152.Size = new System.Drawing.Size(49, 13);
            this.label152.TabIndex = 35;
            this.label152.Text = "Filename";
            // 
            // textBoxBookFilename
            // 
            this.textBoxBookFilename.Location = new System.Drawing.Point(129, 59);
            this.textBoxBookFilename.Name = "textBoxBookFilename";
            this.textBoxBookFilename.ReadOnly = true;
            this.textBoxBookFilename.Size = new System.Drawing.Size(209, 20);
            this.textBoxBookFilename.TabIndex = 34;
            // 
            // label151
            // 
            this.label151.AutoSize = true;
            this.label151.Location = new System.Drawing.Point(129, 83);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(38, 13);
            this.label151.TabIndex = 33;
            this.label151.Text = "Author";
            // 
            // textBoxBookAuthor
            // 
            this.textBoxBookAuthor.Location = new System.Drawing.Point(129, 100);
            this.textBoxBookAuthor.Name = "textBoxBookAuthor";
            this.textBoxBookAuthor.Size = new System.Drawing.Size(209, 20);
            this.textBoxBookAuthor.TabIndex = 32;
            this.textBoxBookAuthor.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(129, 3);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(27, 13);
            this.label6.TabIndex = 31;
            this.label6.Text = "Title";
            // 
            // textBoxBookName
            // 
            this.textBoxBookName.Location = new System.Drawing.Point(129, 20);
            this.textBoxBookName.Name = "textBoxBookName";
            this.textBoxBookName.Size = new System.Drawing.Size(209, 20);
            this.textBoxBookName.TabIndex = 30;
            this.textBoxBookName.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            this.textBoxBookName.Leave += new System.EventHandler(this.textBoxBookName_Leave);
            // 
            // buttonBookMoveDown
            // 
            this.buttonBookMoveDown.Location = new System.Drawing.Point(3, 592);
            this.buttonBookMoveDown.Name = "buttonBookMoveDown";
            this.buttonBookMoveDown.Size = new System.Drawing.Size(120, 23);
            this.buttonBookMoveDown.TabIndex = 29;
            this.buttonBookMoveDown.Text = "Move Down";
            this.buttonBookMoveDown.UseVisualStyleBackColor = true;
            this.buttonBookMoveDown.Click += new System.EventHandler(this.buttonBookMoveDown_Click);
            // 
            // buttonBookMoveUp
            // 
            this.buttonBookMoveUp.Location = new System.Drawing.Point(3, 565);
            this.buttonBookMoveUp.Name = "buttonBookMoveUp";
            this.buttonBookMoveUp.Size = new System.Drawing.Size(120, 23);
            this.buttonBookMoveUp.TabIndex = 28;
            this.buttonBookMoveUp.Text = "Move Up";
            this.buttonBookMoveUp.UseVisualStyleBackColor = true;
            this.buttonBookMoveUp.Click += new System.EventHandler(this.buttonBookMoveUp_Click);
            // 
            // buttonBookRemove
            // 
            this.buttonBookRemove.Location = new System.Drawing.Point(3, 536);
            this.buttonBookRemove.Name = "buttonBookRemove";
            this.buttonBookRemove.Size = new System.Drawing.Size(120, 23);
            this.buttonBookRemove.TabIndex = 27;
            this.buttonBookRemove.Text = "Remove";
            this.buttonBookRemove.UseVisualStyleBackColor = true;
            this.buttonBookRemove.Click += new System.EventHandler(this.buttonBookRemove_Click);
            // 
            // buttonBookAdd
            // 
            this.buttonBookAdd.Location = new System.Drawing.Point(3, 507);
            this.buttonBookAdd.Name = "buttonBookAdd";
            this.buttonBookAdd.Size = new System.Drawing.Size(120, 23);
            this.buttonBookAdd.TabIndex = 26;
            this.buttonBookAdd.Text = "Add";
            this.buttonBookAdd.UseVisualStyleBackColor = true;
            this.buttonBookAdd.Click += new System.EventHandler(this.buttonBookAdd_Click);
            // 
            // listBoxBooks
            // 
            this.listBoxBooks.AccessibleRole = System.Windows.Forms.AccessibleRole.List;
            this.listBoxBooks.FormattingEnabled = true;
            this.listBoxBooks.Location = new System.Drawing.Point(3, 3);
            this.listBoxBooks.Name = "listBoxBooks";
            this.listBoxBooks.Size = new System.Drawing.Size(120, 498);
            this.listBoxBooks.TabIndex = 25;
            // 
            // tabBird
            // 
            this.tabBird.BackColor = System.Drawing.Color.Silver;
            this.tabBird.Controls.Add(this.groupBox30);
            this.tabBird.Location = new System.Drawing.Point(4, 22);
            this.tabBird.Name = "tabBird";
            this.tabBird.Size = new System.Drawing.Size(1317, 624);
            this.tabBird.TabIndex = 6;
            this.tabBird.Text = "Wildlife";
            // 
            // groupBox30
            // 
            this.groupBox30.BackColor = System.Drawing.Color.Gainsboro;
            this.groupBox30.Controls.Add(this.label7);
            this.groupBox30.Controls.Add(this.listBoxBird);
            this.groupBox30.Controls.Add(this.buttonBirdMoveDown);
            this.groupBox30.Controls.Add(this.label146);
            this.groupBox30.Controls.Add(this.buttonBirdMoveUp);
            this.groupBox30.Controls.Add(this.label150);
            this.groupBox30.Controls.Add(this.buttonBirdRemove);
            this.groupBox30.Controls.Add(this.textBoxBirdFemaleSprite);
            this.groupBox30.Controls.Add(this.buttonBirdAdd);
            this.groupBox30.Controls.Add(this.textBoxBirdName);
            this.groupBox30.Controls.Add(this.label147);
            this.groupBox30.Controls.Add(this.textBoxBirdSpawnRatio);
            this.groupBox30.Controls.Add(this.textBoxBirdMaleSprite);
            this.groupBox30.Controls.Add(this.textBoxBirdDesc);
            this.groupBox30.Controls.Add(this.label141);
            this.groupBox30.Controls.Add(this.label149);
            this.groupBox30.Controls.Add(this.textBoxBirdMetricWeight);
            this.groupBox30.Controls.Add(this.label133);
            this.groupBox30.Controls.Add(this.label143);
            this.groupBox30.Controls.Add(this.textBoxBirdOffset);
            this.groupBox30.Controls.Add(this.textBoxBirdMetricHeight);
            this.groupBox30.Controls.Add(this.textBoxBirdLongName);
            this.groupBox30.Controls.Add(this.label139);
            this.groupBox30.Controls.Add(this.label148);
            this.groupBox30.Controls.Add(this.textBoxBirdWeight);
            this.groupBox30.Controls.Add(this.textBoxBirdScale);
            this.groupBox30.Controls.Add(this.label137);
            this.groupBox30.Controls.Add(this.textBoxBirdHeight);
            this.groupBox30.Controls.Add(this.label135);
            this.groupBox30.Location = new System.Drawing.Point(6, 3);
            this.groupBox30.Name = "groupBox30";
            this.groupBox30.Size = new System.Drawing.Size(372, 334);
            this.groupBox30.TabIndex = 127;
            this.groupBox30.TabStop = false;
            this.groupBox30.Text = "Birds";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 13);
            this.label7.TabIndex = 36;
            this.label7.Text = "Name";
            // 
            // listBoxBird
            // 
            this.listBoxBird.AccessibleRole = System.Windows.Forms.AccessibleRole.List;
            this.listBoxBird.FormattingEnabled = true;
            this.listBoxBird.Location = new System.Drawing.Point(6, 99);
            this.listBoxBird.Name = "listBoxBird";
            this.listBoxBird.Size = new System.Drawing.Size(131, 147);
            this.listBoxBird.TabIndex = 30;
            // 
            // buttonBirdMoveDown
            // 
            this.buttonBirdMoveDown.Location = new System.Drawing.Point(76, 286);
            this.buttonBirdMoveDown.Name = "buttonBirdMoveDown";
            this.buttonBirdMoveDown.Size = new System.Drawing.Size(61, 23);
            this.buttonBirdMoveDown.TabIndex = 34;
            this.buttonBirdMoveDown.Text = "Down";
            this.buttonBirdMoveDown.UseVisualStyleBackColor = true;
            this.buttonBirdMoveDown.Click += new System.EventHandler(this.buttonBirdMoveDown_Click);
            // 
            // label146
            // 
            this.label146.AutoSize = true;
            this.label146.Location = new System.Drawing.Point(252, 96);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(71, 13);
            this.label146.TabIndex = 120;
            this.label146.Text = "Female Sprite";
            // 
            // buttonBirdMoveUp
            // 
            this.buttonBirdMoveUp.Location = new System.Drawing.Point(76, 259);
            this.buttonBirdMoveUp.Name = "buttonBirdMoveUp";
            this.buttonBirdMoveUp.Size = new System.Drawing.Size(61, 23);
            this.buttonBirdMoveUp.TabIndex = 33;
            this.buttonBirdMoveUp.Text = "Up";
            this.buttonBirdMoveUp.UseVisualStyleBackColor = true;
            this.buttonBirdMoveUp.Click += new System.EventHandler(this.buttonBirdMoveUp_Click);
            // 
            // label150
            // 
            this.label150.AutoSize = true;
            this.label150.Location = new System.Drawing.Point(289, 136);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(68, 13);
            this.label150.TabIndex = 126;
            this.label150.Text = "Spawn Ratio";
            // 
            // buttonBirdRemove
            // 
            this.buttonBirdRemove.Location = new System.Drawing.Point(9, 286);
            this.buttonBirdRemove.Name = "buttonBirdRemove";
            this.buttonBirdRemove.Size = new System.Drawing.Size(61, 23);
            this.buttonBirdRemove.TabIndex = 32;
            this.buttonBirdRemove.Text = "Remove";
            this.buttonBirdRemove.UseVisualStyleBackColor = true;
            this.buttonBirdRemove.Click += new System.EventHandler(this.buttonBirdRemove_Click);
            // 
            // textBoxBirdFemaleSprite
            // 
            this.textBoxBirdFemaleSprite.Location = new System.Drawing.Point(252, 113);
            this.textBoxBirdFemaleSprite.Name = "textBoxBirdFemaleSprite";
            this.textBoxBirdFemaleSprite.Size = new System.Drawing.Size(100, 20);
            this.textBoxBirdFemaleSprite.TabIndex = 119;
            this.textBoxBirdFemaleSprite.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // buttonBirdAdd
            // 
            this.buttonBirdAdd.Location = new System.Drawing.Point(9, 259);
            this.buttonBirdAdd.Name = "buttonBirdAdd";
            this.buttonBirdAdd.Size = new System.Drawing.Size(61, 23);
            this.buttonBirdAdd.TabIndex = 31;
            this.buttonBirdAdd.Text = "Add";
            this.buttonBirdAdd.UseVisualStyleBackColor = true;
            this.buttonBirdAdd.Click += new System.EventHandler(this.buttonBirdAdd_Click);
            // 
            // textBoxBirdName
            // 
            this.textBoxBirdName.Location = new System.Drawing.Point(6, 33);
            this.textBoxBirdName.Name = "textBoxBirdName";
            this.textBoxBirdName.Size = new System.Drawing.Size(131, 20);
            this.textBoxBirdName.TabIndex = 35;
            this.textBoxBirdName.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            this.textBoxBirdName.Leave += new System.EventHandler(this.textBoxBirdName_Leave);
            // 
            // label147
            // 
            this.label147.AutoSize = true;
            this.label147.Location = new System.Drawing.Point(143, 96);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(60, 13);
            this.label147.TabIndex = 118;
            this.label147.Text = "Male Sprite";
            // 
            // textBoxBirdSpawnRatio
            // 
            this.textBoxBirdSpawnRatio.Location = new System.Drawing.Point(289, 153);
            this.textBoxBirdSpawnRatio.Name = "textBoxBirdSpawnRatio";
            this.textBoxBirdSpawnRatio.Size = new System.Drawing.Size(63, 20);
            this.textBoxBirdSpawnRatio.TabIndex = 125;
            this.textBoxBirdSpawnRatio.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // textBoxBirdMaleSprite
            // 
            this.textBoxBirdMaleSprite.Location = new System.Drawing.Point(143, 113);
            this.textBoxBirdMaleSprite.Name = "textBoxBirdMaleSprite";
            this.textBoxBirdMaleSprite.Size = new System.Drawing.Size(100, 20);
            this.textBoxBirdMaleSprite.TabIndex = 117;
            this.textBoxBirdMaleSprite.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // textBoxBirdDesc
            // 
            this.textBoxBirdDesc.Location = new System.Drawing.Point(143, 198);
            this.textBoxBirdDesc.Multiline = true;
            this.textBoxBirdDesc.Name = "textBoxBirdDesc";
            this.textBoxBirdDesc.Size = new System.Drawing.Size(214, 111);
            this.textBoxBirdDesc.TabIndex = 105;
            this.textBoxBirdDesc.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label141
            // 
            this.label141.AutoSize = true;
            this.label141.Location = new System.Drawing.Point(252, 56);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(73, 13);
            this.label141.TabIndex = 116;
            this.label141.Text = "Metric Weight";
            // 
            // label149
            // 
            this.label149.AutoSize = true;
            this.label149.Location = new System.Drawing.Point(216, 136);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(35, 13);
            this.label149.TabIndex = 124;
            this.label149.Text = "Offset";
            // 
            // textBoxBirdMetricWeight
            // 
            this.textBoxBirdMetricWeight.Location = new System.Drawing.Point(252, 73);
            this.textBoxBirdMetricWeight.Name = "textBoxBirdMetricWeight";
            this.textBoxBirdMetricWeight.Size = new System.Drawing.Size(100, 20);
            this.textBoxBirdMetricWeight.TabIndex = 115;
            this.textBoxBirdMetricWeight.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Location = new System.Drawing.Point(143, 181);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(60, 13);
            this.label133.TabIndex = 106;
            this.label133.Text = "Description";
            // 
            // label143
            // 
            this.label143.AutoSize = true;
            this.label143.Location = new System.Drawing.Point(143, 56);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(70, 13);
            this.label143.TabIndex = 114;
            this.label143.Text = "Metric Height";
            // 
            // textBoxBirdOffset
            // 
            this.textBoxBirdOffset.Location = new System.Drawing.Point(216, 153);
            this.textBoxBirdOffset.Name = "textBoxBirdOffset";
            this.textBoxBirdOffset.Size = new System.Drawing.Size(67, 20);
            this.textBoxBirdOffset.TabIndex = 123;
            this.textBoxBirdOffset.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // textBoxBirdMetricHeight
            // 
            this.textBoxBirdMetricHeight.Location = new System.Drawing.Point(143, 73);
            this.textBoxBirdMetricHeight.Name = "textBoxBirdMetricHeight";
            this.textBoxBirdMetricHeight.Size = new System.Drawing.Size(100, 20);
            this.textBoxBirdMetricHeight.TabIndex = 113;
            this.textBoxBirdMetricHeight.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // textBoxBirdLongName
            // 
            this.textBoxBirdLongName.Location = new System.Drawing.Point(6, 73);
            this.textBoxBirdLongName.Name = "textBoxBirdLongName";
            this.textBoxBirdLongName.Size = new System.Drawing.Size(131, 20);
            this.textBoxBirdLongName.TabIndex = 107;
            this.textBoxBirdLongName.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label139
            // 
            this.label139.AutoSize = true;
            this.label139.Location = new System.Drawing.Point(252, 16);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(41, 13);
            this.label139.TabIndex = 112;
            this.label139.Text = "Weight";
            // 
            // label148
            // 
            this.label148.AutoSize = true;
            this.label148.Location = new System.Drawing.Point(143, 136);
            this.label148.Name = "label148";
            this.label148.Size = new System.Drawing.Size(34, 13);
            this.label148.TabIndex = 122;
            this.label148.Text = "Scale";
            // 
            // textBoxBirdWeight
            // 
            this.textBoxBirdWeight.Location = new System.Drawing.Point(252, 33);
            this.textBoxBirdWeight.Name = "textBoxBirdWeight";
            this.textBoxBirdWeight.Size = new System.Drawing.Size(100, 20);
            this.textBoxBirdWeight.TabIndex = 111;
            this.textBoxBirdWeight.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // textBoxBirdScale
            // 
            this.textBoxBirdScale.Location = new System.Drawing.Point(143, 153);
            this.textBoxBirdScale.Name = "textBoxBirdScale";
            this.textBoxBirdScale.Size = new System.Drawing.Size(67, 20);
            this.textBoxBirdScale.TabIndex = 121;
            this.textBoxBirdScale.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label137
            // 
            this.label137.AutoSize = true;
            this.label137.Location = new System.Drawing.Point(143, 16);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(38, 13);
            this.label137.TabIndex = 110;
            this.label137.Text = "Height";
            // 
            // textBoxBirdHeight
            // 
            this.textBoxBirdHeight.Location = new System.Drawing.Point(143, 33);
            this.textBoxBirdHeight.Name = "textBoxBirdHeight";
            this.textBoxBirdHeight.Size = new System.Drawing.Size(100, 20);
            this.textBoxBirdHeight.TabIndex = 109;
            this.textBoxBirdHeight.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label135
            // 
            this.label135.AutoSize = true;
            this.label135.Location = new System.Drawing.Point(6, 56);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(81, 13);
            this.label135.TabIndex = 108;
            this.label135.Text = "Scientific Name";
            // 
            // tabExp
            // 
            this.tabExp.BackColor = System.Drawing.Color.Silver;
            this.tabExp.Controls.Add(this.panelGrowthDisplay);
            this.tabExp.Controls.Add(this.label40);
            this.tabExp.Controls.Add(this.textBoxGrowthName);
            this.tabExp.Controls.Add(this.buttonGrowthMoveDown);
            this.tabExp.Controls.Add(this.buttonGrowthMoveUp);
            this.tabExp.Controls.Add(this.buttonGrowthRemove);
            this.tabExp.Controls.Add(this.buttonGrowthAdd);
            this.tabExp.Controls.Add(this.listBoxGrowth);
            this.tabExp.Location = new System.Drawing.Point(4, 22);
            this.tabExp.Name = "tabExp";
            this.tabExp.Size = new System.Drawing.Size(1317, 624);
            this.tabExp.TabIndex = 9;
            this.tabExp.Text = "Growth Curves";
            // 
            // panelGrowthDisplay
            // 
            this.panelGrowthDisplay.BackColor = System.Drawing.Color.Black;
            this.panelGrowthDisplay.Location = new System.Drawing.Point(132, 65);
            this.panelGrowthDisplay.Name = "panelGrowthDisplay";
            this.panelGrowthDisplay.Size = new System.Drawing.Size(1173, 484);
            this.panelGrowthDisplay.TabIndex = 46;
            this.panelGrowthDisplay.Paint += new System.Windows.Forms.PaintEventHandler(this.panelGrowthDisplay_Paint);
            this.panelGrowthDisplay.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panelGrowthDisplay_MouseMove);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(129, 3);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(35, 13);
            this.label40.TabIndex = 43;
            this.label40.Text = "Name";
            // 
            // textBoxGrowthName
            // 
            this.textBoxGrowthName.Location = new System.Drawing.Point(129, 20);
            this.textBoxGrowthName.Name = "textBoxGrowthName";
            this.textBoxGrowthName.Size = new System.Drawing.Size(100, 20);
            this.textBoxGrowthName.TabIndex = 42;
            this.textBoxGrowthName.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            this.textBoxGrowthName.Leave += new System.EventHandler(this.textBoxGrowthName_Leave);
            // 
            // buttonGrowthMoveDown
            // 
            this.buttonGrowthMoveDown.Location = new System.Drawing.Point(3, 592);
            this.buttonGrowthMoveDown.Name = "buttonGrowthMoveDown";
            this.buttonGrowthMoveDown.Size = new System.Drawing.Size(120, 23);
            this.buttonGrowthMoveDown.TabIndex = 41;
            this.buttonGrowthMoveDown.Text = "Move Down";
            this.buttonGrowthMoveDown.UseVisualStyleBackColor = true;
            this.buttonGrowthMoveDown.Click += new System.EventHandler(this.buttonGrowthMoveDown_Click);
            // 
            // buttonGrowthMoveUp
            // 
            this.buttonGrowthMoveUp.Location = new System.Drawing.Point(3, 565);
            this.buttonGrowthMoveUp.Name = "buttonGrowthMoveUp";
            this.buttonGrowthMoveUp.Size = new System.Drawing.Size(120, 23);
            this.buttonGrowthMoveUp.TabIndex = 40;
            this.buttonGrowthMoveUp.Text = "Move Up";
            this.buttonGrowthMoveUp.UseVisualStyleBackColor = true;
            this.buttonGrowthMoveUp.Click += new System.EventHandler(this.buttonGrowthMoveUp_Click);
            // 
            // buttonGrowthRemove
            // 
            this.buttonGrowthRemove.Location = new System.Drawing.Point(3, 536);
            this.buttonGrowthRemove.Name = "buttonGrowthRemove";
            this.buttonGrowthRemove.Size = new System.Drawing.Size(120, 23);
            this.buttonGrowthRemove.TabIndex = 39;
            this.buttonGrowthRemove.Text = "Remove";
            this.buttonGrowthRemove.UseVisualStyleBackColor = true;
            this.buttonGrowthRemove.Click += new System.EventHandler(this.buttonGrowthRemove_Click);
            // 
            // buttonGrowthAdd
            // 
            this.buttonGrowthAdd.Location = new System.Drawing.Point(3, 507);
            this.buttonGrowthAdd.Name = "buttonGrowthAdd";
            this.buttonGrowthAdd.Size = new System.Drawing.Size(120, 23);
            this.buttonGrowthAdd.TabIndex = 38;
            this.buttonGrowthAdd.Text = "Add";
            this.buttonGrowthAdd.UseVisualStyleBackColor = true;
            this.buttonGrowthAdd.Click += new System.EventHandler(this.buttonGrowthAdd_Click);
            // 
            // listBoxGrowth
            // 
            this.listBoxGrowth.AccessibleRole = System.Windows.Forms.AccessibleRole.List;
            this.listBoxGrowth.FormattingEnabled = true;
            this.listBoxGrowth.Location = new System.Drawing.Point(3, 3);
            this.listBoxGrowth.Name = "listBoxGrowth";
            this.listBoxGrowth.Size = new System.Drawing.Size(120, 498);
            this.listBoxGrowth.TabIndex = 37;
            this.listBoxGrowth.SelectedIndexChanged += new System.EventHandler(this.listBoxGrowth_SelectedIndexChanged);
            // 
            // tabBattleScripts
            // 
            this.tabBattleScripts.BackColor = System.Drawing.Color.Silver;
            this.tabBattleScripts.Controls.Add(this.groupBox10);
            this.tabBattleScripts.Controls.Add(this.groupBox9);
            this.tabBattleScripts.Location = new System.Drawing.Point(4, 22);
            this.tabBattleScripts.Name = "tabBattleScripts";
            this.tabBattleScripts.Size = new System.Drawing.Size(1317, 624);
            this.tabBattleScripts.TabIndex = 11;
            this.tabBattleScripts.Text = "Battle Scripts";
            // 
            // groupBox10
            // 
            this.groupBox10.BackColor = System.Drawing.Color.Gainsboro;
            this.groupBox10.Controls.Add(this.listBoxStatus);
            this.groupBox10.Controls.Add(this.label129);
            this.groupBox10.Controls.Add(this.buttonStatusAdd);
            this.groupBox10.Controls.Add(this.textBoxStatusShader);
            this.groupBox10.Controls.Add(this.buttonStatusRemove);
            this.groupBox10.Controls.Add(this.label128);
            this.groupBox10.Controls.Add(this.buttonStatusMoveUp);
            this.groupBox10.Controls.Add(this.textBoxStatusFinalText);
            this.groupBox10.Controls.Add(this.buttonStatusMoveDown);
            this.groupBox10.Controls.Add(this.label127);
            this.groupBox10.Controls.Add(this.textBoxStatusName);
            this.groupBox10.Controls.Add(this.textBoxStatusSprite);
            this.groupBox10.Controls.Add(this.label5);
            this.groupBox10.Controls.Add(this.checkBoxStatusHarmful);
            this.groupBox10.Controls.Add(this.textBoxStatusDesc);
            this.groupBox10.Controls.Add(this.label125);
            this.groupBox10.Controls.Add(this.label124);
            this.groupBox10.Controls.Add(this.textBoxStatusMaxTurns);
            this.groupBox10.Controls.Add(this.textBoxStatusMinTurns);
            this.groupBox10.Controls.Add(this.label126);
            this.groupBox10.Location = new System.Drawing.Point(17, 283);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(365, 279);
            this.groupBox10.TabIndex = 126;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Status Effects";
            // 
            // listBoxStatus
            // 
            this.listBoxStatus.AccessibleRole = System.Windows.Forms.AccessibleRole.List;
            this.listBoxStatus.FormattingEnabled = true;
            this.listBoxStatus.Location = new System.Drawing.Point(6, 19);
            this.listBoxStatus.Name = "listBoxStatus";
            this.listBoxStatus.Size = new System.Drawing.Size(120, 134);
            this.listBoxStatus.TabIndex = 20;
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Location = new System.Drawing.Point(241, 148);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(41, 13);
            this.label129.TabIndex = 124;
            this.label129.Text = "Shader";
            // 
            // buttonStatusAdd
            // 
            this.buttonStatusAdd.Location = new System.Drawing.Point(9, 159);
            this.buttonStatusAdd.Name = "buttonStatusAdd";
            this.buttonStatusAdd.Size = new System.Drawing.Size(120, 23);
            this.buttonStatusAdd.TabIndex = 21;
            this.buttonStatusAdd.Text = "Add";
            this.buttonStatusAdd.UseVisualStyleBackColor = true;
            // 
            // textBoxStatusShader
            // 
            this.textBoxStatusShader.Location = new System.Drawing.Point(244, 164);
            this.textBoxStatusShader.Name = "textBoxStatusShader";
            this.textBoxStatusShader.Size = new System.Drawing.Size(97, 20);
            this.textBoxStatusShader.TabIndex = 123;
            this.textBoxStatusShader.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // buttonStatusRemove
            // 
            this.buttonStatusRemove.Location = new System.Drawing.Point(9, 188);
            this.buttonStatusRemove.Name = "buttonStatusRemove";
            this.buttonStatusRemove.Size = new System.Drawing.Size(120, 23);
            this.buttonStatusRemove.TabIndex = 22;
            this.buttonStatusRemove.Text = "Remove";
            this.buttonStatusRemove.UseVisualStyleBackColor = true;
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Location = new System.Drawing.Point(145, 187);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(53, 13);
            this.label128.TabIndex = 122;
            this.label128.Text = "Final Text";
            // 
            // buttonStatusMoveUp
            // 
            this.buttonStatusMoveUp.Location = new System.Drawing.Point(9, 217);
            this.buttonStatusMoveUp.Name = "buttonStatusMoveUp";
            this.buttonStatusMoveUp.Size = new System.Drawing.Size(120, 23);
            this.buttonStatusMoveUp.TabIndex = 23;
            this.buttonStatusMoveUp.Text = "Move Up";
            this.buttonStatusMoveUp.UseVisualStyleBackColor = true;
            // 
            // textBoxStatusFinalText
            // 
            this.textBoxStatusFinalText.Location = new System.Drawing.Point(145, 204);
            this.textBoxStatusFinalText.Name = "textBoxStatusFinalText";
            this.textBoxStatusFinalText.Size = new System.Drawing.Size(201, 20);
            this.textBoxStatusFinalText.TabIndex = 121;
            this.textBoxStatusFinalText.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // buttonStatusMoveDown
            // 
            this.buttonStatusMoveDown.Location = new System.Drawing.Point(9, 244);
            this.buttonStatusMoveDown.Name = "buttonStatusMoveDown";
            this.buttonStatusMoveDown.Size = new System.Drawing.Size(120, 23);
            this.buttonStatusMoveDown.TabIndex = 24;
            this.buttonStatusMoveDown.Text = "Move Down";
            this.buttonStatusMoveDown.UseVisualStyleBackColor = true;
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Location = new System.Drawing.Point(145, 147);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(34, 13);
            this.label127.TabIndex = 120;
            this.label127.Text = "Sprite";
            // 
            // textBoxStatusName
            // 
            this.textBoxStatusName.Location = new System.Drawing.Point(145, 30);
            this.textBoxStatusName.Name = "textBoxStatusName";
            this.textBoxStatusName.Size = new System.Drawing.Size(100, 20);
            this.textBoxStatusName.TabIndex = 25;
            this.textBoxStatusName.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // textBoxStatusSprite
            // 
            this.textBoxStatusSprite.Location = new System.Drawing.Point(145, 164);
            this.textBoxStatusSprite.Name = "textBoxStatusSprite";
            this.textBoxStatusSprite.Size = new System.Drawing.Size(97, 20);
            this.textBoxStatusSprite.TabIndex = 119;
            this.textBoxStatusSprite.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(145, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 26;
            this.label5.Text = "Name";
            // 
            // checkBoxStatusHarmful
            // 
            this.checkBoxStatusHarmful.AutoSize = true;
            this.checkBoxStatusHarmful.Location = new System.Drawing.Point(276, 243);
            this.checkBoxStatusHarmful.Name = "checkBoxStatusHarmful";
            this.checkBoxStatusHarmful.Size = new System.Drawing.Size(68, 17);
            this.checkBoxStatusHarmful.TabIndex = 118;
            this.checkBoxStatusHarmful.Text = "Harmful?";
            this.checkBoxStatusHarmful.UseVisualStyleBackColor = true;
            // 
            // textBoxStatusDesc
            // 
            this.textBoxStatusDesc.Location = new System.Drawing.Point(145, 70);
            this.textBoxStatusDesc.Multiline = true;
            this.textBoxStatusDesc.Name = "textBoxStatusDesc";
            this.textBoxStatusDesc.Size = new System.Drawing.Size(196, 74);
            this.textBoxStatusDesc.TabIndex = 103;
            this.textBoxStatusDesc.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Location = new System.Drawing.Point(207, 227);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(57, 13);
            this.label125.TabIndex = 117;
            this.label125.Text = "Max Turns";
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Location = new System.Drawing.Point(145, 53);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(60, 13);
            this.label124.TabIndex = 104;
            this.label124.Text = "Description";
            // 
            // textBoxStatusMaxTurns
            // 
            this.textBoxStatusMaxTurns.Location = new System.Drawing.Point(210, 243);
            this.textBoxStatusMaxTurns.Name = "textBoxStatusMaxTurns";
            this.textBoxStatusMaxTurns.Size = new System.Drawing.Size(60, 20);
            this.textBoxStatusMaxTurns.TabIndex = 116;
            this.textBoxStatusMaxTurns.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // textBoxStatusMinTurns
            // 
            this.textBoxStatusMinTurns.Location = new System.Drawing.Point(146, 243);
            this.textBoxStatusMinTurns.Name = "textBoxStatusMinTurns";
            this.textBoxStatusMinTurns.Size = new System.Drawing.Size(60, 20);
            this.textBoxStatusMinTurns.TabIndex = 114;
            this.textBoxStatusMinTurns.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Location = new System.Drawing.Point(144, 227);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(54, 13);
            this.label126.TabIndex = 115;
            this.label126.Text = "Min Turns";
            // 
            // groupBox9
            // 
            this.groupBox9.BackColor = System.Drawing.Color.Gainsboro;
            this.groupBox9.Controls.Add(this.checkBoxEndPhasePause);
            this.groupBox9.Controls.Add(this.label168);
            this.groupBox9.Controls.Add(this.textBoxEndPhaseConditionScript);
            this.groupBox9.Controls.Add(this.checkBoxEndPhaseTarget);
            this.groupBox9.Controls.Add(this.buttonEndPhaseMoveDown);
            this.groupBox9.Controls.Add(this.label165);
            this.groupBox9.Controls.Add(this.buttonEndPhaseMoveUp);
            this.groupBox9.Controls.Add(this.textBoxEndPhaseLength);
            this.groupBox9.Controls.Add(this.buttonEndPhaseRemove);
            this.groupBox9.Controls.Add(this.label164);
            this.groupBox9.Controls.Add(this.buttonEndPhaseAdd);
            this.groupBox9.Controls.Add(this.textBoxEndPhaseName);
            this.groupBox9.Controls.Add(this.listBoxEndPhase);
            this.groupBox9.Controls.Add(this.label160);
            this.groupBox9.Controls.Add(this.label163);
            this.groupBox9.Controls.Add(this.textBoxEndPhaseScript);
            this.groupBox9.Controls.Add(this.textBoxEndPhaseFgSprite);
            this.groupBox9.Controls.Add(this.label161);
            this.groupBox9.Controls.Add(this.textBoxEndPhaseBgSprite);
            this.groupBox9.Controls.Add(this.textBoxEndPhaseTargetSprite);
            this.groupBox9.Controls.Add(this.label162);
            this.groupBox9.Location = new System.Drawing.Point(17, 15);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(365, 262);
            this.groupBox9.TabIndex = 0;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "End Phase";
            // 
            // checkBoxEndPhasePause
            // 
            this.checkBoxEndPhasePause.AutoSize = true;
            this.checkBoxEndPhasePause.Location = new System.Drawing.Point(251, 125);
            this.checkBoxEndPhasePause.Name = "checkBoxEndPhasePause";
            this.checkBoxEndPhasePause.Size = new System.Drawing.Size(62, 17);
            this.checkBoxEndPhasePause.TabIndex = 148;
            this.checkBoxEndPhasePause.Text = "Pause?";
            this.checkBoxEndPhasePause.UseVisualStyleBackColor = true;
            // 
            // label168
            // 
            this.label168.AutoSize = true;
            this.label168.Location = new System.Drawing.Point(248, 148);
            this.label168.Name = "label168";
            this.label168.Size = new System.Drawing.Size(81, 13);
            this.label168.TabIndex = 146;
            this.label168.Text = "Condition Script";
            // 
            // textBoxEndPhaseConditionScript
            // 
            this.textBoxEndPhaseConditionScript.Location = new System.Drawing.Point(248, 165);
            this.textBoxEndPhaseConditionScript.Name = "textBoxEndPhaseConditionScript";
            this.textBoxEndPhaseConditionScript.Size = new System.Drawing.Size(100, 20);
            this.textBoxEndPhaseConditionScript.TabIndex = 145;
            this.textBoxEndPhaseConditionScript.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // checkBoxEndPhaseTarget
            // 
            this.checkBoxEndPhaseTarget.AutoSize = true;
            this.checkBoxEndPhaseTarget.Location = new System.Drawing.Point(251, 102);
            this.checkBoxEndPhaseTarget.Name = "checkBoxEndPhaseTarget";
            this.checkBoxEndPhaseTarget.Size = new System.Drawing.Size(68, 17);
            this.checkBoxEndPhaseTarget.TabIndex = 147;
            this.checkBoxEndPhaseTarget.Text = "Targets?";
            this.checkBoxEndPhaseTarget.UseVisualStyleBackColor = true;
            // 
            // buttonEndPhaseMoveDown
            // 
            this.buttonEndPhaseMoveDown.Location = new System.Drawing.Point(246, 220);
            this.buttonEndPhaseMoveDown.Name = "buttonEndPhaseMoveDown";
            this.buttonEndPhaseMoveDown.Size = new System.Drawing.Size(100, 23);
            this.buttonEndPhaseMoveDown.TabIndex = 46;
            this.buttonEndPhaseMoveDown.Text = "Move Down";
            this.buttonEndPhaseMoveDown.UseVisualStyleBackColor = true;
            this.buttonEndPhaseMoveDown.Click += new System.EventHandler(this.buttonEndPhaseMoveDown_Click);
            // 
            // label165
            // 
            this.label165.AutoSize = true;
            this.label165.Location = new System.Drawing.Point(288, 18);
            this.label165.Name = "label165";
            this.label165.Size = new System.Drawing.Size(40, 13);
            this.label165.TabIndex = 144;
            this.label165.Text = "Length";
            // 
            // buttonEndPhaseMoveUp
            // 
            this.buttonEndPhaseMoveUp.Location = new System.Drawing.Point(246, 191);
            this.buttonEndPhaseMoveUp.Name = "buttonEndPhaseMoveUp";
            this.buttonEndPhaseMoveUp.Size = new System.Drawing.Size(100, 23);
            this.buttonEndPhaseMoveUp.TabIndex = 45;
            this.buttonEndPhaseMoveUp.Text = "Move Up";
            this.buttonEndPhaseMoveUp.UseVisualStyleBackColor = true;
            this.buttonEndPhaseMoveUp.Click += new System.EventHandler(this.buttonEndPhaseMoveUp_Click);
            // 
            // textBoxEndPhaseLength
            // 
            this.textBoxEndPhaseLength.Location = new System.Drawing.Point(291, 36);
            this.textBoxEndPhaseLength.Name = "textBoxEndPhaseLength";
            this.textBoxEndPhaseLength.Size = new System.Drawing.Size(55, 20);
            this.textBoxEndPhaseLength.TabIndex = 143;
            this.textBoxEndPhaseLength.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // buttonEndPhaseRemove
            // 
            this.buttonEndPhaseRemove.Location = new System.Drawing.Point(140, 220);
            this.buttonEndPhaseRemove.Name = "buttonEndPhaseRemove";
            this.buttonEndPhaseRemove.Size = new System.Drawing.Size(100, 23);
            this.buttonEndPhaseRemove.TabIndex = 44;
            this.buttonEndPhaseRemove.Text = "Remove";
            this.buttonEndPhaseRemove.UseVisualStyleBackColor = true;
            this.buttonEndPhaseRemove.Click += new System.EventHandler(this.buttonEndPhaseRemove_Click);
            // 
            // label164
            // 
            this.label164.AutoSize = true;
            this.label164.Location = new System.Drawing.Point(140, 19);
            this.label164.Name = "label164";
            this.label164.Size = new System.Drawing.Size(35, 13);
            this.label164.TabIndex = 142;
            this.label164.Text = "Name";
            // 
            // buttonEndPhaseAdd
            // 
            this.buttonEndPhaseAdd.Location = new System.Drawing.Point(140, 191);
            this.buttonEndPhaseAdd.Name = "buttonEndPhaseAdd";
            this.buttonEndPhaseAdd.Size = new System.Drawing.Size(100, 23);
            this.buttonEndPhaseAdd.TabIndex = 43;
            this.buttonEndPhaseAdd.Text = "Add";
            this.buttonEndPhaseAdd.UseVisualStyleBackColor = true;
            this.buttonEndPhaseAdd.Click += new System.EventHandler(this.buttonEndPhaseAdd_Click);
            // 
            // textBoxEndPhaseName
            // 
            this.textBoxEndPhaseName.Location = new System.Drawing.Point(140, 36);
            this.textBoxEndPhaseName.Name = "textBoxEndPhaseName";
            this.textBoxEndPhaseName.Size = new System.Drawing.Size(145, 20);
            this.textBoxEndPhaseName.TabIndex = 141;
            this.textBoxEndPhaseName.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            this.textBoxEndPhaseName.Leave += new System.EventHandler(this.buttonEndPhaseName_Leave);
            // 
            // listBoxEndPhase
            // 
            this.listBoxEndPhase.AccessibleRole = System.Windows.Forms.AccessibleRole.List;
            this.listBoxEndPhase.FormattingEnabled = true;
            this.listBoxEndPhase.Location = new System.Drawing.Point(14, 19);
            this.listBoxEndPhase.Name = "listBoxEndPhase";
            this.listBoxEndPhase.Size = new System.Drawing.Size(120, 160);
            this.listBoxEndPhase.TabIndex = 42;
            // 
            // label160
            // 
            this.label160.AutoSize = true;
            this.label160.Location = new System.Drawing.Point(140, 148);
            this.label160.Name = "label160";
            this.label160.Size = new System.Drawing.Size(34, 13);
            this.label160.TabIndex = 140;
            this.label160.Text = "Script";
            // 
            // label163
            // 
            this.label163.AutoSize = true;
            this.label163.Location = new System.Drawing.Point(137, 59);
            this.label163.Name = "label163";
            this.label163.Size = new System.Drawing.Size(91, 13);
            this.label163.TabIndex = 134;
            this.label163.Text = "Foreground Sprite";
            // 
            // textBoxEndPhaseScript
            // 
            this.textBoxEndPhaseScript.Location = new System.Drawing.Point(140, 165);
            this.textBoxEndPhaseScript.Name = "textBoxEndPhaseScript";
            this.textBoxEndPhaseScript.Size = new System.Drawing.Size(100, 20);
            this.textBoxEndPhaseScript.TabIndex = 139;
            this.textBoxEndPhaseScript.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // textBoxEndPhaseFgSprite
            // 
            this.textBoxEndPhaseFgSprite.Location = new System.Drawing.Point(140, 76);
            this.textBoxEndPhaseFgSprite.Name = "textBoxEndPhaseFgSprite";
            this.textBoxEndPhaseFgSprite.Size = new System.Drawing.Size(100, 20);
            this.textBoxEndPhaseFgSprite.TabIndex = 133;
            this.textBoxEndPhaseFgSprite.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label161
            // 
            this.label161.AutoSize = true;
            this.label161.Location = new System.Drawing.Point(137, 99);
            this.label161.Name = "label161";
            this.label161.Size = new System.Drawing.Size(68, 13);
            this.label161.TabIndex = 138;
            this.label161.Text = "Target Sprite";
            // 
            // textBoxEndPhaseBgSprite
            // 
            this.textBoxEndPhaseBgSprite.Location = new System.Drawing.Point(246, 76);
            this.textBoxEndPhaseBgSprite.Name = "textBoxEndPhaseBgSprite";
            this.textBoxEndPhaseBgSprite.Size = new System.Drawing.Size(100, 20);
            this.textBoxEndPhaseBgSprite.TabIndex = 135;
            this.textBoxEndPhaseBgSprite.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // textBoxEndPhaseTargetSprite
            // 
            this.textBoxEndPhaseTargetSprite.Location = new System.Drawing.Point(140, 116);
            this.textBoxEndPhaseTargetSprite.Name = "textBoxEndPhaseTargetSprite";
            this.textBoxEndPhaseTargetSprite.Size = new System.Drawing.Size(100, 20);
            this.textBoxEndPhaseTargetSprite.TabIndex = 137;
            this.textBoxEndPhaseTargetSprite.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label162
            // 
            this.label162.AutoSize = true;
            this.label162.Location = new System.Drawing.Point(246, 59);
            this.label162.Name = "label162";
            this.label162.Size = new System.Drawing.Size(95, 13);
            this.label162.TabIndex = 136;
            this.label162.Text = "Background Sprite";
            // 
            // tabAXHud
            // 
            this.tabAXHud.BackColor = System.Drawing.Color.Silver;
            this.tabAXHud.Controls.Add(this.groupBox31);
            this.tabAXHud.Controls.Add(this.groupBox25);
            this.tabAXHud.Controls.Add(this.groupBox24);
            this.tabAXHud.Controls.Add(this.groupBox23);
            this.tabAXHud.Controls.Add(this.groupBox22);
            this.tabAXHud.Controls.Add(this.groupBox18);
            this.tabAXHud.Controls.Add(this.groupBox13);
            this.tabAXHud.Location = new System.Drawing.Point(4, 22);
            this.tabAXHud.Name = "tabAXHud";
            this.tabAXHud.Size = new System.Drawing.Size(1317, 624);
            this.tabAXHud.TabIndex = 12;
            this.tabAXHud.Text = "HUDs/Textboxes";
            // 
            // groupBox31
            // 
            this.groupBox31.BackColor = System.Drawing.Color.Gainsboro;
            this.groupBox31.Controls.Add(this.buttonItemEventDown);
            this.groupBox31.Controls.Add(this.listBoxItemEvent);
            this.groupBox31.Controls.Add(this.buttonItemEventUp);
            this.groupBox31.Controls.Add(this.buttonItemEventAdd);
            this.groupBox31.Controls.Add(this.buttonItemEventRemove);
            this.groupBox31.Location = new System.Drawing.Point(148, 439);
            this.groupBox31.Name = "groupBox31";
            this.groupBox31.Size = new System.Drawing.Size(139, 179);
            this.groupBox31.TabIndex = 175;
            this.groupBox31.TabStop = false;
            this.groupBox31.Text = "Item Event List";
            // 
            // buttonItemEventDown
            // 
            this.buttonItemEventDown.Location = new System.Drawing.Point(69, 138);
            this.buttonItemEventDown.Name = "buttonItemEventDown";
            this.buttonItemEventDown.Size = new System.Drawing.Size(58, 24);
            this.buttonItemEventDown.TabIndex = 164;
            this.buttonItemEventDown.Text = "Down";
            this.buttonItemEventDown.UseVisualStyleBackColor = true;
            this.buttonItemEventDown.Click += new System.EventHandler(this.buttonItemEventMoveDown_Click);
            // 
            // listBoxItemEvent
            // 
            this.listBoxItemEvent.AccessibleRole = System.Windows.Forms.AccessibleRole.List;
            this.listBoxItemEvent.FormattingEnabled = true;
            this.listBoxItemEvent.Location = new System.Drawing.Point(9, 19);
            this.listBoxItemEvent.Name = "listBoxItemEvent";
            this.listBoxItemEvent.Size = new System.Drawing.Size(120, 82);
            this.listBoxItemEvent.TabIndex = 160;
            this.listBoxItemEvent.DoubleClick += new System.EventHandler(this.listBoxItemEvent_DoubleClick);
            // 
            // buttonItemEventUp
            // 
            this.buttonItemEventUp.Location = new System.Drawing.Point(69, 108);
            this.buttonItemEventUp.Name = "buttonItemEventUp";
            this.buttonItemEventUp.Size = new System.Drawing.Size(58, 24);
            this.buttonItemEventUp.TabIndex = 163;
            this.buttonItemEventUp.Text = "Up";
            this.buttonItemEventUp.UseVisualStyleBackColor = true;
            this.buttonItemEventUp.Click += new System.EventHandler(this.buttonItemEventMoveUp_Click);
            // 
            // buttonItemEventAdd
            // 
            this.buttonItemEventAdd.Location = new System.Drawing.Point(7, 107);
            this.buttonItemEventAdd.Name = "buttonItemEventAdd";
            this.buttonItemEventAdd.Size = new System.Drawing.Size(58, 25);
            this.buttonItemEventAdd.TabIndex = 161;
            this.buttonItemEventAdd.Text = "Add";
            this.buttonItemEventAdd.UseVisualStyleBackColor = true;
            this.buttonItemEventAdd.Click += new System.EventHandler(this.buttonItemEventAdd_Click);
            // 
            // buttonItemEventRemove
            // 
            this.buttonItemEventRemove.Location = new System.Drawing.Point(7, 138);
            this.buttonItemEventRemove.Name = "buttonItemEventRemove";
            this.buttonItemEventRemove.Size = new System.Drawing.Size(58, 25);
            this.buttonItemEventRemove.TabIndex = 162;
            this.buttonItemEventRemove.Text = "Remove";
            this.buttonItemEventRemove.UseVisualStyleBackColor = true;
            this.buttonItemEventRemove.Click += new System.EventHandler(this.buttonItemEventRemove_Click);
            // 
            // groupBox25
            // 
            this.groupBox25.BackColor = System.Drawing.Color.Gainsboro;
            this.groupBox25.Controls.Add(this.buttonEventDown);
            this.groupBox25.Controls.Add(this.listBoxEvent);
            this.groupBox25.Controls.Add(this.buttonEventUp);
            this.groupBox25.Controls.Add(this.buttonEventAdd);
            this.groupBox25.Controls.Add(this.buttonEventRemove);
            this.groupBox25.Location = new System.Drawing.Point(6, 439);
            this.groupBox25.Name = "groupBox25";
            this.groupBox25.Size = new System.Drawing.Size(139, 179);
            this.groupBox25.TabIndex = 174;
            this.groupBox25.TabStop = false;
            this.groupBox25.Text = "Event List";
            // 
            // buttonEventDown
            // 
            this.buttonEventDown.Location = new System.Drawing.Point(69, 138);
            this.buttonEventDown.Name = "buttonEventDown";
            this.buttonEventDown.Size = new System.Drawing.Size(58, 24);
            this.buttonEventDown.TabIndex = 164;
            this.buttonEventDown.Text = "Down";
            this.buttonEventDown.UseVisualStyleBackColor = true;
            this.buttonEventDown.Click += new System.EventHandler(this.buttonEventMoveDown_Click);
            // 
            // listBoxEvent
            // 
            this.listBoxEvent.AccessibleRole = System.Windows.Forms.AccessibleRole.List;
            this.listBoxEvent.FormattingEnabled = true;
            this.listBoxEvent.Location = new System.Drawing.Point(9, 19);
            this.listBoxEvent.Name = "listBoxEvent";
            this.listBoxEvent.Size = new System.Drawing.Size(120, 82);
            this.listBoxEvent.TabIndex = 160;
            this.listBoxEvent.DoubleClick += new System.EventHandler(this.listBoxEvent_DoubleClick);
            // 
            // buttonEventUp
            // 
            this.buttonEventUp.Location = new System.Drawing.Point(69, 108);
            this.buttonEventUp.Name = "buttonEventUp";
            this.buttonEventUp.Size = new System.Drawing.Size(58, 24);
            this.buttonEventUp.TabIndex = 163;
            this.buttonEventUp.Text = "Up";
            this.buttonEventUp.UseVisualStyleBackColor = true;
            this.buttonEventUp.Click += new System.EventHandler(this.buttonEventMoveUp_Click);
            // 
            // buttonEventAdd
            // 
            this.buttonEventAdd.Location = new System.Drawing.Point(7, 107);
            this.buttonEventAdd.Name = "buttonEventAdd";
            this.buttonEventAdd.Size = new System.Drawing.Size(58, 25);
            this.buttonEventAdd.TabIndex = 161;
            this.buttonEventAdd.Text = "Add";
            this.buttonEventAdd.UseVisualStyleBackColor = true;
            this.buttonEventAdd.Click += new System.EventHandler(this.buttonEventAdd_Click);
            // 
            // buttonEventRemove
            // 
            this.buttonEventRemove.Location = new System.Drawing.Point(7, 138);
            this.buttonEventRemove.Name = "buttonEventRemove";
            this.buttonEventRemove.Size = new System.Drawing.Size(58, 25);
            this.buttonEventRemove.TabIndex = 162;
            this.buttonEventRemove.Text = "Remove";
            this.buttonEventRemove.UseVisualStyleBackColor = true;
            this.buttonEventRemove.Click += new System.EventHandler(this.buttonEventRemove_Click);
            // 
            // groupBox24
            // 
            this.groupBox24.BackColor = System.Drawing.Color.Gainsboro;
            this.groupBox24.Controls.Add(this.label231);
            this.groupBox24.Controls.Add(this.textBoxVoicemailDialogID);
            this.groupBox24.Controls.Add(this.label232);
            this.groupBox24.Controls.Add(this.textBoxVoicemailDesc);
            this.groupBox24.Controls.Add(this.label233);
            this.groupBox24.Controls.Add(this.textBoxVoicemailSender);
            this.groupBox24.Controls.Add(this.label234);
            this.groupBox24.Controls.Add(this.buttonVoicemailMoveDown);
            this.groupBox24.Controls.Add(this.textBoxVoicemailName);
            this.groupBox24.Controls.Add(this.listBoxVoicemail);
            this.groupBox24.Controls.Add(this.buttonVoicemailMoveUp);
            this.groupBox24.Controls.Add(this.buttonVoicemailAdd);
            this.groupBox24.Controls.Add(this.buttonVoicemailRemove);
            this.groupBox24.Location = new System.Drawing.Point(1044, 439);
            this.groupBox24.Name = "groupBox24";
            this.groupBox24.Size = new System.Drawing.Size(260, 179);
            this.groupBox24.TabIndex = 174;
            this.groupBox24.TabStop = false;
            this.groupBox24.Text = "Voicemail";
            // 
            // label231
            // 
            this.label231.AutoSize = true;
            this.label231.Location = new System.Drawing.Point(116, 55);
            this.label231.Name = "label231";
            this.label231.Size = new System.Drawing.Size(51, 13);
            this.label231.TabIndex = 171;
            this.label231.Text = "Dialog ID";
            // 
            // textBoxVoicemailDialogID
            // 
            this.textBoxVoicemailDialogID.Location = new System.Drawing.Point(119, 71);
            this.textBoxVoicemailDialogID.Name = "textBoxVoicemailDialogID";
            this.textBoxVoicemailDialogID.Size = new System.Drawing.Size(132, 20);
            this.textBoxVoicemailDialogID.TabIndex = 170;
            this.textBoxVoicemailDialogID.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label232
            // 
            this.label232.AutoSize = true;
            this.label232.Location = new System.Drawing.Point(133, 94);
            this.label232.Name = "label232";
            this.label232.Size = new System.Drawing.Size(28, 13);
            this.label232.TabIndex = 169;
            this.label232.Text = "Text";
            // 
            // textBoxVoicemailDesc
            // 
            this.textBoxVoicemailDesc.Location = new System.Drawing.Point(133, 108);
            this.textBoxVoicemailDesc.Multiline = true;
            this.textBoxVoicemailDesc.Name = "textBoxVoicemailDesc";
            this.textBoxVoicemailDesc.Size = new System.Drawing.Size(118, 62);
            this.textBoxVoicemailDesc.TabIndex = 168;
            this.textBoxVoicemailDesc.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label233
            // 
            this.label233.AutoSize = true;
            this.label233.Location = new System.Drawing.Point(185, 17);
            this.label233.Name = "label233";
            this.label233.Size = new System.Drawing.Size(41, 13);
            this.label233.TabIndex = 166;
            this.label233.Text = "Sender";
            // 
            // textBoxVoicemailSender
            // 
            this.textBoxVoicemailSender.Location = new System.Drawing.Point(188, 33);
            this.textBoxVoicemailSender.Name = "textBoxVoicemailSender";
            this.textBoxVoicemailSender.Size = new System.Drawing.Size(63, 20);
            this.textBoxVoicemailSender.TabIndex = 165;
            this.textBoxVoicemailSender.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label234
            // 
            this.label234.AutoSize = true;
            this.label234.Location = new System.Drawing.Point(116, 17);
            this.label234.Name = "label234";
            this.label234.Size = new System.Drawing.Size(35, 13);
            this.label234.TabIndex = 164;
            this.label234.Text = "Name";
            // 
            // buttonVoicemailMoveDown
            // 
            this.buttonVoicemailMoveDown.Location = new System.Drawing.Point(69, 138);
            this.buttonVoicemailMoveDown.Name = "buttonVoicemailMoveDown";
            this.buttonVoicemailMoveDown.Size = new System.Drawing.Size(58, 24);
            this.buttonVoicemailMoveDown.TabIndex = 164;
            this.buttonVoicemailMoveDown.Text = "Down";
            this.buttonVoicemailMoveDown.UseVisualStyleBackColor = true;
            this.buttonVoicemailMoveDown.Click += new System.EventHandler(this.buttonVoicemailMoveDown_Click);
            // 
            // textBoxVoicemailName
            // 
            this.textBoxVoicemailName.Location = new System.Drawing.Point(119, 33);
            this.textBoxVoicemailName.Name = "textBoxVoicemailName";
            this.textBoxVoicemailName.Size = new System.Drawing.Size(63, 20);
            this.textBoxVoicemailName.TabIndex = 163;
            this.textBoxVoicemailName.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            this.textBoxVoicemailName.Leave += new System.EventHandler(this.textBoxVoicemailName_Leave);
            // 
            // listBoxVoicemail
            // 
            this.listBoxVoicemail.AccessibleRole = System.Windows.Forms.AccessibleRole.List;
            this.listBoxVoicemail.FormattingEnabled = true;
            this.listBoxVoicemail.Location = new System.Drawing.Point(9, 19);
            this.listBoxVoicemail.Name = "listBoxVoicemail";
            this.listBoxVoicemail.Size = new System.Drawing.Size(104, 82);
            this.listBoxVoicemail.TabIndex = 160;
            // 
            // buttonVoicemailMoveUp
            // 
            this.buttonVoicemailMoveUp.Location = new System.Drawing.Point(69, 108);
            this.buttonVoicemailMoveUp.Name = "buttonVoicemailMoveUp";
            this.buttonVoicemailMoveUp.Size = new System.Drawing.Size(58, 24);
            this.buttonVoicemailMoveUp.TabIndex = 163;
            this.buttonVoicemailMoveUp.Text = "Up";
            this.buttonVoicemailMoveUp.UseVisualStyleBackColor = true;
            this.buttonVoicemailMoveUp.Click += new System.EventHandler(this.buttonVoicemailMoveUp_Click);
            // 
            // buttonVoicemailAdd
            // 
            this.buttonVoicemailAdd.Location = new System.Drawing.Point(7, 107);
            this.buttonVoicemailAdd.Name = "buttonVoicemailAdd";
            this.buttonVoicemailAdd.Size = new System.Drawing.Size(58, 25);
            this.buttonVoicemailAdd.TabIndex = 161;
            this.buttonVoicemailAdd.Text = "Add";
            this.buttonVoicemailAdd.UseVisualStyleBackColor = true;
            this.buttonVoicemailAdd.Click += new System.EventHandler(this.buttonVoicemailAdd_Click);
            // 
            // buttonVoicemailRemove
            // 
            this.buttonVoicemailRemove.Location = new System.Drawing.Point(7, 138);
            this.buttonVoicemailRemove.Name = "buttonVoicemailRemove";
            this.buttonVoicemailRemove.Size = new System.Drawing.Size(58, 25);
            this.buttonVoicemailRemove.TabIndex = 162;
            this.buttonVoicemailRemove.Text = "Remove";
            this.buttonVoicemailRemove.UseVisualStyleBackColor = true;
            this.buttonVoicemailRemove.Click += new System.EventHandler(this.buttonVoicemailRemove_Click);
            // 
            // groupBox23
            // 
            this.groupBox23.BackColor = System.Drawing.Color.Gainsboro;
            this.groupBox23.Controls.Add(this.label226);
            this.groupBox23.Controls.Add(this.textBoxSMSTitle);
            this.groupBox23.Controls.Add(this.label225);
            this.groupBox23.Controls.Add(this.textBoxSMSText);
            this.groupBox23.Controls.Add(this.label229);
            this.groupBox23.Controls.Add(this.textBoxSMSSender);
            this.groupBox23.Controls.Add(this.label230);
            this.groupBox23.Controls.Add(this.buttonSMSMoveDown);
            this.groupBox23.Controls.Add(this.textBoxSMSName);
            this.groupBox23.Controls.Add(this.listBoxSMS);
            this.groupBox23.Controls.Add(this.buttonSMSMoveUp);
            this.groupBox23.Controls.Add(this.buttonSMSAdd);
            this.groupBox23.Controls.Add(this.buttonSMSRemove);
            this.groupBox23.Location = new System.Drawing.Point(757, 439);
            this.groupBox23.Name = "groupBox23";
            this.groupBox23.Size = new System.Drawing.Size(281, 179);
            this.groupBox23.TabIndex = 173;
            this.groupBox23.TabStop = false;
            this.groupBox23.Text = "SMS";
            // 
            // label226
            // 
            this.label226.AutoSize = true;
            this.label226.Location = new System.Drawing.Point(132, 55);
            this.label226.Name = "label226";
            this.label226.Size = new System.Drawing.Size(27, 13);
            this.label226.TabIndex = 171;
            this.label226.Text = "Title";
            // 
            // textBoxSMSTitle
            // 
            this.textBoxSMSTitle.Location = new System.Drawing.Point(135, 71);
            this.textBoxSMSTitle.Name = "textBoxSMSTitle";
            this.textBoxSMSTitle.Size = new System.Drawing.Size(132, 20);
            this.textBoxSMSTitle.TabIndex = 170;
            this.textBoxSMSTitle.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label225
            // 
            this.label225.AutoSize = true;
            this.label225.Location = new System.Drawing.Point(133, 94);
            this.label225.Name = "label225";
            this.label225.Size = new System.Drawing.Size(28, 13);
            this.label225.TabIndex = 169;
            this.label225.Text = "Text";
            // 
            // textBoxSMSText
            // 
            this.textBoxSMSText.Location = new System.Drawing.Point(133, 108);
            this.textBoxSMSText.Multiline = true;
            this.textBoxSMSText.Name = "textBoxSMSText";
            this.textBoxSMSText.Size = new System.Drawing.Size(134, 62);
            this.textBoxSMSText.TabIndex = 168;
            this.textBoxSMSText.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label229
            // 
            this.label229.AutoSize = true;
            this.label229.Location = new System.Drawing.Point(201, 17);
            this.label229.Name = "label229";
            this.label229.Size = new System.Drawing.Size(41, 13);
            this.label229.TabIndex = 166;
            this.label229.Text = "Sender";
            // 
            // textBoxSMSSender
            // 
            this.textBoxSMSSender.Location = new System.Drawing.Point(204, 33);
            this.textBoxSMSSender.Name = "textBoxSMSSender";
            this.textBoxSMSSender.Size = new System.Drawing.Size(63, 20);
            this.textBoxSMSSender.TabIndex = 165;
            this.textBoxSMSSender.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label230
            // 
            this.label230.AutoSize = true;
            this.label230.Location = new System.Drawing.Point(132, 17);
            this.label230.Name = "label230";
            this.label230.Size = new System.Drawing.Size(35, 13);
            this.label230.TabIndex = 164;
            this.label230.Text = "Name";
            // 
            // buttonSMSMoveDown
            // 
            this.buttonSMSMoveDown.Location = new System.Drawing.Point(69, 138);
            this.buttonSMSMoveDown.Name = "buttonSMSMoveDown";
            this.buttonSMSMoveDown.Size = new System.Drawing.Size(58, 24);
            this.buttonSMSMoveDown.TabIndex = 164;
            this.buttonSMSMoveDown.Text = "Down";
            this.buttonSMSMoveDown.UseVisualStyleBackColor = true;
            this.buttonSMSMoveDown.Click += new System.EventHandler(this.buttonSMSMoveDown_Click);
            // 
            // textBoxSMSName
            // 
            this.textBoxSMSName.Location = new System.Drawing.Point(135, 33);
            this.textBoxSMSName.Name = "textBoxSMSName";
            this.textBoxSMSName.Size = new System.Drawing.Size(63, 20);
            this.textBoxSMSName.TabIndex = 163;
            this.textBoxSMSName.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            this.textBoxSMSName.Leave += new System.EventHandler(this.textBoxSMSName_Leave);
            // 
            // listBoxSMS
            // 
            this.listBoxSMS.AccessibleRole = System.Windows.Forms.AccessibleRole.List;
            this.listBoxSMS.FormattingEnabled = true;
            this.listBoxSMS.Location = new System.Drawing.Point(9, 19);
            this.listBoxSMS.Name = "listBoxSMS";
            this.listBoxSMS.Size = new System.Drawing.Size(120, 82);
            this.listBoxSMS.TabIndex = 160;
            // 
            // buttonSMSMoveUp
            // 
            this.buttonSMSMoveUp.Location = new System.Drawing.Point(69, 108);
            this.buttonSMSMoveUp.Name = "buttonSMSMoveUp";
            this.buttonSMSMoveUp.Size = new System.Drawing.Size(58, 24);
            this.buttonSMSMoveUp.TabIndex = 163;
            this.buttonSMSMoveUp.Text = "Up";
            this.buttonSMSMoveUp.UseVisualStyleBackColor = true;
            this.buttonSMSMoveUp.Click += new System.EventHandler(this.buttonSMSMoveUp_Click);
            // 
            // buttonSMSAdd
            // 
            this.buttonSMSAdd.Location = new System.Drawing.Point(7, 107);
            this.buttonSMSAdd.Name = "buttonSMSAdd";
            this.buttonSMSAdd.Size = new System.Drawing.Size(58, 25);
            this.buttonSMSAdd.TabIndex = 161;
            this.buttonSMSAdd.Text = "Add";
            this.buttonSMSAdd.UseVisualStyleBackColor = true;
            this.buttonSMSAdd.Click += new System.EventHandler(this.buttonSMSAdd_Click);
            // 
            // buttonSMSRemove
            // 
            this.buttonSMSRemove.Location = new System.Drawing.Point(7, 138);
            this.buttonSMSRemove.Name = "buttonSMSRemove";
            this.buttonSMSRemove.Size = new System.Drawing.Size(58, 25);
            this.buttonSMSRemove.TabIndex = 162;
            this.buttonSMSRemove.Text = "Remove";
            this.buttonSMSRemove.UseVisualStyleBackColor = true;
            this.buttonSMSRemove.Click += new System.EventHandler(this.buttonSMSRemove_Click);
            // 
            // groupBox22
            // 
            this.groupBox22.BackColor = System.Drawing.Color.Gainsboro;
            this.groupBox22.Controls.Add(this.comboBoxTaskCompleteID);
            this.groupBox22.Controls.Add(this.comboBoxTaskEventID);
            this.groupBox22.Controls.Add(this.label224);
            this.groupBox22.Controls.Add(this.label223);
            this.groupBox22.Controls.Add(this.textBoxTaskDesc);
            this.groupBox22.Controls.Add(this.textBoxTaskSprite);
            this.groupBox22.Controls.Add(this.label222);
            this.groupBox22.Controls.Add(this.label221);
            this.groupBox22.Controls.Add(this.label220);
            this.groupBox22.Controls.Add(this.textBoxTaskName);
            this.groupBox22.Controls.Add(this.label219);
            this.groupBox22.Controls.Add(this.buttonTasksMoveDown);
            this.groupBox22.Controls.Add(this.textBoxTaskID);
            this.groupBox22.Controls.Add(this.listBoxTasks);
            this.groupBox22.Controls.Add(this.buttonTasksMoveUp);
            this.groupBox22.Controls.Add(this.buttonTasksAdd);
            this.groupBox22.Controls.Add(this.buttonTasksRemove);
            this.groupBox22.Location = new System.Drawing.Point(293, 439);
            this.groupBox22.Name = "groupBox22";
            this.groupBox22.Size = new System.Drawing.Size(458, 179);
            this.groupBox22.TabIndex = 13;
            this.groupBox22.TabStop = false;
            this.groupBox22.Text = "Tasks";
            // 
            // comboBoxTaskCompleteID
            // 
            this.comboBoxTaskCompleteID.FormattingEnabled = true;
            this.comboBoxTaskCompleteID.Items.AddRange(new object[] {
            "text",
            "move",
            "exit"});
            this.comboBoxTaskCompleteID.Location = new System.Drawing.Point(220, 77);
            this.comboBoxTaskCompleteID.Name = "comboBoxTaskCompleteID";
            this.comboBoxTaskCompleteID.Size = new System.Drawing.Size(82, 21);
            this.comboBoxTaskCompleteID.TabIndex = 173;
            // 
            // comboBoxTaskEventID
            // 
            this.comboBoxTaskEventID.FormattingEnabled = true;
            this.comboBoxTaskEventID.Items.AddRange(new object[] {
            "text",
            "move",
            "exit"});
            this.comboBoxTaskEventID.Location = new System.Drawing.Point(220, 33);
            this.comboBoxTaskEventID.Name = "comboBoxTaskEventID";
            this.comboBoxTaskEventID.Size = new System.Drawing.Size(82, 21);
            this.comboBoxTaskEventID.TabIndex = 160;
            // 
            // label224
            // 
            this.label224.AutoSize = true;
            this.label224.Location = new System.Drawing.Point(305, 17);
            this.label224.Name = "label224";
            this.label224.Size = new System.Drawing.Size(28, 13);
            this.label224.TabIndex = 169;
            this.label224.Text = "Text";
            // 
            // label223
            // 
            this.label223.AutoSize = true;
            this.label223.Location = new System.Drawing.Point(132, 105);
            this.label223.Name = "label223";
            this.label223.Size = new System.Drawing.Size(34, 13);
            this.label223.TabIndex = 172;
            this.label223.Text = "Sprite";
            // 
            // textBoxTaskDesc
            // 
            this.textBoxTaskDesc.Location = new System.Drawing.Point(308, 33);
            this.textBoxTaskDesc.Multiline = true;
            this.textBoxTaskDesc.Name = "textBoxTaskDesc";
            this.textBoxTaskDesc.Size = new System.Drawing.Size(142, 123);
            this.textBoxTaskDesc.TabIndex = 168;
            // 
            // textBoxTaskSprite
            // 
            this.textBoxTaskSprite.Location = new System.Drawing.Point(135, 121);
            this.textBoxTaskSprite.Name = "textBoxTaskSprite";
            this.textBoxTaskSprite.Size = new System.Drawing.Size(79, 20);
            this.textBoxTaskSprite.TabIndex = 171;
            this.textBoxTaskSprite.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label222
            // 
            this.label222.AutoSize = true;
            this.label222.Location = new System.Drawing.Point(217, 61);
            this.label222.Name = "label222";
            this.label222.Size = new System.Drawing.Size(65, 13);
            this.label222.TabIndex = 170;
            this.label222.Text = "Complete ID";
            // 
            // label221
            // 
            this.label221.AutoSize = true;
            this.label221.Location = new System.Drawing.Point(217, 17);
            this.label221.Name = "label221";
            this.label221.Size = new System.Drawing.Size(49, 13);
            this.label221.TabIndex = 168;
            this.label221.Text = "Event ID";
            // 
            // label220
            // 
            this.label220.AutoSize = true;
            this.label220.Location = new System.Drawing.Point(132, 61);
            this.label220.Name = "label220";
            this.label220.Size = new System.Drawing.Size(35, 13);
            this.label220.TabIndex = 166;
            this.label220.Text = "Name";
            // 
            // textBoxTaskName
            // 
            this.textBoxTaskName.Location = new System.Drawing.Point(135, 77);
            this.textBoxTaskName.Name = "textBoxTaskName";
            this.textBoxTaskName.Size = new System.Drawing.Size(79, 20);
            this.textBoxTaskName.TabIndex = 165;
            this.textBoxTaskName.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label219
            // 
            this.label219.AutoSize = true;
            this.label219.Location = new System.Drawing.Point(132, 17);
            this.label219.Name = "label219";
            this.label219.Size = new System.Drawing.Size(18, 13);
            this.label219.TabIndex = 164;
            this.label219.Text = "ID";
            // 
            // buttonTasksMoveDown
            // 
            this.buttonTasksMoveDown.Location = new System.Drawing.Point(69, 138);
            this.buttonTasksMoveDown.Name = "buttonTasksMoveDown";
            this.buttonTasksMoveDown.Size = new System.Drawing.Size(57, 24);
            this.buttonTasksMoveDown.TabIndex = 164;
            this.buttonTasksMoveDown.Text = "Down";
            this.buttonTasksMoveDown.UseVisualStyleBackColor = true;
            this.buttonTasksMoveDown.Click += new System.EventHandler(this.buttonTasksMoveDown_Click);
            // 
            // textBoxTaskID
            // 
            this.textBoxTaskID.Location = new System.Drawing.Point(135, 33);
            this.textBoxTaskID.Name = "textBoxTaskID";
            this.textBoxTaskID.Size = new System.Drawing.Size(79, 20);
            this.textBoxTaskID.TabIndex = 163;
            this.textBoxTaskID.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            this.textBoxTaskID.Leave += new System.EventHandler(this.textBoxTaskName_Leave);
            // 
            // listBoxTasks
            // 
            this.listBoxTasks.AccessibleRole = System.Windows.Forms.AccessibleRole.List;
            this.listBoxTasks.FormattingEnabled = true;
            this.listBoxTasks.Location = new System.Drawing.Point(9, 19);
            this.listBoxTasks.Name = "listBoxTasks";
            this.listBoxTasks.Size = new System.Drawing.Size(117, 82);
            this.listBoxTasks.TabIndex = 160;
            // 
            // buttonTasksMoveUp
            // 
            this.buttonTasksMoveUp.Location = new System.Drawing.Point(69, 108);
            this.buttonTasksMoveUp.Name = "buttonTasksMoveUp";
            this.buttonTasksMoveUp.Size = new System.Drawing.Size(57, 24);
            this.buttonTasksMoveUp.TabIndex = 163;
            this.buttonTasksMoveUp.Text = "Up";
            this.buttonTasksMoveUp.UseVisualStyleBackColor = true;
            this.buttonTasksMoveUp.Click += new System.EventHandler(this.buttonTasksMoveUp_Click);
            // 
            // buttonTasksAdd
            // 
            this.buttonTasksAdd.Location = new System.Drawing.Point(7, 107);
            this.buttonTasksAdd.Name = "buttonTasksAdd";
            this.buttonTasksAdd.Size = new System.Drawing.Size(56, 25);
            this.buttonTasksAdd.TabIndex = 161;
            this.buttonTasksAdd.Text = "Add";
            this.buttonTasksAdd.UseVisualStyleBackColor = true;
            this.buttonTasksAdd.Click += new System.EventHandler(this.buttonTasksAdd_Click);
            // 
            // buttonTasksRemove
            // 
            this.buttonTasksRemove.Location = new System.Drawing.Point(7, 138);
            this.buttonTasksRemove.Name = "buttonTasksRemove";
            this.buttonTasksRemove.Size = new System.Drawing.Size(56, 25);
            this.buttonTasksRemove.TabIndex = 162;
            this.buttonTasksRemove.Text = "Remove";
            this.buttonTasksRemove.UseVisualStyleBackColor = true;
            this.buttonTasksRemove.Click += new System.EventHandler(this.buttonTasksRemove_Click);
            // 
            // groupBox18
            // 
            this.groupBox18.BackColor = System.Drawing.Color.Gainsboro;
            this.groupBox18.Controls.Add(this.groupBox20);
            this.groupBox18.Controls.Add(this.label200);
            this.groupBox18.Controls.Add(this.textBoxTextboxEventID);
            this.groupBox18.Controls.Add(this.label201);
            this.groupBox18.Controls.Add(this.buttonTextboxMoveDown);
            this.groupBox18.Controls.Add(this.buttonTextboxMoveUp);
            this.groupBox18.Controls.Add(this.textBoxTextboxName);
            this.groupBox18.Controls.Add(this.listBoxTextbox);
            this.groupBox18.Controls.Add(this.buttonTextboxRemove);
            this.groupBox18.Controls.Add(this.buttonTextboxAdd);
            this.groupBox18.Location = new System.Drawing.Point(732, 12);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(572, 421);
            this.groupBox18.TabIndex = 12;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "Text Boxes";
            // 
            // groupBox20
            // 
            this.groupBox20.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox20.Controls.Add(this.buttonTextEntryRemove);
            this.groupBox20.Controls.Add(this.label203);
            this.groupBox20.Controls.Add(this.groupBox19);
            this.groupBox20.Controls.Add(this.listBoxTextEntry);
            this.groupBox20.Controls.Add(this.buttonTextEntryAdd);
            this.groupBox20.Controls.Add(this.textBoxTextEntryText);
            this.groupBox20.Location = new System.Drawing.Point(135, 69);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(431, 346);
            this.groupBox20.TabIndex = 13;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "Text Entries";
            // 
            // buttonTextEntryRemove
            // 
            this.buttonTextEntryRemove.Location = new System.Drawing.Point(9, 315);
            this.buttonTextEntryRemove.Name = "buttonTextEntryRemove";
            this.buttonTextEntryRemove.Size = new System.Drawing.Size(91, 25);
            this.buttonTextEntryRemove.TabIndex = 161;
            this.buttonTextEntryRemove.Text = "Remove";
            this.buttonTextEntryRemove.UseVisualStyleBackColor = true;
            this.buttonTextEntryRemove.Click += new System.EventHandler(this.buttonTextEntryRemove_Click);
            // 
            // label203
            // 
            this.label203.AutoSize = true;
            this.label203.Location = new System.Drawing.Point(103, 19);
            this.label203.Name = "label203";
            this.label203.Size = new System.Drawing.Size(28, 13);
            this.label203.TabIndex = 166;
            this.label203.Text = "Text";
            // 
            // groupBox19
            // 
            this.groupBox19.BackColor = System.Drawing.Color.LightGray;
            this.groupBox19.Controls.Add(this.label202);
            this.groupBox19.Controls.Add(this.textBoxTextGraphicsAngle);
            this.groupBox19.Controls.Add(this.label212);
            this.groupBox19.Controls.Add(this.textBoxTextGraphicsB);
            this.groupBox19.Controls.Add(this.label210);
            this.groupBox19.Controls.Add(this.textBoxTextGraphicsG);
            this.groupBox19.Controls.Add(this.label211);
            this.groupBox19.Controls.Add(this.textBoxTextGraphicsR);
            this.groupBox19.Controls.Add(this.label209);
            this.groupBox19.Controls.Add(this.textBoxTextGraphicsAlpha);
            this.groupBox19.Controls.Add(this.label208);
            this.groupBox19.Controls.Add(this.textBoxTextGraphicsSprite);
            this.groupBox19.Controls.Add(this.label206);
            this.groupBox19.Controls.Add(this.textBoxTextGraphicsYScale);
            this.groupBox19.Controls.Add(this.label207);
            this.groupBox19.Controls.Add(this.textBoxTextGraphicsXScale);
            this.groupBox19.Controls.Add(this.label205);
            this.groupBox19.Controls.Add(this.textBoxTextGraphicsY);
            this.groupBox19.Controls.Add(this.label204);
            this.groupBox19.Controls.Add(this.textBoxTextGraphicsX);
            this.groupBox19.Controls.Add(this.buttonTextGraphicsRemove);
            this.groupBox19.Controls.Add(this.listBoxTextGraphics);
            this.groupBox19.Controls.Add(this.buttonTextGraphicsAdd);
            this.groupBox19.Location = new System.Drawing.Point(106, 86);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(319, 140);
            this.groupBox19.TabIndex = 167;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "Graphics";
            // 
            // label202
            // 
            this.label202.AutoSize = true;
            this.label202.Location = new System.Drawing.Point(261, 97);
            this.label202.Name = "label202";
            this.label202.Size = new System.Drawing.Size(34, 13);
            this.label202.TabIndex = 185;
            this.label202.Text = "Angle";
            // 
            // textBoxTextGraphicsAngle
            // 
            this.textBoxTextGraphicsAngle.Location = new System.Drawing.Point(264, 113);
            this.textBoxTextGraphicsAngle.Name = "textBoxTextGraphicsAngle";
            this.textBoxTextGraphicsAngle.Size = new System.Drawing.Size(45, 20);
            this.textBoxTextGraphicsAngle.TabIndex = 184;
            this.textBoxTextGraphicsAngle.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label212
            // 
            this.label212.AutoSize = true;
            this.label212.Location = new System.Drawing.Point(210, 99);
            this.label212.Name = "label212";
            this.label212.Size = new System.Drawing.Size(14, 13);
            this.label212.TabIndex = 183;
            this.label212.Text = "B";
            // 
            // textBoxTextGraphicsB
            // 
            this.textBoxTextGraphicsB.Location = new System.Drawing.Point(213, 115);
            this.textBoxTextGraphicsB.Name = "textBoxTextGraphicsB";
            this.textBoxTextGraphicsB.Size = new System.Drawing.Size(32, 20);
            this.textBoxTextGraphicsB.TabIndex = 182;
            this.textBoxTextGraphicsB.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label210
            // 
            this.label210.AutoSize = true;
            this.label210.Location = new System.Drawing.Point(172, 98);
            this.label210.Name = "label210";
            this.label210.Size = new System.Drawing.Size(15, 13);
            this.label210.TabIndex = 181;
            this.label210.Text = "G";
            // 
            // textBoxTextGraphicsG
            // 
            this.textBoxTextGraphicsG.Location = new System.Drawing.Point(175, 114);
            this.textBoxTextGraphicsG.Name = "textBoxTextGraphicsG";
            this.textBoxTextGraphicsG.Size = new System.Drawing.Size(32, 20);
            this.textBoxTextGraphicsG.TabIndex = 180;
            this.textBoxTextGraphicsG.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label211
            // 
            this.label211.AutoSize = true;
            this.label211.Location = new System.Drawing.Point(134, 98);
            this.label211.Name = "label211";
            this.label211.Size = new System.Drawing.Size(15, 13);
            this.label211.TabIndex = 179;
            this.label211.Text = "R";
            // 
            // textBoxTextGraphicsR
            // 
            this.textBoxTextGraphicsR.Location = new System.Drawing.Point(137, 114);
            this.textBoxTextGraphicsR.Name = "textBoxTextGraphicsR";
            this.textBoxTextGraphicsR.Size = new System.Drawing.Size(32, 20);
            this.textBoxTextGraphicsR.TabIndex = 178;
            this.textBoxTextGraphicsR.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label209
            // 
            this.label209.AutoSize = true;
            this.label209.Location = new System.Drawing.Point(261, 55);
            this.label209.Name = "label209";
            this.label209.Size = new System.Drawing.Size(34, 13);
            this.label209.TabIndex = 177;
            this.label209.Text = "Alpha";
            // 
            // textBoxTextGraphicsAlpha
            // 
            this.textBoxTextGraphicsAlpha.Location = new System.Drawing.Point(264, 71);
            this.textBoxTextGraphicsAlpha.Name = "textBoxTextGraphicsAlpha";
            this.textBoxTextGraphicsAlpha.Size = new System.Drawing.Size(45, 20);
            this.textBoxTextGraphicsAlpha.TabIndex = 176;
            this.textBoxTextGraphicsAlpha.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label208
            // 
            this.label208.AutoSize = true;
            this.label208.Location = new System.Drawing.Point(134, 55);
            this.label208.Name = "label208";
            this.label208.Size = new System.Drawing.Size(34, 13);
            this.label208.TabIndex = 161;
            this.label208.Text = "Sprite";
            // 
            // textBoxTextGraphicsSprite
            // 
            this.textBoxTextGraphicsSprite.Location = new System.Drawing.Point(137, 71);
            this.textBoxTextGraphicsSprite.Name = "textBoxTextGraphicsSprite";
            this.textBoxTextGraphicsSprite.Size = new System.Drawing.Size(121, 20);
            this.textBoxTextGraphicsSprite.TabIndex = 160;
            this.textBoxTextGraphicsSprite.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            this.textBoxTextGraphicsSprite.Leave += new System.EventHandler(this.textBoxTextGraphicsSprite_Leave);
            // 
            // label206
            // 
            this.label206.AutoSize = true;
            this.label206.Location = new System.Drawing.Point(261, 16);
            this.label206.Name = "label206";
            this.label206.Size = new System.Drawing.Size(37, 13);
            this.label206.TabIndex = 175;
            this.label206.Text = "yscale";
            // 
            // textBoxTextGraphicsYScale
            // 
            this.textBoxTextGraphicsYScale.Location = new System.Drawing.Point(264, 32);
            this.textBoxTextGraphicsYScale.Name = "textBoxTextGraphicsYScale";
            this.textBoxTextGraphicsYScale.Size = new System.Drawing.Size(47, 20);
            this.textBoxTextGraphicsYScale.TabIndex = 174;
            this.textBoxTextGraphicsYScale.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label207
            // 
            this.label207.AutoSize = true;
            this.label207.Location = new System.Drawing.Point(210, 16);
            this.label207.Name = "label207";
            this.label207.Size = new System.Drawing.Size(37, 13);
            this.label207.TabIndex = 173;
            this.label207.Text = "xscale";
            // 
            // textBoxTextGraphicsXScale
            // 
            this.textBoxTextGraphicsXScale.Location = new System.Drawing.Point(213, 32);
            this.textBoxTextGraphicsXScale.Name = "textBoxTextGraphicsXScale";
            this.textBoxTextGraphicsXScale.Size = new System.Drawing.Size(45, 20);
            this.textBoxTextGraphicsXScale.TabIndex = 172;
            this.textBoxTextGraphicsXScale.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label205
            // 
            this.label205.AutoSize = true;
            this.label205.Location = new System.Drawing.Point(172, 16);
            this.label205.Name = "label205";
            this.label205.Size = new System.Drawing.Size(14, 13);
            this.label205.TabIndex = 171;
            this.label205.Text = "Y";
            // 
            // textBoxTextGraphicsY
            // 
            this.textBoxTextGraphicsY.Location = new System.Drawing.Point(175, 32);
            this.textBoxTextGraphicsY.Name = "textBoxTextGraphicsY";
            this.textBoxTextGraphicsY.Size = new System.Drawing.Size(32, 20);
            this.textBoxTextGraphicsY.TabIndex = 170;
            this.textBoxTextGraphicsY.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label204
            // 
            this.label204.AutoSize = true;
            this.label204.Location = new System.Drawing.Point(134, 16);
            this.label204.Name = "label204";
            this.label204.Size = new System.Drawing.Size(14, 13);
            this.label204.TabIndex = 157;
            this.label204.Text = "X";
            // 
            // textBoxTextGraphicsX
            // 
            this.textBoxTextGraphicsX.Location = new System.Drawing.Point(137, 32);
            this.textBoxTextGraphicsX.Name = "textBoxTextGraphicsX";
            this.textBoxTextGraphicsX.Size = new System.Drawing.Size(32, 20);
            this.textBoxTextGraphicsX.TabIndex = 156;
            this.textBoxTextGraphicsX.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // buttonTextGraphicsRemove
            // 
            this.buttonTextGraphicsRemove.Location = new System.Drawing.Point(70, 107);
            this.buttonTextGraphicsRemove.Name = "buttonTextGraphicsRemove";
            this.buttonTextGraphicsRemove.Size = new System.Drawing.Size(58, 25);
            this.buttonTextGraphicsRemove.TabIndex = 169;
            this.buttonTextGraphicsRemove.Text = "Remove";
            this.buttonTextGraphicsRemove.UseVisualStyleBackColor = true;
            this.buttonTextGraphicsRemove.Click += new System.EventHandler(this.buttonTextGraphicsRemove_Click);
            // 
            // listBoxTextGraphics
            // 
            this.listBoxTextGraphics.AccessibleRole = System.Windows.Forms.AccessibleRole.List;
            this.listBoxTextGraphics.FormattingEnabled = true;
            this.listBoxTextGraphics.Location = new System.Drawing.Point(6, 19);
            this.listBoxTextGraphics.Name = "listBoxTextGraphics";
            this.listBoxTextGraphics.Size = new System.Drawing.Size(122, 82);
            this.listBoxTextGraphics.TabIndex = 168;
            // 
            // buttonTextGraphicsAdd
            // 
            this.buttonTextGraphicsAdd.Location = new System.Drawing.Point(6, 107);
            this.buttonTextGraphicsAdd.Name = "buttonTextGraphicsAdd";
            this.buttonTextGraphicsAdd.Size = new System.Drawing.Size(58, 25);
            this.buttonTextGraphicsAdd.TabIndex = 168;
            this.buttonTextGraphicsAdd.Text = "Add";
            this.buttonTextGraphicsAdd.UseVisualStyleBackColor = true;
            this.buttonTextGraphicsAdd.Click += new System.EventHandler(this.buttonTextGraphicsAdd_Click);
            // 
            // listBoxTextEntry
            // 
            this.listBoxTextEntry.AccessibleRole = System.Windows.Forms.AccessibleRole.List;
            this.listBoxTextEntry.FormattingEnabled = true;
            this.listBoxTextEntry.Location = new System.Drawing.Point(9, 19);
            this.listBoxTextEntry.Name = "listBoxTextEntry";
            this.listBoxTextEntry.Size = new System.Drawing.Size(91, 264);
            this.listBoxTextEntry.TabIndex = 164;
            this.listBoxTextEntry.SelectedIndexChanged += new System.EventHandler(this.listBoxTextEntry_SelectedIndexChanged);
            // 
            // buttonTextEntryAdd
            // 
            this.buttonTextEntryAdd.Location = new System.Drawing.Point(9, 286);
            this.buttonTextEntryAdd.Name = "buttonTextEntryAdd";
            this.buttonTextEntryAdd.Size = new System.Drawing.Size(91, 25);
            this.buttonTextEntryAdd.TabIndex = 160;
            this.buttonTextEntryAdd.Text = "Add";
            this.buttonTextEntryAdd.UseVisualStyleBackColor = true;
            this.buttonTextEntryAdd.Click += new System.EventHandler(this.buttonTextEntryAdd_Click);
            // 
            // textBoxTextEntryText
            // 
            this.textBoxTextEntryText.Location = new System.Drawing.Point(106, 35);
            this.textBoxTextEntryText.Multiline = true;
            this.textBoxTextEntryText.Name = "textBoxTextEntryText";
            this.textBoxTextEntryText.Size = new System.Drawing.Size(243, 43);
            this.textBoxTextEntryText.TabIndex = 165;
            this.textBoxTextEntryText.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            this.textBoxTextEntryText.Leave += new System.EventHandler(this.textBoxTextEntryText_Leave);
            // 
            // label200
            // 
            this.label200.AutoSize = true;
            this.label200.Location = new System.Drawing.Point(267, 19);
            this.label200.Name = "label200";
            this.label200.Size = new System.Drawing.Size(49, 13);
            this.label200.TabIndex = 162;
            this.label200.Text = "Event ID";
            // 
            // textBoxTextboxEventID
            // 
            this.textBoxTextboxEventID.Location = new System.Drawing.Point(270, 35);
            this.textBoxTextboxEventID.Name = "textBoxTextboxEventID";
            this.textBoxTextboxEventID.Size = new System.Drawing.Size(145, 20);
            this.textBoxTextboxEventID.TabIndex = 161;
            this.textBoxTextboxEventID.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label201
            // 
            this.label201.AutoSize = true;
            this.label201.Location = new System.Drawing.Point(132, 19);
            this.label201.Name = "label201";
            this.label201.Size = new System.Drawing.Size(35, 13);
            this.label201.TabIndex = 160;
            this.label201.Text = "Name";
            // 
            // buttonTextboxMoveDown
            // 
            this.buttonTextboxMoveDown.Location = new System.Drawing.Point(71, 387);
            this.buttonTextboxMoveDown.Name = "buttonTextboxMoveDown";
            this.buttonTextboxMoveDown.Size = new System.Drawing.Size(58, 24);
            this.buttonTextboxMoveDown.TabIndex = 158;
            this.buttonTextboxMoveDown.Text = "Down";
            this.buttonTextboxMoveDown.UseVisualStyleBackColor = true;
            this.buttonTextboxMoveDown.Click += new System.EventHandler(this.buttonTextboxMoveDown_Click);
            // 
            // buttonTextboxMoveUp
            // 
            this.buttonTextboxMoveUp.Location = new System.Drawing.Point(71, 356);
            this.buttonTextboxMoveUp.Name = "buttonTextboxMoveUp";
            this.buttonTextboxMoveUp.Size = new System.Drawing.Size(58, 24);
            this.buttonTextboxMoveUp.TabIndex = 157;
            this.buttonTextboxMoveUp.Text = "Up";
            this.buttonTextboxMoveUp.UseVisualStyleBackColor = true;
            this.buttonTextboxMoveUp.Click += new System.EventHandler(this.buttonTextboxMoveUp_Click);
            // 
            // textBoxTextboxName
            // 
            this.textBoxTextboxName.Location = new System.Drawing.Point(135, 35);
            this.textBoxTextboxName.Name = "textBoxTextboxName";
            this.textBoxTextboxName.Size = new System.Drawing.Size(129, 20);
            this.textBoxTextboxName.TabIndex = 159;
            this.textBoxTextboxName.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            this.textBoxTextboxName.Leave += new System.EventHandler(this.textBoxTextboxName_Leave);
            // 
            // listBoxTextbox
            // 
            this.listBoxTextbox.AccessibleRole = System.Windows.Forms.AccessibleRole.List;
            this.listBoxTextbox.FormattingEnabled = true;
            this.listBoxTextbox.Location = new System.Drawing.Point(6, 19);
            this.listBoxTextbox.Name = "listBoxTextbox";
            this.listBoxTextbox.Size = new System.Drawing.Size(120, 329);
            this.listBoxTextbox.TabIndex = 154;
            this.listBoxTextbox.SelectedIndexChanged += new System.EventHandler(this.listBoxTextbox_SelectedIndexChanged);
            // 
            // buttonTextboxRemove
            // 
            this.buttonTextboxRemove.Location = new System.Drawing.Point(6, 386);
            this.buttonTextboxRemove.Name = "buttonTextboxRemove";
            this.buttonTextboxRemove.Size = new System.Drawing.Size(58, 25);
            this.buttonTextboxRemove.TabIndex = 156;
            this.buttonTextboxRemove.Text = "Remove";
            this.buttonTextboxRemove.UseVisualStyleBackColor = true;
            this.buttonTextboxRemove.Click += new System.EventHandler(this.buttonTextboxRemove_Click);
            // 
            // buttonTextboxAdd
            // 
            this.buttonTextboxAdd.Location = new System.Drawing.Point(6, 355);
            this.buttonTextboxAdd.Name = "buttonTextboxAdd";
            this.buttonTextboxAdd.Size = new System.Drawing.Size(58, 25);
            this.buttonTextboxAdd.TabIndex = 155;
            this.buttonTextboxAdd.Text = "Add";
            this.buttonTextboxAdd.UseVisualStyleBackColor = true;
            this.buttonTextboxAdd.Click += new System.EventHandler(this.buttonTextboxAdd_Click);
            // 
            // groupBox13
            // 
            this.groupBox13.BackColor = System.Drawing.Color.Gainsboro;
            this.groupBox13.Controls.Add(this.groupBox15);
            this.groupBox13.Controls.Add(this.groupBox14);
            this.groupBox13.Controls.Add(this.textBoxHUDText);
            this.groupBox13.Controls.Add(this.label175);
            this.groupBox13.Controls.Add(this.label174);
            this.groupBox13.Controls.Add(this.textBoxHUDDefaultName);
            this.groupBox13.Controls.Add(this.buttonHUDMoveDown);
            this.groupBox13.Controls.Add(this.label173);
            this.groupBox13.Controls.Add(this.buttonHUDMoveUp);
            this.groupBox13.Controls.Add(this.textBoxHUDID);
            this.groupBox13.Controls.Add(this.buttonHUDRemove);
            this.groupBox13.Controls.Add(this.groupBox12);
            this.groupBox13.Controls.Add(this.buttonHUDAdd);
            this.groupBox13.Controls.Add(this.listBoxHUD);
            this.groupBox13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox13.Location = new System.Drawing.Point(6, 12);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(720, 421);
            this.groupBox13.TabIndex = 11;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "HUD";
            // 
            // groupBox15
            // 
            this.groupBox15.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox15.Controls.Add(this.label180);
            this.groupBox15.Controls.Add(this.textBoxHUDChildCustomHUDText);
            this.groupBox15.Controls.Add(this.label179);
            this.groupBox15.Controls.Add(this.textBoxHUDChildVar);
            this.groupBox15.Controls.Add(this.label182);
            this.groupBox15.Controls.Add(this.buttonHUDChildMoveDown);
            this.groupBox15.Controls.Add(this.buttonHUDChildMoveUp);
            this.groupBox15.Controls.Add(this.textBoxHUDChildName);
            this.groupBox15.Controls.Add(this.listBoxHUDChild);
            this.groupBox15.Controls.Add(this.buttonHUDChildRemove);
            this.groupBox15.Controls.Add(this.label181);
            this.groupBox15.Controls.Add(this.comboBoxHUDChildTemplate);
            this.groupBox15.Controls.Add(this.buttonHUDChildAdd);
            this.groupBox15.Location = new System.Drawing.Point(142, 193);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(244, 218);
            this.groupBox15.TabIndex = 149;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Children";
            // 
            // label180
            // 
            this.label180.AutoSize = true;
            this.label180.Location = new System.Drawing.Point(3, 171);
            this.label180.Name = "label180";
            this.label180.Size = new System.Drawing.Size(93, 13);
            this.label180.TabIndex = 155;
            this.label180.Text = "Custom HUD Text";
            // 
            // textBoxHUDChildCustomHUDText
            // 
            this.textBoxHUDChildCustomHUDText.Location = new System.Drawing.Point(6, 187);
            this.textBoxHUDChildCustomHUDText.Name = "textBoxHUDChildCustomHUDText";
            this.textBoxHUDChildCustomHUDText.Size = new System.Drawing.Size(217, 20);
            this.textBoxHUDChildCustomHUDText.TabIndex = 154;
            this.textBoxHUDChildCustomHUDText.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label179
            // 
            this.label179.AutoSize = true;
            this.label179.Location = new System.Drawing.Point(132, 98);
            this.label179.Name = "label179";
            this.label179.Size = new System.Drawing.Size(45, 13);
            this.label179.TabIndex = 153;
            this.label179.Text = "Variable";
            // 
            // textBoxHUDChildVar
            // 
            this.textBoxHUDChildVar.Location = new System.Drawing.Point(135, 114);
            this.textBoxHUDChildVar.Name = "textBoxHUDChildVar";
            this.textBoxHUDChildVar.Size = new System.Drawing.Size(85, 20);
            this.textBoxHUDChildVar.TabIndex = 152;
            this.textBoxHUDChildVar.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label182
            // 
            this.label182.AutoSize = true;
            this.label182.Location = new System.Drawing.Point(132, 59);
            this.label182.Name = "label182";
            this.label182.Size = new System.Drawing.Size(35, 13);
            this.label182.TabIndex = 151;
            this.label182.Text = "Name";
            // 
            // buttonHUDChildMoveDown
            // 
            this.buttonHUDChildMoveDown.Location = new System.Drawing.Point(71, 139);
            this.buttonHUDChildMoveDown.Name = "buttonHUDChildMoveDown";
            this.buttonHUDChildMoveDown.Size = new System.Drawing.Size(58, 24);
            this.buttonHUDChildMoveDown.TabIndex = 14;
            this.buttonHUDChildMoveDown.Text = "Down";
            this.buttonHUDChildMoveDown.UseVisualStyleBackColor = true;
            this.buttonHUDChildMoveDown.Click += new System.EventHandler(this.buttonHUDChildMoveDown_Click);
            // 
            // buttonHUDChildMoveUp
            // 
            this.buttonHUDChildMoveUp.Location = new System.Drawing.Point(71, 108);
            this.buttonHUDChildMoveUp.Name = "buttonHUDChildMoveUp";
            this.buttonHUDChildMoveUp.Size = new System.Drawing.Size(58, 24);
            this.buttonHUDChildMoveUp.TabIndex = 13;
            this.buttonHUDChildMoveUp.Text = "Up";
            this.buttonHUDChildMoveUp.UseVisualStyleBackColor = true;
            this.buttonHUDChildMoveUp.Click += new System.EventHandler(this.buttonHUDChildMoveUp_Click);
            // 
            // textBoxHUDChildName
            // 
            this.textBoxHUDChildName.Location = new System.Drawing.Point(135, 75);
            this.textBoxHUDChildName.Name = "textBoxHUDChildName";
            this.textBoxHUDChildName.Size = new System.Drawing.Size(85, 20);
            this.textBoxHUDChildName.TabIndex = 150;
            this.textBoxHUDChildName.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            this.textBoxHUDChildName.Leave += new System.EventHandler(this.textBoxHUDChildName_Leave);
            // 
            // listBoxHUDChild
            // 
            this.listBoxHUDChild.AccessibleRole = System.Windows.Forms.AccessibleRole.List;
            this.listBoxHUDChild.FormattingEnabled = true;
            this.listBoxHUDChild.Location = new System.Drawing.Point(6, 19);
            this.listBoxHUDChild.Name = "listBoxHUDChild";
            this.listBoxHUDChild.Size = new System.Drawing.Size(120, 82);
            this.listBoxHUDChild.TabIndex = 11;
            // 
            // buttonHUDChildRemove
            // 
            this.buttonHUDChildRemove.Location = new System.Drawing.Point(6, 138);
            this.buttonHUDChildRemove.Name = "buttonHUDChildRemove";
            this.buttonHUDChildRemove.Size = new System.Drawing.Size(58, 25);
            this.buttonHUDChildRemove.TabIndex = 12;
            this.buttonHUDChildRemove.Text = "Remove";
            this.buttonHUDChildRemove.UseVisualStyleBackColor = true;
            this.buttonHUDChildRemove.Click += new System.EventHandler(this.buttonHUDChildRemove_Click);
            // 
            // label181
            // 
            this.label181.AutoSize = true;
            this.label181.Location = new System.Drawing.Point(132, 19);
            this.label181.Name = "label181";
            this.label181.Size = new System.Drawing.Size(51, 13);
            this.label181.TabIndex = 109;
            this.label181.Text = "Template";
            // 
            // comboBoxHUDChildTemplate
            // 
            this.comboBoxHUDChildTemplate.FormattingEnabled = true;
            this.comboBoxHUDChildTemplate.Location = new System.Drawing.Point(135, 35);
            this.comboBoxHUDChildTemplate.Name = "comboBoxHUDChildTemplate";
            this.comboBoxHUDChildTemplate.Size = new System.Drawing.Size(85, 21);
            this.comboBoxHUDChildTemplate.TabIndex = 108;
            // 
            // buttonHUDChildAdd
            // 
            this.buttonHUDChildAdd.Location = new System.Drawing.Point(6, 107);
            this.buttonHUDChildAdd.Name = "buttonHUDChildAdd";
            this.buttonHUDChildAdd.Size = new System.Drawing.Size(58, 25);
            this.buttonHUDChildAdd.TabIndex = 11;
            this.buttonHUDChildAdd.Text = "Add";
            this.buttonHUDChildAdd.UseVisualStyleBackColor = true;
            this.buttonHUDChildAdd.Click += new System.EventHandler(this.buttonHUDChildAdd_Click);
            // 
            // groupBox14
            // 
            this.groupBox14.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox14.Controls.Add(this.label184);
            this.groupBox14.Controls.Add(this.textBoxHUDDefReq);
            this.groupBox14.Controls.Add(this.textBoxHUDDefArg);
            this.groupBox14.Controls.Add(this.label178);
            this.groupBox14.Controls.Add(this.label177);
            this.groupBox14.Controls.Add(this.comboBoxHUDDefFunc);
            this.groupBox14.Controls.Add(this.label176);
            this.groupBox14.Controls.Add(this.comboBoxHUDDefInput);
            this.groupBox14.Controls.Add(this.buttonHUDDefMoveDown);
            this.groupBox14.Controls.Add(this.buttonHUDDefMoveUp);
            this.groupBox14.Controls.Add(this.listBoxHUDDef);
            this.groupBox14.Controls.Add(this.buttonHUDDefRemove);
            this.groupBox14.Controls.Add(this.buttonHUDDefAdd);
            this.groupBox14.Location = new System.Drawing.Point(394, 207);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(313, 204);
            this.groupBox14.TabIndex = 29;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Definitions";
            // 
            // label184
            // 
            this.label184.AutoSize = true;
            this.label184.Location = new System.Drawing.Point(132, 56);
            this.label184.Name = "label184";
            this.label184.Size = new System.Drawing.Size(91, 13);
            this.label184.TabIndex = 159;
            this.label184.Text = "Required Variable";
            // 
            // textBoxHUDDefReq
            // 
            this.textBoxHUDDefReq.Location = new System.Drawing.Point(135, 72);
            this.textBoxHUDDefReq.Name = "textBoxHUDDefReq";
            this.textBoxHUDDefReq.Size = new System.Drawing.Size(85, 20);
            this.textBoxHUDDefReq.TabIndex = 158;
            this.textBoxHUDDefReq.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // textBoxHUDDefArg
            // 
            this.textBoxHUDDefArg.Location = new System.Drawing.Point(132, 107);
            this.textBoxHUDDefArg.Multiline = true;
            this.textBoxHUDDefArg.Name = "textBoxHUDDefArg";
            this.textBoxHUDDefArg.Size = new System.Drawing.Size(176, 81);
            this.textBoxHUDDefArg.TabIndex = 29;
            this.textBoxHUDDefArg.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label178
            // 
            this.label178.AutoSize = true;
            this.label178.Location = new System.Drawing.Point(132, 92);
            this.label178.Name = "label178";
            this.label178.Size = new System.Drawing.Size(57, 13);
            this.label178.TabIndex = 30;
            this.label178.Text = "Arguments";
            // 
            // label177
            // 
            this.label177.AutoSize = true;
            this.label177.Location = new System.Drawing.Point(236, 16);
            this.label177.Name = "label177";
            this.label177.Size = new System.Drawing.Size(48, 13);
            this.label177.TabIndex = 148;
            this.label177.Text = "Function";
            // 
            // comboBoxHUDDefFunc
            // 
            this.comboBoxHUDDefFunc.FormattingEnabled = true;
            this.comboBoxHUDDefFunc.Items.AddRange(new object[] {
            "text",
            "move",
            "exit"});
            this.comboBoxHUDDefFunc.Location = new System.Drawing.Point(239, 32);
            this.comboBoxHUDDefFunc.Name = "comboBoxHUDDefFunc";
            this.comboBoxHUDDefFunc.Size = new System.Drawing.Size(69, 21);
            this.comboBoxHUDDefFunc.TabIndex = 147;
            this.comboBoxHUDDefFunc.Leave += new System.EventHandler(this.comboBoxHUDDefFunc_Leave);
            // 
            // label176
            // 
            this.label176.AutoSize = true;
            this.label176.Location = new System.Drawing.Point(132, 16);
            this.label176.Name = "label176";
            this.label176.Size = new System.Drawing.Size(31, 13);
            this.label176.TabIndex = 109;
            this.label176.Text = "Input";
            // 
            // comboBoxHUDDefInput
            // 
            this.comboBoxHUDDefInput.FormattingEnabled = true;
            this.comboBoxHUDDefInput.Items.AddRange(new object[] {
            "select",
            "cancel",
            "control",
            "left",
            "right",
            "up",
            "down",
            "open_menu",
            "access1",
            "access2",
            "access3",
            "access4",
            "access5",
            "access6",
            "access7",
            "access8",
            "access9",
            "access0"});
            this.comboBoxHUDDefInput.Location = new System.Drawing.Point(135, 32);
            this.comboBoxHUDDefInput.Name = "comboBoxHUDDefInput";
            this.comboBoxHUDDefInput.Size = new System.Drawing.Size(92, 21);
            this.comboBoxHUDDefInput.TabIndex = 108;
            this.comboBoxHUDDefInput.Leave += new System.EventHandler(this.comboBoxHUDDefInput_Leave);
            // 
            // buttonHUDDefMoveDown
            // 
            this.buttonHUDDefMoveDown.Location = new System.Drawing.Point(66, 164);
            this.buttonHUDDefMoveDown.Name = "buttonHUDDefMoveDown";
            this.buttonHUDDefMoveDown.Size = new System.Drawing.Size(58, 24);
            this.buttonHUDDefMoveDown.TabIndex = 14;
            this.buttonHUDDefMoveDown.Text = "Down";
            this.buttonHUDDefMoveDown.UseVisualStyleBackColor = true;
            this.buttonHUDDefMoveDown.Click += new System.EventHandler(this.buttonHUDDefMoveDown_Click);
            // 
            // buttonHUDDefMoveUp
            // 
            this.buttonHUDDefMoveUp.Location = new System.Drawing.Point(66, 135);
            this.buttonHUDDefMoveUp.Name = "buttonHUDDefMoveUp";
            this.buttonHUDDefMoveUp.Size = new System.Drawing.Size(58, 24);
            this.buttonHUDDefMoveUp.TabIndex = 13;
            this.buttonHUDDefMoveUp.Text = "Up";
            this.buttonHUDDefMoveUp.UseVisualStyleBackColor = true;
            this.buttonHUDDefMoveUp.Click += new System.EventHandler(this.buttonHUDDefMoveUp_Click);
            // 
            // listBoxHUDDef
            // 
            this.listBoxHUDDef.AccessibleRole = System.Windows.Forms.AccessibleRole.List;
            this.listBoxHUDDef.FormattingEnabled = true;
            this.listBoxHUDDef.Location = new System.Drawing.Point(6, 19);
            this.listBoxHUDDef.Name = "listBoxHUDDef";
            this.listBoxHUDDef.Size = new System.Drawing.Size(120, 108);
            this.listBoxHUDDef.TabIndex = 11;
            // 
            // buttonHUDDefRemove
            // 
            this.buttonHUDDefRemove.Location = new System.Drawing.Point(4, 164);
            this.buttonHUDDefRemove.Name = "buttonHUDDefRemove";
            this.buttonHUDDefRemove.Size = new System.Drawing.Size(58, 25);
            this.buttonHUDDefRemove.TabIndex = 12;
            this.buttonHUDDefRemove.Text = "Remove";
            this.buttonHUDDefRemove.UseVisualStyleBackColor = true;
            this.buttonHUDDefRemove.Click += new System.EventHandler(this.buttonHUDDefRemove_Click);
            // 
            // buttonHUDDefAdd
            // 
            this.buttonHUDDefAdd.Location = new System.Drawing.Point(4, 135);
            this.buttonHUDDefAdd.Name = "buttonHUDDefAdd";
            this.buttonHUDDefAdd.Size = new System.Drawing.Size(58, 25);
            this.buttonHUDDefAdd.TabIndex = 11;
            this.buttonHUDDefAdd.Text = "Add";
            this.buttonHUDDefAdd.UseVisualStyleBackColor = true;
            this.buttonHUDDefAdd.Click += new System.EventHandler(this.buttonHUDDefAdd_Click);
            // 
            // textBoxHUDText
            // 
            this.textBoxHUDText.Location = new System.Drawing.Point(142, 74);
            this.textBoxHUDText.Multiline = true;
            this.textBoxHUDText.Name = "textBoxHUDText";
            this.textBoxHUDText.Size = new System.Drawing.Size(244, 113);
            this.textBoxHUDText.TabIndex = 29;
            this.textBoxHUDText.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // label175
            // 
            this.label175.AutoSize = true;
            this.label175.Location = new System.Drawing.Point(142, 57);
            this.label175.Name = "label175";
            this.label175.Size = new System.Drawing.Size(84, 13);
            this.label175.TabIndex = 30;
            this.label175.Text = "HUD Open Text";
            // 
            // label174
            // 
            this.label174.AutoSize = true;
            this.label174.Location = new System.Drawing.Point(241, 16);
            this.label174.Name = "label174";
            this.label174.Size = new System.Drawing.Size(72, 13);
            this.label174.TabIndex = 146;
            this.label174.Text = "Default Name";
            // 
            // textBoxHUDDefaultName
            // 
            this.textBoxHUDDefaultName.Location = new System.Drawing.Point(241, 33);
            this.textBoxHUDDefaultName.Name = "textBoxHUDDefaultName";
            this.textBoxHUDDefaultName.Size = new System.Drawing.Size(145, 20);
            this.textBoxHUDDefaultName.TabIndex = 145;
            this.textBoxHUDDefaultName.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // buttonHUDMoveDown
            // 
            this.buttonHUDMoveDown.Location = new System.Drawing.Point(77, 386);
            this.buttonHUDMoveDown.Name = "buttonHUDMoveDown";
            this.buttonHUDMoveDown.Size = new System.Drawing.Size(59, 23);
            this.buttonHUDMoveDown.TabIndex = 9;
            this.buttonHUDMoveDown.Text = "Down";
            this.buttonHUDMoveDown.UseVisualStyleBackColor = true;
            this.buttonHUDMoveDown.Click += new System.EventHandler(this.buttonHUDDown_Click);
            // 
            // label173
            // 
            this.label173.AutoSize = true;
            this.label173.Location = new System.Drawing.Point(142, 16);
            this.label173.Name = "label173";
            this.label173.Size = new System.Drawing.Size(18, 13);
            this.label173.TabIndex = 144;
            this.label173.Text = "ID";
            // 
            // buttonHUDMoveUp
            // 
            this.buttonHUDMoveUp.Location = new System.Drawing.Point(77, 359);
            this.buttonHUDMoveUp.Name = "buttonHUDMoveUp";
            this.buttonHUDMoveUp.Size = new System.Drawing.Size(59, 23);
            this.buttonHUDMoveUp.TabIndex = 8;
            this.buttonHUDMoveUp.Text = "Up";
            this.buttonHUDMoveUp.UseVisualStyleBackColor = true;
            this.buttonHUDMoveUp.Click += new System.EventHandler(this.buttonHUDUp_Click);
            // 
            // textBoxHUDID
            // 
            this.textBoxHUDID.Location = new System.Drawing.Point(142, 33);
            this.textBoxHUDID.Name = "textBoxHUDID";
            this.textBoxHUDID.Size = new System.Drawing.Size(92, 20);
            this.textBoxHUDID.TabIndex = 143;
            this.textBoxHUDID.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            this.textBoxHUDID.Leave += new System.EventHandler(this.textBoxHUDName_Leave);
            // 
            // buttonHUDRemove
            // 
            this.buttonHUDRemove.Location = new System.Drawing.Point(6, 388);
            this.buttonHUDRemove.Name = "buttonHUDRemove";
            this.buttonHUDRemove.Size = new System.Drawing.Size(59, 23);
            this.buttonHUDRemove.TabIndex = 7;
            this.buttonHUDRemove.Text = "Remove";
            this.buttonHUDRemove.UseVisualStyleBackColor = true;
            this.buttonHUDRemove.Click += new System.EventHandler(this.buttonHUDRemove_Click);
            // 
            // groupBox12
            // 
            this.groupBox12.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox12.Controls.Add(this.label183);
            this.groupBox12.Controls.Add(this.textBoxHUDInfokeysReq);
            this.groupBox12.Controls.Add(this.buttonHUDInfokeyMoveDown);
            this.groupBox12.Controls.Add(this.textBoxHUDInfokeysText);
            this.groupBox12.Controls.Add(this.buttonHUDInfokeyMoveUp);
            this.groupBox12.Controls.Add(this.listBoxHUDInfokeys);
            this.groupBox12.Controls.Add(this.buttonHUDInfokeyRemove);
            this.groupBox12.Controls.Add(this.label171);
            this.groupBox12.Controls.Add(this.buttonHUDInfokeyAdd);
            this.groupBox12.Location = new System.Drawing.Point(392, 16);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(315, 185);
            this.groupBox12.TabIndex = 10;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Infokeys";
            // 
            // label183
            // 
            this.label183.AutoSize = true;
            this.label183.Location = new System.Drawing.Point(132, 88);
            this.label183.Name = "label183";
            this.label183.Size = new System.Drawing.Size(91, 13);
            this.label183.TabIndex = 157;
            this.label183.Text = "Required Variable";
            // 
            // textBoxHUDInfokeysReq
            // 
            this.textBoxHUDInfokeysReq.Location = new System.Drawing.Point(135, 104);
            this.textBoxHUDInfokeysReq.Name = "textBoxHUDInfokeysReq";
            this.textBoxHUDInfokeysReq.Size = new System.Drawing.Size(85, 20);
            this.textBoxHUDInfokeysReq.TabIndex = 156;
            this.textBoxHUDInfokeysReq.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            // 
            // buttonHUDInfokeyMoveDown
            // 
            this.buttonHUDInfokeyMoveDown.Location = new System.Drawing.Point(215, 159);
            this.buttonHUDInfokeyMoveDown.Name = "buttonHUDInfokeyMoveDown";
            this.buttonHUDInfokeyMoveDown.Size = new System.Drawing.Size(77, 23);
            this.buttonHUDInfokeyMoveDown.TabIndex = 14;
            this.buttonHUDInfokeyMoveDown.Text = "Move Down";
            this.buttonHUDInfokeyMoveDown.UseVisualStyleBackColor = true;
            this.buttonHUDInfokeyMoveDown.Click += new System.EventHandler(this.buttonHUDInfokeyMoveDown_Click);
            // 
            // textBoxHUDInfokeysText
            // 
            this.textBoxHUDInfokeysText.Location = new System.Drawing.Point(132, 33);
            this.textBoxHUDInfokeysText.Multiline = true;
            this.textBoxHUDInfokeysText.Name = "textBoxHUDInfokeysText";
            this.textBoxHUDInfokeysText.Size = new System.Drawing.Size(163, 52);
            this.textBoxHUDInfokeysText.TabIndex = 27;
            this.textBoxHUDInfokeysText.DoubleClick += new System.EventHandler(this.DoubleClickTextbox);
            this.textBoxHUDInfokeysText.Leave += new System.EventHandler(this.textBoxHUDInfokeysText_Leave);
            // 
            // buttonHUDInfokeyMoveUp
            // 
            this.buttonHUDInfokeyMoveUp.Location = new System.Drawing.Point(215, 130);
            this.buttonHUDInfokeyMoveUp.Name = "buttonHUDInfokeyMoveUp";
            this.buttonHUDInfokeyMoveUp.Size = new System.Drawing.Size(77, 23);
            this.buttonHUDInfokeyMoveUp.TabIndex = 13;
            this.buttonHUDInfokeyMoveUp.Text = "Move Up";
            this.buttonHUDInfokeyMoveUp.UseVisualStyleBackColor = true;
            this.buttonHUDInfokeyMoveUp.Click += new System.EventHandler(this.buttonHUDInfokeyMoveUp_Click);
            // 
            // listBoxHUDInfokeys
            // 
            this.listBoxHUDInfokeys.AccessibleRole = System.Windows.Forms.AccessibleRole.List;
            this.listBoxHUDInfokeys.FormattingEnabled = true;
            this.listBoxHUDInfokeys.Location = new System.Drawing.Point(6, 19);
            this.listBoxHUDInfokeys.Name = "listBoxHUDInfokeys";
            this.listBoxHUDInfokeys.Size = new System.Drawing.Size(120, 134);
            this.listBoxHUDInfokeys.TabIndex = 11;
            // 
            // buttonHUDInfokeyRemove
            // 
            this.buttonHUDInfokeyRemove.Location = new System.Drawing.Point(132, 159);
            this.buttonHUDInfokeyRemove.Name = "buttonHUDInfokeyRemove";
            this.buttonHUDInfokeyRemove.Size = new System.Drawing.Size(77, 23);
            this.buttonHUDInfokeyRemove.TabIndex = 12;
            this.buttonHUDInfokeyRemove.Text = "Remove";
            this.buttonHUDInfokeyRemove.UseVisualStyleBackColor = true;
            this.buttonHUDInfokeyRemove.Click += new System.EventHandler(this.buttonHUDInfokeyRemove_Click);
            // 
            // label171
            // 
            this.label171.AutoSize = true;
            this.label171.Location = new System.Drawing.Point(132, 16);
            this.label171.Name = "label171";
            this.label171.Size = new System.Drawing.Size(28, 13);
            this.label171.TabIndex = 28;
            this.label171.Text = "Text";
            // 
            // buttonHUDInfokeyAdd
            // 
            this.buttonHUDInfokeyAdd.Location = new System.Drawing.Point(132, 130);
            this.buttonHUDInfokeyAdd.Name = "buttonHUDInfokeyAdd";
            this.buttonHUDInfokeyAdd.Size = new System.Drawing.Size(77, 23);
            this.buttonHUDInfokeyAdd.TabIndex = 11;
            this.buttonHUDInfokeyAdd.Text = "Add";
            this.buttonHUDInfokeyAdd.UseVisualStyleBackColor = true;
            this.buttonHUDInfokeyAdd.Click += new System.EventHandler(this.buttonHUDInfokeyAdd_Click);
            // 
            // buttonHUDAdd
            // 
            this.buttonHUDAdd.Location = new System.Drawing.Point(6, 359);
            this.buttonHUDAdd.Name = "buttonHUDAdd";
            this.buttonHUDAdd.Size = new System.Drawing.Size(59, 23);
            this.buttonHUDAdd.TabIndex = 6;
            this.buttonHUDAdd.Text = "Add";
            this.buttonHUDAdd.UseVisualStyleBackColor = true;
            this.buttonHUDAdd.Click += new System.EventHandler(this.buttonHUDAdd_Click);
            // 
            // listBoxHUD
            // 
            this.listBoxHUD.AccessibleRole = System.Windows.Forms.AccessibleRole.List;
            this.listBoxHUD.FormattingEnabled = true;
            this.listBoxHUD.Location = new System.Drawing.Point(6, 19);
            this.listBoxHUD.Name = "listBoxHUD";
            this.listBoxHUD.Size = new System.Drawing.Size(130, 329);
            this.listBoxHUD.TabIndex = 5;
            this.listBoxHUD.SelectedIndexChanged += new System.EventHandler(this.listBoxHUD_SelectedIndexChanged);
            // 
            // buttonExportAll
            // 
            this.buttonExportAll.Location = new System.Drawing.Point(103, 672);
            this.buttonExportAll.Name = "buttonExportAll";
            this.buttonExportAll.Size = new System.Drawing.Size(75, 23);
            this.buttonExportAll.TabIndex = 4;
            this.buttonExportAll.Text = "Export All";
            this.buttonExportAll.UseVisualStyleBackColor = true;
            this.buttonExportAll.Click += new System.EventHandler(this.buttonExportAll_Click);
            // 
            // openFileDialogImport
            // 
            this.openFileDialogImport.FileName = "openFileDialog1";
            // 
            // openFileDialogBook
            // 
            this.openFileDialogBook.FileName = "openFileDialog1";
            // 
            // buttonImportAll
            // 
            this.buttonImportAll.Location = new System.Drawing.Point(22, 672);
            this.buttonImportAll.Name = "buttonImportAll";
            this.buttonImportAll.Size = new System.Drawing.Size(75, 23);
            this.buttonImportAll.TabIndex = 6;
            this.buttonImportAll.Text = "Import All";
            this.buttonImportAll.UseVisualStyleBackColor = true;
            this.buttonImportAll.Click += new System.EventHandler(this.buttonImportAll_Click);
            // 
            // checkBoxDialogEndNode
            // 
            this.checkBoxDialogEndNode.AutoSize = true;
            this.checkBoxDialogEndNode.Location = new System.Drawing.Point(217, 23);
            this.checkBoxDialogEndNode.Name = "checkBoxDialogEndNode";
            this.checkBoxDialogEndNode.Size = new System.Drawing.Size(74, 17);
            this.checkBoxDialogEndNode.TabIndex = 178;
            this.checkBoxDialogEndNode.Text = "End Node";
            this.checkBoxDialogEndNode.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.ClientSize = new System.Drawing.Size(1349, 707);
            this.Controls.Add(this.buttonImportAll);
            this.Controls.Add(this.buttonExportAll);
            this.Controls.Add(this.Tabs);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.Text = "Sequences Stat Builder";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Tabs.ResumeLayout(false);
            this.tabCharacters.ResumeLayout(false);
            this.groupBox36.ResumeLayout(false);
            this.groupBox36.PerformLayout();
            this.groupBox38.ResumeLayout(false);
            this.groupBox37.ResumeLayout(false);
            this.groupBox35.ResumeLayout(false);
            this.groupBox35.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarCharacterLevel)).EndInit();
            this.groupBox34.ResumeLayout(false);
            this.groupBox34.PerformLayout();
            this.groupBox33.ResumeLayout(false);
            this.groupBox33.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarCharacterHP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarCharacterMP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarCharacterPhyPower)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarCharacterPhyGuard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarCharacterMagPower)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarCharacterSpeGuard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarCharacterMagGuard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarCharacterSpePower)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabMonsters.ResumeLayout(false);
            this.tabMonsters.PerformLayout();
            this.groupBox32.ResumeLayout(false);
            this.groupBox32.PerformLayout();
            this.groupBox21.ResumeLayout(false);
            this.groupBox21.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarEnemySpeGuard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarEnemySpePower)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarEnemyMagGuard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarEnemyMagPower)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarEnemyPhyGuard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarEnemyPhyPower)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarEnemyMP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarEnemyHP)).EndInit();
            this.tabItems.ResumeLayout(false);
            this.tabItems.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarTabletSpeGuard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarTabletSpePower)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarTabletMagGuard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarTabletMagPower)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarTabletPhyGuard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarTabletPhyPower)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarItemSpeGuard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarItemSpePower)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarItemMagGuard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarItemMagPower)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarItemPhyGuard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarItemPhyPower)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.tabMove.ResumeLayout(false);
            this.groupBox28.ResumeLayout(false);
            this.groupBox28.PerformLayout();
            this.groupBox29.ResumeLayout(false);
            this.groupBox29.PerformLayout();
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox27.ResumeLayout(false);
            this.groupBox27.PerformLayout();
            this.groupBox26.ResumeLayout(false);
            this.groupBox26.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.tabBook.ResumeLayout(false);
            this.tabBook.PerformLayout();
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.tabBird.ResumeLayout(false);
            this.groupBox30.ResumeLayout(false);
            this.groupBox30.PerformLayout();
            this.tabExp.ResumeLayout(false);
            this.tabExp.PerformLayout();
            this.tabBattleScripts.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.tabAXHud.ResumeLayout(false);
            this.groupBox31.ResumeLayout(false);
            this.groupBox25.ResumeLayout(false);
            this.groupBox24.ResumeLayout(false);
            this.groupBox24.PerformLayout();
            this.groupBox23.ResumeLayout(false);
            this.groupBox23.PerformLayout();
            this.groupBox22.ResumeLayout(false);
            this.groupBox22.PerformLayout();
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.groupBox20.ResumeLayout(false);
            this.groupBox20.PerformLayout();
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl Tabs;
        private System.Windows.Forms.TabPage tabCharacters;
        private System.Windows.Forms.TabPage tabMonsters;
        private System.Windows.Forms.TabPage tabItems;
        private System.Windows.Forms.TabPage tabMove;
        private System.Windows.Forms.TabPage tabBook;
        private System.Windows.Forms.TabPage tabBird;
        private System.Windows.Forms.Button buttonExportAll;
        private System.Windows.Forms.Button buttonCharacterMoveDown;
        private System.Windows.Forms.Button buttonCharacterMoveUp;
        private System.Windows.Forms.Button buttonCharacterRemove;
        private System.Windows.Forms.Button buttonCharacterAdd;
        private System.Windows.Forms.ListBox listBoxCharacters;
        private System.Windows.Forms.Button buttonMonsterMoveDown;
        private System.Windows.Forms.Button buttonMonsterMoveUp;
        private System.Windows.Forms.Button buttonMonsterRemove;
        private System.Windows.Forms.Button buttonMonsterAdd;
        private System.Windows.Forms.ListBox listBoxMonsters;
        private System.Windows.Forms.Button buttonItemMoveDown;
        private System.Windows.Forms.Button buttonItemMoveUp;
        private System.Windows.Forms.Button buttonItemRemove;
        private System.Windows.Forms.Button buttonItemAdd;
        private System.Windows.Forms.ListBox listBoxItems;
        private System.Windows.Forms.Button buttonMovesMoveDown;
        private System.Windows.Forms.Button buttonMovesMoveUp;
        private System.Windows.Forms.Button buttonMovesRemove;
        private System.Windows.Forms.Button buttonMovesAdd;
        private System.Windows.Forms.ListBox listBoxMoves;
        private System.Windows.Forms.Button buttonBookMoveDown;
        private System.Windows.Forms.Button buttonBookMoveUp;
        private System.Windows.Forms.Button buttonBookRemove;
        private System.Windows.Forms.Button buttonBookAdd;
        private System.Windows.Forms.ListBox listBoxBooks;
        private System.Windows.Forms.Button buttonBirdMoveDown;
        private System.Windows.Forms.Button buttonBirdMoveUp;
        private System.Windows.Forms.Button buttonBirdRemove;
        private System.Windows.Forms.Button buttonBirdAdd;
        private System.Windows.Forms.ListBox listBoxBird;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxCharacterName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxEnemyName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxItemName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxMoveName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxBookName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxBirdName;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxCharacterBattleDesc;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxCharacterAbility;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBoxCharacterVisualDesc;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.TextBox textBoxCharacterColor;
        private System.Windows.Forms.Label labelCharacterHP;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TrackBar trackBarCharacterHP;
        private System.Windows.Forms.Label labelCharacterSpeGuard;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TrackBar trackBarCharacterSpeGuard;
        private System.Windows.Forms.Label labelCharacterSpePower;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TrackBar trackBarCharacterSpePower;
        private System.Windows.Forms.Label labelCharacterMagGuard;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TrackBar trackBarCharacterMagGuard;
        private System.Windows.Forms.Label labelCharacterMagPower;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TrackBar trackBarCharacterMagPower;
        private System.Windows.Forms.Label labelCharacterPhyGuard;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TrackBar trackBarCharacterPhyGuard;
        private System.Windows.Forms.Label labelCharacterPhyPower;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TrackBar trackBarCharacterPhyPower;
        private System.Windows.Forms.Label labelCharacterMP;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TrackBar trackBarCharacterMP;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label labelCharacterBlue;
        private System.Windows.Forms.Label labelCharacterGreen;
        private System.Windows.Forms.Label labelCharacterRed;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBoxCharacterSpriteIdleFront;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox textBoxCharacterSpriteFace;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox textBoxCharacterSpriteSpeed;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox textBoxCharacterSpriteCast1Back;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox textBoxCharacterSpriteCast1Front;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox textBoxCharacterSpriteCast0Back;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox textBoxCharacterSpriteCast0Front;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox textBoxCharacterSpriteJump1Back;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox textBoxCharacterSpriteJump1Front;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox textBoxCharacterSpriteJump0Back;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox textBoxCharacterSpriteJump0Front;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox textBoxCharacterSpriteHitBack;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox textBoxCharacterSpriteHitFront;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox textBoxCharacterSpriteIdleBack;
        private System.Windows.Forms.Panel panelCharacterStat;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.SaveFileDialog saveFileExportThis;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.ComboBox comboBoxCharacterType;
        private System.Windows.Forms.OpenFileDialog openFileDialogImport;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Panel panelEnemyStat;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox textBoxEnemySpriteScale;
        private System.Windows.Forms.TextBox textBoxEnemySpriteIdleFront;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.TextBox textBoxEnemySpriteIdleBack;
        private System.Windows.Forms.TextBox textBoxEnemySpriteSpeed;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TextBox textBoxEnemySpriteHitFront;
        private System.Windows.Forms.TextBox textBoxEnemySpriteCast1Back;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.TextBox textBoxEnemySpriteHitBack;
        private System.Windows.Forms.TextBox textBoxEnemySpriteCast1Front;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.TextBox textBoxEnemySpriteJump0Front;
        private System.Windows.Forms.TextBox textBoxEnemySpriteCast0Back;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.TextBox textBoxEnemySpriteJump0Back;
        private System.Windows.Forms.TextBox textBoxEnemySpriteCast0Front;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.TextBox textBoxEnemySpriteJump1Front;
        private System.Windows.Forms.TextBox textBoxEnemySpriteJump1Back;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label labelEnemySpeGuard;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.TrackBar trackBarEnemySpeGuard;
        private System.Windows.Forms.Label labelEnemySpePower;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.TrackBar trackBarEnemySpePower;
        private System.Windows.Forms.Label labelEnemyMagGuard;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.TrackBar trackBarEnemyMagGuard;
        private System.Windows.Forms.Label labelEnemyMagPower;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.TrackBar trackBarEnemyMagPower;
        private System.Windows.Forms.Label labelEnemyPhyGuard;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.TrackBar trackBarEnemyPhyGuard;
        private System.Windows.Forms.Label labelEnemyPhyPower;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.TrackBar trackBarEnemyPhyPower;
        private System.Windows.Forms.Label labelEnemyMP;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.TrackBar trackBarEnemyMP;
        private System.Windows.Forms.Label labelEnemyHP;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.TrackBar trackBarEnemyHP;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.TextBox textBoxEnemyVisualDesc;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.TextBox textBoxEnemyFlavor0;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.ComboBox comboBoxEnemyType;
        private System.Windows.Forms.Label labelEnemyBlue;
        private System.Windows.Forms.Label labelEnemyGreen;
        private System.Windows.Forms.Label labelEnemyRed;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox textBoxEnemyColor;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox textBoxEnemyAbility;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.TextBox textBoxEnemySpriteZoomY;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.TextBox textBoxEnemySpriteZoomX;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.TextBox textBoxEnemySpriteShadowY;
        private System.Windows.Forms.TabPage tabExp;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox textBoxGrowthName;
        private System.Windows.Forms.Button buttonGrowthMoveDown;
        private System.Windows.Forms.Button buttonGrowthMoveUp;
        private System.Windows.Forms.Button buttonGrowthRemove;
        private System.Windows.Forms.Button buttonGrowthAdd;
        private System.Windows.Forms.ListBox listBoxGrowth;
        private System.Windows.Forms.Panel panelGrowthDisplay;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TrackBar trackBarCharacterLevel;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.ComboBox comboBoxCharacterEXPGrowth;
        private System.Windows.Forms.TextBox textBoxCharacterEXPTotal;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Panel panelCharacterSpeGuardGrowth;
        private System.Windows.Forms.Panel panelCharacterPhyGuardGrowth;
        private System.Windows.Forms.Panel panelCharacterMagGuardGrowth;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.ComboBox comboBoxCharacterSpeGuardGrowth;
        private System.Windows.Forms.ComboBox comboBoxCharacterPhyGuardGrowth;
        private System.Windows.Forms.ComboBox comboBoxCharacterMagGuardGrowth;
        private System.Windows.Forms.Panel panelCharacterSpePowerGrowth;
        private System.Windows.Forms.Panel panelCharacterPhyPowerGrowth;
        private System.Windows.Forms.ComboBox comboBoxCharacterSpePowerGrowth;
        private System.Windows.Forms.ComboBox comboBoxCharacterPhyPowerGrowth;
        private System.Windows.Forms.Panel panelCharacterMagPowerGrowth;
        private System.Windows.Forms.ComboBox comboBoxCharacterMagPowerGrowth;
        private System.Windows.Forms.Panel panelCharacterEXPGrowth;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Panel panelCharacterMPGrowth;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.ComboBox comboBoxCharacterMPGrowth;
        private System.Windows.Forms.Panel panelCharacterHPGrowth;
        private System.Windows.Forms.ComboBox comboBoxCharacterHPGrowth;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.TextBox textBoxEnemyFlavor2;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.TextBox textBoxEnemyFlavor1;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.TextBox textBoxEnemyDesc;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.TextBox textBoxEnemyMaxMoney;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.TextBox textBoxEnemyMinMoney;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.TextBox textBoxEnemyBribe;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.TextBox textBoxEnemyExp;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.TextBox textBoxItemDesc;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.ComboBox comboBoxItemType;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.ComboBox comboBoxBattleMove;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.TextBox textBoxItemOverworldScript;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.TextBox textBoxItemBookName;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.TextBox textBoxItemSprite;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckBox checkBoxItemCanToss;
        private System.Windows.Forms.CheckBox checkBoxItemCanGive;
        private System.Windows.Forms.CheckBox checkBoxItemSingleUse;
        private System.Windows.Forms.CheckBox checkBoxItemCanSell;
        private System.Windows.Forms.Label labelItemSpeGuard;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.TrackBar trackBarItemSpeGuard;
        private System.Windows.Forms.Label labelItemSpePower;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.TrackBar trackBarItemSpePower;
        private System.Windows.Forms.Label labelItemMagGuard;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.TrackBar trackBarItemMagGuard;
        private System.Windows.Forms.Label labelItemMagPower;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.TrackBar trackBarItemMagPower;
        private System.Windows.Forms.Label labelItemPhyGuard;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.TrackBar trackBarItemPhyGuard;
        private System.Windows.Forms.Label labelItemPhyPower;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.TrackBar trackBarItemPhyPower;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.TextBox textBoxItemPrice;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.CheckBox checkBoxMoveRotateCamera;
        private System.Windows.Forms.CheckBox checkBoxMoveTargetSelf;
        private System.Windows.Forms.CheckBox checkBoxMoveTargetAlly;
        private System.Windows.Forms.CheckBox checkBoxMoveTargetSkip;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.ComboBox comboBoxMoveType;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.TextBox textBoxMoveDesc;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.ComboBox comboBoxMovePhysMag;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.TextBox textBoxMoveScript;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.TextBox textBoxMoveTargets;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.TextBox textBoxMoveMPCost;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.TextBox textBoxMoveDamage;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.TextBox textBoxMoveSpeedMultiplier;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.TextBox textBoxMovePriority;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.TextBox textBoxMoveEffectPercent;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.ComboBox comboBoxMoveItem;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.TextBox textBoxMoveUserSprite;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.TextBox textBoxMoveTargetSprite;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.TextBox textBoxMoveBackgroundSprite;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.TextBox textBoxMoveForegroundSprite;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.TextBox textBoxMoveDuration;
        private System.Windows.Forms.Label label122;
        private System.Windows.Forms.ComboBox comboBoxMoveBattlerVisibility;
        private System.Windows.Forms.CheckBox checkBoxMoveKnockback;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button buttonMoveTextRemove;
        private System.Windows.Forms.Button buttonMoveTextAdd;
        private System.Windows.Forms.ListBox listBoxMoveTextList;
        private System.Windows.Forms.TextBox textBoxMoveTextLine;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.TextBox textBoxMoveStatusPercent;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.ComboBox comboBoxMoveStatus;
        private System.Windows.Forms.Button buttonMoveStatusRemove;
        private System.Windows.Forms.Button buttonMoveStatusAdd;
        private System.Windows.Forms.ListBox listBoxMoveStatusList;
        private System.Windows.Forms.Button buttonMoveTextUpdate;
        private System.Windows.Forms.Label label135;
        private System.Windows.Forms.TextBox textBoxBirdLongName;
        private System.Windows.Forms.Label label133;
        private System.Windows.Forms.TextBox textBoxBirdDesc;
        private System.Windows.Forms.Label label141;
        private System.Windows.Forms.TextBox textBoxBirdMetricWeight;
        private System.Windows.Forms.Label label143;
        private System.Windows.Forms.TextBox textBoxBirdMetricHeight;
        private System.Windows.Forms.Label label139;
        private System.Windows.Forms.TextBox textBoxBirdWeight;
        private System.Windows.Forms.Label label137;
        private System.Windows.Forms.TextBox textBoxBirdHeight;
        private System.Windows.Forms.Label label146;
        private System.Windows.Forms.TextBox textBoxBirdFemaleSprite;
        private System.Windows.Forms.Label label147;
        private System.Windows.Forms.TextBox textBoxBirdMaleSprite;
        private System.Windows.Forms.Label label148;
        private System.Windows.Forms.TextBox textBoxBirdScale;
        private System.Windows.Forms.Label label149;
        private System.Windows.Forms.TextBox textBoxBirdOffset;
        private System.Windows.Forms.Label label150;
        private System.Windows.Forms.TextBox textBoxBirdSpawnRatio;
        private System.Windows.Forms.Button buttonBookSave;
        private System.Windows.Forms.Button buttonBookLoad;
        private System.Windows.Forms.RichTextBox richTextBoxBook;
        private System.Windows.Forms.Label label152;
        private System.Windows.Forms.TextBox textBoxBookFilename;
        private System.Windows.Forms.Label label151;
        private System.Windows.Forms.TextBox textBoxBookAuthor;
        private System.Windows.Forms.OpenFileDialog openFileDialogBook;
        private System.Windows.Forms.SaveFileDialog saveFileDialogBook;
        private System.Windows.Forms.Button buttonImportAll;
        private System.Windows.Forms.FolderBrowserDialog folderSelector;
        private System.Windows.Forms.Label label153;
        private System.Windows.Forms.TextBox textBoxMoveCharacter;
        private System.Windows.Forms.TabPage tabBattleScripts;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label label160;
        private System.Windows.Forms.Label label163;
        private System.Windows.Forms.TextBox textBoxEndPhaseScript;
        private System.Windows.Forms.TextBox textBoxEndPhaseFgSprite;
        private System.Windows.Forms.Label label161;
        private System.Windows.Forms.TextBox textBoxEndPhaseBgSprite;
        private System.Windows.Forms.TextBox textBoxEndPhaseTargetSprite;
        private System.Windows.Forms.Label label162;
        private System.Windows.Forms.Label label165;
        private System.Windows.Forms.TextBox textBoxEndPhaseLength;
        private System.Windows.Forms.Label label164;
        private System.Windows.Forms.TextBox textBoxEndPhaseName;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.ListBox listBoxStatus;
        private System.Windows.Forms.Label label129;
        private System.Windows.Forms.Button buttonStatusAdd;
        private System.Windows.Forms.TextBox textBoxStatusShader;
        private System.Windows.Forms.Button buttonStatusRemove;
        private System.Windows.Forms.Label label128;
        private System.Windows.Forms.Button buttonStatusMoveUp;
        private System.Windows.Forms.TextBox textBoxStatusFinalText;
        private System.Windows.Forms.Button buttonStatusMoveDown;
        private System.Windows.Forms.Label label127;
        private System.Windows.Forms.TextBox textBoxStatusName;
        private System.Windows.Forms.TextBox textBoxStatusSprite;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox checkBoxStatusHarmful;
        private System.Windows.Forms.TextBox textBoxStatusDesc;
        private System.Windows.Forms.Label label125;
        private System.Windows.Forms.Label label124;
        private System.Windows.Forms.TextBox textBoxStatusMaxTurns;
        private System.Windows.Forms.TextBox textBoxStatusMinTurns;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.Button buttonEndPhaseMoveDown;
        private System.Windows.Forms.Button buttonEndPhaseMoveUp;
        private System.Windows.Forms.Button buttonEndPhaseRemove;
        private System.Windows.Forms.Button buttonEndPhaseAdd;
        private System.Windows.Forms.ListBox listBoxEndPhase;
        private System.Windows.Forms.Label label145;
        private System.Windows.Forms.ComboBox comboBoxTabletMove;
        private System.Windows.Forms.Label labelTabletSpeGuard;
        private System.Windows.Forms.Label label134;
        private System.Windows.Forms.TrackBar trackBarTabletSpeGuard;
        private System.Windows.Forms.Label labelTabletSpePower;
        private System.Windows.Forms.Label label136;
        private System.Windows.Forms.TrackBar trackBarTabletSpePower;
        private System.Windows.Forms.Label labelTabletMagGuard;
        private System.Windows.Forms.Label label138;
        private System.Windows.Forms.TrackBar trackBarTabletMagGuard;
        private System.Windows.Forms.Label labelTabletMagPower;
        private System.Windows.Forms.Label label140;
        private System.Windows.Forms.TrackBar trackBarTabletMagPower;
        private System.Windows.Forms.Label labelTabletPhyGuard;
        private System.Windows.Forms.Label label142;
        private System.Windows.Forms.TrackBar trackBarTabletPhyGuard;
        private System.Windows.Forms.Label labelTabletPhyPower;
        private System.Windows.Forms.Label label144;
        private System.Windows.Forms.TrackBar trackBarTabletPhyPower;
        private System.Windows.Forms.Label label130;
        private System.Windows.Forms.TextBox textBoxTabletChar;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.TextBox textBoxTabletDesc;
        private System.Windows.Forms.Label label132;
        private System.Windows.Forms.TextBox textBoxTabletName;
        private System.Windows.Forms.Button buttonTabletMoveDown;
        private System.Windows.Forms.Button buttonTabletMoveUp;
        private System.Windows.Forms.Button buttonTabletRemove;
        private System.Windows.Forms.Button buttonTabletAdd;
        private System.Windows.Forms.ListBox listBoxTablet;
        private System.Windows.Forms.Label label167;
        private System.Windows.Forms.Label label166;
        private System.Windows.Forms.Label label156;
        private System.Windows.Forms.Label label157;
        private System.Windows.Forms.TextBox textBoxMonsterMovesLevel;
        private System.Windows.Forms.ComboBox comboBoxMonsterMovesMove;
        private System.Windows.Forms.Button buttonMonsterMovesRemove;
        private System.Windows.Forms.Button buttonMonsterMovesAdd;
        private System.Windows.Forms.ListBox listBoxMonsterMovesList;
        private System.Windows.Forms.ListBox listBoxMonsterMoves;
        private System.Windows.Forms.Label label155;
        private System.Windows.Forms.Label label154;
        private System.Windows.Forms.TextBox textBoxCharacterMovesLevel;
        private System.Windows.Forms.ComboBox comboBoxCharacterMovesMove;
        private System.Windows.Forms.Button buttonCharacterMovesRemove;
        private System.Windows.Forms.Button buttonCharacterMovesAdd;
        private System.Windows.Forms.ListBox listBoxCharacterMovesList;
        private System.Windows.Forms.ListBox listBoxCharacterMoves;
        private System.Windows.Forms.Label label168;
        private System.Windows.Forms.TextBox textBoxEndPhaseConditionScript;
        private System.Windows.Forms.CheckBox checkBoxEndPhaseTarget;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.ComboBox comboBoxWordMove;
        private System.Windows.Forms.ListBox listBoxWords;
        private System.Windows.Forms.Button buttonWordAdd;
        private System.Windows.Forms.Button buttonWordRemove;
        private System.Windows.Forms.Button buttonWordUp;
        private System.Windows.Forms.Button buttonWordDown;
        private System.Windows.Forms.Label label172;
        private System.Windows.Forms.Label label169;
        private System.Windows.Forms.TextBox textBoxWordName;
        private System.Windows.Forms.CheckBox checkBoxEndPhasePause;
        private System.Windows.Forms.Label label170;
        private System.Windows.Forms.TextBox textBoxWordDesc;
        private System.Windows.Forms.TabPage tabAXHud;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Button buttonHUDInfokeyMoveDown;
        private System.Windows.Forms.TextBox textBoxHUDInfokeysText;
        private System.Windows.Forms.Button buttonHUDInfokeyMoveUp;
        private System.Windows.Forms.ListBox listBoxHUDInfokeys;
        private System.Windows.Forms.Button buttonHUDInfokeyRemove;
        private System.Windows.Forms.Label label171;
        private System.Windows.Forms.Button buttonHUDInfokeyAdd;
        private System.Windows.Forms.Button buttonHUDMoveDown;
        private System.Windows.Forms.Button buttonHUDMoveUp;
        private System.Windows.Forms.Button buttonHUDRemove;
        private System.Windows.Forms.Button buttonHUDAdd;
        private System.Windows.Forms.ListBox listBoxHUD;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.Label label182;
        private System.Windows.Forms.Button buttonHUDChildMoveDown;
        private System.Windows.Forms.Button buttonHUDChildMoveUp;
        private System.Windows.Forms.TextBox textBoxHUDChildName;
        private System.Windows.Forms.ListBox listBoxHUDChild;
        private System.Windows.Forms.Button buttonHUDChildRemove;
        private System.Windows.Forms.Label label181;
        private System.Windows.Forms.ComboBox comboBoxHUDChildTemplate;
        private System.Windows.Forms.Button buttonHUDChildAdd;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.TextBox textBoxHUDDefArg;
        private System.Windows.Forms.Label label178;
        private System.Windows.Forms.Label label177;
        private System.Windows.Forms.ComboBox comboBoxHUDDefFunc;
        private System.Windows.Forms.Label label176;
        private System.Windows.Forms.ComboBox comboBoxHUDDefInput;
        private System.Windows.Forms.Button buttonHUDDefMoveDown;
        private System.Windows.Forms.Button buttonHUDDefMoveUp;
        private System.Windows.Forms.ListBox listBoxHUDDef;
        private System.Windows.Forms.Button buttonHUDDefRemove;
        private System.Windows.Forms.Button buttonHUDDefAdd;
        private System.Windows.Forms.TextBox textBoxHUDText;
        private System.Windows.Forms.Label label175;
        private System.Windows.Forms.Label label174;
        private System.Windows.Forms.TextBox textBoxHUDDefaultName;
        private System.Windows.Forms.Label label173;
        private System.Windows.Forms.TextBox textBoxHUDID;
        private System.Windows.Forms.Label label179;
        private System.Windows.Forms.TextBox textBoxHUDChildVar;
        private System.Windows.Forms.Label label180;
        private System.Windows.Forms.TextBox textBoxHUDChildCustomHUDText;
        private System.Windows.Forms.Label label183;
        private System.Windows.Forms.TextBox textBoxHUDInfokeysReq;
        private System.Windows.Forms.Label label184;
        private System.Windows.Forms.TextBox textBoxHUDDefReq;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.Label label188;
        private System.Windows.Forms.TextBox textBoxDiagramSprite;
        private System.Windows.Forms.Label label185;
        private System.Windows.Forms.TextBox textBoxDiagramAltText;
        private System.Windows.Forms.Label label186;
        private System.Windows.Forms.TextBox textBoxDiagramYScale;
        private System.Windows.Forms.Label label187;
        private System.Windows.Forms.Button buttonDiagramMoveDown;
        private System.Windows.Forms.Button buttonDiagramMoveUp;
        private System.Windows.Forms.TextBox textBoxDiagramXScale;
        private System.Windows.Forms.ListBox listBoxDiagram;
        private System.Windows.Forms.Button buttonDiagramRemove;
        private System.Windows.Forms.Button buttonDiagramAdd;
        private System.Windows.Forms.Label label189;
        private System.Windows.Forms.TextBox textBoxDiagramName;
        private System.Windows.Forms.Label label190;
        private System.Windows.Forms.TextBox textBoxDiagramYOffset;
        private System.Windows.Forms.Label label191;
        private System.Windows.Forms.TextBox textBoxDiagramXOffset;
        private System.Windows.Forms.TextBox textBoxDiagramBlend;
        private System.Windows.Forms.Label label192;
        private System.Windows.Forms.Label label193;
        private System.Windows.Forms.TextBox textBoxItemHelp;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.TextBox textBoxMoveAnimationSound;
        private System.Windows.Forms.TextBox textBoxMoveAnimationSprite;
        private System.Windows.Forms.TextBox textBoxMoveAnimationLunge;
        private System.Windows.Forms.TextBox textBoxMoveAnimationJump;
        private System.Windows.Forms.Button buttonMoveAnimationAdd;
        private System.Windows.Forms.Button buttonMoveAnimationRemove;
        private System.Windows.Forms.ListBox listBoxMoveAnimation;
        private System.Windows.Forms.Button buttonMoveAnimationUp;
        private System.Windows.Forms.Button buttonMoveAnimationDown;
        private System.Windows.Forms.Label label197;
        private System.Windows.Forms.Label label196;
        private System.Windows.Forms.Label label195;
        private System.Windows.Forms.Label label194;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.TextBox textBoxMoveAnimationFrame;
        private System.Windows.Forms.ComboBox comboBoxMoveAnimationTarget;
        private System.Windows.Forms.Label label198;
        private System.Windows.Forms.Label label199;
        private System.Windows.Forms.TextBox textBoxMoveAnimationJumpHeight;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.GroupBox groupBox20;
        private System.Windows.Forms.Button buttonTextEntryRemove;
        private System.Windows.Forms.Label label203;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.Label label212;
        private System.Windows.Forms.TextBox textBoxTextGraphicsB;
        private System.Windows.Forms.Label label210;
        private System.Windows.Forms.TextBox textBoxTextGraphicsG;
        private System.Windows.Forms.Label label211;
        private System.Windows.Forms.TextBox textBoxTextGraphicsR;
        private System.Windows.Forms.Label label209;
        private System.Windows.Forms.TextBox textBoxTextGraphicsAlpha;
        private System.Windows.Forms.Label label208;
        private System.Windows.Forms.TextBox textBoxTextGraphicsSprite;
        private System.Windows.Forms.Label label206;
        private System.Windows.Forms.TextBox textBoxTextGraphicsYScale;
        private System.Windows.Forms.Label label207;
        private System.Windows.Forms.TextBox textBoxTextGraphicsXScale;
        private System.Windows.Forms.Label label205;
        private System.Windows.Forms.TextBox textBoxTextGraphicsY;
        private System.Windows.Forms.Label label204;
        private System.Windows.Forms.TextBox textBoxTextGraphicsX;
        private System.Windows.Forms.Button buttonTextGraphicsRemove;
        private System.Windows.Forms.ListBox listBoxTextGraphics;
        private System.Windows.Forms.Button buttonTextGraphicsAdd;
        private System.Windows.Forms.ListBox listBoxTextEntry;
        private System.Windows.Forms.Button buttonTextEntryAdd;
        private System.Windows.Forms.TextBox textBoxTextEntryText;
        private System.Windows.Forms.Label label200;
        private System.Windows.Forms.TextBox textBoxTextboxEventID;
        private System.Windows.Forms.Label label201;
        private System.Windows.Forms.Button buttonTextboxMoveDown;
        private System.Windows.Forms.Button buttonTextboxMoveUp;
        private System.Windows.Forms.TextBox textBoxTextboxName;
        private System.Windows.Forms.ListBox listBoxTextbox;
        private System.Windows.Forms.Button buttonTextboxRemove;
        private System.Windows.Forms.Button buttonTextboxAdd;
        private System.Windows.Forms.Label label202;
        private System.Windows.Forms.TextBox textBoxTextGraphicsAngle;
        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.ListBox listBoxEncounters;
        private System.Windows.Forms.CheckBox checkBoxEncounterRandom;
        private System.Windows.Forms.Label label218;
        private System.Windows.Forms.TextBox textBoxEncounterName;
        private System.Windows.Forms.Label label217;
        private System.Windows.Forms.TextBox textBoxEncounterEventDisabled;
        private System.Windows.Forms.Label label216;
        private System.Windows.Forms.TextBox textBoxEncounterEventEnabled;
        private System.Windows.Forms.Label label215;
        private System.Windows.Forms.TextBox textBoxEncounterEnemy3;
        private System.Windows.Forms.Label label214;
        private System.Windows.Forms.TextBox textBoxEncounterEnemy2;
        private System.Windows.Forms.Label label213;
        private System.Windows.Forms.TextBox textBoxEncounterEnemy1;
        private System.Windows.Forms.Button buttonEncounterMoveDown;
        private System.Windows.Forms.Button buttonEncounterAdd;
        private System.Windows.Forms.Button buttonEncounterMoveUp;
        private System.Windows.Forms.Button buttonEncounterRemove;
        private System.Windows.Forms.GroupBox groupBox22;
        private System.Windows.Forms.Button buttonTasksMoveDown;
        private System.Windows.Forms.ListBox listBoxTasks;
        private System.Windows.Forms.Button buttonTasksMoveUp;
        private System.Windows.Forms.Button buttonTasksAdd;
        private System.Windows.Forms.Button buttonTasksRemove;
        private System.Windows.Forms.Label label224;
        private System.Windows.Forms.Label label223;
        private System.Windows.Forms.TextBox textBoxTaskDesc;
        private System.Windows.Forms.TextBox textBoxTaskSprite;
        private System.Windows.Forms.Label label222;
        private System.Windows.Forms.Label label221;
        private System.Windows.Forms.Label label220;
        private System.Windows.Forms.TextBox textBoxTaskName;
        private System.Windows.Forms.Label label219;
        private System.Windows.Forms.TextBox textBoxTaskID;
        private System.Windows.Forms.GroupBox groupBox23;
        private System.Windows.Forms.Label label225;
        private System.Windows.Forms.TextBox textBoxSMSText;
        private System.Windows.Forms.Label label229;
        private System.Windows.Forms.TextBox textBoxSMSSender;
        private System.Windows.Forms.Label label230;
        private System.Windows.Forms.Button buttonSMSMoveDown;
        private System.Windows.Forms.TextBox textBoxSMSName;
        private System.Windows.Forms.ListBox listBoxSMS;
        private System.Windows.Forms.Button buttonSMSMoveUp;
        private System.Windows.Forms.Button buttonSMSAdd;
        private System.Windows.Forms.Button buttonSMSRemove;
        private System.Windows.Forms.Label label226;
        private System.Windows.Forms.TextBox textBoxSMSTitle;
        private System.Windows.Forms.Label label227;
        private System.Windows.Forms.TextBox textBoxMoveAnimationLungePos;
        private System.Windows.Forms.Label label228;
        private System.Windows.Forms.TextBox textBoxCharacterPhone;
        private System.Windows.Forms.GroupBox groupBox24;
        private System.Windows.Forms.Label label231;
        private System.Windows.Forms.TextBox textBoxVoicemailDialogID;
        private System.Windows.Forms.Label label232;
        private System.Windows.Forms.TextBox textBoxVoicemailDesc;
        private System.Windows.Forms.Label label233;
        private System.Windows.Forms.TextBox textBoxVoicemailSender;
        private System.Windows.Forms.Label label234;
        private System.Windows.Forms.Button buttonVoicemailMoveDown;
        private System.Windows.Forms.TextBox textBoxVoicemailName;
        private System.Windows.Forms.ListBox listBoxVoicemail;
        private System.Windows.Forms.Button buttonVoicemailMoveUp;
        private System.Windows.Forms.Button buttonVoicemailAdd;
        private System.Windows.Forms.Button buttonVoicemailRemove;
        private System.Windows.Forms.GroupBox groupBox25;
        private System.Windows.Forms.Button buttonEventDown;
        private System.Windows.Forms.ListBox listBoxEvent;
        private System.Windows.Forms.Button buttonEventUp;
        private System.Windows.Forms.Button buttonEventAdd;
        private System.Windows.Forms.Button buttonEventRemove;
        private System.Windows.Forms.GroupBox groupBox29;
        private System.Windows.Forms.GroupBox groupBox28;
        private System.Windows.Forms.GroupBox groupBox27;
        private System.Windows.Forms.GroupBox groupBox26;
        private System.Windows.Forms.GroupBox groupBox30;
        private System.Windows.Forms.GroupBox groupBox31;
        private System.Windows.Forms.Button buttonItemEventDown;
        private System.Windows.Forms.ListBox listBoxItemEvent;
        private System.Windows.Forms.Button buttonItemEventUp;
        private System.Windows.Forms.Button buttonItemEventAdd;
        private System.Windows.Forms.Button buttonItemEventRemove;
        private System.Windows.Forms.GroupBox groupBox32;
        private System.Windows.Forms.CheckBox checkBoxMonsterFlee;
        private System.Windows.Forms.CheckBox checkBoxMonsterMove;
        private System.Windows.Forms.CheckBox checkBoxMonsterJump;
        private System.Windows.Forms.ComboBox comboBoxTaskEventID;
        private System.Windows.Forms.ComboBox comboBoxTaskCompleteID;
        private System.Windows.Forms.CheckBox checkBoxMonsterIgnoreCry;
        private System.Windows.Forms.CheckBox checkBoxMonsterIgnoreDex;
        private System.Windows.Forms.GroupBox groupBox36;
        private System.Windows.Forms.GroupBox groupBox38;
        private System.Windows.Forms.Button buttonCharacterOptionDown;
        private System.Windows.Forms.ListBox listBoxCharacterDialogOptions;
        private System.Windows.Forms.Button buttonCharacterOptionUp;
        private System.Windows.Forms.Button buttonCharacterOptionAdd;
        private System.Windows.Forms.Button buttonCharacterOptionRemove;
        private System.Windows.Forms.GroupBox groupBox37;
        private System.Windows.Forms.Button buttonCharacterTextDown;
        private System.Windows.Forms.ListBox listBoxCharacterDialogText;
        private System.Windows.Forms.Button buttonCharacterTextUp;
        private System.Windows.Forms.Button buttonCharacterTextAdd;
        private System.Windows.Forms.Button buttonCharacterTextRemove;
        private System.Windows.Forms.TextBox textBoxCharacterDialogName;
        private System.Windows.Forms.Button buttonCharacterDialogDown;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.ListBox listBoxCharacterDialog;
        private System.Windows.Forms.Button buttonCharacterDialogUp;
        private System.Windows.Forms.Button buttonCharacterDialogAdd;
        private System.Windows.Forms.Button buttonCharacterDialogRemove;
        private System.Windows.Forms.GroupBox groupBox35;
        private System.Windows.Forms.GroupBox groupBox34;
        private System.Windows.Forms.GroupBox groupBox33;
        private System.Windows.Forms.CheckBox checkBoxDialogEndNode;
    }
}

