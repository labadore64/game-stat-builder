﻿
using StatBuilder.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;

namespace StatBuilder
{
    public partial class MainForm : Form
    {
        #region Tasks

        #region properties
        public static BindingList<Task> TaskData { get; set; } = new BindingList<Task>();
        public static BindingSource TaskBinding;
        public static List<BindingSource> TaskEventBinding = new List<BindingSource>();
        #endregion

        #region Bindings
        private void BindTasks()
        {
            TaskBinding = new BindingSource(TaskData, null);

            listBoxTasks.DataSource = TaskBinding;

            TaskEventBinding.Add(new BindingSource(EventData, null));
            TaskEventBinding.Add(new BindingSource(EventData, null));

            comboBoxTaskEventID.DataSource = TaskEventBinding[0];
            comboBoxTaskCompleteID.DataSource = TaskEventBinding[1];

            textBoxTaskName.DataBindings.Add(
            new Binding("Text", TaskBinding,
                "name", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxTaskID.DataBindings.Add(
            new Binding("Text", TaskBinding,
                "id", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxTaskDesc.DataBindings.Add(
            new Binding("Text", TaskBinding,
                "desc", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxTaskSprite.DataBindings.Add(
            new Binding("Text", TaskBinding,
                "sprite", false, DataSourceUpdateMode.OnPropertyChanged));
            comboBoxTaskEventID.DataBindings.Add(
            new Binding("SelectedItem", TaskBinding,
                "Event", false, DataSourceUpdateMode.OnPropertyChanged));
            comboBoxTaskCompleteID.DataBindings.Add(
            new Binding("SelectedItem", TaskBinding,
                "CompletedEvent", false, DataSourceUpdateMode.OnPropertyChanged));
        }
        #endregion

        #region Buttons
        private void buttonTasksAdd_Click(object sender, EventArgs e)
        {
            listboxAdd(listBoxTasks, TaskData, new Task());
        }

        private void buttonTasksRemove_Click(object sender, EventArgs e)
        {
            listboxRemove(listBoxTasks, TaskData);
        }

        private void buttonTasksMoveUp_Click(object sender, EventArgs e)
        {
            listboxMoveUp(listBoxTasks, TaskData);
        }

        private void buttonTasksMoveDown_Click(object sender, EventArgs e)
        {
            listboxMoveDown(listBoxTasks, TaskData);
        }

        #endregion

        #region Element
        private void textBoxTaskName_Leave(object sender, EventArgs e)
        {
            TaskBinding.ResetBindings(false);
        }
        #endregion

        #endregion
    }
}
