﻿
using StatBuilder.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace StatBuilder
{
    public partial class MainForm : Form
    {
        #region HUD

        #region Properties
        public static BindingList<HUD> HUDData { get; set; } = new BindingList<HUD>();
        public static BindingSource HUDBinding;
        public static BindingSource HUDForChildBinding;

        public static BindingSource HUDInfokeyBinding;
        public static BindingSource HUDDefBinding;
        public static BindingSource HUDChildBinding;
        #endregion

        #region Binding
        private void BindHUDData()
        {
            //HUD bindings
            HUDBinding = new BindingSource(HUDData, null);
            HUDForChildBinding = new BindingSource(HUDData, null);

            listBoxHUD.DataSource = HUDBinding;
            comboBoxHUDChildTemplate.DataSource = HUDForChildBinding;

            textBoxHUDID.DataBindings.Add(
            new Binding("Text", HUDBinding,
                "id", false, DataSourceUpdateMode.OnPropertyChanged));


            textBoxHUDDefaultName.DataBindings.Add(
            new Binding("Text", HUDBinding,
                "name", false, DataSourceUpdateMode.OnPropertyChanged));


            textBoxHUDText.DataBindings.Add(
            new Binding("Text", HUDBinding,
               "read_text", false, DataSourceUpdateMode.OnPropertyChanged));

            HUDUpdateBindings();


        }

        private void HUDUpdateBindings()
        {

            HUD selectedHUD = (HUD)listBoxHUD.SelectedItem;

            if (selectedHUD != null)
            {
                HUDInfokeyBinding = new BindingSource(selectedHUD.infokeys, null);

                listBoxHUDInfokeys.DataSource = HUDInfokeyBinding;

                RedoBinding(HUDInfokeyBinding, textBoxHUDInfokeysText, "Text", "text");
                RedoBinding(HUDInfokeyBinding, textBoxHUDInfokeysReq, "Text", "req");

                HUDInfokeyBinding.ResetBindings(false);



                HUDDefBinding = new BindingSource(selectedHUD.def, null);

                listBoxHUDDef.DataSource = HUDDefBinding;

                RedoBinding(HUDDefBinding, comboBoxHUDDefInput, "SelectedItem", "def");
                RedoBinding(HUDDefBinding, comboBoxHUDDefFunc, "SelectedItem", "func");
                RedoBinding(HUDDefBinding, textBoxHUDDefArg, "Text", "args");
                RedoBinding(HUDDefBinding, textBoxHUDDefReq, "Text", "req");

                HUDDefBinding.ResetBindings(false);


                HUDChildBinding = new BindingSource(selectedHUD.children, null);

                listBoxHUDChild.DataSource = HUDChildBinding;

                RedoBinding(HUDChildBinding, comboBoxHUDChildTemplate, "SelectedItem", "ChildTemplate");
                RedoBinding(HUDChildBinding, textBoxHUDChildName, "Text", "name");
                RedoBinding(HUDChildBinding, textBoxHUDChildVar, "Text", "varname");
                RedoBinding(HUDChildBinding, textBoxHUDChildCustomHUDText, "Text", "custom");

                HUDChildBinding.ResetBindings(false);
            }
        }

        #endregion


        #region Buttons
        private void buttonHUDAdd_Click(object sHUDer, EventArgs e)
        {
            listboxAdd(listBoxHUD, HUDData, new HUD());
            HUDUpdateBindings();
        }

        private void buttonHUDRemove_Click(object sHUDer, EventArgs e)
        {
            listboxRemove(listBoxHUD, HUDData);
            HUDUpdateBindings();
        }

        private void buttonHUDUp_Click(object sHUDer, EventArgs e)
        {
            listboxMoveUp(listBoxHUD, HUDData);
            HUDUpdateBindings();
        }

        private void buttonHUDDown_Click(object sHUDer, EventArgs e)
        {
            listboxMoveDown(listBoxHUD, HUDData);
            HUDUpdateBindings();
        }

        private void buttonHUDInfokeyAdd_Click(object sender, EventArgs e)
        {
            HUD selectedHUD = (HUD)listBoxHUD.SelectedItem;
            listboxAdd(listBoxHUDInfokeys, selectedHUD.infokeys, new HUD.HUDInfokey());
        }

        private void buttonHUDInfokeyRemove_Click(object sender, EventArgs e)
        {
            HUD selectedHUD = (HUD)listBoxHUD.SelectedItem;
            listboxRemove(listBoxHUDInfokeys, selectedHUD.infokeys);
        }

        private void buttonHUDInfokeyMoveUp_Click(object sender, EventArgs e)
        {
            HUD selectedHUD = (HUD)listBoxHUD.SelectedItem;
            listboxMoveUp(listBoxHUDInfokeys, selectedHUD.infokeys);
        }

        private void buttonHUDInfokeyMoveDown_Click(object sender, EventArgs e)
        {
            HUD selectedHUD = (HUD)listBoxHUD.SelectedItem;
            listboxMoveDown(listBoxHUDInfokeys, selectedHUD.infokeys);
        }

        private void buttonHUDDefAdd_Click(object sender, EventArgs e)
        {
            HUD selectedHUD = (HUD)listBoxHUD.SelectedItem;
            listboxAdd(listBoxHUDDef, selectedHUD.def, new HUD.HUDDef());
        }

        private void buttonHUDDefRemove_Click(object sender, EventArgs e)
        {
            HUD selectedHUD = (HUD)listBoxHUD.SelectedItem;
            listboxRemove(listBoxHUDDef, selectedHUD.def);
        }

        private void buttonHUDDefMoveUp_Click(object sender, EventArgs e)
        {
            HUD selectedHUD = (HUD)listBoxHUD.SelectedItem;
            listboxMoveUp(listBoxHUDDef, selectedHUD.def);
        }

        private void buttonHUDDefMoveDown_Click(object sender, EventArgs e)
        {
            HUD selectedHUD = (HUD)listBoxHUD.SelectedItem;
            listboxMoveDown(listBoxHUDDef, selectedHUD.def);
        }


        private void buttonHUDChildAdd_Click(object sender, EventArgs e)
        {
            HUD selectedHUD = (HUD)listBoxHUD.SelectedItem;
            listboxAdd(listBoxHUDChild, selectedHUD.children, new HUD.HUDChild());
        }

        private void buttonHUDChildRemove_Click(object sender, EventArgs e)
        {
            HUD selectedHUD = (HUD)listBoxHUD.SelectedItem;
            listboxRemove(listBoxHUDChild, selectedHUD.children);
        }

        private void buttonHUDChildMoveUp_Click(object sender, EventArgs e)
        {
            HUD selectedHUD = (HUD)listBoxHUD.SelectedItem;
            listboxMoveUp(listBoxHUDChild, selectedHUD.children);
        }

        private void buttonHUDChildMoveDown_Click(object sender, EventArgs e)
        {
            HUD selectedHUD = (HUD)listBoxHUD.SelectedItem;
            listboxMoveDown(listBoxHUDChild, selectedHUD.children);
        }
        #endregion

        #region Elements
        private void textBoxHUDName_Leave(object sHUDer, EventArgs e)
        {
            HUDBinding.ResetBindings(false);
            HUDForChildBinding.ResetBindings(false);
        }
        private void textBoxHUDInfokeysText_Leave(object sender, EventArgs e)
        {
            HUDInfokeyBinding.ResetBindings(false);
        }

        private void listBoxHUD_SelectedIndexChanged(object sender, EventArgs e)
        {
            HUDUpdateBindings();
        }

        private void textBoxHUDChildName_Leave(object sender, EventArgs e)
        {
            HUDChildBinding.ResetBindings(false);
        }

        private void comboBoxHUDDefInput_Leave(object sender, EventArgs e)
        {
            HUD.HUDDef selected = (HUD.HUDDef)listBoxHUDDef.SelectedItem;
            selected.def = (string)comboBoxHUDDefInput.SelectedItem;
            HUDDefBinding.ResetBindings(false);
        }

        private void comboBoxHUDDefFunc_Leave(object sender, EventArgs e)
        {
            HUD.HUDDef selected = (HUD.HUDDef)listBoxHUDDef.SelectedItem;
            selected.func = (string)comboBoxHUDDefFunc.SelectedItem;
            HUDDefBinding.ResetBindings(false);
        }
        #endregion

        #endregion
    }
}
