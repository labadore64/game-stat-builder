﻿
using StatBuilder.model;
using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace StatBuilder
{
    public partial class MainForm : Form
    {
        #region Status

        #region properties
        public static BindingList<Status> StatusData { get; set; } = new BindingList<Status>();
        public static BindingSource StatusBinding;
        #endregion

        #region Binding
        private void BindStatus()
        {
            //item bindings
            StatusBinding = new BindingSource(StatusData, null);

            listBoxStatus.DataSource = StatusBinding;

            textBoxStatusName.DataBindings.Add(
            new Binding("Text", StatusBinding,
                "name", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxStatusDesc.DataBindings.Add(
            new Binding("Text", StatusBinding,
                "desc", false, DataSourceUpdateMode.OnPropertyChanged));

            textBoxStatusSprite.DataBindings.Add(
            new Binding("Text", StatusBinding,
                "sprite", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxStatusShader.DataBindings.Add(
            new Binding("Text", StatusBinding,
                "shader_id", false, DataSourceUpdateMode.OnPropertyChanged));

            textBoxStatusMinTurns.DataBindings.Add(
            new Binding("Text", StatusBinding,
                "turns_min", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxStatusMaxTurns.DataBindings.Add(
            new Binding("Text", StatusBinding,
                "turns_max", false, DataSourceUpdateMode.OnPropertyChanged));

            checkBoxStatusHarmful.DataBindings.Add(
            new Binding("Checked", StatusBinding,
                "harmful", false, DataSourceUpdateMode.OnPropertyChanged));
        }
        #endregion

        #region Buttons
        private void buttonStatusAdd_Click(object sender, EventArgs e)
        {
            listboxAdd(listBoxStatus, StatusData, new Status());
        }

        private void buttonStatusRemove_Click(object sender, EventArgs e)
        {
            listboxRemove(listBoxStatus, StatusData);
        }

        private void buttonStatusMoveUp_Click(object sender, EventArgs e)
        {
            listboxMoveUp(listBoxStatus, StatusData);
        }

        private void buttonStatusMoveDown_Click(object sender, EventArgs e)
        {
            listboxMoveDown(listBoxStatus, StatusData);
        }
        #endregion

        #region Elements
        private void textBoxStatusName_Leave(object sender, EventArgs e)
        {
            StatusBinding.ResetBindings(false);
        }
        #endregion

        #endregion
    }
}
