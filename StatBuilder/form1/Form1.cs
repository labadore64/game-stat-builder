﻿using StatBuilder.formEvent;
using StatBuilder.formItemEvent;
using StatBuilder.formTextInput;
using StatBuilder.model;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace StatBuilder
{
    public partial class MainForm : Form
    {
        #region Properties

        public bool Loading { get; set; } = false;

        public static string[] Types = new string[]
        {
            "None",
            "Nature",
            "Mind",
            "Spirit",
            "???"
        };

        string IniFilter = "ini files (*.ini)|*.ini|All files (*.*)|*.*";

        #endregion

        #region init

        public MainForm()
        {
            InitializeComponent();
            InitLists();
        }

        private void InitLists()
        {
            if (GrowthData.Count == 0)
            {
                GrowthData.Add(new ExpGrowth());
            }
            if (EnemyData.Count == 0)
            {
                EnemyData.Add(new Enemy());
            }
            if (CharacterData.Count == 0)
            {
                CharacterData.Add(new Character());
            }
            if (ItemData.Count == 0)
            {
                ItemData.Add(new ItemData());
            }
            if (MoveData.Count == 0)
            {
                MoveData.Add(new Move());
            }
            if (StatusData.Count == 0)
            {
                StatusData.Add(new Status());
            }
            if (TabletData.Count == 0)
            {
                TabletData.Add(new Tablet());
            }
            if (BirdData.Count == 0)
            {
                BirdData.Add(new Bird());
            }
            if (BookData.Count == 0)
            {
                BookData.Add(new Book());
            }
            if (EndData.Count == 0)
            {
                EndData.Add(new EndPhase());
            }
            if(WordData.Count == 0)
            {
                WordData.Add(new Word());
            }
            if (HUDData.Count == 0)
            {
                HUDData.Add(new HUD());
            }
            if (DiagramData.Count == 0)
            {
                DiagramData.Add(new Diagram());
            }
            if(TextboxData.Count == 0)
            {
                TextboxData.Add(new Textbox());
            }

            if(EncounterData.Count == 0)
            {
                EncounterData.Add(new Encounter());
            }

            if(TaskData.Count == 0)
            {
                TaskData.Add(new Task());
            }
            if(SMSData.Count == 0)
            {
                SMSData.Add(new SMS());
            }
            if (VoicemailData.Count == 0)
            {
                VoicemailData.Add(new Voicemail());
            }

            if (EventData.Count == 0)
            {
                EventData.Add(new Event());
            }
            if (ItemEventData.Count == 0)
            {
                ItemEventData.Add(new ItemEvent());
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            UpdateBindings();
        }

        #endregion

        #region generic functions

        private string ShowTextForm(object sender)
        {
            if (sender is TextBox)
            {
                TextBox textbox = (TextBox)sender;

                GenericTextForm form = new GenericTextForm();
                form.Text = "Text Input";
                form.Multiline = textbox.Multiline;
                Tabs.Enabled = false;
                form.Width = Math.Max(textbox.Width + 40,170);
                form.Height = Math.Max(textbox.Height + 100,140);
                form.TextString = textbox.Text;
                
                form.ShowDialog();
                Tabs.Enabled = true;
                textbox.Text = form.TextString;
            }
            return "";
        }

        private void DoubleClickTextbox(object sender, EventArgs e)
        {
            ShowTextForm(sender);
        }

        public static string StringNewlineExport(string input)
        {
            return input.Replace(Environment.NewLine, "\\n");
        }

        public static string StringNewlineImport(string input)
        {
            return input.Replace("\\n", Environment.NewLine);
        }
        private double GetDistance(Point p1, Point p2)
        {
            return Math.Sqrt(Math.Pow((p2.X - p1.X), 2) + Math.Pow((p2.Y - p1.Y), 2));
        }

        private void listboxAdd<T>(ListBox listbox, BindingList<T> list, T newobj)
        {
            if (listbox.SelectedIndex >= 0)
            {
                if (listbox.SelectedIndex == list.Count - 1)
                {
                    list.Add(newobj);
                    if (listbox.SelectedIndex != listbox.Items.Count - 1)
                    {
                        listbox.SelectedIndex++;
                    }
                }
                else
                {
                    list.Insert(listbox.SelectedIndex + 1, newobj);
                    if (listbox.SelectedIndex != listbox.Items.Count - 1)
                    {
                        listbox.SelectedIndex++;
                    }
                }
            }
            else
            {
                list.Add(newobj);
            }
        }

        private void listboxRemove<T>(ListBox listbox, BindingList<T> list)
        {
            if (listbox.Items.Count > 1)
            {
                if (listbox.SelectedIndex >= 0)
                {
                    list.RemoveAt(listbox.SelectedIndex);
                    if (listbox.SelectedIndex > 0 && listbox.SelectedIndex != list.Count - 1)
                    {
                        listbox.SelectedIndex--;
                    }
                }
            }
        }

        private void listboxMoveUp<T>(ListBox listbox, BindingList<T> list)
        {
            if (listbox.SelectedIndex > 0)
            {
                T[] characters = new T[2];

                characters[0] = list[listbox.SelectedIndex];
                characters[1] = list[listbox.SelectedIndex - 1];

                list[listbox.SelectedIndex] = characters[1];
                list[listbox.SelectedIndex - 1] = characters[0];
                listbox.SelectedIndex--;
            }
        }

        private void listboxMoveDown<T>(ListBox listbox, BindingList<T> list)
        {
            if (listbox.SelectedIndex < list.Count - 1)
            {
                T[] characters = new T[2];

                characters[0] = list[listbox.SelectedIndex];
                characters[1] = list[listbox.SelectedIndex + 1];

                list[listbox.SelectedIndex] = characters[1];
                list[listbox.SelectedIndex + 1] = characters[0];
                listbox.SelectedIndex++;
            }
        }
        private void UpdateColor<T>(TextBox box, BindingList<T> list, Label[] labels)
        {
            if (list.Count > 0)
            {
                string[] col = new string[3];

                for(int i = 0; i < 3; i++)
                {

                    if(labels[i].Text == "")
                    {
                        col[i] = "0";
                        labels[i].Text = col[i];
                    } else
                    {
                        col[i] = labels[i].Text;
                    }
                }



                box.BackColor =
                    Color.FromArgb(
                        Convert.ToInt32(col[0]),
                        Convert.ToInt32(col[1]),
                        Convert.ToInt32(col[2]));
            }
            else
            {
                box.BackColor =
                    Color.FromArgb(127, 127, 127);
            }
        }

        #endregion

        #region bindings
        private void UpdateBindings()
        {
            BindGrowth();
            BindCharacter();

            BindEnemy();

            BindEncounter();

            BindItems();

            BindMoves();

            BindStatus();

            BindTablet();

            BindBird();

            BindTextbox();

            BindTasks();
            BindSMS();
            BindVoicemail();

            BindBooks();

            BindEndData();

            BindWordData();
            BindDiagram();

            BindHUDData();

            BindEvent();
            BindItemEvent();

            InitCharacterDialog();
            BindDialog();

            BindCharacterMoves();
            BindMonsterMoves();
        }

        private void InitCharacterDialog()
        {
            for (int i = 0; i < CharacterData.Count; i++) {
                CharacterData[i].InitDialog();
            }
        }

        private void RedoBinding(object Datasource, Control control, string Type, string propertyName)
        {
            control.DataBindings.Clear();
            control.DataBindings.Add(
            new Binding(Type, Datasource,
               propertyName, false, DataSourceUpdateMode.OnPropertyChanged));
        }

        #endregion

        #region tab control

        private void Tabs_SelectedIndexChanged(object sender, EventArgs e)
        {
            BirdData.ResetBindings();
            ItemData.ResetBindings();
            CharacterData.ResetBindings();
            EnemyData.ResetBindings();
            TabletData.ResetBindings();
            BookData.ResetBindings();
            GrowthData.ResetBindings();
            MoveData.ResetBindings();
            EndData.ResetBindings();
            WordData.ResetBindings();
        }

        #endregion

        private void listBoxEvent_DoubleClick(object sender, EventArgs e)
        {
            if (listBoxEvent.SelectedIndex > -1)
            {
                Event selected = (Event)listBoxEvent.Items[listBoxEvent.SelectedIndex];

                FormEvent form = new FormEvent();
                Tabs.Enabled = false;
                form.TextString = selected.name;
                form.eventObj = selected;
                form.ShowDialog();
                Tabs.Enabled = true;
                EventBinding.ResetBindings(false);
            }
        }

        private void listBoxItemEvent_DoubleClick(object sender, EventArgs e)
        {
            if (listBoxItemEvent.SelectedIndex > -1)
            {
                ItemEvent selected = (ItemEvent)listBoxItemEvent.Items[listBoxItemEvent.SelectedIndex];

                FormItemEvent form = new FormItemEvent();
                Tabs.Enabled = false;
                form.TextString = selected.name;
                form.Quantity = selected.quantity;
                form.eventObj = selected;
                form.ItemBinding = new BindingSource(ItemData, null);
                form.ItemEventBinding = new BindingSource(selected, null);
                form.SetBinding();
                form.BindItem();
                form.ShowDialog();
                Tabs.Enabled = true;
                ItemEventBinding.ResetBindings(false);
            }
        }

        private void listBoxCharacterDialogText_DoubleClick(object sender, EventArgs e)
        {
            if (listBoxCharacters.SelectedIndex > -1)
            {
                Character chara = CharacterData[listBoxCharacters.SelectedIndex];

                if (chara != null)
                {
                    if (listBoxCharacterDialog.SelectedIndex > -1)
                    {
                        Dialog dia = chara.Dialogs[listBoxCharacterDialog.SelectedIndex];

                        if (dia != null)
                        {
                            ButtonCharacterDialogTextOpenMenu(dia, false);
                        }
                    }
                }
            }
        }

        private void listBoxCharacterDialogOptions_DoubleClick(object sender, EventArgs e)
        {
            if (listBoxCharacters.SelectedIndex > -1)
            {
                Character chara = CharacterData[listBoxCharacters.SelectedIndex];

                if (chara != null)
                {
                    if (listBoxCharacterDialog.SelectedIndex > -1)
                    {
                        Dialog dia = chara.Dialogs[listBoxCharacterDialog.SelectedIndex];

                        if (dia != null)
                        {
                            ButtonCharacterDialogOptionOpenMenu(dia, false);
                        }
                    }
                }
            }
        }

    }
}
