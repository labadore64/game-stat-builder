﻿
using StatBuilder.model;
using System;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;

namespace StatBuilder
{
    public partial class MainForm : Form
    {
        #region Books

        #region properties
        public static BindingList<Book> BookData { get; set; } = new BindingList<Book>();
        public static BindingSource BookBinding;
        #endregion

        #region Bindings
        private void BindBooks()
        {
            BookBinding = new BindingSource(BookData, null);

            listBoxBooks.DataSource = BookBinding;


            textBoxBookName.DataBindings.Add(
            new Binding("Text", BookBinding,
                "name", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxBookAuthor.DataBindings.Add(
            new Binding("Text", BookBinding,
                "author", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxBookFilename.DataBindings.Add(
            new Binding("Text", BookBinding,
                "FileLocation", false, DataSourceUpdateMode.OnPropertyChanged));
        }
        #endregion

        #region Buttons
        private void buttonBookAdd_Click(object sender, EventArgs e)
        {
            listboxAdd(listBoxBooks, BookData, new Book());
        }

        private void buttonBookRemove_Click(object sender, EventArgs e)
        {
            listboxRemove(listBoxBooks, BookData);
        }

        private void buttonBookMoveUp_Click(object sender, EventArgs e)
        {
            listboxMoveUp(listBoxBooks, BookData);
        }

        private void buttonBookMoveDown_Click(object sender, EventArgs e)
        {
            listboxMoveDown(listBoxBooks, BookData);
        }

        private void buttonBookLoad_Click(object sender, EventArgs e)
        {
            try
            {
                if (openFileDialogBook.ShowDialog() == DialogResult.OK)
                {
                    Book selected = (Book)listBoxBooks.SelectedItem;

                    if (selected != null)
                    {
                        selected.FileLocation = openFileDialogBook.FileName;
                        richTextBoxBook.Text = selected.FileText;
                        BookBinding.ResetBindings(false);
                    }
                }
            }
            catch (Exception ea)
            {
                throw ea;
            }
        }

        private void buttonBookSave_Click(object sender, EventArgs e)
        {
            try
            {
                Book selected = (Book)listBoxBooks.SelectedItem;

                if (selected != null)
                {
                    if (saveFileDialogBook.ShowDialog() == DialogResult.OK)
                    {
                        File.WriteAllText(saveFileDialogBook.FileName, richTextBoxBook.Text);
                        selected.FileLocation = saveFileDialogBook.FileName;
                        BookBinding.ResetBindings(false);
                    }
                }
            }
            catch (Exception ea)
            {
                throw ea;
            }
        }

        #endregion

        #region Element
        private void textBoxBookName_Leave(object sender, EventArgs e)
        {
            BookBinding.ResetBindings(false);
        }
        #endregion

        #endregion
    }
}
