﻿
using StatBuilder.model;
using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace StatBuilder
{
    public partial class MainForm : Form
    {
        #region Monsters

        #region properties
        public static BindingList<Enemy> EnemyData { get; set; } = new BindingList<Enemy>();
        public static BindingSource EnemyBinding;
        #endregion

        #region Binding
        private void BindEnemy()
        {
            // set type
            comboBoxEnemyType.Items.AddRange(Types);

            //Enemy bindings
            EnemyBinding = new BindingSource(EnemyData, null);

            listBoxMonsters.DataSource = EnemyBinding;

            textBoxEnemyName.DataBindings.Add(
                    new Binding("Text", EnemyBinding,
                        "name", false, DataSourceUpdateMode.OnPropertyChanged));


            textBoxEnemyAbility.DataBindings.Add(
                    new Binding("Text", EnemyBinding,
                        "ability", false, DataSourceUpdateMode.OnPropertyChanged));

            textBoxEnemyFlavor0.DataBindings.Add(
                    new Binding("Text", EnemyBinding,
                        "flavor0", false, DataSourceUpdateMode.OnPropertyChanged));

            textBoxEnemyFlavor1.DataBindings.Add(
                    new Binding("Text", EnemyBinding,
                        "flavor1", false, DataSourceUpdateMode.OnPropertyChanged));

            textBoxEnemyFlavor2.DataBindings.Add(
                    new Binding("Text", EnemyBinding,
                        "flavor2", false, DataSourceUpdateMode.OnPropertyChanged));

            textBoxEnemyVisualDesc.DataBindings.Add(
                new Binding("Text", EnemyBinding,
                    "ax_description", false, DataSourceUpdateMode.OnPropertyChanged));

            textBoxEnemyDesc.DataBindings.Add(
            new Binding("Text", EnemyBinding,
                "desc", false, DataSourceUpdateMode.OnPropertyChanged));

            // stat binding

            trackBarEnemyHP.DataBindings.Add(
                new Binding("Value", EnemyBinding,
                    "hp", false, DataSourceUpdateMode.OnPropertyChanged));

            trackBarEnemyMP.DataBindings.Add(
                new Binding("Value", EnemyBinding,
                    "mp", false, DataSourceUpdateMode.OnPropertyChanged));

            trackBarEnemyPhyPower.DataBindings.Add(
                new Binding("Value", EnemyBinding,
                    "phy_power", false, DataSourceUpdateMode.OnPropertyChanged));

            trackBarEnemyPhyGuard.DataBindings.Add(
                new Binding("Value", EnemyBinding,
                    "phy_guard", false, DataSourceUpdateMode.OnPropertyChanged));

            trackBarEnemyMagPower.DataBindings.Add(
                new Binding("Value", EnemyBinding,
                    "mag_power", false, DataSourceUpdateMode.OnPropertyChanged));

            trackBarEnemyMagGuard.DataBindings.Add(
                new Binding("Value", EnemyBinding,
                    "mag_guard", false, DataSourceUpdateMode.OnPropertyChanged));

            trackBarEnemySpePower.DataBindings.Add(
                new Binding("Value", EnemyBinding,
                    "spe_power", false, DataSourceUpdateMode.OnPropertyChanged));

            trackBarEnemySpeGuard.DataBindings.Add(
                new Binding("Value", EnemyBinding,
                    "spe_guard", false, DataSourceUpdateMode.OnPropertyChanged));

            // color binding
            labelEnemyRed.DataBindings.Add(
                new Binding("Text", EnemyBinding,
                    "red", false, DataSourceUpdateMode.OnPropertyChanged));

            labelEnemyGreen.DataBindings.Add(
                new Binding("Text", EnemyBinding,
                    "green", false, DataSourceUpdateMode.OnPropertyChanged));

            labelEnemyBlue.DataBindings.Add(
                new Binding("Text", EnemyBinding,
                    "blue", false, DataSourceUpdateMode.OnPropertyChanged));

            // sprite binding
            textBoxEnemySpriteIdleFront.DataBindings.Add(
                new Binding("Text", EnemyBinding,
                    "BattleSprite.sprite_idle_front", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxEnemySpriteIdleBack.DataBindings.Add(
                new Binding("Text", EnemyBinding,
                    "BattleSprite.sprite_idle_back", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxEnemySpriteHitFront.DataBindings.Add(
                new Binding("Text", EnemyBinding,
                    "BattleSprite.sprite_hit_front", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxEnemySpriteHitBack.DataBindings.Add(
                new Binding("Text", EnemyBinding,
                    "BattleSprite.sprite_hit_back", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxEnemySpriteJump0Front.DataBindings.Add(
                new Binding("Text", EnemyBinding,
                    "BattleSprite.sprite_jump0_front", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxEnemySpriteJump0Back.DataBindings.Add(
                new Binding("Text", EnemyBinding,
                    "BattleSprite.sprite_jump0_back", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxEnemySpriteJump1Front.DataBindings.Add(
                new Binding("Text", EnemyBinding,
                    "BattleSprite.sprite_jump1_front", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxEnemySpriteJump1Back.DataBindings.Add(
                new Binding("Text", EnemyBinding,
                    "BattleSprite.sprite_jump1_back", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxEnemySpriteCast0Front.DataBindings.Add(
                new Binding("Text", EnemyBinding,
                    "BattleSprite.sprite_cast0_front", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxEnemySpriteCast0Back.DataBindings.Add(
                new Binding("Text", EnemyBinding,
                    "BattleSprite.sprite_cast0_back", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxEnemySpriteCast1Front.DataBindings.Add(
                new Binding("Text", EnemyBinding,
                    "BattleSprite.sprite_cast1_front", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxEnemySpriteCast1Back.DataBindings.Add(
                new Binding("Text", EnemyBinding,
                    "BattleSprite.sprite_cast1_back", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxEnemySpriteSpeed.DataBindings.Add(
                new Binding("Text", EnemyBinding,
                    "BattleSprite.sprite_default_speed", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxEnemySpriteScale.DataBindings.Add(
                new Binding("Text", EnemyBinding,
                    "sprite_scale", false, DataSourceUpdateMode.OnPropertyChanged));

            textBoxEnemySpriteZoomX.DataBindings.Add(
                new Binding("Text", EnemyBinding,
                    "zoom_x", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxEnemySpriteZoomY.DataBindings.Add(
                new Binding("Text", EnemyBinding,
                    "zoom_y", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxEnemySpriteShadowY.DataBindings.Add(
                new Binding("Text", EnemyBinding,
                    "shadow_y", false, DataSourceUpdateMode.OnPropertyChanged));

            // type
            comboBoxEnemyType.DataBindings.Add(
                new Binding("SelectedItem", EnemyBinding,
                    "type", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxEnemyExp.DataBindings.Add(
                new Binding("Text", EnemyBinding,
                    "exp", false, DataSourceUpdateMode.OnPropertyChanged));

            // bribe
            textBoxEnemyBribe.DataBindings.Add(
                new Binding("Text", EnemyBinding,
                    "bribe", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxEnemyMinMoney.DataBindings.Add(
                new Binding("Text", EnemyBinding,
                    "min_money", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxEnemyMaxMoney.DataBindings.Add(
                new Binding("Text", EnemyBinding,
                    "max_money", false, DataSourceUpdateMode.OnPropertyChanged));

            // Triggers
            checkBoxMonsterFlee.DataBindings.Add(
            new Binding("Checked", EnemyBinding,
                "cant_flee", false, DataSourceUpdateMode.OnPropertyChanged));
            checkBoxMonsterJump.DataBindings.Add(
            new Binding("Checked", EnemyBinding,
                "cant_jump", false, DataSourceUpdateMode.OnPropertyChanged));
            checkBoxMonsterMove.DataBindings.Add(
            new Binding("Checked", EnemyBinding,
                "cant_move", false, DataSourceUpdateMode.OnPropertyChanged));
            checkBoxMonsterIgnoreDex.DataBindings.Add(
            new Binding("Checked", EnemyBinding,
                "ignore_dex", false, DataSourceUpdateMode.OnPropertyChanged));
            checkBoxMonsterIgnoreCry.DataBindings.Add(
            new Binding("Checked", EnemyBinding,
                "ignore_cry", false, DataSourceUpdateMode.OnPropertyChanged));
        }
            #endregion

        #region Elements
        private void textBoxEnemyName_Leave(object sender, EventArgs e)
        {
            EnemyBinding.ResetBindings(false);
        }
        private void textBoxEnemyColor_Click(object sender, EventArgs e)
        {
            colorDialog1.ShowDialog();

            textBoxEnemyColor.BackColor = colorDialog1.Color;
            labelEnemyRed.Text = colorDialog1.Color.R.ToString();
            labelEnemyGreen.Text = colorDialog1.Color.G.ToString();
            labelEnemyBlue.Text = colorDialog1.Color.B.ToString();
        }
        #endregion

        #region Color
        private void enemyUpdateColor()
        {
            UpdateColor(
                textBoxEnemyColor,
                EnemyData,
                new Label[]{
                    labelEnemyRed,
                    labelEnemyGreen,
                    labelEnemyBlue
                    });
        }

        private void labelEnemyRed_TextChanged(object sender, EventArgs e)
        {
            enemyUpdateColor();
        }

        private void labelEnemyGreen_TextChanged(object sender, EventArgs e)
        {
            enemyUpdateColor();
        }

        private void labelEnemyBlue_TextChanged(object sender, EventArgs e)
        {
            enemyUpdateColor();
        }

        #endregion

        #region Stats
        private void EnemyUpdateStatDisplay()
        {
            Enemy enemy = (Enemy)listBoxMonsters.SelectedItem;
            if (enemy != null)
            {
                labelEnemyHP.Text = enemy.hp.ToString();
                labelEnemyMP.Text = enemy.mp.ToString();
                labelEnemyPhyPower.Text = enemy.phy_power.ToString();
                labelEnemyPhyGuard.Text = enemy.phy_guard.ToString();
                labelEnemyMagPower.Text = enemy.mag_power.ToString();
                labelEnemyMagGuard.Text = enemy.mag_guard.ToString();
                labelEnemySpePower.Text = enemy.spe_power.ToString();
                labelEnemySpeGuard.Text = enemy.spe_guard.ToString();
                panelEnemyStat.Refresh();
            }
        }

        private void trackBarEnemyHP_Scroll(object sender, EventArgs e)
        {
            Enemy enemy = (Enemy)listBoxMonsters.SelectedItem;
            if (enemy != null)
            {
                enemy.hp = trackBarEnemyHP.Value;
            }
            EnemyUpdateStatDisplay();
        }

        private void trackBarEnemyMP_Scroll(object sender, EventArgs e)
        {
            Enemy enemy = (Enemy)listBoxMonsters.SelectedItem;
            if (enemy != null)
            {
                enemy.mp = trackBarEnemyMP.Value;
            }
            EnemyUpdateStatDisplay();
        }

        private void trackBarEnemyPhyPower_Scroll(object sender, EventArgs e)
        {
            Enemy enemy = (Enemy)listBoxMonsters.SelectedItem;
            if (enemy != null)
            {
                enemy.phy_power = trackBarEnemyPhyPower.Value;
            }
            EnemyUpdateStatDisplay();
        }

        private void trackBarEnemyPhyGuard_Scroll(object sender, EventArgs e)
        {
            Enemy enemy = (Enemy)listBoxMonsters.SelectedItem;
            if (enemy != null)
            {
                enemy.phy_guard = trackBarEnemyPhyGuard.Value;
            }
            EnemyUpdateStatDisplay();
        }

        private void trackBarEnemyMagPower_Scroll(object sender, EventArgs e)
        {
            Enemy enemy = (Enemy)listBoxMonsters.SelectedItem;
            if (enemy != null)
            {
                enemy.mag_power = trackBarEnemyMagPower.Value;
            }
            EnemyUpdateStatDisplay();
        }

        private void trackBarEnemyMagGuard_Scroll(object sender, EventArgs e)
        {
            Enemy enemy = (Enemy)listBoxMonsters.SelectedItem;
            if (enemy != null)
            {
                enemy.mag_guard = trackBarEnemyMagGuard.Value;
            }
            EnemyUpdateStatDisplay();
        }

        private void trackBarEnemySpePower_Scroll(object sender, EventArgs e)
        {
            Enemy enemy = (Enemy)listBoxMonsters.SelectedItem;
            if (enemy != null)
            {
               enemy.spe_power = trackBarEnemySpePower.Value;
            }
            EnemyUpdateStatDisplay();
        }

        private void trackBarEnemySpeGuard_Scroll(object sender, EventArgs e)
        {
            Enemy enemy = (Enemy)listBoxMonsters.SelectedItem;
            if (enemy != null)
            {
                enemy.spe_guard = trackBarEnemySpeGuard.Value;
            }
            EnemyUpdateStatDisplay();
        }
        #endregion

        #region Buttons
        private void buttonMonsterAdd_Click(object sender, EventArgs e)
        {
            listboxAdd(listBoxMonsters, EnemyData, new Enemy());
            EnemyUpdateStatDisplay();
        }

        private void buttonMonsterRemove_Click(object sender, EventArgs e)
        {
            listboxRemove(listBoxMonsters, EnemyData);
            EnemyUpdateStatDisplay();
        }

        private void buttonMonsterMoveUp_Click(object sender, EventArgs e)
        {
            listboxMoveUp(listBoxMonsters, EnemyData);
        }

        private void buttonMonsterMoveDown_Click(object sender, EventArgs e)
        {
            listboxMoveDown(listBoxMonsters, EnemyData);
        }

        private void listBoxMonsters_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnemyUpdateStatDisplay();
        }

        #endregion

        #region Draw
        private void panelEnemyStat_Paint(object sender, PaintEventArgs e)
        {
            if (listBoxMonsters.SelectedItem != null)
            {
                Enemy chara = (Enemy)listBoxMonsters.SelectedItem;
                double[] stats = new double[] {
                        (double)chara.phy_power/(double)trackBarCharacterPhyPower.Maximum,
                        (double)chara.phy_guard/(double)trackBarCharacterPhyGuard.Maximum,
                        (double)chara.mag_guard/(double)trackBarCharacterMagGuard.Maximum,
                        (double)chara.mag_power/(double)trackBarCharacterMagPower.Maximum,
                        (double)chara.spe_guard/(double)trackBarCharacterSpeGuard.Maximum,
                        (double)chara.spe_power/(double)trackBarCharacterSpePower.Maximum
                    };

                PanelDrawStat(sender, e, stats);
                ;
            }
        }
        #endregion

        #endregion
    }
}
