﻿
using StatBuilder.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace StatBuilder
{
    public partial class MainForm : Form
    {
        #region End

        #region Properties
        public static BindingList<EndPhase> EndData { get; set; } = new BindingList<EndPhase>();
        public static BindingSource EndBinding;
        #endregion

        #region Binding
        private void BindEndData()
        {
            //End bindings
            EndBinding = new BindingSource(EndData, null);

            listBoxEndPhase.DataSource = EndBinding;

            textBoxEndPhaseName.DataBindings.Add(
            new Binding("Text", EndBinding,
                "name", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxEndPhaseScript.DataBindings.Add(
            new Binding("Text", EndBinding,
                "script", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxEndPhaseTargetSprite.DataBindings.Add(
            new Binding("Text", EndBinding,
                "sprite_animation", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxEndPhaseFgSprite.DataBindings.Add(
            new Binding("Text", EndBinding,
                "fg_animation", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxEndPhaseBgSprite.DataBindings.Add(
            new Binding("Text", EndBinding,
                "bg_animation", false, DataSourceUpdateMode.OnPropertyChanged));
            
            textBoxEndPhaseLength.DataBindings.Add(
            new Binding("Text", EndBinding,
                "animation_length", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxEndPhaseConditionScript.DataBindings.Add(
            new Binding("Text", EndBinding,
                "condition_script", false, DataSourceUpdateMode.OnPropertyChanged));

            checkBoxEndPhaseTarget.DataBindings.Add(
            new Binding("Checked", EndBinding,
                "target", false, DataSourceUpdateMode.OnPropertyChanged));
            checkBoxEndPhasePause.DataBindings.Add(
            new Binding("Checked", EndBinding,
                "pause", false, DataSourceUpdateMode.OnPropertyChanged));
        }

        #endregion

        
        #region Buttons
        private void buttonEndPhaseAdd_Click(object sender, EventArgs e)
        {
            listboxAdd(listBoxEndPhase, EndData, new EndPhase());
        }

        private void buttonEndPhaseRemove_Click(object sender, EventArgs e)
        {
            listboxRemove(listBoxEndPhase, EndData);
        }

        private void buttonEndPhaseMoveUp_Click(object sender, EventArgs e)
        {
            listboxMoveUp(listBoxEndPhase, EndData);
        }

        private void buttonEndPhaseMoveDown_Click(object sender, EventArgs e)
        {
            listboxMoveDown(listBoxEndPhase, EndData);
        }
        #endregion

        #region Elements
        private void buttonEndPhaseName_Leave(object sender, EventArgs e)
        {
            EndBinding.ResetBindings(false);
        }
        #endregion
        

        #endregion
    }
}
