﻿
using StatBuilder.model;
using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace StatBuilder
{
    public partial class MainForm : Form
    {
        #region Voicemails

        #region properties
        public static BindingList<Voicemail> VoicemailData { get; set; } = new BindingList<Voicemail>();
        public static BindingSource VoicemailBinding;
        #endregion

        #region Bindings
        private void BindVoicemail()
        {
            VoicemailBinding = new BindingSource(VoicemailData, null);

            listBoxVoicemail.DataSource = VoicemailBinding;


            textBoxVoicemailName.DataBindings.Add(
            new Binding("Text", VoicemailBinding,
                "name", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxVoicemailDesc.DataBindings.Add(
            new Binding("Text", VoicemailBinding,
                "desc", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxVoicemailDialogID.DataBindings.Add(
            new Binding("Text", VoicemailBinding,
                "title", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxVoicemailSender.DataBindings.Add(
            new Binding("Text", VoicemailBinding,
                "sender", false, DataSourceUpdateMode.OnPropertyChanged));
        }
        #endregion

        #region Buttons
        private void buttonVoicemailAdd_Click(object sender, EventArgs e)
        {
            listboxAdd(listBoxVoicemail, VoicemailData, new Voicemail());
        }

        private void buttonVoicemailRemove_Click(object sender, EventArgs e)
        {
            listboxRemove(listBoxVoicemail, VoicemailData);
        }

        private void buttonVoicemailMoveUp_Click(object sender, EventArgs e)
        {
            listboxMoveUp(listBoxVoicemail, VoicemailData);
        }

        private void buttonVoicemailMoveDown_Click(object sender, EventArgs e)
        {
            listboxMoveDown(listBoxVoicemail, VoicemailData);
        }

        #endregion

        #region Element
        private void textBoxVoicemailName_Leave(object sender, EventArgs e)
        {
            VoicemailBinding.ResetBindings(false);
        }
        #endregion

        #endregion
    }
}
