﻿
using StatBuilder.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace StatBuilder
{
    public partial class MainForm : Form
    {
        #region Word

        #region Properties
        public static BindingList<Word> WordData { get; set; } = new BindingList<Word>();
        public static BindingSource WordBinding;

        public static BindingSource WordMoveBinding;
        #endregion

        #region Binding
        private void BindWordData()
        {
            //Word bindings
            WordBinding = new BindingSource(WordData, null);
           
            listBoxWords.DataSource = WordBinding;

            WordMoveBinding = new BindingSource(MoveData, null);

            comboBoxWordMove.DataSource = WordMoveBinding;

            textBoxWordName.DataBindings.Add(
            new Binding("Text", WordBinding,
                "name", false, DataSourceUpdateMode.OnPropertyChanged));


            textBoxWordDesc.DataBindings.Add(
            new Binding("Text", WordBinding,
                "desc", false, DataSourceUpdateMode.OnPropertyChanged));


            comboBoxWordMove.DataBindings.Add(
            new Binding("SelectedItem", WordBinding,
               "Move", false, DataSourceUpdateMode.OnPropertyChanged));
            
        }

        #endregion

        
        #region Buttons
        private void buttonWordAdd_Click(object sWorder, EventArgs e)
        {
            listboxAdd(listBoxWords, WordData, new Word());
        }

        private void buttonWordRemove_Click(object sWorder, EventArgs e)
        {
            listboxRemove(listBoxWords, WordData);
        }

        private void buttonWordUp_Click(object sWorder, EventArgs e)
        {
            listboxMoveUp(listBoxWords, WordData);
        }

        private void buttonWordDown_Click(object sWorder, EventArgs e)
        {
            listboxMoveDown(listBoxWords, WordData);
        }
        #endregion

        #region Elements
        private void textBoxWordName_Leave(object sWorder, EventArgs e)
        {
            WordBinding.ResetBindings(false);
        }
        #endregion

        #endregion
    }
}
