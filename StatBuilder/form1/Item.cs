﻿
using StatBuilder.model;
using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace StatBuilder
{
    public partial class MainForm : Form
    {
        #region Items

        #region properties
        public static BindingList<ItemData> ItemData { get; set; } = new BindingList<ItemData>();
        public static BindingSource ItemBinding;
        public static BindingSource MoveItemBinding;
        #endregion

        #region Stats
        private void ItemUpdateStatDisplay()
        {
            ItemData item = (ItemData)listBoxItems.SelectedItem;
            if (item != null)
            {
                labelItemPhyPower.Text = item.phy_power.ToString();
                labelItemPhyGuard.Text = item.phy_guard.ToString();
                labelItemMagPower.Text = item.mag_power.ToString();
                labelItemMagGuard.Text = item.mag_guard.ToString();
                labelItemSpePower.Text = item.spe_power.ToString();
                labelItemSpeGuard.Text = item.spe_guard.ToString();
            }
        }


        private void trackBarItemPhyPower_Scroll(object sender, EventArgs e)
        {
            ItemUpdateStatDisplay();
        }

        private void trackBarItemPhyGuard_Scroll(object sender, EventArgs e)
        {
            ItemUpdateStatDisplay();
        }

        private void trackBarItemMagPower_Scroll(object sender, EventArgs e)
        {
            ItemUpdateStatDisplay();
        }

        private void trackBarItemMagGuard_Scroll(object sender, EventArgs e)
        {
            ItemUpdateStatDisplay();
        }

        private void trackBarItemSpePower_Scroll(object sender, EventArgs e)
        {
            ItemUpdateStatDisplay();
        }

        private void trackBarItemSpeGuard_Scroll(object sender, EventArgs e)
        {
            ItemUpdateStatDisplay();
        }

        #endregion

        #region Element
        private void textBoxItemName_Leave(object sender, EventArgs e)
        {
            ItemBinding.ResetBindings(false);
            MoveResetBindings();
        }

        private void listBoxItems_SelectedIndexChanged(object sender, EventArgs e)
        {
            ItemUpdateStatDisplay();
        }
        #endregion

        #region Bindings
        private void BindItems()
        {
            // First populate any dropdowns
            ItemsPopulateCollections();

            //item bindings
            ItemBinding = new BindingSource(ItemData, null);

            listBoxItems.DataSource = ItemBinding;

            MoveItemBinding = new BindingSource(MoveData, null);

            comboBoxBattleMove.DataSource = MoveItemBinding;


            textBoxItemName.DataBindings.Add(
            new Binding("Text", ItemBinding,
                "name", false, DataSourceUpdateMode.OnPropertyChanged));

            textBoxItemDesc.DataBindings.Add(
            new Binding("Text", ItemBinding,
                "desc", false, DataSourceUpdateMode.OnPropertyChanged));

            textBoxItemHelp.DataBindings.Add(
            new Binding("Text", ItemBinding,
                "help", false, DataSourceUpdateMode.OnPropertyChanged));

            //comboBoxItemType
            comboBoxItemType.DataBindings.Add(
            new Binding("SelectedItem", ItemBinding,
                "type", false, DataSourceUpdateMode.OnPropertyChanged));

            comboBoxBattleMove.DataBindings.Add(
            new Binding("SelectedItem", ItemBinding,
                "Move", false, DataSourceUpdateMode.OnPropertyChanged));

            textBoxItemOverworldScript.DataBindings.Add(
            new Binding("Text", ItemBinding,
                "overworld_effect", false, DataSourceUpdateMode.OnPropertyChanged));

            textBoxItemBookName.DataBindings.Add(
            new Binding("Text", ItemBinding,
                "book_name", false, DataSourceUpdateMode.OnPropertyChanged));

            textBoxItemPrice.DataBindings.Add(
            new Binding("Text", ItemBinding,
                "price", false, DataSourceUpdateMode.OnPropertyChanged));
            
            checkBoxItemCanGive.DataBindings.Add(
            new Binding("Checked", ItemBinding,
                "can_give", false, DataSourceUpdateMode.OnPropertyChanged));

            checkBoxItemCanToss.DataBindings.Add(
            new Binding("Checked", ItemBinding,
                "can_toss", false, DataSourceUpdateMode.OnPropertyChanged));
            checkBoxItemCanSell.DataBindings.Add(
            new Binding("Checked", ItemBinding,
                "can_sell", false, DataSourceUpdateMode.OnPropertyChanged));
            checkBoxItemSingleUse.DataBindings.Add(
            new Binding("Checked", ItemBinding,
                "single_use", false, DataSourceUpdateMode.OnPropertyChanged));
            
            textBoxItemSprite.DataBindings.Add(
            new Binding("Text", ItemBinding,
                "sprite", false, DataSourceUpdateMode.OnPropertyChanged));

            trackBarItemPhyPower.DataBindings.Add(
            new Binding("Value", ItemBinding,
                "phy_power", false, DataSourceUpdateMode.OnPropertyChanged));
            trackBarItemPhyGuard.DataBindings.Add(
            new Binding("Value", ItemBinding,
                "phy_guard", false, DataSourceUpdateMode.OnPropertyChanged));
            trackBarItemMagPower.DataBindings.Add(
            new Binding("Value", ItemBinding,
                "mag_power", false, DataSourceUpdateMode.OnPropertyChanged));
            trackBarItemMagGuard.DataBindings.Add(
            new Binding("Value", ItemBinding,
                "mag_guard", false, DataSourceUpdateMode.OnPropertyChanged));
            trackBarItemSpePower.DataBindings.Add(
            new Binding("Value", ItemBinding,
                "spe_power", false, DataSourceUpdateMode.OnPropertyChanged));
            trackBarItemSpeGuard.DataBindings.Add(
            new Binding("Value", ItemBinding,
                "spe_guard", false, DataSourceUpdateMode.OnPropertyChanged));

        }

        private void ItemsPopulateCollections()
        {
            comboBoxItemType.Items.Clear();
            comboBoxItemType.Items.AddRange(model.ItemData.ItemTypes);
            comboBoxItemType.SelectedIndex = 0;
        }
        #endregion

        #region Buttons
        private void buttonItemAdd_Click(object sender, EventArgs e)
        {
            listboxAdd(listBoxItems, ItemData, new ItemData());
            MoveResetBindings();
        }

        private void buttonItemRemove_Click(object sender, EventArgs e)
        {
            listboxRemove(listBoxItems, ItemData);
            MoveResetBindings();
        }

        private void buttonItemMoveUp_Click(object sender, EventArgs e)
        {
            listboxMoveUp(listBoxItems, ItemData);
        }

        private void buttonItemMoveDown_Click(object sender, EventArgs e)
        {
            listboxMoveDown(listBoxItems, ItemData);
        }
        #endregion

        #endregion
    }
}
