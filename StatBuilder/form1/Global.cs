﻿
using IniParser;
using IniParser.Model;
using StatBuilder.model;
using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace StatBuilder
{
    public partial class MainForm : Form
    {
        #region Global Form

        #region Directory Import/Export

        private void buttonExportAll_Click(object sender, EventArgs e)
        {

            if (folderSelector.ShowDialog() == DialogResult.OK)
            {
                ExportList(BirdData, folderSelector.SelectedPath);
                ExportList(BookData, folderSelector.SelectedPath);
                ExportList(CharacterData, folderSelector.SelectedPath);
                ExportList(EnemyData, folderSelector.SelectedPath);
                ExportList(GrowthData, folderSelector.SelectedPath);
                ExportList(ItemData, folderSelector.SelectedPath);
                ExportList(MoveData, folderSelector.SelectedPath);
                ExportList(StatusData, folderSelector.SelectedPath);
                ExportList(TabletData, folderSelector.SelectedPath);
                ExportList(TextboxData, folderSelector.SelectedPath);

                ExportList(TaskData, folderSelector.SelectedPath);
                ExportList(SMSData, folderSelector.SelectedPath);
                ExportList(VoicemailData, folderSelector.SelectedPath);

                ExportList(EncounterData, folderSelector.SelectedPath);

                ExportList(EndData, folderSelector.SelectedPath);
                ExportList(WordData, folderSelector.SelectedPath);
                ExportList(DiagramData, folderSelector.SelectedPath);

                ExportList(HUDData, folderSelector.SelectedPath);

                ExportEvents(folderSelector.SelectedPath);
                ExportItemEvents(folderSelector.SelectedPath);

                ExportDialog(folderSelector.SelectedPath);

                // after importing everything set up the moves
                ExportMoves(CharacterData, folderSelector.SelectedPath);
                ExportMoves(EnemyData, folderSelector.SelectedPath);
            }

        }

        private void ExportList<T>(BindingList<T> list, string dir)
             where T : Model
        {
            if (list.Count > 0)
            {
                Model first = list[0];

                string DirPath = dir + "\\" + first.SaveDirectory;
                string fileStart = first.SaveName;

                if (!Directory.Exists(DirPath))
                {
                    Directory.CreateDirectory(DirPath);
                }

                for (int i = 0; i < list.Count; i++)
                {
                    if (list[i].numbering)
                    {
                        list[i].ExportToINI(DirPath + "\\" + fileStart + i + ".ini");
                    } else
                    {
                        list[i].ExportToINI(DirPath + "\\" + list[i].SaveName + ".ini");
                    }
                }
            }
        }

        private void ExportMoves<T>(BindingList<T> list, string dir)
         where T : MoveHaver
        {
            for (int i = 0; i < list.Count; i++)
            {
                list[i].ExportMovesToINI(dir, i);
            }
        }

        private void ImportList<T>(BindingList<T> list, string dir)
        where T : Model
        {
            string dirpath = list[0].SaveDirectory;
            string filepart = list[0].SaveName;
            bool numbering = list[0].numbering;
            list.Clear();
            dir += "\\" + dirpath;
            if (Directory.Exists(dir))
            {
                string[] fileEntries;
                if (numbering)
                {
                    fileEntries = Directory.GetFiles(dir)
                        .Where(file => new string[] { ".ini" }
                        .Contains(Path.GetExtension(file)))
                        .OrderBy(q => Convert.ToInt32(Regex.Match(Path.GetFileNameWithoutExtension(q), @"\d+$", RegexOptions.RightToLeft).Value)).ToArray();
                } else
                {
                    fileEntries = Directory.GetFiles(dir)
                        .Where(file => new string[] { ".ini" }
                        .Contains(Path.GetExtension(file))).ToArray();
                }

                foreach (string fileName in fileEntries)
                {
                    if (numbering)
                    {
                        if (Path.GetFileName(fileName).Contains(".ini") &&
                            Path.GetFileName(fileName).Contains(filepart))
                        {
                            ListImportItem(list, fileName);
                        }
                    }
                    else
                    {
                        if (Path.GetFileName(fileName).Contains(".ini"))
                        {
                            ListImportItem(list, fileName);
                        }
                    }

                }
            }

            if (list.Count == 0)
            {
                T item = (T)Activator.CreateInstance(typeof(T));
                list.Add(item);
            }
        }

        private void ImportMoves<T>(BindingList<T> list, string dir)
         where T : MoveHaver
        {
            for (int i = 0; i < list.Count; i++)
            {
                list[i].ImportMovesFromINI(dir, i);
            }
        }

        private void ImportDialog(string dir)
        {
            string dirpath;
            for (int i = 1; i < CharacterData.Count; i++)
            {
                dirpath = dir + "\\dialog\\character" + i + "\\";
                if (Directory.Exists(dirpath))
                {
                    string[] fileEntries;

                    fileEntries = Directory.GetFiles(dirpath)
                        .Where(file => new string[] { ".ini" }
                        .Contains(Path.GetExtension(file))).ToArray();

                    foreach (string fileName in fileEntries)
                    {
                        if (Path.GetFileName(fileName).Contains(".ini"))
                        {
                            CharacterData[i].Dialogs.Add(new Dialog(fileName));
                        }
                    }
                }
            }
        }

        private void ExportDialog(string dir)
        {
            string dirpath;
            for (int i = 1; i < CharacterData.Count; i++)
            {
                dirpath = dir + "\\dialog\\character" + i + "\\";
                if (!Directory.Exists(dirpath))
                {
                    Directory.CreateDirectory(dirpath);
                }

                for(int j = 0; j < CharacterData[i].Dialogs.Count; j++)
                {
                    CharacterData[i].Dialogs[j].ExportToINI(dirpath + CharacterData[i].Dialogs[j].name + ".ini");
                }
                
            }
        }


        private void buttonImportAll_Click(object sender, EventArgs e)
        {
            if (folderSelector.ShowDialog() == DialogResult.OK)
            {
                Loading = true;
                ImportList(GrowthData, folderSelector.SelectedPath);
                ImportList(BirdData, folderSelector.SelectedPath);
                ImportList(BookData, folderSelector.SelectedPath);
                ImportList(CharacterData, folderSelector.SelectedPath);
                ImportList(EnemyData, folderSelector.SelectedPath);
                ImportList(ItemData, folderSelector.SelectedPath);
                ImportList(MoveData, folderSelector.SelectedPath);
                ImportList(StatusData, folderSelector.SelectedPath);
                ImportList(TabletData, folderSelector.SelectedPath);
                ImportList(EncounterData, folderSelector.SelectedPath);

                ImportList(TextboxData, folderSelector.SelectedPath);

                ImportEvents(folderSelector.SelectedPath);
                ImportItemEvents(folderSelector.SelectedPath);

                ImportList(TaskData, folderSelector.SelectedPath);
                ImportList(SMSData, folderSelector.SelectedPath);
                ImportList(VoicemailData, folderSelector.SelectedPath);

                UpdateTextboxBinding();
                //UpdateTextboxGraphicsBinding();

                ImportList(EndData, folderSelector.SelectedPath);
                ImportList(WordData, folderSelector.SelectedPath);
                ImportList(DiagramData, folderSelector.SelectedPath);
                ImportList(HUDData, folderSelector.SelectedPath);

                //ImportList(ItemEventData, folderSelector.SelectedPath);
                // update HUD data after loading
                foreach (HUD h in HUDData)
                {
                    h.UpdateChildren();
                }

                ImportDialog(folderSelector.SelectedPath);
                InitCharacterDialog();
                BindDialog();
                for (int i = 0; i < CharacterData.Count; i++)
                {
                    CharacterData[i].CorrectDialogNulls();
                }
                CharacterUpdateDialog();

                HUDUpdateBindings();
                UpdateMoveBinding();

                // after importing everything set up the moves
                ImportMoves(CharacterData, folderSelector.SelectedPath);
                ImportMoves(EnemyData, folderSelector.SelectedPath);
                Loading = false;
            }

        }

        private void ExportEvents(string dir)
        {
            try
            {
                var parser = new FileIniDataParser();
                IniData data = new IniData();

                Event dummy = new Event();
                string DirPath = dir + "\\" + dummy.SaveDirectory + "\\" + dummy.SaveName + ".ini";

                //Add a new section and some keys
                data.Sections.AddSection(dummy.SectionName);

                for (var i = 0; i < EventData.Count; i++)
                {
                    data[dummy.SectionName].AddKey("name" + i, EventData[i].name);
                }

                parser.WriteFile(DirPath, data);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private void ImportItemEvents(string dir)
        {

            var parser = new FileIniDataParser();

            ItemEvent dummy = new ItemEvent();
            string DirPath = dir + "\\" + dummy.SaveDirectory + "\\" + dummy.SaveName + ".ini";

            if (File.Exists(DirPath))
            {
                IniData data = parser.ReadFile(DirPath);
                string name;
                int quantity;
                int item_id;

                int counter = 0;

                ItemEventData.Clear();

                while (data[dummy.SectionName]["name" + counter] != null)
                {
                    name = "";

                    if (data[dummy.SectionName]["name" + counter] != "")
                    {
                        name = data[dummy.SectionName]["name" + counter];
                    }
                    quantity = Convert.ToInt32(data[dummy.SectionName]["quantity" + counter]);
                    item_id = Convert.ToInt32(data[dummy.SectionName]["item_id" + counter]);

                    ItemEventData.Add(new ItemEvent(name,item_id,quantity));

                    counter++;
                }
            }
        }

        private void ExportItemEvents(string dir)
        {
            try
            {
                var parser = new FileIniDataParser();
                IniData data = new IniData();

                ItemEvent dummy = new ItemEvent();
                string DirPath = dir + "\\" + dummy.SaveDirectory + "\\" + dummy.SaveName + ".ini";

                //Add a new section and some keys
                data.Sections.AddSection(dummy.SectionName);

                for (var i = 0; i < ItemEventData.Count; i++)
                {
                    data[dummy.SectionName].AddKey("name" + i, ItemEventData[i].name);
                    data[dummy.SectionName].AddKey("quantity" + i, ItemEventData[i].quantity.ToString());
                    data[dummy.SectionName].AddKey("item_id" + i, ItemEventData[i].item_id.ToString());
                }

                parser.WriteFile(DirPath, data);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private void ImportEvents(string dir)
        {

            var parser = new FileIniDataParser();

            Event dummy = new Event();
            string DirPath = dir + "\\" + dummy.SaveDirectory + "\\" + dummy.SaveName + ".ini";

            if (File.Exists(DirPath))
            {
                IniData data = parser.ReadFile(DirPath);
                string name = "";

                int counter = 0;

                EventData.Clear();

                while (data[dummy.SectionName]["name" + counter] != null)
                {
                    name = "";


                    if (data[dummy.SectionName]["name" + counter] != "")
                    {
                        name = data[dummy.SectionName]["name" + counter];
                    }
                    EventData.Add(new Event(name));

                    counter++;
                }
            }
        }
        #endregion

        #region Generic Methods

        private void ListImportItem<T>(BindingList<T> list, string Filename)
         where T : Model
        {
            T item = (T)Activator.CreateInstance(typeof(T));
            item.ImportFromINI(Filename);
            list.Add(item);
        }

        #endregion

        #region draw
        private void PanelDrawStat(object sender, PaintEventArgs e, double[] stats)
        {
            var p = sender as Panel;
            var g = e.Graphics;

            g.FillRectangle(new SolidBrush(Color.FromArgb(0, Color.Black)), p.DisplayRectangle);

            Point[] points = new Point[6];
            double spacer = Math.PI / 3;

            double dist = p.Width * .49;
            double poss = p.Width * .5;

            for (int i = 0; i < 6; i++)
            {
                points[i] = new Point(
                                Convert.ToInt32(poss + dist * stats[i] * Math.Cos(spacer * i)),
                                Convert.ToInt32(poss + dist * stats[i] * Math.Sin(spacer * i)));

            }

            Pen brush = new Pen(Color.White, 3);

            Color[] colors = new Color[]
            {
                Color.Red,
                Color.Cyan,
                Color.Lime,
                Color.Magenta,
                Color.Blue,
                Color.Yellow
            };

            g.DrawPolygon(brush, points);

            int radius = 5;

            for (int i = 0; i < 6; i++)
            {
                g.FillEllipse(new SolidBrush(colors[i]),
                    points[i].X - radius,
                    points[i].Y - radius,
                    radius + radius,
                    radius + radius);
            }
        }
        #endregion

        #endregion
    }
}
