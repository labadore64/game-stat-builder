﻿
using StatBuilder.model;
using System;
using System.ComponentModel;
using System.Windows.Forms;
using static StatBuilder.model.Textbox;

namespace StatBuilder
{
    public partial class MainForm : Form
    {
        #region Textboxs

        #region Properties
        public static BindingList<Textbox> TextboxData { get; set; } = new BindingList<Textbox>();
        public static BindingSource TextboxBinding;
        #endregion

        #region Bindings
        private void BindTextbox()
        {
            TextboxBinding = new BindingSource(TextboxData, null);

            listBoxTextbox.DataSource = TextboxBinding;

            textBoxTextboxName.DataBindings.Add(
            new Binding("Text", TextboxBinding,
                "name", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxTextboxEventID.DataBindings.Add(
            new Binding("Text", TextboxBinding,
                "event_id", false, DataSourceUpdateMode.OnPropertyChanged));
            UpdateTextboxBinding();
        }

        private void UpdateTextboxBinding()
        {
            Textbox current = (Textbox)listBoxTextbox.SelectedItem;
            if (current != null)
            {
                listBoxTextEntry.DataSource = current.TextSource;
                RedoBinding(current.TextSource, textBoxTextEntryText, "Text", "text");
            }
        }

        private void UpdateTextboxGraphicsBinding()
        {
            TextEntry current = (TextEntry)listBoxTextEntry.SelectedItem;
            if (current != null)
            {
                listBoxTextGraphics.DataSource = current.GraphicsSource;

                RedoBinding(current.GraphicsSource, textBoxTextGraphicsX, "Text", "x");
                RedoBinding(current.GraphicsSource, textBoxTextGraphicsY, "Text", "y");
                RedoBinding(current.GraphicsSource, textBoxTextGraphicsR, "Text", "r");
                RedoBinding(current.GraphicsSource, textBoxTextGraphicsG, "Text", "g");
                RedoBinding(current.GraphicsSource, textBoxTextGraphicsB, "Text", "b");
                RedoBinding(current.GraphicsSource, textBoxTextGraphicsSprite, "Text", "sprite");
                RedoBinding(current.GraphicsSource, textBoxTextGraphicsXScale, "Text", "xscale");
                RedoBinding(current.GraphicsSource, textBoxTextGraphicsYScale, "Text", "yscale");
                RedoBinding(current.GraphicsSource, textBoxTextGraphicsAlpha, "Text", "alpha");
                RedoBinding(current.GraphicsSource, textBoxTextGraphicsAngle, "Text", "angle");
            }
        }
        #endregion

        #region Element
        private void textBoxTextboxName_Leave(object sender, EventArgs e)
        {
            TextboxBinding.ResetBindings(false);
        }
        #endregion

        #region Buttons

        private void buttonTextboxAdd_Click(object sender, EventArgs e)
        {
            listboxAdd(listBoxTextbox, TextboxData, new Textbox());
            UpdateTextboxBinding();
            UpdateTextboxGraphicsBinding();
        }

        private void buttonTextboxRemove_Click(object sender, EventArgs e)
        {
            listboxRemove(listBoxTextbox, TextboxData);
            UpdateTextboxBinding();
            UpdateTextboxGraphicsBinding();
        }

        private void buttonTextboxMoveUp_Click(object sender, EventArgs e)
        {
            listboxMoveUp(listBoxTextbox, TextboxData);
            UpdateTextboxBinding();
            UpdateTextboxGraphicsBinding();
        }

        private void buttonTextboxMoveDown_Click(object sender, EventArgs e)
        {
            listboxMoveDown(listBoxTextbox, TextboxData);
            UpdateTextboxBinding();
            UpdateTextboxGraphicsBinding();
        }

        private void buttonTextEntryAdd_Click(object sender, EventArgs e)
        {
            Textbox current = (Textbox)listBoxTextbox.SelectedItem;
            if (current != null)
            {
                listboxAdd(listBoxTextEntry, current.TextEntries, new TextEntry());
                UpdateTextboxBinding();
                UpdateTextboxGraphicsBinding();
            }
        }

        private void buttonTextEntryRemove_Click(object sender, EventArgs e)
        {
            Textbox current = (Textbox)listBoxTextbox.SelectedItem;
            if (current != null)
            {
                listboxRemove(listBoxTextEntry, current.TextEntries);
                UpdateTextboxBinding();
                UpdateTextboxGraphicsBinding();
            }
        }

        private void buttonTextGraphicsAdd_Click(object sender, EventArgs e) 
        {
            TextEntry current = (TextEntry)listBoxTextEntry.SelectedItem;
            if (current != null)
            {
                listboxAdd(listBoxTextGraphics, current.graphics, new TextEntry.GraphicsEntry());
                UpdateTextboxBinding();
                UpdateTextboxGraphicsBinding();
            }
        }

        private void buttonTextGraphicsRemove_Click(object sender, EventArgs e)
        {
            TextEntry current = (TextEntry)listBoxTextEntry.SelectedItem;
            if (current != null)
            {
                listboxRemove(listBoxTextGraphics, current.graphics);
                UpdateTextboxBinding();
                UpdateTextboxGraphicsBinding();
            }
        }

        private void listBoxTextbox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!Loading)
            {
                UpdateTextboxBinding();
                UpdateTextboxGraphicsBinding();
            }
        }

        private void listBoxTextEntry_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!Loading)
            {
                UpdateTextboxBinding();
                UpdateTextboxGraphicsBinding();
            }
        }

        private void textBoxTextEntryText_Leave(object sender, EventArgs e)
        {
            Textbox current = (Textbox)listBoxTextbox.SelectedItem;
            if (current != null)
            {
                current.TextSource.ResetBindings(false);
            }
        }
        private void textBoxTextGraphicsSprite_Leave(object sender, EventArgs e)
        {
            TextEntry current = (TextEntry)listBoxTextEntry.SelectedItem;
            if (current != null)
            {
                current.GraphicsSource.ResetBindings(false);
            }
        }

        #endregion

        #endregion
    }
}
