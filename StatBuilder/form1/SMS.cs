﻿
using StatBuilder.model;
using System;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;

namespace StatBuilder
{
    public partial class MainForm : Form
    {
        #region SMSs

        #region properties
        public static BindingList<SMS> SMSData { get; set; } = new BindingList<SMS>();
        public static BindingSource SMSBinding;
        #endregion

        #region Bindings
        private void BindSMS()
        {
            SMSBinding = new BindingSource(SMSData, null);

            listBoxSMS.DataSource = SMSBinding;


            textBoxSMSName.DataBindings.Add(
            new Binding("Text", SMSBinding,
                "name", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxSMSText.DataBindings.Add(
            new Binding("Text", SMSBinding,
                "desc", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxSMSTitle.DataBindings.Add(
            new Binding("Text", SMSBinding,
                "title", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxSMSSender.DataBindings.Add(
            new Binding("Text", SMSBinding,
                "sender", false, DataSourceUpdateMode.OnPropertyChanged));
        }
        #endregion

        #region Buttons
        private void buttonSMSAdd_Click(object sender, EventArgs e)
        {
            listboxAdd(listBoxSMS, SMSData, new SMS());
        }

        private void buttonSMSRemove_Click(object sender, EventArgs e)
        {
            listboxRemove(listBoxSMS, SMSData);
        }

        private void buttonSMSMoveUp_Click(object sender, EventArgs e)
        {
            listboxMoveUp(listBoxSMS, SMSData);
        }

        private void buttonSMSMoveDown_Click(object sender, EventArgs e)
        {
            listboxMoveDown(listBoxSMS, SMSData);
        }

        #endregion

        #region Element
        private void textBoxSMSName_Leave(object sender, EventArgs e)
        {
            SMSBinding.ResetBindings(false);
        }
        #endregion

        #endregion
    }
}
