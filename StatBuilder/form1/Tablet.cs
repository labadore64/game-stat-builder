﻿
using StatBuilder.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace StatBuilder
{
    public partial class MainForm : Form
    {
        #region Tablet

        #region Properties
        public static BindingList<Tablet> TabletData { get; set; } = new BindingList<Tablet>();
        public static BindingSource TabletBinding;
        #endregion

        #region Stats
        private void TabletUpdateStatDisplay()
        {
            Tablet Tablet = (Tablet)listBoxTablet.SelectedItem;
            if (Tablet != null)
            {
                labelTabletPhyPower.Text = trackBarTabletPhyPower.Value.ToString();
                labelTabletPhyGuard.Text = trackBarTabletPhyGuard.Value.ToString();
                labelTabletMagPower.Text = trackBarTabletMagPower.Value.ToString();
                labelTabletMagGuard.Text = trackBarTabletMagGuard.Value.ToString();
                labelTabletSpePower.Text = trackBarTabletSpePower.Value.ToString();
                labelTabletSpeGuard.Text = trackBarTabletSpeGuard.Value.ToString();
            }
        }

        private void trackBarTabletPhyPower_Scroll(object sender, EventArgs e)
        {
            TabletUpdateStatDisplay();
        }

        private void trackBarTabletPhyGuard_Scroll(object sender, EventArgs e)
        {
            TabletUpdateStatDisplay();
        }

        private void trackBarTabletMagPower_Scroll(object sender, EventArgs e)
        {
            TabletUpdateStatDisplay();
        }

        private void trackBarTabletMagGuard_Scroll(object sender, EventArgs e)
        {
            TabletUpdateStatDisplay();
        }

        private void trackBarTabletSpePower_Scroll(object sender, EventArgs e)
        {
            TabletUpdateStatDisplay();
        }

        private void trackBarTabletSpeGuard_Scroll(object sender, EventArgs e)
        {
            TabletUpdateStatDisplay();
        }

        #endregion

        #region properties

        List<BindingSource> TabletBindingSources = new List<BindingSource>();

        #endregion

        #region Binding
        private void BindTablet()
        {
            //Tablet bindings
            TabletBinding = new BindingSource(TabletData, null);

            listBoxTablet.DataSource = TabletBinding;

            textBoxTabletName.DataBindings.Add(
            new Binding("Text", TabletBinding,
                "name", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxTabletDesc.DataBindings.Add(
            new Binding("Text", TabletBinding,
                "desc", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxTabletChar.DataBindings.Add(
            new Binding("Text", TabletBinding,
                "character", false, DataSourceUpdateMode.OnPropertyChanged));

            trackBarTabletPhyPower.DataBindings.Add(
            new Binding("Value", TabletBinding,
                "phy_power", false, DataSourceUpdateMode.OnPropertyChanged));
            trackBarTabletPhyGuard.DataBindings.Add(
            new Binding("Value", TabletBinding,
                "phy_guard", false, DataSourceUpdateMode.OnPropertyChanged));
            trackBarTabletMagPower.DataBindings.Add(
            new Binding("Value", TabletBinding,
                "mag_power", false, DataSourceUpdateMode.OnPropertyChanged));
            trackBarTabletMagGuard.DataBindings.Add(
            new Binding("Value", TabletBinding,
                "mag_guard", false, DataSourceUpdateMode.OnPropertyChanged));
            trackBarTabletSpePower.DataBindings.Add(
            new Binding("Value", TabletBinding,
                "spe_power", false, DataSourceUpdateMode.OnPropertyChanged));
            trackBarTabletSpeGuard.DataBindings.Add(
            new Binding("Value", TabletBinding,
                "spe_guard", false, DataSourceUpdateMode.OnPropertyChanged));

            BindingSource tmp = new BindingSource(MoveData, null);
            comboBoxTabletMove.DataSource = tmp;
            TabletBindingSources.Add(tmp);

            comboBoxTabletMove.DataBindings.Add(
                new Binding("SelectedItem", TabletBinding,
                    "Move", false, DataSourceUpdateMode.OnPropertyChanged));
        }

        private void TabletResetBindings()
        {
            for (int i = 0; i < TabletBindingSources.Count; i++)
            {
                TabletBindingSources[i].ResetBindings(false);
            }
        }
        #endregion

        #region Buttons
        private void buttonTabletAdd_Click(object sender, EventArgs e)
        {
            listboxAdd(listBoxTablet, TabletData, new Tablet());
        }

        private void buttonTabletRemove_Click(object sender, EventArgs e)
        {
            listboxRemove(listBoxTablet, TabletData);
        }

        private void buttonTabletMoveUp_Click(object sender, EventArgs e)
        {
            listboxMoveUp(listBoxTablet, TabletData);
        }

        private void buttonTabletMoveDown_Click(object sender, EventArgs e)
        {
            listboxMoveDown(listBoxTablet, TabletData);
        }
        #endregion

        #region Elements
        private void textBoxTabletName_Leave(object sender, EventArgs e)
        {
            TabletBinding.ResetBindings(false);
        }

        private void comboBoxTabletMove_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        #endregion

        #endregion
    }
}
