﻿
using StatBuilder.model;
using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace StatBuilder
{
    public partial class MainForm : Form
    {
        #region Encounters

        #region Properties
        public static BindingList<Encounter> EncounterData { get; set; } = new BindingList<Encounter>();
        public static BindingSource EncounterBinding;
        #endregion

        #region Bindings
        private void BindEncounter()
        {
            EncounterBinding = new BindingSource(EncounterData, null);

            listBoxEncounters.DataSource = EncounterBinding;

            textBoxEncounterName.DataBindings.Add(
            new Binding("Text", EncounterBinding,
                "name", false, DataSourceUpdateMode.OnPropertyChanged));

            textBoxEncounterEnemy1.DataBindings.Add(
            new Binding("Text", EncounterBinding,
                "enemy0", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxEncounterEnemy2.DataBindings.Add(
            new Binding("Text", EncounterBinding,
                "enemy1", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxEncounterEnemy3.DataBindings.Add(
            new Binding("Text", EncounterBinding,
                "enemy2", false, DataSourceUpdateMode.OnPropertyChanged));

            textBoxEncounterEventEnabled.DataBindings.Add(
            new Binding("Text", EncounterBinding,
                "event_enabled", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxEncounterEventDisabled.DataBindings.Add(
            new Binding("Text", EncounterBinding,
                "event_disabled", false, DataSourceUpdateMode.OnPropertyChanged));

            checkBoxEncounterRandom.DataBindings.Add(
            new Binding("Checked", EncounterBinding,
                "random_order", false, DataSourceUpdateMode.OnPropertyChanged));
        }
        #endregion

        #region Element
        private void textBoxEncounterName_Leave(object sender, EventArgs e)
        {
            EncounterBinding.ResetBindings(false);
        }
        #endregion

        #region Buttons

        private void buttonEncounterAdd_Click(object sender, EventArgs e)
        {
            listboxAdd(listBoxEncounters, EncounterData, new Encounter());
        }

        private void buttonEncounterRemove_Click(object sender, EventArgs e)
        {
            listboxRemove(listBoxEncounters, EncounterData);
        }

        private void buttonEncounterMoveUp_Click(object sender, EventArgs e)
        {
            listboxMoveUp(listBoxEncounters, EncounterData);
        }

        private void buttonEncounterMoveDown_Click(object sender, EventArgs e)
        {
            listboxMoveDown(listBoxEncounters, EncounterData);
        }

        #endregion

        #endregion
    }
}
