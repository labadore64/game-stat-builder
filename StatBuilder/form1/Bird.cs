﻿
using StatBuilder.model;
using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace StatBuilder
{
    public partial class MainForm : Form
    {
        #region Birds

        #region Properties
        public static BindingList<Bird> BirdData { get; set; } = new BindingList<Bird>();
        public static BindingSource BirdBinding;
        #endregion

        #region Bindings
        private void BindBird()
        {
            BirdBinding = new BindingSource(BirdData, null);

            listBoxBird.DataSource = BirdBinding;

            textBoxBirdName.DataBindings.Add(
            new Binding("Text", BirdBinding,
                "name", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxBirdLongName.DataBindings.Add(
            new Binding("Text", BirdBinding,
                "long_name", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxBirdDesc.DataBindings.Add(
            new Binding("Text", BirdBinding,
                "desc", false, DataSourceUpdateMode.OnPropertyChanged));

            textBoxBirdHeight.DataBindings.Add(
            new Binding("Text", BirdBinding,
                "height", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxBirdWeight.DataBindings.Add(
            new Binding("Text", BirdBinding,
                "weight", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxBirdMetricHeight.DataBindings.Add(
            new Binding("Text", BirdBinding,
                "metric_height", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxBirdMetricWeight.DataBindings.Add(
            new Binding("Text", BirdBinding,
                "metric_weight", false, DataSourceUpdateMode.OnPropertyChanged));

            textBoxBirdMaleSprite.DataBindings.Add(
            new Binding("Text", BirdBinding,
                "male_sprite", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxBirdFemaleSprite.DataBindings.Add(
            new Binding("Text", BirdBinding,
                "female_sprite", false, DataSourceUpdateMode.OnPropertyChanged));

            textBoxBirdScale.DataBindings.Add(
            new Binding("Text", BirdBinding,
                "scale", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxBirdOffset.DataBindings.Add(
            new Binding("Text", BirdBinding,
                "offset", false, DataSourceUpdateMode.OnPropertyChanged));
            textBoxBirdSpawnRatio.DataBindings.Add(
            new Binding("Text", BirdBinding,
                "spawn_rate", false, DataSourceUpdateMode.OnPropertyChanged));
        }
        #endregion

        #region Element
        private void textBoxBirdName_Leave(object sender, EventArgs e)
        {
            BirdBinding.ResetBindings(false);
        }
        #endregion

        #region Buttons

        private void buttonBirdAdd_Click(object sender, EventArgs e)
        {
            listboxAdd(listBoxBird, BirdData, new Bird());
        }

        private void buttonBirdRemove_Click(object sender, EventArgs e)
        {
            listboxRemove(listBoxBird, BirdData);
        }

        private void buttonBirdMoveUp_Click(object sender, EventArgs e)
        {
            listboxMoveUp(listBoxBird, BirdData);
        }

        private void buttonBirdMoveDown_Click(object sender, EventArgs e)
        {
            listboxMoveDown(listBoxBird, BirdData);
        }

        #endregion

        #endregion
    }
}
