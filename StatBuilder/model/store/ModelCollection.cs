﻿using System;
using System.ComponentModel;

namespace StatBuilder.model.store
{
    public class ModelCollection<T>
    {
        public BindingList<Model> Items { get; set; } = new BindingList<Model>();
    }
}
