﻿using System.Drawing;

namespace StatBuilder.model.store
{
    public class GrowthPoints
    {
        public Point[] Points { get; set; } = new Point[10];
    }
}
