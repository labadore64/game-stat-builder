﻿using IniParser;
using IniParser.Model;
using System;
using System.IO;

namespace StatBuilder.model
{
    public class Encounter: Model
    { 
        public override string SaveDirectory => "battle//encounter";

        public override string SaveName => "enc";

        public override string name { get; set; } = "encounter_name";

        public string enemy0 { get; set; } = "";
        public string enemy1 { get; set; } = "";
        public string enemy2 { get; set; } = "";

        public string event_enabled { get; set; } = "";
        public string event_disabled { get; set; } = "";
        public bool random_order { get; set; } = true;

        public override void ExportToINI(string Path)
        {
            try
            {
                var parser = new FileIniDataParser();
                IniData data = new IniData();

                //Add a new section and some keys
                data.Sections.AddSection(SectionName);
                data[SectionName].AddKey("name", name);

                if (enemy0 != "")
                {
                    data[SectionName].AddKey("enemy0", enemy0);
                }
                if (enemy1 != "")
                {
                    data[SectionName].AddKey("enemy1", enemy1);
                }
                if (enemy2 != "")
                {
                    data[SectionName].AddKey("enemy2", enemy2);
                }

                if (event_enabled != "")
                {
                    data[SectionName].AddKey("event_enabled", event_enabled);
                }
                if (event_disabled != "")
                {
                    data[SectionName].AddKey("event_disabled", event_disabled);
                }

                data[SectionName].AddKey("random", random_order.ToString());

                parser.WriteFile(Path, data);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public override void ImportFromINI(string Path)
        {
            if (File.Exists(Path))
            {
                var parser = new FileIniDataParser();
                IniData data = parser.ReadFile(Path);

                if (data[SectionName]["name"] != null)
                {
                    name = data[SectionName]["name"];
                }

                if (data[SectionName]["enemy0"] != null)
                {
                    enemy0 = data[SectionName]["enemy0"];
                }
                if (data[SectionName]["enemy1"] != null)
                {
                    enemy1 = data[SectionName]["enemy1"];
                }
                if (data[SectionName]["enemy2"] != null)
                {
                    enemy2 = data[SectionName]["enemy2"];
                }

                if (data[SectionName]["event_enabled"] != null)
                {
                    event_enabled = data[SectionName]["event_enabled"];
                }

                if (data[SectionName]["event_disabled"] != null)
                {
                    event_disabled = data[SectionName]["event_disabled"];
                }

                if (data[SectionName]["random"] != null)
                {
                    random_order = Boolean.Parse(data[SectionName]["random"]);
                }
            }
        }

        public override string ToString()
        {
            return name;
        }
    }
}
