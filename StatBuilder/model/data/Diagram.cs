﻿
using IniParser;
using IniParser.Model;
using System;
using System.IO;

namespace StatBuilder.model
{
    public class Diagram : Model
    {
        public override string SaveDirectory
        {
            get
            {
                return "diagram";
            }
        }

        public override string SaveName
        {
            get
            {
                return "dia";
            }
        }
        public override string name { get; set; } = "Diagram";
        public string alt_text { get; set; } = "";
        public string sprite { get; set; } = "";
        public double x_scale { get; set; } = 1;
        public double y_scale { get; set; } = 1;

        public double x_offset { get; set; } = 0;
        public double y_offset { get; set; } = 0;

        public int r { get; set; } = 255;
        public int g { get; set; } = 255;
        public int b { get; set; } = 255;

        public override void ImportFromINI(string Path)
        {
            if (File.Exists(Path))
            {
                var parser = new FileIniDataParser();
                IniData data = parser.ReadFile(Path);

                if (data[SectionName]["name"] != null)
                {
                    name = data[SectionName]["name"];
                }
                if (data[SectionName]["alt_text"] != null)
                {
                    alt_text = data[SectionName]["alt_text"];
                }
                if (data[SectionName]["sprite"] != null)
                {
                    sprite = data[SectionName]["sprite"];
                }

                x_scale = Convert.ToDouble(data[SectionName]["x_scale"]);
                y_scale = Convert.ToDouble(data[SectionName]["y_scale"]);

                x_offset = Convert.ToDouble(data[SectionName]["x_offset"]);
                y_offset = Convert.ToDouble(data[SectionName]["y_offset"]);

                r = Convert.ToInt32(data[SectionName]["r"]);
                g = Convert.ToInt32(data[SectionName]["g"]);
                b = Convert.ToInt32(data[SectionName]["b"]);
            }
        }

        public override void ExportToINI(string Path)
        {
            try
            {
                var parser = new FileIniDataParser();
                IniData data = new IniData();

                //Add a new section and some keys
                data.Sections.AddSection(SectionName);
                data[SectionName].AddKey("name", name);
                data[SectionName].AddKey("alt_text", alt_text);
                data[SectionName].AddKey("sprite", sprite);
                data[SectionName].AddKey("x_scale", x_scale.ToString());
                data[SectionName].AddKey("y_scale", y_scale.ToString());
                data[SectionName].AddKey("x_offset", x_offset.ToString());
                data[SectionName].AddKey("y_offset", y_offset.ToString());

                data[SectionName].AddKey("r", r.ToString());
                data[SectionName].AddKey("g", g.ToString());
                data[SectionName].AddKey("b", b.ToString());
                parser.WriteFile(Path, data);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
