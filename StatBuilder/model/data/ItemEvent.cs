﻿using IniParser;
using IniParser.Model;
using System;
using System.IO;

namespace StatBuilder.model
{
    public class ItemEvent : Event
    {
        public ItemEvent()
        {

        }

        public ItemEvent(string name, int item_id, int quantity)
        {
            this.item_id = item_id;
            if (name != null)
            {
                this.name = name;
            }
            this.quantity = quantity;
        }
        public override string SaveName
        {
            get
            {
                return "item";
            }
        }

        public override string ToString()
        {
            if (Item != null)
            {
                return name + ":" + Item.name;
            } else
            {
                return name + ":" + "None";
            }
        }

        public override string name { get; set; } = "itemEvent";

        public int item_id
        {
            get
            {
                return MainForm.ItemData.IndexOf(Item);
            }
            set
            {
                if (value > -1 && value < MainForm.ItemData.Count)
                {
                    Item = MainForm.ItemData[value];
                }
                else
                {
                    Item = null;
                }
            }
        }
        public ItemData Item { get; set; } = null;

        public int quantity { get; set; } = 1;

        public override void ImportFromINI(string Path)
        {
            if (File.Exists(Path))
            {
                var parser = new FileIniDataParser();
                IniData data = parser.ReadFile(Path);

                if (data[SectionName]["name"] != null)
                {
                    name = data[SectionName]["name"];
                }
            }
        }

        public override void ExportToINI(string Path)
        {
            try
            {
                var parser = new FileIniDataParser();
                IniData data = new IniData();

                //Add a new section and some keys
                data.Sections.AddSection(SectionName);
                data[SectionName].AddKey("name", name);
                data[SectionName].AddKey("item_id", item_id.ToString());
                item_id = Convert.ToInt32(data[SectionName]["item_id"]);

                parser.WriteFile(Path, data);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
