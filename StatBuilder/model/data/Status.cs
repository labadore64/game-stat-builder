﻿using IniParser;
using IniParser.Model;
using System;
using System.IO;

namespace StatBuilder.model
{
    public class Status : Model
    {
        public override string SaveDirectory
        {
            get
            {
                return "battle//status";
            }
        }

        public override string SaveName
        {
            get
            {
                return "status";
            }
        }
        public override string name { get; set; } = "Status";
        public string desc { get; set; } = "";
        public string final_text { get; set; } = "";
        public bool harmful { get; set; } = false;

        public string sprite { get; set; } = "";
        public string shader_id { get; set; } = "";

        public int turns_min { get; set; } = 3;
        public int turns_max { get; set; } = 3;
        public override void ImportFromINI(string Path)
        {
            if (File.Exists(Path))
            {
                var parser = new FileIniDataParser();
                IniData data = parser.ReadFile(Path);

                if (data[SectionName]["name"] != null)
                {
                    name = data[SectionName]["name"];
                }
                if (data[SectionName]["desc"] != null)
                {
                    desc = MainForm.StringNewlineImport(data[SectionName]["desc"]);
                }
                if (data[SectionName]["final_text"] != null)
                {
                    final_text = data[SectionName]["final_text"];
                }
                harmful = Convert.ToBoolean(data[SectionName]["harmful"]);

                if (data[SectionName]["sprite"] != null)
                {
                    sprite = data[SectionName]["sprite"];
                }
                if (data[SectionName]["shader_id"] != null)
                {
                    shader_id = data[SectionName]["shader_id"];
                }

                turns_min = Convert.ToInt32(data[SectionName]["turns_min"]);
                turns_max = Convert.ToInt32(data[SectionName]["turns_max"]);
            }
        }

        public override void ExportToINI(string Path)
        {
            try
            {
                var parser = new FileIniDataParser();
                IniData data = new IniData();

                //Add a new section and some keys
                data.Sections.AddSection(SectionName);
                data[SectionName].AddKey("name", name);
                data[SectionName].AddKey("desc", MainForm.StringNewlineExport(desc));
                data[SectionName].AddKey("final_text", final_text);
                data[SectionName].AddKey("harmful", harmful.ToString());
                data[SectionName].AddKey("sprite", sprite);
                data[SectionName].AddKey("shader_id", shader_id);
                data[SectionName].AddKey("turns_min", turns_min.ToString());
                data[SectionName].AddKey("turns_max", turns_max.ToString());

                parser.WriteFile(Path, data);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
