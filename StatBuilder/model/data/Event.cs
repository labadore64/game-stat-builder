﻿using IniParser;
using IniParser.Model;
using System;
using System.IO;

namespace StatBuilder.model
{
    public class Event : Model
    {
        public Event()
        {

        }

        public Event(string name)
        {
            this.name = name;
        }

        public override string SaveDirectory
        {
            get
            {
                return "event";
            }
        }
        public override string SaveName
        {
            get
            {
                return "event";
            }
        }

        public override string ToString()
        {
            return name;
        }

        public override string name { get; set; } = "event";

        public override void ImportFromINI(string Path)
        {

        }

        public override void ExportToINI(string Path)
        {

        }
    }
}
