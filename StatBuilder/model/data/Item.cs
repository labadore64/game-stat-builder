﻿using IniParser;
using IniParser.Model;
using System;
using System.IO;

namespace StatBuilder.model
{
    public class ItemData : Model
    {
        public override string SectionName { get; set; } = "stats";

        public override string SaveDirectory
        {
            get
            {
                return "item";
            }
        }

        public override string SaveName
        {
            get
            {
                return "item";
            }
        }

        public static string[] ItemTypes = new string[]
        {
            "Other",
            "Equip",
            "Heal",
            "Battle",
            "Overworld",
            "Book",
            "Food",
            "Craft",
            "Decor"
        };

        public override string name  { get; set; } = "Item";
        public string desc { get; set; } = "";
        public string type { get; set; } = "";
        public string overworld_effect { get; set; } = "";

        public int battle_effect { get; private set; } = -1;
        public int level { get; set; }
        public Move Move
        {
            get
            {
                return _move;
            }
            set
            {
                _move = value;
                battle_effect = MainForm.MoveData.IndexOf(_move);
            }
        }

        Move _move = null;

        public string book_name { get; set; } = "";
        public int price { get; set; } = 0;

        public bool can_toss { get; set; } = true;
        public bool can_give { get; set; } = true;
        public bool can_sell { get; set; } = true;
        public bool single_use { get; set; } = true;
        public string sprite { get; set; } = "";

        public int phy_power { get; set; } = 0;
        public int phy_guard { get; set; } = 0;
        public int mag_power { get; set; } = 0;
        public int mag_guard { get; set; } = 0;
        public int spe_power { get; set; } = 0;
        public int spe_guard { get; set; } = 0;

        public string help { get; set; } = "";
        public override void ImportFromINI(string Path)
        {
            if (File.Exists(Path))
            {
                var parser = new FileIniDataParser();
                IniData data = parser.ReadFile(Path);

                if (data[SectionName]["name"] != null)
                {
                    name = data[SectionName]["name"];
                }
                if (data[SectionName]["desc"] != null)
                {
                    desc = MainForm.StringNewlineImport(data[SectionName]["desc"]);
                }
                
                if (data[SectionName]["item_type"] != null)
                {
                    type = data[SectionName]["item_type"];
                }

                if (data[SectionName]["sprite"] != null)
                {
                    sprite = data[SectionName]["sprite"];
                }
                if (data[SectionName]["overworld_effect"] != null)
                {
                    overworld_effect = data[SectionName]["overworld_effect"];
                }

                battle_effect = Convert.ToInt32(data[SectionName]["battle_effect"]);

                if (data[SectionName]["book_name"] != null)
                {
                    book_name = data[SectionName]["book_name"];
                }

                price = Convert.ToInt32(data[SectionName]["price"]);

                can_toss = Convert.ToBoolean(data[SectionName]["can_toss"]);
                can_give = Convert.ToBoolean(data[SectionName]["can_give"]);
                can_sell = Convert.ToBoolean(data[SectionName]["can_sell"]);
                single_use = Convert.ToBoolean(data[SectionName]["single_use"]);

                phy_power = Convert.ToInt32(data[SectionName]["stat_attack"]);
                phy_guard = Convert.ToInt32(data[SectionName]["stat_defense"]);
                mag_power = Convert.ToInt32(data[SectionName]["stat_magic"]);
                mag_guard = Convert.ToInt32(data[SectionName]["stat_resistance"]);
                spe_power = Convert.ToInt32(data[SectionName]["stat_agility"]);
                spe_guard = Convert.ToInt32(data[SectionName]["stat_mpr"]);

                if(data[SectionName]["help"] != null)
                {
                    help = data[SectionName]["help"];
                }
            }
        }

        public override void ExportToINI(string Path)
        {
            try
            {
                var parser = new FileIniDataParser();
                IniData data = new IniData();

                //Add a new section and some keys
                data.Sections.AddSection(SectionName);
                data[SectionName].AddKey("name", name);
                data[SectionName].AddKey("desc", MainForm.StringNewlineExport(desc));
                data[SectionName].AddKey("item_type", type);

                data[SectionName].AddKey("sprite", sprite);
                data[SectionName].AddKey("overworld_effect", overworld_effect);
                data[SectionName].AddKey("battle_effect", battle_effect.ToString());

                if (book_name.Trim() != "")
                {
                    data[SectionName].AddKey("book_name", book_name);
                }
                data[SectionName].AddKey("price", price.ToString());

                data[SectionName].AddKey("can_toss", can_toss.ToString());
                data[SectionName].AddKey("can_give", can_give.ToString());
                data[SectionName].AddKey("can_sell", can_sell.ToString());
                data[SectionName].AddKey("single_use", single_use.ToString());

                data[SectionName].AddKey("stat_attack", phy_power.ToString());
                data[SectionName].AddKey("stat_defense", phy_guard.ToString());
                data[SectionName].AddKey("stat_magic", mag_power.ToString());
                data[SectionName].AddKey("stat_resistance", mag_guard.ToString());
                data[SectionName].AddKey("stat_agility", spe_power.ToString());
                data[SectionName].AddKey("stat_mpr", spe_guard.ToString());

                if (help.Trim() != "")
                {
                    data[SectionName].AddKey("help", help);
                }

                parser.WriteFile(Path, data);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

    }
}
