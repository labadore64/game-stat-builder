﻿using IniParser;
using IniParser.Model;
using System;
using System.IO;

namespace StatBuilder.model
{
    public class SMS : Model
    {
        public override string SaveDirectory
        {
            get
            {
                return "task";
            }
        }
        public override string SaveName
        {
            get
            {
                return "sms";
            }
        }

        public override string ToString()
        {
            return name;
        }

        public override string name { get; set; } = "SMS";
        public string title { get; set; } = "";
        public string desc { get; set; } = "";
        public string sender { get; set; } = "";

        public override void ImportFromINI(string Path)
        {
            if (File.Exists(Path))
            {
                var parser = new FileIniDataParser();
                IniData data = parser.ReadFile(Path);

                if (data[SectionName]["name"] != null)
                {
                    name = data[SectionName]["name"];
                }
                if (data[SectionName]["desc"] != null)
                {
                    desc = MainForm.StringNewlineImport(data[SectionName]["desc"]);
                }

                if (data[SectionName]["title"] != null)
                {
                    title = data[SectionName]["title"];
                }
                if (data[SectionName]["title"] != null)
                {
                    title = data[SectionName]["title"];
                }

                if (data[SectionName]["sender"] != null)
                {
                    sender = data[SectionName]["sender"];
                }
            }
        }

        public override void ExportToINI(string Path)
        {
            try
            {
                var parser = new FileIniDataParser();
                IniData data = new IniData();

                //Add a new section and some keys
                data.Sections.AddSection(SectionName);
                data[SectionName].AddKey("title", title) ;
                data[SectionName].AddKey("name", name);

                data[SectionName].AddKey("desc", MainForm.StringNewlineExport(desc));
                data[SectionName].AddKey("sender", sender);

                parser.WriteFile(Path, data);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
