﻿using IniParser;
using IniParser.Model;
using System;
using System.IO;

namespace StatBuilder.model
{
    public class Tablet : Model
    {
        public override string SectionName { get; set; } = "tablet";

        public override string SaveDirectory
        {
            get
            {
                return "tablet";
            }
        }

        public override string SaveName
        {
            get
            {
                return "tablet";
            }
        }
        public override string name { get; set; } = "Default";
        public string desc { get; set; } = "";

        public string character { get; set; } = "a";

        public Move Move { get; set; }

        public int move_id
        {
            get
            {
                return MainForm.MoveData.IndexOf(Move);
            }
            set 
            {
                if(value > -1 && value < MainForm.MoveData.Count)
                {
                    Move = MainForm.MoveData[value];
                } else
                {
                    Move = null;
                }
            }
        }

        // stat boosts when tablet is not in recovery mode.
        public int phy_power { get; set; } = 0;
        public int phy_guard { get; set; } = 0;
        public int mag_power { get; set; } = 0;
        public int mag_guard { get; set; } = 0;
        public int spe_power { get; set; } = 0;
        public int spe_guard { get; set; } = 0;


        public override void ImportFromINI(string Path)
        {
            if (File.Exists(Path))
            {
                var parser = new FileIniDataParser();
                IniData data = parser.ReadFile(Path);

                if (data[SectionName]["name"] != null)
                {
                    name = data[SectionName]["name"];
                }
                if (data[SectionName]["desc"] != null)
                {
                    desc = MainForm.StringNewlineImport(data[SectionName]["desc"]);
                }
                if (data[SectionName]["char"] != null)
                {
                    character = MainForm.StringNewlineImport(data[SectionName]["char"]);
                }

                move_id = Convert.ToInt32(data[SectionName]["move_id"]);

                phy_power = Convert.ToInt32(data[SectionName]["stat_attack"]);
                phy_guard = Convert.ToInt32(data[SectionName]["stat_defense"]);
                mag_power = Convert.ToInt32(data[SectionName]["stat_magic"]);
                mag_guard = Convert.ToInt32(data[SectionName]["stat_resistance"]);
                spe_power = Convert.ToInt32(data[SectionName]["stat_agility"]);
                spe_guard = Convert.ToInt32(data[SectionName]["stat_mpr"]);

            }
        }

        public override void ExportToINI(string Path)
        {
            try
            {
                var parser = new FileIniDataParser();
                IniData data = new IniData();

                //Add a new section and some keys
                data.Sections.AddSection(SectionName);
                data[SectionName].AddKey("name", name);
                data[SectionName].AddKey("desc", MainForm.StringNewlineExport(desc));
                data[SectionName].AddKey("char", character);

                data[SectionName].AddKey("move_id", move_id.ToString());

                data[SectionName].AddKey("stat_attack", phy_power.ToString());
                data[SectionName].AddKey("stat_defense", phy_guard.ToString());
                data[SectionName].AddKey("stat_magic", mag_power.ToString());
                data[SectionName].AddKey("stat_resistance", mag_guard.ToString());
                data[SectionName].AddKey("stat_agility", spe_power.ToString());
                data[SectionName].AddKey("stat_mpr", spe_guard.ToString());

                parser.WriteFile(Path, data);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
