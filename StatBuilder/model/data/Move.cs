﻿using IniParser;
using IniParser.Model;
using Newtonsoft.Json;
using System;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;

namespace StatBuilder.model
{
    public class Move : Model
    {
        public class MoveAnimation : INotifyPropertyChanged
        {
            public event PropertyChangedEventHandler PropertyChanged;

            public double jump_height { get; set; } = -1;
            public int jump { get; set; } = -1;
            public int frame { get; set; } = -1;
            public int lunge { get; set; } = -1;
            public double lunge_pos { get; set; } = 1;
            public string sound_fx { get; set; } = "";
            public string sprite { get; set; } = "";

            public string target { get; set; } = "none";

            public override string ToString()
            {
                return target + ":" + frame.ToString();
            }

            public MoveAnimation()
            {

            }
            public MoveAnimation(int jump, double jump_height, int frame, int lunge, int lunge_pos, string sound_fx, string sprite,string target)
            {
                if (sound_fx != null)
                {
                    this.sound_fx = sound_fx;
                }
                if (sprite != null)
                {
                    this.sprite = sprite;
                }
                this.lunge_pos = lunge_pos;
                this.jump = jump;
                this.jump_height = jump_height;
                this.frame = frame;
                this.lunge = lunge;
                this.target = target;
            }
        }

        public BindingSource AnimationSource { get; protected set; }

        public BindingSource TextSource { get; protected set; }
        public BindingSource FilterSource { get; protected set; }

        public override string SaveDirectory
        {
            get
            {
                return "move";
            }
        }

        public override string SaveName
        {
            get
            {
                return "move";
            }
        }

        public override string SectionName { get; set; } = "stats";

        public static string[] MovePhyMagTypes = new string[]
        {
            "Physical",
            "Magic"
        };

        public static string[] ShowBattlerTypes = new string[]
        {   
            "Show All",
            "Show Only User and Target",
            "Show Only User",
            "Show Only Target",
            "Hide All"
        };

        public override string name { get; set; } = "Move";
        public string character { get; set; }

        public string desc { get; set; } = "";

        public int damage { get; set; } = 0;
        public int mp_cost { get; set; } = 0;
        public int target_number { get; set; } = 1;

        public string move_type { get; set; } = "";
        public string element { get; set; } = MainForm.Types[0];

        public bool target_skip { get; set; } = false;
        public bool target_ally { get; set; } = false;
        public bool target_self { get; set; } = false;
        public bool camera_rotate { get; set; } = false;

        public string script_battle { get; set; } = "";

        public double speed_modifier { get; set; } = 1;
        public int speed_priority { get; set; } = 0;

        public string bg_animation { get; set; } = "";
        public string fg_animation { get; set; } = "";
        public string sprite_animation { get; set; } = "";
        public string self_animation { get; set; } = "";

        public int item_id
        {
            get
            {
                return MainForm.ItemData.IndexOf(Item);
            }
            set
            {
                if(value > -1 && value < MainForm.ItemData.Count)
                {
                    Item = MainForm.ItemData[value];
                } else
                {
                    Item = null;
                }
            }
        }
        public ItemData Item { get; set; } = null;
        public double effect_percent { get; set; } = 0;
        public double filter_percent { get; set; } = 0;

        public int animation_length { get; set; } = 1;
        public bool animation_knockback { get; set; } = false;
        public string show_battler { get; set; } = "Show Only User and Target";
        public BindingList<string> battle_text { get; set; } = new BindingList<string>();

        public BindingList<string> filter { get; set; } = new BindingList<string>();

        public BindingList<MoveAnimation> Animations { get; set; } = new BindingList<MoveAnimation>();

        public override void Init()
        {
            TextSource = new BindingSource(battle_text, null);
            FilterSource = new BindingSource(filter, null);
            AnimationSource = new BindingSource(Animations, null);

            Animations.Add(new MoveAnimation());
        }

        public override void ImportFromINI(string Path)
        {
            if (File.Exists(Path))
            {
                string tmp;

                Animations.Clear();

                var parser = new FileIniDataParser();
                IniData data = parser.ReadFile(Path);

                if (data[SectionName]["name"] != null)
                {
                    name = data[SectionName]["name"];
                }
                if (data[SectionName]["desc"] != null)
                {
                    desc = MainForm.StringNewlineImport(data[SectionName]["desc"]);
                }

                damage = Convert.ToInt32(data[SectionName]["damage"]);
                mp_cost = Convert.ToInt32(data[SectionName]["mp_cost"]);
                target_number = Convert.ToInt32(data[SectionName]["target_number"]);

                target_skip = Convert.ToBoolean(data[SectionName]["target_skip"]);
                target_ally = Convert.ToBoolean(data[SectionName]["target_ally"]);
                target_self = Convert.ToBoolean(data[SectionName]["target_self"]);
                camera_rotate = Convert.ToBoolean(data[SectionName]["camera_rotate"]);

                if (data[SectionName]["script_battle"] != null)
                {
                    script_battle = data[SectionName]["script_battle"];
                }

                if (data[SectionName]["move_type"] != null)
                {
                    move_type = data[SectionName]["move_type"];
                }
                if (data[SectionName]["element"] != null)
                {
                    element = data[SectionName]["element"];
                }

                speed_modifier = Convert.ToDouble(data[SectionName]["speed_modifier"]);
                speed_priority = Convert.ToInt32(data[SectionName]["speed_priority"]);

                if (data[SectionName]["bg_animation"] != null)
                {
                    bg_animation = data[SectionName]["bg_animation"];
                }
                if (data[SectionName]["fg_animation"] != null)
                {
                    fg_animation = data[SectionName]["fg_animation"];
                }
                if (data[SectionName]["sprite_animation"] != null)
                {
                    sprite_animation = data[SectionName]["sprite_animation"];
                }
                if (data[SectionName]["self_animation"] != null)
                {
                    self_animation = data[SectionName]["self_animation"];
                }

                item_id = Convert.ToInt32(data[SectionName]["item_id"]);
                effect_percent = Convert.ToDouble(data[SectionName]["effect_percent"]);
                filter_percent = Convert.ToDouble(data[SectionName]["filter_percent"]);

                animation_length = Convert.ToInt32(data[SectionName]["animation_length"]);


                tmp = data[SectionName]["animation_knockback"];

                if (tmp != null)
                {
                    if (tmp.Length > 0)
                    {
                        animation_knockback = Convert.ToBoolean(tmp);
                    }
                }

  
                show_battler = data[SectionName]["show_battler"];

                if (data[SectionName]["char"] != null)
                {
                    character = data[SectionName]["char"];
                }

                int counter = 0;

                while (data[SectionName]["battle_text" + counter] != null)
                {
                    if (data[SectionName]["battle_text" + counter] != "")
                    {
                        battle_text.Add(data[SectionName]["battle_text" + counter]);
                    }
                    counter++;
                }

                counter = 0;
                while (data[SectionName]["filter"+counter] != null)
                {
                    if (data[SectionName]["filter" + counter] != "")
                    {
                        filter.Add(data[SectionName]["filter" + counter]);
                    }
                    counter++;
                }

                counter = 0;
                while (data[SectionName]["anim_def" + counter] != null)
                {
                    if (data[SectionName]["anim_def" + counter] != "")
                    {
                        Animations.Add(JsonConvert.DeserializeObject<MoveAnimation>(data[SectionName]["anim_def" + counter]));
                    }
                    counter++;
                }

                if(Animations.Count == 0)
                {
                    Animations.Add(new MoveAnimation());
                }
            }
        }

        public override void ExportToINI(string Path)
        {
            try
            {
                var parser = new FileIniDataParser();
                IniData data = new IniData();

                //Add a new section and some keys
                data.Sections.AddSection(SectionName);
                data[SectionName].AddKey("name", name);
                data[SectionName].AddKey("desc", MainForm.StringNewlineExport(desc));

                data[SectionName].AddKey("damage", damage.ToString());
                data[SectionName].AddKey("mp_cost", mp_cost.ToString());
                data[SectionName].AddKey("target_number", target_number.ToString());

                data[SectionName].AddKey("target_skip", target_skip.ToString());
                data[SectionName].AddKey("target_ally", target_ally.ToString());
                data[SectionName].AddKey("target_self", target_self.ToString());
                data[SectionName].AddKey("camera_rotate", camera_rotate.ToString());

                data[SectionName].AddKey("script_battle", script_battle);

                data[SectionName].AddKey("move_type", move_type);
                data[SectionName].AddKey("element", element);

                data[SectionName].AddKey("speed_modifier", speed_modifier.ToString());
                data[SectionName].AddKey("speed_priority", speed_priority.ToString());

                data[SectionName].AddKey("bg_animation", bg_animation);
                data[SectionName].AddKey("fg_animation", fg_animation);
                data[SectionName].AddKey("sprite_animation", sprite_animation);
                data[SectionName].AddKey("self_animation", self_animation);

                data[SectionName].AddKey("item_id", item_id.ToString());
                data[SectionName].AddKey("effect_percent", effect_percent.ToString());
                data[SectionName].AddKey("filter_percent", filter_percent.ToString());

                data[SectionName].AddKey("char", character);

                data[SectionName].AddKey("animation_length", animation_length.ToString());
                data[SectionName].AddKey("animation_knockback", animation_knockback.ToString());
                data[SectionName].AddKey("show_battler", show_battler);

                for (int i = 0; i < battle_text.Count; i++)
                {
                    data[SectionName].AddKey("battle_text" + i, battle_text[i]);
                }

                for(int i = 0; i < filter.Count; i++)
                {
                    data[SectionName].AddKey("filter"+i, filter[i]);
                }

                int insertCount = 0;
                for (int i = 0; i < Animations.Count; i++)
                {
                    if (Animations[i].target != "none" && Animations[i].frame != -1)
                    {
                        data[SectionName].AddKey("anim_def" + insertCount, JsonConvert.SerializeObject(Animations[i]));
                        insertCount++;
                    }
                }

                parser.WriteFile(Path, data);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }


    }
}
