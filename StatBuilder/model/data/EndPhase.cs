﻿using IniParser;
using IniParser.Model;
using System;
using System.IO;

namespace StatBuilder.model
{
    public class EndPhase : Model
    {
        public override string SaveDirectory => "battle//endphase";

        public override string SaveName => "end";

        public override string name { get; set; } = "EndPhase";
        public string script { get; set; } = "";
        public string condition_script { get; set; } = "";
        public string bg_animation { get; set; } = "";
        public string fg_animation { get; set; } = "";
        public string sprite_animation { get; set; } = "";
        public bool target { get; set; }
        public bool pause { get; set; }
        public int animation_length { get; set; } = 0;

        public override void ExportToINI(string Path)
        {
            try
            {
                var parser = new FileIniDataParser();
                IniData data = new IniData();

                //Add a new section and some keys
                data.Sections.AddSection(SectionName);
                data[SectionName].AddKey("name", name);
                data[SectionName].AddKey("script", script);
                data[SectionName].AddKey("condition_script", condition_script);

                data[SectionName].AddKey("bg_animation",bg_animation);
                data[SectionName].AddKey("fg_animation", fg_animation);
                data[SectionName].AddKey("sprite_animation", sprite_animation);
                data[SectionName].AddKey("animation_length", animation_length.ToString());

                data[SectionName].AddKey("target", target.ToString());
                data[SectionName].AddKey("pause", pause.ToString());

                parser.WriteFile(Path, data);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public override void ImportFromINI(string Path)
        {
            if (File.Exists(Path))
            {
                var parser = new FileIniDataParser();
                IniData data = parser.ReadFile(Path);

                if (data[SectionName]["name"] != null)
                {
                    name = data[SectionName]["name"];
                }
                if (data[SectionName]["script"] != null)
                {
                    script = MainForm.StringNewlineImport(data[SectionName]["script"]);
                }
                if (data[SectionName]["condition_script"] != null)
                {
                    condition_script = MainForm.StringNewlineImport(data[SectionName]["condition_script"]);
                }

                if (data[SectionName]["bg_animation"] != null)
                {
                    bg_animation = data[SectionName]["bg_animation"];
                }
                if (data[SectionName]["fg_animation"] != null)
                {
                    fg_animation = data[SectionName]["fg_animation"];
                }
                if (data[SectionName]["sprite_animation"] != null)
                {
                    sprite_animation = data[SectionName]["sprite_animation"];
                }
                if (data[SectionName]["animation_length"] != null)
                {
                    animation_length = Convert.ToInt32(data[SectionName]["animation_length"]);
                }
                if (data[SectionName]["target"] != null)
                {
                    target = Convert.ToBoolean(data[SectionName]["target"]);
                }
                if (data[SectionName]["pause"] != null)
                {
                    pause = Convert.ToBoolean(data[SectionName]["pause"]);
                }
            }
        }

        public override string ToString()
        {
            return name;
        }
    }
}
