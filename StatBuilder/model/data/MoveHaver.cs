﻿using IniParser;
using IniParser.Model;
using System;
using System.IO;
using static StatBuilder.model.MoveLearn;

namespace StatBuilder.model
{
    public abstract class MoveHaver : Model
    {
        public virtual MoveLearn movesLearn { get; set; } = new MoveLearn();

        public override void Init()
        {
            base.Init();
            movesLearn.moves.Add(new MoveLearnPiece(null, 1));
        }

        public void ImportMovesFromINI(string dir, int ID)
        {
            if (movesLearn.moves.Count == 0)
            {
                movesLearn.moves.Add(new MoveLearnPiece(null, -1));
            }

            Model first = movesLearn.moves[0];

            string DirPath = dir + "\\" + movesLearn.SaveDirectory;

            if (!Directory.Exists(DirPath))
            {
                Directory.CreateDirectory(DirPath);
            }

            string fileStart = DirPath + "\\" + movesLearn.SaveName + ID + ".ini";

            try
            {
                var parser = new FileIniDataParser();
                IniData data = parser.ReadFile(fileStart);

                int counter = 0;

                movesLearn.moves.Clear();

                while (data[movesLearn.SectionName]["move_id" + counter] != null)
                {
                    if (Convert.ToInt32(data[movesLearn.SectionName]["move_id" + counter]) > -1)
                    {
                        movesLearn.moves.Add(new MoveLearn.MoveLearnPiece(
                            MainForm.MoveData[Convert.ToInt32(data[movesLearn.SectionName]["move_id" + counter])],
                            Convert.ToInt32(data[movesLearn.SectionName]["move_level" + counter])
                            )
                        );
                    }
                    counter++;
                }

                if(movesLearn.moves.Count == 0)
                {
                    movesLearn.moves.Add(new MoveLearnPiece(null, 1));
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void ExportMovesToINI(string dir, int ID)
        {
            Model first;
            if (movesLearn.moves.Count == 0)
            {
                first = new MoveLearnPiece(null, -1);
            } else {

                first = movesLearn.moves[0];
            }

            string DirPath = dir + "\\" + movesLearn.SaveDirectory;

            if (!Directory.Exists(DirPath))
            {
                Directory.CreateDirectory(DirPath);
            }

            string fileStart = DirPath + "\\" + movesLearn.SaveName + ID + ".ini";


            try
            {
                var parser = new FileIniDataParser();
                IniData data = new IniData();

                for (int i = 0; i < movesLearn.moves.Count; i++)
                {
                    data[first.SectionName].AddKey("move_id"+i, movesLearn.moves[i].move_id.ToString());
                    data[first.SectionName].AddKey("move_level"+i, movesLearn.moves[i].level.ToString());
                }
                parser.WriteFile(fileStart, data);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }
    }
}
