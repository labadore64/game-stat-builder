﻿using IniParser;
using IniParser.Model;
using System;
using System.IO;

namespace StatBuilder.model
{
    public class Word : Model
    {
        public override string SaveDirectory => "word";

        public override string SaveName => "word";

        public override string name { get; set; } = "";
        public string desc { get; set; } = "";
        public Move Move
        {
            get
            {
                return _move;
            }
            set
            {
                _move = value;
                move_id = MainForm.MoveData.IndexOf(_move);
            }
        }
        Move _move;
        int move_id { get; set; }

        public override void ExportToINI(string Path)
        {
            try
            {
                var parser = new FileIniDataParser();
                IniData data = new IniData();

                //Add a new section and some keys
                data.Sections.AddSection(SectionName);
                data[SectionName].AddKey("name", name);
                data[SectionName].AddKey("desc", MainForm.StringNewlineExport(desc));
                data[SectionName].AddKey("move_id", move_id.ToString());

                parser.WriteFile(Path, data);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public override void ImportFromINI(string Path)
        {
            if (File.Exists(Path))
            {
                var parser = new FileIniDataParser();
                IniData data = parser.ReadFile(Path);

                if (data[SectionName]["name"] != null)
                {
                    name = data[SectionName]["name"];
                }
                if (data[SectionName]["desc"] != null)
                {
                    desc = MainForm.StringNewlineImport(data[SectionName]["desc"]);
                }
                move_id = Convert.ToInt32(data[SectionName]["move_id"]);
                if (move_id > -1)
                {
                    Move = MainForm.MoveData[move_id];
                }

            }
        }
    }
}
