﻿using IniParser;
using IniParser.Model;
using System;
using System.ComponentModel;
using System.IO;
using System.Linq;

namespace StatBuilder.model
{
    public class HUD : Model
    {
        public class HUDChild : INotifyPropertyChanged
        {
            public event PropertyChangedEventHandler PropertyChanged;

            public string template { get; set; }
            public string name { get; set; } = "";
            public string varname { get; set; } = "";

            public string custom { get; set; } = "";

            public HUD ChildTemplate
            {
                get
                {
                    return _childTemplate;
                }
                set
                {
                    _childTemplate = value;
                    template = _childTemplate.id;
                }
            }

            HUD _childTemplate;

            public override string ToString()
            {
                return name;
            }

            public HUDChild()
            {

            }

            public HUDChild(string name, string template, string varname, string custom)
            {
                if (name != null)
                {
                    this.name = name;
                }
                if (template != null)
                {
                    this.template = template;
                }
                if (varname != null)
                {
                    this.varname = varname;
                }
                if (custom != null)
                {
                    this.custom = custom;
                }
            }

            public void Update()
            {
                HUD hud = MainForm.HUDData.ToList().Find(x => x.id == template);
                if(hud != null)
                {
                    ChildTemplate = hud;
                }
            }
        }

        public class HUDDef : INotifyPropertyChanged
        {
            public event PropertyChangedEventHandler PropertyChanged;

            public string def { get; set; } = "select";
            public string func { get; set; } = "text";
            public string args { get; set; } = "";
            public string req { get; set; } = "";

            public override string ToString()
            {
                return def + ":" + func;
            }

            public HUDDef()
            {

            }
            public HUDDef(string def, string func, string args, string req)
            {
                if(req != null)
                {
                    this.req = req;
                }
                if (def != null)
                {
                    this.def = def;
                }
                if (func != null)
                {
                    this.func = func;
                }
                if (args != null)
                {
                    this.args = args;
                }
            }
        }

        public class HUDInfokey : INotifyPropertyChanged
        {
            public event PropertyChangedEventHandler PropertyChanged;

            public string text { get; set; } = "text";
            public string req { get; set; } = "";

            public override string ToString()
            {
                return text;
            }
            public HUDInfokey()
            {

            }

            public HUDInfokey(string value, string req)
            {
                if(value == null)
                {
                    value = "";
                }
                if(req == null)
                {
                    req = "";
                }
                text = value;
                this.req = req;
            }
        }

        public override string SaveDirectory
        {
            get
            {
                return "ui";
            }
        }

        public override string SaveName
        {
            get
            {
                return "hud";
            }
        }
        public string id { get; set; } = "test";
        public string read_text { get; set; } = "";

        public BindingList<HUDInfokey> infokeys { get; set; } = new BindingList<HUDInfokey>();

        public BindingList<HUDDef> def { get; set; } = new BindingList<HUDDef>();

        public BindingList<HUDChild> children { get; set; } = new BindingList<HUDChild>();

        public override string ToString()
        {
            return id;
        }

        public override void Init()
        {
            infokeys.Add(new HUDInfokey());
            def.Add(new HUDDef());
            children.Add(new HUDChild());
        }

        public void UpdateChildren()
        {
            foreach(HUDChild c in children)
            {
                c.Update();
            }
        }

        public override void ImportFromINI(string Path)
        {
            if (File.Exists(Path))
            {
                var parser = new FileIniDataParser();
                IniData data = parser.ReadFile(Path);

                children.Clear();
                def.Clear();
                infokeys.Clear();



                // object data
                if (data["object"]["name"] != null)
                {
                    name = data["object"]["name"];
                }
                if (data["object"]["id"] != null)
                {
                    id = data["object"]["id"];
                }
                if (data["object"]["text"] != null)
                {
                    read_text = data["object"]["text"];
                }

                int counter = 0;

                // children
                counter = 0;
                while (data["children"]["child_template" + counter] != null)
                {
                    if (data["children"]["child_template" + counter] != "")
                    {

                        children.Add(new HUDChild(
                                    data["children"]["child_name" + counter],
                                    data["children"]["child_template" + counter],
                                    data["children"]["child_var" + counter],
                                    data["children"]["custom" + counter]
                                    ));
                    }
                    counter++;
                }

                // def
                counter = 0;

                while (data["definition"]["key_def" + counter] != null)
                {
                    if (data["definition"]["key_def" + counter] != "")
                    {
                        def.Add(new HUDDef(
                                    data["definition"]["key_def" + counter],
                                    data["definition"]["key_func" + counter],
                                    data["definition"]["key_arg" + counter],
                                    data["definition"]["key_req" + counter]
                                    ));
                    }
                    counter++;
                }

                // infokeys
                counter = 0;

                while (data["infokey"]["info" + counter] != null)
                {
                    if (data["infokey"]["info" + counter] != "")
                    {
                        infokeys.Add(new HUDInfokey(
                                     data["infokey"]["info" + counter],
                                     data["infokey"]["req" + counter]
                                     ));
                    }
                    counter++;
                }

                if (infokeys.Count == 0)
                {
                    infokeys.Add(new HUDInfokey());
                }
                if (def.Count == 0)
                {
                    def.Add(new HUDDef());
                }
                if (children.Count == 0)
                {
                    children.Add(new HUDChild());
                }
            }
        }

        public override void ExportToINI(string Path)
        {
            try
            {
                var parser = new FileIniDataParser();
                IniData data = new IniData();

                //Add a new section and some keys
                data.Sections.AddSection("object");
                data["object"].AddKey("name", name);
                data["object"].AddKey("id", id);
                data["object"].AddKey("text", read_text);

                data.Sections.AddSection("children");
                for (int i = 0; i < children.Count; i++)
                {
                    if (children[i].name != "")
                    {
                        data["children"].AddKey("child_name" + i, children[i].name);
                        data["children"].AddKey("child_template" + i, children[i].template);
                        data["children"].AddKey("child_var" + i, children[i].varname);
                        data["children"].AddKey("custom" + i, children[i].custom);
                    }
                }

                data.Sections.AddSection("definition");
                for (int i = 0; i < def.Count; i++)
                {
                    data["definition"].AddKey("key_def" + i, def[i].def);
                    data["definition"].AddKey("key_func" + i, def[i].func);
                    if (def[i].args != "")
                    {
                        data["definition"].AddKey("key_arg" + i, def[i].args);
                    }
                    if (def[i].req != "")
                    {
                        data["definition"].AddKey("key_req" + i, def[i].req);
                    }
                }

                data.Sections.AddSection("infokey");
                for(int i = 0; i < infokeys.Count; i++)
                {
                    if (infokeys[i].text != "")
                    {
                        data["infokey"].AddKey("info" + i, infokeys[i].text);
                        if (infokeys[i].req != "")
                        {
                            data["infokey"].AddKey("req" + i, infokeys[i].req);
                        }
                    }
                }

                parser.WriteFile(Path, data);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
