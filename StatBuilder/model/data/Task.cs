﻿using IniParser;
using IniParser.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Controls;

namespace StatBuilder.model
{
    public class Task : Model
    {
        public override string SaveDirectory
        {
            get
            {
                return "task";
            }
        }
        public override string SaveName
        {
            get
            {
                return "task";
            }
        }

        public override string ToString()
        {
            return id;
        }

        public override string name { get; set; } = "Task";
        public string desc { get; set; } = "";
        public string id { get; set; } = "task";
        public string sprite { get; set; } = "";
        public string event_id
        {
            get
            {
                if(Event != null)
                {
                    return Event.name;
                }
                return "";
            }
            set
            {
                List<Event> data = MainForm.EventData.ToList();
                Event = data.Find(x => x.name == value);
            }
        }

        public Event Event { get; set; }

        public string completed_event_id
        {
            get
            {
                if (CompletedEvent != null)
                {
                    return CompletedEvent.name;
                }
                return "";
            }
            set
            {
                List<Event> data = MainForm.EventData.ToList();
                CompletedEvent = data.Find(x => x.name == value);
            }
        }

        public Event CompletedEvent { get; set; }

        public override void ImportFromINI(string Path)
        {
            if (File.Exists(Path))
            {
                var parser = new FileIniDataParser();
                IniData data = parser.ReadFile(Path);

                if (data[SectionName]["title"] != null)
                {
                    name = data[SectionName]["title"];
                }
                if (data[SectionName]["desc"] != null)
                {
                    desc = data[SectionName]["desc"];
                }

                if (data[SectionName]["name"] != null)
                {
                    id = data[SectionName]["name"];
                }

                if (data[SectionName]["sprite"] != null)
                {
                    sprite = data[SectionName]["sprite"];
                }

                if (data[SectionName]["event_id"] != null)
                {
                    event_id = data[SectionName]["event_id"];
                }

                if (data[SectionName]["completed_event_id"] != null)
                {
                    completed_event_id = data[SectionName]["completed_event_id"];
                }
            }
        }

        public override void ExportToINI(string Path)
        {
            try
            {
                var parser = new FileIniDataParser();
                IniData data = new IniData();

                //Add a new section and some keys
                data.Sections.AddSection(SectionName);
                data[SectionName].AddKey("title", name);
                data[SectionName].AddKey("name", id);

                data[SectionName].AddKey("desc", desc);
                data[SectionName].AddKey("sprite", sprite);
                data[SectionName].AddKey("event_id", event_id);
                data[SectionName].AddKey("completed_event_id", completed_event_id);

                parser.WriteFile(Path, data);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
