﻿namespace StatBuilder.model
{
    public class BattleSprite
    {
        public double sprite_default_speed { get; set; } = 1;
        public string sprite_idle_front { get; set; } = "";
        public string sprite_idle_back { get; set; } = "";
        public string sprite_hit_front { get; set; } = "";
        public string sprite_hit_back { get; set; } = "";
        public string sprite_jump0_front { get; set; } = "";
        public string sprite_jump0_back { get; set; } = "";
        public string sprite_jump1_front { get; set; } = "";
        public string sprite_jump1_back { get; set; } = "";
        public string sprite_cast0_front { get; set; } = "";
        public string sprite_cast0_back { get; set; } = "";
        public string sprite_cast1_front { get; set; } = "";
        public string sprite_cast1_back { get; set; } = "";
    }
}
