﻿using System.ComponentModel;

namespace StatBuilder.model
{
    public abstract class Model: INotifyPropertyChanged
    {
        public virtual bool numbering { get; set; } = true;

        public virtual string SectionName { get; set; } = "data";

        public virtual string name { get; set; }
        public abstract string SaveDirectory { get; }

        public abstract string SaveName { get; }

        public event PropertyChangedEventHandler PropertyChanged;

        public Model()
        {
            Init();
        }

        /// <summary>
        /// Initializes the object
        /// </summary>
        public virtual void Init()
        {

        }

        /// <summary>
        /// Imports data into this object from an INI.
        /// </summary>
        /// <param name="Path"></param>
        public abstract void ImportFromINI(string Path);

        /// <summary>
        /// Exports data to an INI.
        /// </summary>
        /// <param name="Path"></param>
        public abstract void ExportToINI(string Path);

        public override string ToString()
        {
            return name;
        }
    }
}
