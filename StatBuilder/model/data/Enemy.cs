﻿using IniParser;
using IniParser.Model;
using System;
using System.IO;

namespace StatBuilder.model
{
    public class Enemy : MoveHaver
    {
        public override MoveLearn movesLearn { get; set; } = new EnemyMoveLearn();
        public override string SectionName { get; set; } = "stats";

        public override string SaveDirectory
        {
            get
            {
                return "stats";
            }
        }

        public override string SaveName
        {
            get
            {
                return "monster";
            }
        }

        public override string name { get; set; } = "Monster";
        public string ability { get; set; } = "";
        public string ax_description { get; set; } = "";
        public string ai_script { get; set; } = "";
        public string ai_target_script { get; set; } = "";
        public string flavor0 { get; set; } = "";
        public string flavor1 { get; set; } = "";
        public string flavor2 { get; set; } = "";
        public string desc { get; set; } = "";
        public int red { get; set; } = 0;
        public int green { get; set; } = 0;
        public int blue { get; set; } = 0;
        public int hp { get; set; } = 128;
        public int mp { get; set; } = 128;
        public int phy_power { get; set; } = 500;
        public int phy_guard { get; set; } = 500;
        public int mag_power { get; set; } = 500;
        public int mag_guard { get; set; } = 500;
        public int spe_power { get; set; } = 500;
        public int spe_guard { get; set; } = 20;
        public int exp { get; set; } = 19;
        public string type { get; set; } = MainForm.Types[0];

        public bool jump_hit { get; set; } = true;
        public int drop_normal { get; set; } = 37;
        public int drop_bribe { get; set; } = 72;
        public int drop_megabribe { get; set; } = 74;
        public int drop_special { get; set; } = 75;

        public int bribe { get; set; } = 180;
        public int min_money { get; set; } = 64;
        public int max_money { get; set; } = 64;

        public int zoom_x { get; set; } = -3;
        public int zoom_y { get; set; } = 45;
        public double sprite_scale { get; set; } = 0.85;
        public int shadow_y { get; set; } = 8;

        public bool cant_jump { get; set; }
        public bool cant_flee { get; set; }
        public bool cant_move { get; set; }
        public bool ignore_dex { get; set; }
        public bool ignore_cry { get; set; }

        public BattleSprite BattleSprite { get; set; } = new BattleSprite();

        public override void ImportFromINI(string Path)
        {
            if (File.Exists(Path))
            {
                var parser = new FileIniDataParser();
                IniData data = parser.ReadFile(Path);

                if (data[SectionName]["name"] != null)
                {
                    name = data[SectionName]["name"];
                }
                if (data[SectionName]["ability"] != null)
                {
                    ability = data[SectionName]["ability"];
                }
                if (data[SectionName]["ai_script"] != null)
                {
                    ai_script = data[SectionName]["ai_script"];
                }
                if (data[SectionName]["ai_target_script"] != null)
                {
                    ai_target_script = data[SectionName]["ai_target_script"];
                }
                if (data[SectionName]["ax_description"] != null)
                {
                    ax_description = data[SectionName]["ax_description"];
                }
                if (data[SectionName]["description"] != null)
                {
                    desc = data[SectionName]["description"];
                }

                if (data[SectionName]["flavor0"] != null)
                {
                    flavor0 = MainForm.StringNewlineImport(data[SectionName]["flavor0"]);
                }
                if (data[SectionName]["flavor1"] != null)
                {
                    flavor1 = MainForm.StringNewlineImport(data[SectionName]["flavor1"]);
                }
                if (data[SectionName]["flavor2"] != null)
                {
                    flavor2 = MainForm.StringNewlineImport(data[SectionName]["flavor2"]);
                }

                hp = Convert.ToInt32(data[SectionName]["hp"]);
                mp = Convert.ToInt32(data[SectionName]["mp"]);
                phy_power = Convert.ToInt32(data[SectionName]["phy_power"]);
                phy_guard = Convert.ToInt32(data[SectionName]["phy_guard"]);
                mag_power = Convert.ToInt32(data[SectionName]["mag_power"]);
                mag_guard = Convert.ToInt32(data[SectionName]["mag_guard"]);
                spe_power = Convert.ToInt32(data[SectionName]["spe_power"]);
                spe_guard = Convert.ToInt32(data[SectionName]["spe_guard"]);

                red = Convert.ToInt32(data[SectionName]["red"]);
                green = Convert.ToInt32(data[SectionName]["green"]);
                blue = Convert.ToInt32(data[SectionName]["blue"]);

                exp = Convert.ToInt32(data[SectionName]["exp"]);
                if (data[SectionName]["type"] != null)
                {
                    type = data[SectionName]["type"];
                }

                cant_flee = Convert.ToBoolean(data[SectionName]["cant_flee"]);
                cant_jump = Convert.ToBoolean(data[SectionName]["cant_jump"]);
                cant_move = Convert.ToBoolean(data[SectionName]["cant_move"]);
                ignore_dex = Convert.ToBoolean(data[SectionName]["ignore_dex"]);
                ignore_cry = Convert.ToBoolean(data[SectionName]["ignore_cry"]);

                drop_normal = Convert.ToInt32(data[SectionName]["drop_normal"]);
                drop_bribe = Convert.ToInt32(data[SectionName]["drop_bribe"]);
                drop_megabribe = Convert.ToInt32(data[SectionName]["drop_megabribe"]);
                drop_special = Convert.ToInt32(data[SectionName]["drop_special"]);
                bribe = Convert.ToInt32(data[SectionName]["drop_special"]);
                min_money = Convert.ToInt32(data[SectionName]["min_money"]);
                max_money = Convert.ToInt32(data[SectionName]["max_money"]);

                zoom_x = Convert.ToInt32(data[SectionName]["zoom_x"]);
                zoom_y = Convert.ToInt32(data[SectionName]["zoom_y"]);
                sprite_scale = Convert.ToDouble(data[SectionName]["sprite_scale"]);
                shadow_y = Convert.ToInt32(data[SectionName]["shadow_y"]);

                BattleSprite.sprite_default_speed = Convert.ToDouble(data[SectionName]["sprite_default_speed"]);
                if (data[SectionName]["sprite_idle_front"] != null)
                {
                    BattleSprite.sprite_idle_front = data[SectionName]["sprite_idle_front"];
                }
                if (data[SectionName]["sprite_idle_back"] != null)
                {
                    BattleSprite.sprite_idle_back = data[SectionName]["sprite_idle_back"];
                }
                if (data[SectionName]["sprite_hit_front"] != null)
                {
                    BattleSprite.sprite_hit_front = data[SectionName]["sprite_hit_front"];
                }
                if (data[SectionName]["sprite_hit_back"] != null)
                {
                    BattleSprite.sprite_hit_back = data[SectionName]["sprite_hit_back"];
                }
                if (data[SectionName]["sprite_jump0_front"] != null)
                {
                    BattleSprite.sprite_jump0_front = data[SectionName]["sprite_jump0_front"];
                }
                if (data[SectionName]["sprite_jump0_back"] != null)
                {
                    BattleSprite.sprite_jump0_back = data[SectionName]["sprite_jump0_back"];
                }
                if (data[SectionName]["sprite_jump1_front"] != null)
                {
                    BattleSprite.sprite_jump1_front = data[SectionName]["sprite_jump1_front"];
                }
                if (data[SectionName]["sprite_jump1_back"] != null)
                {
                    BattleSprite.sprite_jump1_back = data[SectionName]["sprite_jump1_back"];
                }
                if (data[SectionName]["sprite_cast0_front"] != null)
                {
                    BattleSprite.sprite_cast0_front = data[SectionName]["sprite_cast0_front"];
                }
                if (data[SectionName]["sprite_cast0_back"] != null)
                {
                    BattleSprite.sprite_cast0_back = data[SectionName]["sprite_cast0_back"];
                }
                if (data[SectionName]["sprite_cast1_front"] != null)
                {
                    BattleSprite.sprite_cast1_front = data[SectionName]["sprite_cast1_front"];
                }
                if (data[SectionName]["sprite_cast1_back"] != null)
                {
                    BattleSprite.sprite_cast1_back = data[SectionName]["sprite_cast1_back"];
                }
            }
        }

        public override void ExportToINI(string Path)
        {
            try
            {
                var parser = new FileIniDataParser();
                IniData data = new IniData();

                //Add a new section and some keys
                data.Sections.AddSection(SectionName);
                data[SectionName].AddKey("name", name);
                data[SectionName].AddKey("ability", ability);
                data[SectionName].AddKey("ai_script", ai_script);
                data[SectionName].AddKey("ai_target_script", ai_target_script);
                data[SectionName].AddKey("ax_description", ax_description);
                data[SectionName].AddKey("description", desc);

                data[SectionName].AddKey("flavor0", MainForm.StringNewlineExport(flavor0));
                data[SectionName].AddKey("flavor1", MainForm.StringNewlineExport(flavor1));
                data[SectionName].AddKey("flavor2", MainForm.StringNewlineExport(flavor2));

                data[SectionName].AddKey("hp", hp.ToString());
                data[SectionName].AddKey("mp", mp.ToString());
                data[SectionName].AddKey("phy_power", phy_power.ToString());
                data[SectionName].AddKey("phy_guard", phy_guard.ToString());
                data[SectionName].AddKey("mag_power", mag_power.ToString());
                data[SectionName].AddKey("mag_guard", mag_guard.ToString());
                data[SectionName].AddKey("spe_power", spe_power.ToString());
                data[SectionName].AddKey("spe_guard", spe_guard.ToString());

                data[SectionName].AddKey("red", red.ToString());
                data[SectionName].AddKey("green", green.ToString());
                data[SectionName].AddKey("blue", blue.ToString());

                if (cant_flee)
                {
                    data[SectionName].AddKey("cant_flee", cant_flee.ToString());
                }
                if (cant_jump)
                {
                    data[SectionName].AddKey("cant_jump", cant_jump.ToString());
                }
                if (cant_move)
                {
                    data[SectionName].AddKey("cant_move", cant_move.ToString());
                }
                if (ignore_dex)
                {
                    data[SectionName].AddKey("ignore_dex", ignore_dex.ToString());
                }
                if (ignore_cry)
                {
                    data[SectionName].AddKey("ignore_cry", ignore_cry.ToString());
                }

                data[SectionName].AddKey("exp", exp.ToString());
                data[SectionName].AddKey("type", type);

                
                data[SectionName].AddKey("drop_normal", drop_normal.ToString());
                data[SectionName].AddKey("drop_bribe", drop_bribe.ToString());
                data[SectionName].AddKey("drop_megabribe", drop_megabribe.ToString());
                data[SectionName].AddKey("bribe", bribe.ToString());
                data[SectionName].AddKey("drop_special", drop_special.ToString());
                

                data[SectionName].AddKey("min_money", min_money.ToString());
                data[SectionName].AddKey("max_money", max_money.ToString());

                data[SectionName].AddKey("zoom_x", zoom_x.ToString());
                data[SectionName].AddKey("zoom_y", zoom_y.ToString());
                data[SectionName].AddKey("sprite_scale", sprite_scale.ToString());
                data[SectionName].AddKey("shadow_y", shadow_y.ToString());

                data[SectionName].AddKey("sprite_default_speed", BattleSprite.sprite_default_speed.ToString());
                data[SectionName].AddKey("sprite_idle_front", BattleSprite.sprite_idle_front);
                data[SectionName].AddKey("sprite_idle_back", BattleSprite.sprite_idle_back);
                data[SectionName].AddKey("sprite_hit_front", BattleSprite.sprite_hit_front);
                data[SectionName].AddKey("sprite_hit_back", BattleSprite.sprite_hit_back);
                data[SectionName].AddKey("sprite_jump0_front", BattleSprite.sprite_jump0_front);
                data[SectionName].AddKey("sprite_jump0_back", BattleSprite.sprite_jump0_back);
                data[SectionName].AddKey("sprite_jump1_front", BattleSprite.sprite_jump1_front);
                data[SectionName].AddKey("sprite_jump1_back", BattleSprite.sprite_jump1_back);
                data[SectionName].AddKey("sprite_cast0_front", BattleSprite.sprite_cast0_front);
                data[SectionName].AddKey("sprite_cast0_back", BattleSprite.sprite_cast0_back);
                data[SectionName].AddKey("sprite_cast1_front", BattleSprite.sprite_cast1_front);
                data[SectionName].AddKey("sprite_cast1_back", BattleSprite.sprite_cast1_back);
                parser.WriteFile(Path, data);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

    }
}
