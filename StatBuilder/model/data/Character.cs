﻿using IniParser;
using IniParser.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using static StatBuilder.model.MoveLearn;

namespace StatBuilder.model
{
    public class Character : MoveHaver
    {
        // these values are used for display purposes only

        public override string SaveDirectory
        {
            get
            {
                return "stats";
            }
        }

        public override string SaveName
        {
            get
            {
                return "character";
            }
        }

        public string MoveLearnFile { get; set; }

        public string PhoneNumber { get; set; }

        public int display_hp { get; private set; } = 1;
        public int display_mp { get; private set; } = 1;
        public int display_phy_power { get; private set; } = 1;
        public int display_phy_guard { get; private set; } = 1;
        public int display_mag_power { get; private set; } = 1;
        public int display_mag_guard { get; private set; } = 1;
        public int display_spe_power { get; private set; } = 1;
        public int display_spe_guard { get; private set; } = 1;
        public int level
        {
            get
            {
                return _level;
            }
            set
            {
                _level = value;
                UpdateDisplayStats();
            }
        }
        int _level = 100;
        public void UpdateDisplayStats()
        {
            double[] multi = new double[8];

            if (growth_hp_grow != null)
            {
                multi[0] = growth_hp_grow.GetModifierForLevel(_level);
            }
            if (growth_mp_grow != null)
            {
                multi[1] = growth_mp_grow.GetModifierForLevel(_level);
            }
            if (growth_phy_power_grow != null)
            {
                multi[2] = growth_phy_power_grow.GetModifierForLevel(_level);
            }
            if (growth_phy_guard_grow  != null)
            {
                multi[3] = growth_phy_guard_grow.GetModifierForLevel(_level);
            }
            if (growth_mag_power_grow != null)
            {
                multi[4] = growth_mag_power_grow.GetModifierForLevel(_level);
            }
            if (growth_mag_guard_grow != null)
            {
                multi[5] = growth_mag_guard_grow.GetModifierForLevel(_level);
            }
            if (growth_spe_power_grow != null)
            {
                multi[6] = growth_spe_power_grow.GetModifierForLevel(_level);
            }
            if (growth_spe_guard_grow != null)
            {
                multi[7] = growth_spe_guard_grow.GetModifierForLevel(_level);
            }
            display_hp = 20 + Convert.ToInt32(Math.Floor(multi[0] * _hp));
            display_mp = 20 + Convert.ToInt32(Math.Floor(multi[1] * _mp));

            display_phy_power = 10 + Convert.ToInt32(Math.Floor(multi[2] * _phy_power));
            display_phy_guard = 10 + Convert.ToInt32(Math.Floor(multi[3] * _phy_guard));
            display_mag_power = 10 + Convert.ToInt32(Math.Floor(multi[4] * _mag_power));
            display_mag_guard = 10 + Convert.ToInt32(Math.Floor(multi[5] * _mag_guard));
            display_spe_power = 10 + Convert.ToInt32(Math.Floor(multi[6] * _spe_power));
            display_spe_guard = 2 + Convert.ToInt32(Math.Floor(multi[6] * _spe_guard));
        }

        //end display values

        public override string SectionName { get; set; } = "stats";
        public override string name { get; set; } = "Character";
        public string ability { get; set; } = "";
        public string type { get; set; } = MainForm.Types[0];
        public string battle_description { get; set; } = "";
        public string ax_description { get; set; } = "";
        public int hp
        {
            get
            {
                return _hp;
            }
            set
            {
                _hp = value;
                UpdateDisplayStats();
            }
        }
        int _hp = 500;
        public int mp
        {
            get
            {
                return _mp;
            }
            set
            {
                _mp = value;
                UpdateDisplayStats();
            }
        }
        int _mp = 127;
        public int phy_power
        {
            get
            {
                return _phy_power;
            }
            set
            {
                _phy_power = value;
                UpdateDisplayStats();
            }
        }
        int _phy_power = 500;
        public int phy_guard
        {
            get
            {
                return _phy_guard;
            }
            set
            {
                _phy_guard = value;
                UpdateDisplayStats();
            }
        }
        int _phy_guard = 500;
        public int mag_power
        {
            get
            {
                return _mag_power;
            }
            set
            {
                _mag_power = value;
                UpdateDisplayStats();
            }
        }
        int _mag_power = 500;
        public int mag_guard
        {
            get
            {
                return _mag_guard;
            }
            set
            {
                _mag_guard = value;
                UpdateDisplayStats();
            }
        }
        int _mag_guard = 500;
        public int spe_power
        {
            get
            {
                return _spe_power;
            }
            set
            {
                _spe_power = value;
                UpdateDisplayStats();
            }
        }
        int _spe_power = 500;
        public int spe_guard
        {
            get
            {
                return _spe_guard;
            }
            set
            {
                _spe_guard = value;
                UpdateDisplayStats();
            }
        }
        int _spe_guard = 20;
        public int red { get; set; } = 0;
        public int green { get; set; } = 0;
        public int blue { get; set; } = 0;

        public int total_exp { get; set; } = 5000;

        // note, these are values 0-1 that represents multipliers
        // for gradual growth of stats and exp requriements
        public int exp_grow
        {
            get
            {
                return _exp_grow;
            }
            set
            {
                _exp_grow = value;
                if (value > -1 && value < MainForm.GrowthData.Count)
                {
                    growth_exp_grow = MainForm.GrowthData[value];
                }
            }
        }
        int _exp_grow;


        public int hp_grow
        {
            get
            {
                return _hp_grow;
            }
            set
            {
                _hp_grow = value;
                if (value > -1 && value < MainForm.GrowthData.Count)
                {
                    growth_hp_grow = MainForm.GrowthData[value];
                }
            }
        }
        int _hp_grow;

        public int mp_grow
        {
            get
            {
                return _mp_grow;
            }
            set
            {
                _mp_grow = value;
                if (value > -1 && value < MainForm.GrowthData.Count)
                {
                    growth_mp_grow = MainForm.GrowthData[value];
                }
            }
        }
        int _mp_grow;

        public int phy_power_grow
        {
            get
            {
                return _phy_power_grow;
            }
            set
            {
                if (value > -1 && value < MainForm.GrowthData.Count)
                {
                    _phy_power_grow = value;
                    growth_phy_power_grow = MainForm.GrowthData[value];
                }
            }
        }
        int _phy_power_grow;

        public int phy_guard_grow
        {
            get
            {
                return _phy_guard_grow;
            }
            set
            {
                _phy_guard_grow = value;
                if (value > -1 && value < MainForm.GrowthData.Count)
                {
                    growth_phy_guard_grow = MainForm.GrowthData[value];
                }
            }
        }
        int _phy_guard_grow;


        public int mag_power_grow
        {
            get
            {
                return _mag_power_grow;
            }
            set
            {
                _mag_power_grow = value;
                if (value > -1 && value < MainForm.GrowthData.Count)
                {
                    growth_mag_power_grow = MainForm.GrowthData[value];
                }
            }
        }
        int _mag_power_grow;

        public int mag_guard_grow
        {
            get
            {
                return _mag_guard_grow;
            }
            set
            {
                _mag_guard_grow = value;
                if (value > -1 && value < MainForm.GrowthData.Count)
                {
                    growth_mag_guard_grow = MainForm.GrowthData[value];
                }
            }
        }
        int _mag_guard_grow;

        public int spe_power_grow
        {
            get
            {
                return _spe_power_grow;
            }
            set
            {
                _spe_power_grow = value;
                if (value > -1 && value < MainForm.GrowthData.Count)
                {
                    growth_spe_power_grow = MainForm.GrowthData[_spe_power_grow];
                }
            }
        }
        int _spe_power_grow;

        public int spe_guard_grow
        {
            get
            {
                return _spe_guard_grow;
            }
            set
            {
                _spe_guard_grow = value;
                if (value > -1 && value < MainForm.GrowthData.Count)
                {
                    growth_spe_guard_grow = MainForm.GrowthData[_spe_guard_grow];
                }
            }
        }
        int _spe_guard_grow;
        public ExpGrowth growth_exp_grow
        {
            get
            {
                return _growth_exp_grow;
            }
            set
            {
                _growth_exp_grow = value;
                _exp_grow = MainForm.GrowthBinding.IndexOf(value);
            }
        }
        ExpGrowth _growth_exp_grow;

        public ExpGrowth growth_hp_grow
        {
            get
            {
                return _growth_hp_grow;
            }
            set
            {
                _growth_hp_grow = value;
                _hp_grow = MainForm.GrowthBinding.IndexOf(value);
            }
        }
        ExpGrowth _growth_hp_grow;
        public ExpGrowth growth_mp_grow
        {
            get
            {
                return _growth_mp_grow;
            }
            set
            {
                _growth_mp_grow = value;
                _mp_grow = MainForm.GrowthBinding.IndexOf(value);
            }
        }
        ExpGrowth _growth_mp_grow;
        public ExpGrowth growth_phy_power_grow
        {
            get
            {
                return _growth_phy_power_grow;
            }
            set
            {
                _growth_phy_power_grow = value;
                _phy_power_grow = MainForm.GrowthBinding.IndexOf(value);
            }
        }
        ExpGrowth _growth_phy_power_grow;
        public ExpGrowth growth_phy_guard_grow
        {
            get
            {
                return _growth_phy_guard_grow;
            }
            set
            {
                _growth_phy_guard_grow = value;
                _phy_guard_grow = MainForm.GrowthBinding.IndexOf(value);
            }
        }
        ExpGrowth _growth_phy_guard_grow;
        public ExpGrowth growth_mag_power_grow
        {
            get
            {
                return _growth_mag_power_grow;
            }
            set
            {
                _growth_mag_power_grow = value;
                _mag_power_grow = MainForm.GrowthBinding.IndexOf(value);
            }
        }
        ExpGrowth _growth_mag_power_grow;
        public ExpGrowth growth_mag_guard_grow
        {
            get
            {
                return _growth_mag_guard_grow;
            }
            set
            {
                _growth_mag_guard_grow = value;
                _mag_guard_grow = MainForm.GrowthBinding.IndexOf(value);
            }
        }
        ExpGrowth _growth_mag_guard_grow;
        public ExpGrowth growth_spe_power_grow
        {
            get
            {
                return _growth_spe_power_grow;
            }
            set
            {
                _growth_spe_power_grow = value;
                _mag_power_grow = MainForm.GrowthBinding.IndexOf(value);
            }
        }
        ExpGrowth _growth_spe_power_grow;
        public ExpGrowth growth_spe_guard_grow
        {
            get
            {
                return _growth_spe_guard_grow;
            }
            set
            {
                _growth_spe_guard_grow = value;
                _spe_guard_grow = MainForm.GrowthBinding.IndexOf(value);
            }
        }
        ExpGrowth _growth_spe_guard_grow;

        public BindingList<Dialog> Dialogs { get; set; } = new BindingList<Dialog>();
        public BindingSource DialogBinding { get; private set; }

        public void InitDialog()
        {
            if (Dialogs.Count == 0)
            {
                Dialogs.Add(new Dialog());
                Dialogs[0].name = "none";
            }
            DialogBinding = new BindingSource(Dialogs, null);
        }

        public void CorrectDialogNulls()
        {
            for(int i = 0; i< Dialogs.Count; i++)
            {
                Dialogs[i].CorrectNullValues();
            }
            // move none to the top

            List<Dialog> data = Dialogs.ToList();
            updateDialogsThing("none", 0, data);
            updateDialogsThing("call", 1, data);
            updateDialogsThing("nothing", 2, data);
            updateDialogsThing("switch", 3, data);
            updateDialogsThing("no_switch", 4, data);
        }

        private void updateDialogsThing(string dialogName, int index, List<Dialog> data)
        {
            Dialog first = data.Find(x => x.name == dialogName);
            if (first != null)
            {
                Dialogs.Remove(first);
                Dialogs.Insert(index, first);
            }
        }

        public string face_sprite { get; set; } = "";
        public BattleSprite BattleSprite { get; set; } = new BattleSprite();
        public override void ImportFromINI(string Path)
        {
            if (File.Exists(Path))
            {
                var parser = new FileIniDataParser();
                IniData data = parser.ReadFile(Path);

                if (data[SectionName]["name"] != null)
                {
                    name = data[SectionName]["name"];
                }
                if (data[SectionName]["ability"] != null)
                {
                    ability = data[SectionName]["ability"];
                }
                if (data[SectionName]["battle_description"] != null)
                {
                    battle_description = MainForm.StringNewlineImport(data[SectionName]["battle_description"]);
                }
                if (data[SectionName]["ax_description"] != null)
                {
                    ax_description = data[SectionName]["ax_description"];
                }
                if (data[SectionName]["phone"] != null)
                {
                    PhoneNumber = data[SectionName]["phone"];
                }

                hp = Convert.ToInt32(data[SectionName]["hp"]);
                mp = Convert.ToInt32(data[SectionName]["mp"]);
                phy_power = Convert.ToInt32(data[SectionName]["phy_power"]);
                phy_guard = Convert.ToInt32(data[SectionName]["phy_guard"]);
                mag_power = Convert.ToInt32(data[SectionName]["mag_power"]);
                mag_guard = Convert.ToInt32(data[SectionName]["mag_guard"]);
                spe_power = Convert.ToInt32(data[SectionName]["spe_power"]);
                spe_guard = Convert.ToInt32(data[SectionName]["spe_guard"]);

                red = Convert.ToInt32(data[SectionName]["red"]);
                green = Convert.ToInt32(data[SectionName]["green"]);
                blue = Convert.ToInt32(data[SectionName]["blue"]);

                total_exp = Convert.ToInt32(data[SectionName]["total_exp"]);

                if (data[SectionName]["type"] != null)
                {
                    type = data[SectionName]["type"];
                }

                try
                {
                    exp_grow = Convert.ToInt32(data[SectionName]["exp_grow"]);
                    hp_grow = Convert.ToInt32(data[SectionName]["hp_grow"]);
                    mp_grow = Convert.ToInt32(data[SectionName]["mp_grow"]);
                    phy_power_grow = Convert.ToInt32(data[SectionName]["phy_power_grow"]);
                    phy_guard_grow = Convert.ToInt32(data[SectionName]["phy_guard_grow"]);
                    mag_power_grow = Convert.ToInt32(data[SectionName]["mag_power_grow"]);
                    mag_guard_grow = Convert.ToInt32(data[SectionName]["mag_guard_grow"]);
                    spe_power_grow = Convert.ToInt32(data[SectionName]["spe_power_grow"]);
                    spe_guard_grow = Convert.ToInt32(data[SectionName]["spe_guard_grow"]);
                } catch
                {
                    // silently fail
                }

                if (data[SectionName]["face_sprite"] != null)
                {
                    face_sprite = data[SectionName]["face_sprite"];
                }

                BattleSprite.sprite_default_speed = Convert.ToDouble(data[SectionName]["sprite_default_speed"]);

                if (data[SectionName]["sprite_idle_front"] != null)
                {
                BattleSprite.sprite_idle_front = data[SectionName]["sprite_idle_front"];
                }
                if (data[SectionName]["sprite_idle_back"] != null)
                {
                    BattleSprite.sprite_idle_back = data[SectionName]["sprite_idle_back"];
                }
                if (data[SectionName]["sprite_hit_front"] != null)
                {
                    BattleSprite.sprite_hit_front = data[SectionName]["sprite_hit_front"];
                }
                if (data[SectionName]["sprite_hit_back"] != null)
                {
                    BattleSprite.sprite_hit_back = data[SectionName]["sprite_hit_back"];
                }
                if (data[SectionName]["sprite_jump0_front"] != null)
                {
                    BattleSprite.sprite_jump0_front = data[SectionName]["sprite_jump0_front"];
                }
                if (data[SectionName]["sprite_jump0_back"] != null)
                {
                    BattleSprite.sprite_jump0_back = data[SectionName]["sprite_jump0_back"];
                }
                if (data[SectionName]["sprite_jump1_front"] != null)
                {
                    BattleSprite.sprite_jump1_front = data[SectionName]["sprite_jump1_front"];
                }
                if (data[SectionName]["sprite_jump1_back"] != null)
                {
                    BattleSprite.sprite_jump1_back = data[SectionName]["sprite_jump1_back"];
                }
                if (data[SectionName]["sprite_cast0_front"] != null)
                {
                    BattleSprite.sprite_cast0_front = data[SectionName]["sprite_cast0_front"];
                }
                if (data[SectionName]["sprite_cast0_back"] != null)
                {
                    BattleSprite.sprite_cast0_back = data[SectionName]["sprite_cast0_back"];
                }
                if (data[SectionName]["sprite_cast1_front"] != null)
                {
                    BattleSprite.sprite_cast1_front = data[SectionName]["sprite_cast1_front"];
                }
                if (data[SectionName]["sprite_cast1_back"] != null)
                {
                    BattleSprite.sprite_cast1_back = data[SectionName]["sprite_cast1_back"];
                }
            }
        }

        public override void ExportToINI(string Path)
        {
            var parser = new FileIniDataParser();
            IniData data = new IniData();
            try
            {
                //Add a new section and some keys
                data.Sections.AddSection(SectionName);
                data[SectionName].AddKey("name", name);
                data[SectionName].AddKey("ability", ability);
                data[SectionName].AddKey("battle_description", MainForm.StringNewlineExport(battle_description));
                data[SectionName].AddKey("ax_description", ax_description);

                data[SectionName].AddKey("phone", PhoneNumber);

                data[SectionName].AddKey("hp", hp.ToString());
                data[SectionName].AddKey("mp", mp.ToString());
                data[SectionName].AddKey("phy_power", phy_power.ToString());
                data[SectionName].AddKey("phy_guard", phy_guard.ToString());
                data[SectionName].AddKey("mag_power", mag_power.ToString());
                data[SectionName].AddKey("mag_guard", mag_guard.ToString());
                data[SectionName].AddKey("spe_power", spe_power.ToString());
                data[SectionName].AddKey("spe_guard", spe_guard.ToString());

                data[SectionName].AddKey("red", red.ToString());
                data[SectionName].AddKey("green", green.ToString());
                data[SectionName].AddKey("blue", blue.ToString());

                data[SectionName].AddKey("total_exp", total_exp.ToString());
                data[SectionName].AddKey("exp_grow", exp_grow.ToString());
                data[SectionName].AddKey("hp_grow", hp_grow.ToString());
                data[SectionName].AddKey("mp_grow", mp_grow.ToString());
                data[SectionName].AddKey("phy_power_grow", phy_power_grow.ToString());
                data[SectionName].AddKey("phy_guard_grow", phy_guard_grow.ToString());
                data[SectionName].AddKey("mag_power_grow", mag_power_grow.ToString());
                data[SectionName].AddKey("mag_guard_grow", mag_guard_grow.ToString());
                data[SectionName].AddKey("spe_power_grow", spe_power_grow.ToString());
                data[SectionName].AddKey("spe_guard_grow", spe_guard_grow.ToString());

                data[SectionName].AddKey("face_sprite", face_sprite);
                data[SectionName].AddKey("type",type);

                data[SectionName].AddKey("sprite_default_speed", BattleSprite.sprite_default_speed.ToString());

                spriteExport(data, "sprite_idle_front", BattleSprite.sprite_idle_front);
                spriteExport(data, "sprite_idle_back", BattleSprite.sprite_idle_back);
                spriteExport(data, "sprite_hit_front", BattleSprite.sprite_hit_front);
                spriteExport(data, "sprite_hit_back", BattleSprite.sprite_hit_back);
                spriteExport(data, "sprite_jump0_front", BattleSprite.sprite_jump0_front);
                spriteExport(data, "sprite_jump0_back", BattleSprite.sprite_jump0_back);
                spriteExport(data, "sprite_jump1_front", BattleSprite.sprite_jump1_front);
                spriteExport(data, "sprite_jump1_back", BattleSprite.sprite_jump1_back);
                spriteExport(data, "sprite_cast0_front", BattleSprite.sprite_cast0_front);
                spriteExport(data, "sprite_cast0_back", BattleSprite.sprite_cast0_back);
                spriteExport(data, "sprite_cast1_front", BattleSprite.sprite_cast1_front);
                spriteExport(data, "sprite_cast1_back", BattleSprite.sprite_cast1_back);
                parser.WriteFile(Path, data);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private void spriteExport(IniData data, string id, string value)
        {
            if (value != "")
            {
                data[SectionName].AddKey(id, value);
            }
        }

    }
}
