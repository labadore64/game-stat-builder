﻿using IniParser;
using IniParser.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace StatBuilder.model
{
    public class Textbox : Model
    {
        public class TextEntry : INotifyPropertyChanged
        {
            public event PropertyChangedEventHandler PropertyChanged;

            public string text { get; set; } = "text";
            public BindingList<GraphicsEntry> graphics { get; set; } = new BindingList<GraphicsEntry>();
            public BindingSource GraphicsSource { get; private set; }
            public override string ToString()
            {
                if (text.Length < 10)
                {
                    return text;
                } else
                {
                    return text.Substring(0, 10) + "...";
                }
            }

            public class GraphicsEntry : INotifyPropertyChanged
            {
                public event PropertyChangedEventHandler PropertyChanged;

                public int x { get; set; } = -0;
                public int y { get; set; } = 0;
                public double xscale { get; set; } = 1;
                public double yscale { get; set; } = 1;
                public string sprite { get; set; } = "";
                public int r { get; set; } = 255;
                public int g { get; set; } = 255;
                public int b { get; set; } = 255;
                public double alpha { get; set; } = 1;
                public int angle { get; set; } = 0;

                public string target { get; set; } = "none";

                public override string ToString()
                {
                    return sprite;
                }

                public GraphicsEntry()
                {

                }
            }

            public TextEntry()
            {
                GraphicsSource = new BindingSource(graphics, null);
                graphics.Add(new GraphicsEntry());
            }

            public TextEntry(string text, string json)
            {
                this.text = text;
                if (json != null)
                {
                    graphics = JsonConvert.DeserializeObject<BindingList<GraphicsEntry>>(json);
                }

                if(graphics.Count == 0)
                {
                    graphics.Add(new GraphicsEntry());
                }

                GraphicsSource = new BindingSource(graphics, null);

                GraphicsSource.ResetBindings(true);
            }

        }

        public override bool numbering
        {
            get
            {
                return false;
            }
        }


        public override string SaveDirectory
        {
            get
            {
                return "text";
            }
        }

        public override string SaveName
        {
            get
            {
                return name;
            }
        }
        public BindingList<TextEntry> TextEntries { get; set; } = new BindingList<TextEntry>();
        public BindingSource TextSource { get; private set; }

        public override void Init()
        {
            TextSource = new BindingSource(TextEntries, null);
            if(TextEntries.Count == 0)
            {
                TextEntries.Add(new TextEntry());
            }
        }

        public override string name { get; set; } = "textboxData";
        public string event_id { get; set; } = "";

        public override void ExportToINI(string Path)
        {
            try
            {
                var parser = new FileIniDataParser();
                IniData data = new IniData();

                //Add a new section and some keys
                data.Sections.AddSection(SectionName);
                data[SectionName].AddKey("name", name);
                if (event_id.Trim() != "")
                {
                    data[SectionName].AddKey("event_id", event_id);
                }
                for(int i = 0; i < TextEntries.Count; i++)
                {
                    data[SectionName].AddKey("text"+i, TextEntries[i].text);

                    List<TextEntry.GraphicsEntry> obj = TextEntries[i].graphics.ToList();
                    obj.RemoveAll(x => x.sprite.Trim() == "");
                    if (obj.Count > 0)
                    {
                        data[SectionName].AddKey("graphics" + i, JsonConvert.SerializeObject(obj));
                    }
                }

                parser.WriteFile(Path, data);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public override void ImportFromINI(string Path)
        {
            if (File.Exists(Path))
            {
                var parser = new FileIniDataParser();
                IniData data = parser.ReadFile(Path);

                if (data[SectionName]["name"] != null)
                {
                    name = data[SectionName]["name"];
                }
                if (data[SectionName]["event_id"] != null)
                {
                    event_id = data[SectionName]["event_id"];
                }

                int counter = 0;
                string text;
                string json;

                TextEntries.Clear();

                while (data[SectionName]["text" + counter] != null)
                {
                    text = "";
                    json = "";
                    if (data[SectionName]["text" + counter] != "")
                    {
                        text = data[SectionName]["text" + counter];
                    }
                    if (data[SectionName]["graphics" + counter] != "")
                    {
                        json = data[SectionName]["graphics" + counter];
                    }
                    if(text != "" && json != "")
                    {
                        TextEntries.Add(new TextEntry(text, json));
                    }
                    counter++;
                }
            }
        }
    }
}
