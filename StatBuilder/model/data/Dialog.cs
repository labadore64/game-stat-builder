﻿
using IniParser;
using IniParser.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace StatBuilder.model
{
    public class Dialog : Model
    {
        public class DialogText : INotifyPropertyChanged
        {
            public event PropertyChangedEventHandler PropertyChanged;

            public string text { get; set; } = "Text";
            public string emotion { get; set; } = "Neutral";
            public string bg_sprite { get; set; } = "";
            public string fg_sprite { get; set; } = "";

            public override string ToString()
            {
                return text;
            }

            public DialogText()
            {

            }
            public DialogText(string text, string emotion, string bg_sprite, string fg_sprite)
            {
                if (text != null)
                {
                    this.text = text;
                }
                if (emotion != null)
                {
                    this.emotion = emotion;
                }
                if(bg_sprite != null)
                {
                    this.bg_sprite = bg_sprite;
                }
                if (fg_sprite != null)
                {
                    this.fg_sprite = fg_sprite;
                }
            }
        }

        public class DialogOption : INotifyPropertyChanged
        {
            public event PropertyChangedEventHandler PropertyChanged;

            public string text { get; set; } = "Option";
            public Dialog gotoDialog;
            public string gotoDialogString
            {
                get
                {
                    if (gotoDialog != null)
                    {
                        return gotoDialog.name;
                    }
                    return MainForm.CurrentDialogs[0].name;
                }
                set
                {
                    List<Dialog> data = MainForm.CurrentDialogs.ToList();
                    gotoDialog = data.Find(x => x.name == value);
                    gotoBuffer = value;
                }
            }
            public string gotoBuffer = "none";
            public string event_set
            {
                get
                {
                    if (EventSet != null)
                    {
                        return EventSet.name;
                    }
                    return MainForm.EventData[0].name;
                }
                set
                {
                    List<Event> data = MainForm.EventData.ToList();
                    EventSet = data.Find(x => x.name == value);
                }
            }

            public Event EventSet { get; set; }
            public string event_visible
            {
                get
                {
                    if (EventVisible != null)
                    {
                        return EventVisible.name;
                    }
                    return MainForm.EventData[0].name;
                }
                set
                {
                    List<Event> data = MainForm.EventData.ToList();
                    EventVisible  = data.Find(x => x.name == value);
                }
            }

            public Event EventVisible { get; set; }

            public string event_invisible
            {
                get
                {
                    if (EventInvisible != null)
                    {
                        return EventInvisible.name;
                    }
                    return MainForm.EventData[0].name;
                }
                set
                {
                    List<Event> data = MainForm.EventData.ToList();
                    EventInvisible = data.Find(x => x.name == value);
                    EventInvisible = data.Find(x => x.name == value);
                }
            }

            public Event EventInvisible { get; set; }

            public override string ToString()
            {
                return text;
            }

            public DialogOption()
            {
                event_set = "none";
                event_invisible = "none";
                event_visible = "none";
                gotoDialogString = "none";
            }
        }

        public Dialog()
        {

        }

        public Dialog(string filename)
        {
            ImportFromINI(filename);
        }

        public override string SaveDirectory
        {
            get
            {
                if (Parent == null)
                {
                    return "dialog";
                }
                else
                {
                    return "dialog//character" + MainForm.CharacterData.IndexOf(Parent).ToString();
                }
            }
        }
        public override string SaveName
        {
            get
            {
                return name;
            }
        }

        public override string ToString()
        {
            return name;
        }

        public override bool numbering
        {
            get
            {
                return false;
            }
            set
            {

            }
        }

        public override string SectionName {
            get
            {
                return "dialog";
            }
            set
            {

            }
        }

        public string OptionSectionName
        {
            get
            {
                return "option";
            }
            set
            {

            }
        }

        public void CorrectNullValues()
        {
            for (int i = 0; i < Options.Count; i++)
            {
                if (Options[i].EventSet == null)
                {
                    Options[i].event_set = "none";
                }
                if (Options[i].EventInvisible == null)
                {
                    Options[i].event_invisible = "none";
                }
                if (Options[i].EventVisible == null)
                {
                    Options[i].event_visible = "none";
                }

                if (Options[i].gotoDialogString == null){ 
                    Options[i].gotoDialogString = Options[i].gotoBuffer;
                }
            }
        }

        public override string name { get; set; } = "dialog";
        public bool EndNode { get; set; }
        public BindingList<DialogText> Text { get; set; } = new BindingList<DialogText>();
        public BindingList<DialogOption> Options { get; set; } = new BindingList<DialogOption>();

        public BindingSource TextSource { get; private set; }
        public BindingSource OptionSource { get; private set; }
        public Character Parent { get; set; }

        public override void Init()
        {
            TextSource = new BindingSource(Text, null);
            OptionSource = new BindingSource(Options, null);
            if (Text.Count == 0)
            {
                Text.Add(new DialogText());
            }
            if(Options.Count == 0)
            {
                Options.Add(new DialogOption());
            }
            
        }

        public override void ImportFromINI(string Path)
        {
            if (File.Exists(Path))
            {
                var parser = new FileIniDataParser();
                IniData data = parser.ReadFile(Path);

                if (data[SectionName]["name"] != null)
                {
                    name = data[SectionName]["name"];
                }

                int counter = 0;
                DialogText text;
                Text.Clear();
                while (data[SectionName]["dialog" + counter] != null)
                {
                    if (data[SectionName]["dialog" + counter] != "")
                    {
                        text = new DialogText();
                        text.text = data[SectionName]["dialog" + counter];
                        if (data[SectionName]["emotion" + counter] != null)
                        {
                            text.emotion = data[SectionName]["emotion" + counter];
                        }
                        if (data[SectionName]["sprite" + counter] != null)
                        {
                            text.fg_sprite = data[SectionName]["sprite" + counter];
                        }
                        if (data[SectionName]["background" + counter] != null)
                        {
                            text.bg_sprite = data[SectionName]["background" + counter];
                        }
                        Text.Add(text);
                    }
                    counter++;
                }

                DialogOption option;
                counter = 0;
                Options.Clear();
                while (data[OptionSectionName]["dialog" + counter] != null)
                {

                    if (data[OptionSectionName]["dialog" + counter] != "")
                    {
                        option = new DialogOption();
                        option.text = data[OptionSectionName]["dialog" + counter];

                        if (data[OptionSectionName]["goto" + counter] != null) { 
                            option.gotoBuffer = data[OptionSectionName]["goto" + counter];
                        }

                        if (data[OptionSectionName]["eventTrue" + counter] != null)
                        {
                            option.event_visible = data[OptionSectionName]["eventTrue" + counter];
                        }
                        if (data[OptionSectionName]["eventFalse" + counter] != null)
                        {
                            option.event_invisible = data[OptionSectionName]["eventFalse" + counter];
                        }
                        if (data[OptionSectionName]["eventSet" + counter] != null)
                        {
                            option.event_set = data[OptionSectionName]["eventSet" + counter];
                        }
                        Options.Add(option);
                    }
                    counter++;
                }

                if(counter == 0)
                {
                    EndNode = true;
                }
            }
        }

        public override void ExportToINI(string Path)
        {
            try
            {
                var parser = new FileIniDataParser();
                IniData data = new IniData();

                //Add a new section and some keys
                data.Sections.AddSection(SectionName);
                data[SectionName].AddKey("name", name);

                for(int i = 0; i < Text.Count; i++)
                {
                    data[SectionName].AddKey("dialog"+i, Text[i].text);
                    if (Text[i].emotion.ToLower() != "neutral")
                    {
                        data[SectionName].AddKey("emotion" + i, Text[i].emotion);
                    }
                    if (Text[i].bg_sprite != "")
                    {
                        data[SectionName].AddKey("background" + i, Text[i].bg_sprite);
                    }
                    if (Text[i].fg_sprite != "")
                    {
                        data[SectionName].AddKey("sprite" + i, Text[i].fg_sprite);
                    }
                }

                if (!EndNode)
                {
                    for (int i = 0; i < Options.Count; i++)
                    {
                        data[OptionSectionName].AddKey("dialog" + i, Options[i].text);
                        if (Options[i].event_set != "none")
                        {
                            data[OptionSectionName].AddKey("eventSet" + i, Options[i].event_set);
                        }
                        if (Options[i].event_visible!= "none")
                        {
                            data[OptionSectionName].AddKey("eventTrue" + i, Options[i].event_visible);
                        }
                        if (Options[i].event_invisible != "none")
                        {
                            data[OptionSectionName].AddKey("eventFalse" + i, Options[i].event_invisible);
                        }
                        if (Options[i].gotoDialogString != "none")
                        {
                            data[OptionSectionName].AddKey("goto" + i, Options[i].gotoDialogString);
                        }
                    }
                }

                parser.WriteFile(Path, data);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
