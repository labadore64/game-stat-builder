﻿using IniParser;
using IniParser.Model;
using System;
using System.IO;

namespace StatBuilder.model
{
    public class Book : Model
    {
        public override string SaveDirectory
        {
            get
            {
                return "book";
            }
        }
        public override string SaveName
        {
            get
            {
                return "book";
            }
        }

        public override string name { get; set; } = "Book";
        public string author { get; set; } = "";
   
        public string file
        {
            get
            {
                if(FileLocation != null)
                {
                    return Path.GetFileNameWithoutExtension(FileLocation);
                } else
                {
                    return _file;
                }
            }
            set
            {
                _file = file;
                string testFilename = "..\\books\\" + _file + ".txt";
                if (File.Exists(testFilename))
                {
                    FileLocation = testFilename;
                }
            }
        }

        string _file;

        // NOTE this does IO shit so dont use this directly unless u wanna die
        public string FileText
        {
            get
            {
                if (File.Exists(FileLocation))
                {
                    return File.ReadAllText(FileLocation);
                }

                return "";
            }
        }

        public string FileLocation { get; set; }

        public override void ImportFromINI(string Path)
        {
            if (File.Exists(Path))
            {
                var parser = new FileIniDataParser();
                IniData data = parser.ReadFile(Path);

                if (data[SectionName]["name"] != null)
                {
                    name = data[SectionName]["name"];
                }
                if (data[SectionName]["author"] != null)
                {
                    author = data[SectionName]["author"];
                }
                if (data[SectionName]["file"] != null)
                {
                    file = data[SectionName]["file"];
                }
            }
        }

        public override void ExportToINI(string Path)
        {
            try
            {
                var parser = new FileIniDataParser();
                IniData data = new IniData();

                //Add a new section and some keys
                data.Sections.AddSection(SectionName);
                data[SectionName].AddKey("name", name);
                data[SectionName].AddKey("author", author);
                data[SectionName].AddKey("file", file);

                parser.WriteFile(Path, data);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
