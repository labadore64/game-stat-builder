﻿using IniParser;
using IniParser.Model;
using System;
using System.IO;

namespace StatBuilder.model
{
    public class ExpGrowth : Model
    {

        public override string SaveName
        {
            get
            {
                return "exp";
            }
        }

        public override string SaveDirectory
        {
            get
            {
                return "exp";
            }
        }


        // EXP growth always has the same 8 points...
        public double[] growth = new double[]
        {
            0,
            .15,
            .25,
            .5,
            .66,
            .75,
            .83,
            1
        };


        // the levels that represent key points
        public static int[] Levels = new int[]
        {
            0,
            15,
            25,
            50,
            66,
            75,
            83,
            100
        };

        public override string name { get; set; } = "Pattern";

        public override void ImportFromINI(string Path)
        {
            if (File.Exists(Path))
            {
                var parser = new FileIniDataParser();
                IniData data = parser.ReadFile(Path);

                name = data[SectionName]["name"];
                for (int i = 0; i < growth.Length; i++)
                {
                    growth[i] = Convert.ToDouble(data[SectionName]["growth" + i]);
                }
            }
        }

        public override void ExportToINI(string Path)
        {
            try
            {
                var parser = new FileIniDataParser();
                IniData data = new IniData();

                //Add a new section and some keys
                data.Sections.AddSection(SectionName);
                data[SectionName].AddKey("name", name);
                for(int i = 0; i< growth.Length; i++)
                {
                    data[SectionName].AddKey("growth"+i, growth[i].ToString());
                }

                parser.WriteFile(Path, data);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public double GetModifierForLevel(int level)
        {
            int diff = 0;
            int leveldiff = 0;

            double move = 0;

            if(level >= 0 || level <= 100)
            {
                for(int i = 0; i < Levels.Length-1; i++)
                {
                    if(level > Levels[i] &&
                        level <= Levels[i + 1])
                    {
                        // do work
                        diff = Levels[i + 1] - Levels[i];
                        leveldiff = level - Levels[i];

                        move = growth[i]+(leveldiff * (growth[i + 1] - growth[i])) / diff;

                        return move;
                    }
                }
            }
            return 0;
        }
    }
}
