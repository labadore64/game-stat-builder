﻿
using IniParser;
using IniParser.Model;
using System;
using System.IO;

namespace StatBuilder.model
{
    public class Bird : Model
    {
        public override string SaveDirectory
        {
            get
            {
                return "bird";
            }
        }

        public override string SaveName
        {
            get
            {
                return "bird";
            }
        }
        public override string name { get; set; } = "Bird";
        public string desc { get; set; } = "";
        public string long_name { get; set; } = "";
        public string height { get; set; } = "";
        public string weight { get; set; } = "";
        public string metric_height { get; set; } = "";
        public string metric_weight { get; set; } = "";
        public double scale { get; set; } = 1;
        public double offset { get; set; } = 0;
        public double spawn_rate { get; set; } = 1;
        public string male_sprite { get; set; } = "";
        public string female_sprite { get; set; } = "";

        public override void ImportFromINI(string Path)
        {
            if (File.Exists(Path))
            {
                var parser = new FileIniDataParser();
                IniData data = parser.ReadFile(Path);

                if (data[SectionName]["name"] != null)
                {
                    name = data[SectionName]["name"];
                }
                if (data[SectionName]["desc"] != null)
                {
                    desc = data[SectionName]["desc"];
                }
                if (data[SectionName]["long_name"] != null)
                {
                    long_name = data[SectionName]["long_name"];
                }
                if (data[SectionName]["height"] != null)
                {
                    height = data[SectionName]["height"];
                }
                if (data[SectionName]["width"] != null)
                {
                    weight = data[SectionName]["width"];
                }
                if (data[SectionName]["metric_height"] != null)
                {
                    metric_height = data[SectionName]["metric_height"];
                }
                if (data[SectionName]["metric_width"] != null)
                {
                    metric_weight = data[SectionName]["metric_width"];
                }

                scale = Convert.ToDouble(data[SectionName]["scale"]);
                if (data[SectionName]["male_sprite"] != null)
                {
                    male_sprite = data[SectionName]["male_sprite"];
                }
                if (data[SectionName]["female_sprite"] != null)
                {
                    female_sprite = data[SectionName]["female_sprite"];
                }
                offset = Convert.ToDouble(data[SectionName]["offset"]);
                spawn_rate = Convert.ToDouble(data[SectionName]["spawn_rate"]);
            }
        }

        public override void ExportToINI(string Path)
        {
            try
            {
                var parser = new FileIniDataParser();
                IniData data = new IniData();

                //Add a new section and some keys
                data.Sections.AddSection(SectionName);
                data[SectionName].AddKey("name", name);
                data[SectionName].AddKey("desc", desc);
                data[SectionName].AddKey("long_name", long_name);
                data[SectionName].AddKey("height", height);
                data[SectionName].AddKey("width", weight);
                data[SectionName].AddKey("metric_height", metric_height);
                data[SectionName].AddKey("metric_width", metric_weight);
                data[SectionName].AddKey("spawn_rate", spawn_rate.ToString());
                data[SectionName].AddKey("scale", scale.ToString());
                data[SectionName].AddKey("offset", offset.ToString());
                data[SectionName].AddKey("male_sprite", male_sprite);
                data[SectionName].AddKey("female_sprite", female_sprite);
                parser.WriteFile(Path, data);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
