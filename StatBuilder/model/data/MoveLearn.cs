﻿using System.ComponentModel;

namespace StatBuilder.model
{
    public class MoveLearn
    {
        public string SectionName => "moves";

        public virtual string SaveDirectory => "stats\\move_learn";

        public virtual string SaveName => "character";

        public class MoveLearnPiece : Model
        {
            public override string SectionName => "moves";
            public override string SaveDirectory => "stats\\move_learn";

            public override string SaveName => "character";

            public override void ExportToINI(string Path)
            {

            }

            public override void ImportFromINI(string Path)
            {

            }

            public int move_id { get; private set; } = -1;
            public int level { get; set; }
            public Move Move
            {
                get
                {
                    return _move;
                }
                set
                {
                    _move = value;
                    move_id = MainForm.MoveData.IndexOf(_move);
                }
            }

            Move _move = null;

            public MoveLearnPiece(Move move, int level)
            {
                Move = move;
                this.level = level;
            }

            public override string ToString()
            {
                if (Move != null)
                {
                    return Move.name + ", lv " + level;
                }
                else
                {
                    return "none, lv " + level;
                }
            }
        }

        public BindingList<MoveLearnPiece> moves = new BindingList<MoveLearnPiece>();

    }
}
