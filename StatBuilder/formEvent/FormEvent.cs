﻿using StatBuilder.model;
using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace StatBuilder.formEvent
{
    public partial class FormEvent : Form
    {
        public string TextString
        {
            get
            {
                return textBox1.Text;
            }
            set
            {
                textBox1.Text = value;
            }
        }

        public bool AddMode { get; set; } = false;

        public Event eventObj { get; set; }

        public BindingList<Event> EventList { get; set; }

        public FormEvent()
        {
            InitializeComponent();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            if (TextString != "")
            {
                if (eventObj != null)
                {
                    if (AddMode)
                    {
                        if (EventList != null)
                        {
                            EventList.Add(new Event(TextString));
                        }
                        else
                        {
                            eventObj.name = TextString;
                        }
                    }
                    else
                    {
                        eventObj.name = TextString;
                    }
                }
            }
            Close();
        }
    }
}
