﻿namespace StatBuilder.formDialog
{
    partial class FormDialogOption
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxGoto = new System.Windows.Forms.ComboBox();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonOK = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBoxEventEnabled = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBoxEventDisabled = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBoxEventSet = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // textBox
            // 
            this.textBox.Location = new System.Drawing.Point(16, 27);
            this.textBox.Name = "textBox";
            this.textBox.Size = new System.Drawing.Size(283, 20);
            this.textBox.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Dialog Text";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Go To";
            // 
            // comboBoxGoto
            // 
            this.comboBoxGoto.FormattingEnabled = true;
            this.comboBoxGoto.Location = new System.Drawing.Point(16, 65);
            this.comboBoxGoto.Name = "comboBoxGoto";
            this.comboBoxGoto.Size = new System.Drawing.Size(83, 21);
            this.comboBoxGoto.TabIndex = 10;
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(160, 143);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(63, 23);
            this.buttonCancel.TabIndex = 17;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(91, 143);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(63, 23);
            this.buttonOK.TabIndex = 16;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click_1);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(127, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 19;
            this.label3.Text = "Event Required";
            // 
            // comboBoxEventEnabled
            // 
            this.comboBoxEventEnabled.FormattingEnabled = true;
            this.comboBoxEventEnabled.Location = new System.Drawing.Point(130, 106);
            this.comboBoxEventEnabled.Name = "comboBoxEventEnabled";
            this.comboBoxEventEnabled.Size = new System.Drawing.Size(83, 21);
            this.comboBoxEventEnabled.TabIndex = 18;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(213, 90);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 13);
            this.label4.TabIndex = 21;
            this.label4.Text = "Event Not Set";
            // 
            // comboBoxEventDisabled
            // 
            this.comboBoxEventDisabled.FormattingEnabled = true;
            this.comboBoxEventDisabled.Location = new System.Drawing.Point(216, 106);
            this.comboBoxEventDisabled.Name = "comboBoxEventDisabled";
            this.comboBoxEventDisabled.Size = new System.Drawing.Size(83, 21);
            this.comboBoxEventDisabled.TabIndex = 20;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 90);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 13);
            this.label5.TabIndex = 23;
            this.label5.Text = "Event Set";
            // 
            // comboBoxEventSet
            // 
            this.comboBoxEventSet.FormattingEnabled = true;
            this.comboBoxEventSet.Location = new System.Drawing.Point(16, 106);
            this.comboBoxEventSet.Name = "comboBoxEventSet";
            this.comboBoxEventSet.Size = new System.Drawing.Size(83, 21);
            this.comboBoxEventSet.TabIndex = 22;
            // 
            // FormDialogOption
            // 
            this.AcceptButton = this.buttonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(309, 174);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.comboBoxEventSet);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.comboBoxEventDisabled);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.comboBoxEventEnabled);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBoxGoto);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormDialogOption";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "FormDialogText";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxGoto;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBoxEventEnabled;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBoxEventDisabled;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBoxEventSet;
    }
}