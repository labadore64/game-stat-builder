﻿
using System.ComponentModel;
using System.Diagnostics.Eventing.Reader;
using System.Windows.Forms;
using static StatBuilder.model.Dialog;

namespace StatBuilder.formDialog
{
    public partial class FormDialogText : Form
    {
        public DialogText Obj;
        public bool AddMode { get; set; } = false;
        public BindingList<DialogText> DialogList { get; set; }

        public string DialogText
        {
            get
            {
                return textBox.Text;
            } 
            set
            {
                textBox.Text = value;
            }
        }

        public string BGSprite
        {
            get
            {
                return textBoxBGSprite.Text;
            }
            set
            {
                textBoxBGSprite.Text = value;
            }
        }

        public string FGSprite
        {
            get
            {
                return textBoxFGSprite.Text;
            }
            set
            {
                textBoxFGSprite.Text = value;
            }
        }

        public string Emotion
        {
            get
            {
                string returner = (string)comboBoxEmotion.SelectedItem; ;
                if(returner == null)
                {
                    return "Neutral";
                }
                return returner;
            }
            set
            {
                for (var i = 0; i < comboBoxEmotion.Items.Count; i++)
                {
                    if ((string)comboBoxEmotion.Items[i] == value)
                    {
                        comboBoxEmotion.SelectedIndex = i;
                        break;
                    }
                }
            }
        }
        public FormDialogText()
        {
            InitializeComponent();
        }

        private void buttonOK_Click_1(object sender, System.EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
