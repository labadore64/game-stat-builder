﻿
using StatBuilder.model;
using System.ComponentModel;
using System.Windows.Forms;
using static StatBuilder.model.Dialog;

namespace StatBuilder.formDialog
{
    public partial class FormDialogOption : Form
    {
        public DialogOption Obj;
        public bool AddMode { get; set; } = false;
        public BindingList<DialogText> DialogList { get; set; }

        public BindingSource EventList { get; set; }
        public BindingSource EventNeedList { get; set; }
        public BindingSource EventDisabledList { get; set; }
        public BindingSource DialogSource { get; set; }

        public string OptionText
        {
            get
            {
                return textBox.Text;
            }
            set
            {
                textBox.Text = value;
            }
        }

        public Event eventSet
        {
            get
            {
                return (Event)comboBoxEventSet.Items[comboBoxEventSet.SelectedIndex];
            }
            set
            {
                comboBoxEventSet.SelectedItem = value;
            }
        }

        public Event eventDisabled
        {
            get
            {
                return (Event)comboBoxEventDisabled.Items[comboBoxEventDisabled.SelectedIndex];
            }
            set
            {
                comboBoxEventDisabled.SelectedItem = value;
            }
        }
        public Event eventEnabled
        {
            get
            {
                return (Event)comboBoxEventEnabled.Items[comboBoxEventEnabled.SelectedIndex];
            }
            set
            {
                comboBoxEventEnabled.SelectedItem = value;
            }
        }

        public Dialog gotoNext
        {
            get
            {
                return (Dialog)comboBoxGoto.Items[comboBoxGoto.SelectedIndex];
            }
            set
            {
                comboBoxGoto.SelectedItem = value;
            }
        }

        public FormDialogOption()
        {
            InitializeComponent();
        }

        public void Bind()
        {
            comboBoxEventSet.DataSource = EventList;
            comboBoxEventEnabled.DataSource = EventNeedList;
            comboBoxEventDisabled.DataSource = EventDisabledList;
            comboBoxGoto.DataSource = DialogSource;
        }

        private void buttonOK_Click_1(object sender, System.EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
