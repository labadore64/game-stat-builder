﻿using System;
using System.Windows.Forms;

namespace StatBuilder.formTextInput
{
    public partial class GenericTextForm : Form
    {
        public string TextString
        {
            get
            {
                return textBox.Text;
            }
            set
            {
                textBox.Text = value;
            }
        }

        public bool Multiline
        {
            get
            {
                return textBox.Multiline;
            }
            set
            {
                textBox.Multiline = value;
            }
        }

        public GenericTextForm()
        {
            InitializeComponent();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void GenericTextForm_Shown(object sender, EventArgs e)
        {
            textBox.Focus();
        }
    }
}
