﻿using StatBuilder.model;
using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace StatBuilder.formItemEvent
{
    public partial class FormItemEvent : Form
    {
        public ItemEvent eventObj { get; set; }

        public bool AddMode { get; set; }
        public BindingList<ItemEvent> EventList { get; set; }
        public BindingSource ItemBinding { get; set; }
        public BindingSource ItemEventBinding { get; set; }

        public void SetBinding()
        {
            comboBoxItems.DataSource = ItemBinding;
        }

        public void BindItem()
        {
            comboBoxItems.DataBindings.Add(
            new Binding("SelectedItem", ItemEventBinding,
                "Item", false, DataSourceUpdateMode.OnPropertyChanged));
        }

        public string TextString
        {
            get
            {
                return textBox1.Text;
            }
            set
            {
                textBox1.Text = value;
            }
        }

        public int Quantity
        {
            get
            {
                return Convert.ToInt32(textBoxQuantity.Text);
            }
            set
            {
                textBoxQuantity.Text = value.ToString();
            }
        }

        public FormItemEvent()
        {
            InitializeComponent();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            if (TextString != "")
            {
                if (eventObj != null)
                {
                    if (AddMode)
                    {
                        if (EventList != null)
                        {
                            EventList.Add(new ItemEvent(TextString,comboBoxItems.SelectedIndex,Quantity));
                        }
                        else
                        {
                            eventObj.name = TextString;
                            eventObj.quantity = Quantity;
                        }
                    }
                    else
                    {
                        eventObj.name = TextString;
                        eventObj.quantity = Quantity;
                    }
                }
            }
            Close();
        }
    }
}
